<!-- Dialog script for action of {{ $action }} -->
<script type="text/javascript">
    jQuery(document).on('click', '.{{ $action }}-modal', function () {
        jQuery('#id_{{ $action }}').val(jQuery(this).data('id'));
        jQuery('#name_{{ $action }}').val(jQuery(this).data('name'));
        jQuery('#{{ $action }}Modal').show();
    });

    jQuery('.modal-footer').on('click', '.{{ $action }}', function (evt) {
        evt.preventDefault();

        @if($action == 'delete')
        id = jQuery('#id_{{ $action }}').val();
        jQuery('#{{ $action }}Modal').hide();
        jQuery.ajax({
            type: 'DELETE',
            url: location.origin + '/{{ isset($submit_url)? $submit_url:'' }}/' + id,
            data: {
                '_token': jQuery('input[name=_token]').val(),
            },
            success: function (data) {
                toastr.success('Delete request was handled successfully.', 'Success Alert', {timeOut: 5000});
                jQuery('.item' + data['id']).remove();
            },
            error: function (response) {
                if (response.status == 500) {
                    toastr.error('Server error.', 'Error Alert', {timeOut: 5000});
                } else {
                    if (response.responseJSON.error) {
                        toastr.error(response.responseJSON.error, 'Error Alert', {timeOut: 5000});
                    } else {
                        toastr.error('Bad Request', 'Error Alert', {timeOut: 5000});
                    }
                }
            }
        });
        @else
        jQuery('#form_{{ $action }}').submit();
        @endif
    });

    jQuery('.modal-footer').on('click', '.{{ $action }}_cancel', function (evt) {
        evt.preventDefault();
        jQuery('#{{ $action }}Modal').hide();
    });
</script>
