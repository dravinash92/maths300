<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Setting;

class ContactController extends Controller
{
    public function index()
    {
        $arr['contact'] = Setting::getContactInfo();
        return view('front.contact')->with($arr);
    }
}
