<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    /**
     * Get the invoice that belongs to this OrderItem.
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * totalAmount will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function totalAmountFormat() {
        return number_format($this->total_amount, 2);
    }

    /**
     * tax_amount will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function taxAmountFormat() {
        return number_format($this->tax_amount, 2);
    }
}
