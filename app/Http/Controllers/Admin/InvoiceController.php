<?php

namespace App\Http\Controllers\Admin;

use App\Events\InvoiceStatusChange;
use App\Http\Requests\InvoiceRequest;
use App\Http\Requests\AdhocInvoiceRequest;
use App\Jobs\SendInvoiceReminderEmail;
use App\Models\Discount;
use App\Models\Invoice;
use App\Models\School;
use App\Models\OrderItem;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Validator;
use Webfox\Xero\OauthCredentialManager;
use Storage;

class InvoiceController extends Controller
{
    public $action_not_allowed_msg = "This action is not allowed";
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['download']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Invoice::query();

        if ($request->has('q_to') && $request->q_to != null) {
            Log::info("query invoice of school " . $request->q_to);
            $arr['school_id'] = $request->q_to;
            $query->to($request->q_to);

            $arr['create_invoice_action'] = '';
            // query invoices that were not paid or cancelled
            if (!Invoice::isCreateAllowed($request->q_to)) {
                $arr['create_invoice_action'] = 'disabled';
            }
        }

        if ($request->has('q_status') && $request->q_status != null) {
            Log::debug("query q_status " . $request->q_status);
            $query->status($request->q_status);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        // not Cancelled
//        $query->statusNot(4);
        $arr['invoices'] = $query->get();

       // echo '<pre>';print_r($arr['invoices']);exit;

        return view('admin.invoices.index')->with($arr);
    }

    /**
     * Display a listing of the archived invoices.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showArchived(Request $request)
    {
        $query = Invoice::query();
        if ($request->has('q_to') && $request->q_to != null) {
            Log::info("query invoice of school " . $request->q_to);
            $arr['school_id'] = $request->q_to;
            $query->to($request->q_to);
        }

        // only show status: Cancelled
        $query->status(4);

        $arr['invoices'] = $query->get();

        return view('admin.invoices.showArchived')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $school_id = $request->school_id;

        $school = School::findOrFail($school_id);
        // query invoices that were not paid or cancelled
        if (Invoice::query()->to($school_id)->statusIn(array(0, 1, 3))->count()) {
            return back()->withErrors('Creating invoice is not allowed as pending invoices exist');
        }
        // check school financeOfficer/coordinator exists, in case someone leave it as blank
        if (empty($school->financeOfficer) || empty($school->coordinator)) {
            return back()->withErrors('Creating invoice is not allowed as school financeOfficer or coordinator is empty, please check school information!');
        }

        $arr['discounts'] = data4Select(Discount::class);
        $arr['school_id'] = $school->id;
        $arr['administrative_fee'] = !empty(Setting::get('Oversea school administrative fee')) ? doubleval(Setting::get('Oversea school administrative fee')) : 0;
        return view('admin.invoices.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\InvoiceRequest; $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvoiceRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $school_id = $validated['school_id'];
        $school = School::findOrFail($school_id);
        // query invoices that were not paid or cancelled
        if (Invoice::query()->to($school_id)->statusIn(array(0, 1, 3))->count()) {
            return back()->withErrors('Creating invoice is not allowed as pending invoices exist');
        }
        $issue_date = today();
        $discount_apply = $validated['discount_apply'];
        $discount = null;
        if ($discount_apply) {
            $discount = Discount::findOrFail($validated['discount']);
        }
        $order_id = null;
        if(!empty($validated['order_id'])) {
            $order_id = $validated['order_id'];
        }

        Log::info("new invoice of school " . $school_id);
        // create a new invoice
        $invoice = Invoice::newInvoice($school, $issue_date, $discount_apply, $discount, $order_id);

       /* $invoice_copy = $invoice->toArray();

        $this->CreateXeroInvoice($invoice_copy, $xeroCredentials);*/

        $request->session()->flash('alert-success', 'Invoice created successfully.');

        return redirect()->route('admin.invoices.index', array("q_to" => $invoice->to->id));
    }

     /**
     * Store a newly created adhoc resource in storage.
     *
     * @param  \App\Http\Requests\AdhocInvoiceRequest; $request
     * @return \Illuminate\Http\Response
     */
    public function adhocStore(AdhocInvoiceRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $school_id = $validated['school_id'];
        $school = School::findOrFail($school_id);
        // query invoices that were not paid or cancelled
        if (Invoice::query()->to($school_id)->statusIn(array(0, 1, 3))->count()) {
            return back()->withErrors('Creating invoice is not allowed as pending invoices exist');
        }
        
        $issue_date = today();
        $description = $validated['description'];
        $invoice_amount = $validated['invoice_amount'];
        $notes = $validated['notes'];
        $account_code = $validated['account_code'];
        $another_line = $validated['another_line'];
        $description1 = null;
        $amount1 = null;

        if($another_line){
            $description1 = $validated['description1'];
            $amount1 = $validated['amount1'];
        }

        Log::info("new invoice of school " . $school_id);
        // create a new invoice
        $invoice = Invoice::newAdhocInvoice($school, $issue_date, $invoice_amount, $description, $notes, $another_line, $account_code, $description1, $amount1);

        $request->session()->flash('alert-success', 'Invoice created successfully.');

        return redirect()->route('admin.invoices.index', array("q_to" => $invoice->to->id));
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $arr['invoice'] = $invoice;
        $arr['invoicefrom'] = Invoice::getOrgInfo();
        return view('admin.invoices.show')->with($arr);
    }

    /**
     * download invoice
     *
     * @param  \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function download(Invoice $invoice)
    {
        Invoice::genPDF($invoice);
        return response()->download(storage_path('app/public/' . $invoice->pdf_file_path), 'Invoice-' . $invoice->invoice_id . '.pdf', [], 'attachment');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {

    }

    /**
     * Change status to send.
     *
     * 0: Draft 1:Awaiting payment 2: Paid 3: Overdue 4:Canceled
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, OauthCredentialManager $xeroCredentials)
    {
        $id = $request->id_send;

        $invoice = Invoice::findOrFail($id);
        $school = School::findOrFail($invoice->to->id);
        $order_item = DB::table('order_items')->where('invoice_id', $id)->get();

        Log::info("send invoice " . $id);

        // check invoice status
        if (in_array($invoice->status, array(1, 2, 3, 4))) {
            return back()->withErrors($this->action_not_allowed_msg);
        }

        if ($invoice->status != 1) {
            $invoice->status = 1;
            $invoice->send_date = today();
            $invoice->save();
            event(new InvoiceStatusChange($invoice));
        }

        $invoice_copy = $invoice->toArray();

        $invoice_copy['order_item'] = json_decode(json_encode($order_item, true), true);
        $order_item_names = array_column($invoice_copy['order_item'], 'name');
        $invoice_copy['is_new_school'] = in_array('Maths300 Joining Subscription', $order_item_names);

        //echo '<pre>';print_r($invoice_copy);exit;

        $xeroInvoiceId = $this->CreateXeroInvoice($invoice_copy, $xeroCredentials);

        if($xeroInvoiceId){
            $invoice->xero_invoice_id = $xeroInvoiceId;
            $invoice->save();
        }

         Log::info("Xero Invoice ID " . $xeroInvoiceId);

        $request->session()->flash('alert-success', 'Invoice send successfully.');

        return redirect()->route('admin.invoices.index', array("q_to" => $invoice->to->id));
    }

    public function CreateXeroInvoice($invoice_copy, $xeroCredentials){
         Log::info("Create Xero Invoice Started -");
         if ($xeroCredentials->exists()) {
            
             Log::info("xeroCredentials exists -  ");
             
                $xero             = resolve(\XeroAPI\XeroPHP\Api\AccountingApi::class);
               
                $xeroTenantId = $xeroCredentials->getTenantId();

                $lineitems = [];        
        
                Log::info("Xero Tenant Id -  ".  $xeroTenantId);
               try{
                    foreach ($invoice_copy['order_item'] as $key => $order_item) {
                        $lineitem =  resolve(\XeroAPI\XeroPHP\Models\Accounting\LineItem::class);

                       if($invoice_copy['is_adhoc_invoice']){
                            $account_code = $invoice_copy['xero_account_code'];
                       } else if($invoice_copy['is_new_school']){
                            $account_code = "40460";
                       }else{
                            $account_code = "40461";
                       }
                    
                        $lineitem->setDescription($order_item['name'])
                        ->setUnitAmount($order_item['price'])
                        //for Demo company
                        ->setAccountCode("200")
                        // for AAMT company
                       // ->setAccountCode($account_code)
                        ->setTaxType("OUTPUT")
                        ->setLineAmount($order_item['total_amount']);

                       /* if($invoice_copy['tax_apply']){
                            $lineitem->setTaxAmount($invoice_copy['tax']);
                        }*/


                        array_push($lineitems, $lineitem);
                    }
                    
                } catch(\throwable $e) {
                
                    $error = $e->getMessage();
                    Log::info("Error creating Line Item in xero Invoice - " . $error);
                }

                try{

                    $contact = resolve(\XeroAPI\XeroPHP\Models\Accounting\Contact::class);


                    $where = 'Name=="'.$invoice_copy['to']['name'].'"';

                    $contact_result = $xero->getContacts($xeroTenantId, null, $where)->getContacts();

                    $count = count($contact_result);

                    if($count > 0){

                        $contactId = $contact_result[0]->getContactId();

                    }else{
                        //echo "Count: ".$count;exit;

                        $arr_contacts = [];
                        $contact_1 = resolve(\XeroAPI\XeroPHP\Models\Accounting\Contact::class);

                        $arr_addresses = [];

                        $address  =  resolve(\XeroAPI\XeroPHP\Models\Accounting\Address::class);

                        $address->setAddressType('POBOX')
                                ->setAddressLine1($invoice_copy['to']['billing_address_region'])
                                ->setAddressLine2($invoice_copy['to']['billing_address_suburb'])
                                ->setAddressLine3($invoice_copy['to']['billing_address_postcode']);
                        array_push($arr_addresses, $address);

                        $contact_1->setName($invoice_copy['to']['name'])
                            ->setEmailAddress($invoice_copy['to']['finance_invoice_email'])
                            ->setAddresses($arr_addresses);
                        array_push($arr_contacts, $contact_1);

                        $contacts = resolve(\XeroAPI\XeroPHP\Models\Accounting\Contacts::class);
                        $contacts->setContacts($arr_contacts);

                        $result_contact = $xero->createContacts($xeroTenantId,$contacts);
                        $contactId = $result_contact->getContacts()[0]->getContactId();
                    }
                    $contact->setContactId($contactId);
                }catch(\throwable $e) {
            
                    // This can happen if the credentials have been revoked or there is an error with the organisation (e.g. it's expired)
                    $error = $e->getMessage();
                    Log::info("Error getting Contact in xero Invoice creation -  " . $error);
                }

                $arr_invoices = []; 

                $invoice_1 = resolve(\XeroAPI\XeroPHP\Models\Accounting\Invoice::class);
                try{
                    $invoice_1->setReference('Ref-Maths300')
                        ->setDueDate(new \DateTime($invoice_copy['due_date']))
                        ->setContact($contact)
                        ->setLineItems($lineitems)
                        ->setStatus(\XeroAPI\XeroPHP\Models\Accounting\Invoice::STATUS_AUTHORISED)
                        ->setType(\XeroAPI\XeroPHP\Models\Accounting\Invoice::TYPE_ACCREC)
                        ->setLineAmountTypes(\XeroAPI\XeroPHP\Models\Accounting\LineAmountTypes::INCLUSIVE)
                        ->setInvoiceNumber($invoice_copy['invoice_id']);

                        if($invoice_copy['discount_apply']){  
                            $invoice_1->setTotalDiscount($invoice_copy['discount_amount']); 
                        } 

                        if($invoice_copy['tax_apply']){
                            $invoice_1->setTotalTax($invoice_copy['tax']);
                        }  
                    array_push($arr_invoices, $invoice_1);

                    $invoices = resolve(\XeroAPI\XeroPHP\Models\Accounting\Invoices::class);
                    $invoices->setInvoices($arr_invoices);

                    $result = $xero->createInvoices($xeroTenantId,$invoices); 
                    Log::info("Xero Invoice Created -  " );
                    //[/Invoices:Create]
                    $str = '';
                    $str = $str ."Create Invoice 1 total amount: " . $result->getInvoices()[0]->getTotal() . "<br>" ;
                    $invoiceId = $result->getInvoices()[0]->getInvoiceId();
                    
                   Log::info("Create Xero Invoice End -");
                   return $invoiceId;
                }catch(\throwable $e) {
            
                    // This can happen if the credentials have been revoked or there is an error with the organisation (e.g. it's expired)
                    $error = $e->getMessage();

                    Log::info("Error caught in xero Invoice creation -  " . $error);
                }


            }
             //echo "out";exit;
            Log::info("Create Xero Invoice End -");
    }


    public function downloadXeroInvoice(Request $request, OauthCredentialManager $xeroCredentials){

        $id = $request->id_download;

        $invoice = Invoice::findOrFail($id);

        $xero  = resolve(\XeroAPI\XeroPHP\Api\AccountingApi::class);
               
        $xeroTenantId = $xeroCredentials->getTenantId();

        $invoiceId = $invoice->xero_invoice_id;

        $invoicePdf = $xero->getInvoiceAsPdf($xeroTenantId, $invoiceId, "application/pdf");                      
        // read PDF contents
        $content = $invoicePdf->fread($invoicePdf->getSize());

        Storage::put("xero_invoices/Inv-". $invoice->invoice_id . ".pdf", $content);

        $headers = array(
          'Content-Type: application/pdf',
        );

        return response()->download(storage_path("app/xero_invoices/Inv-". $invoice->invoice_id . ".pdf"), "Invoice-". $invoice->invoice_id . ".pdf" , $headers);
    }


    public function updateXeroInvoice($invoiceStatus, $invoiceId, $xeroCredentials){
        if ($xeroCredentials->exists()) {
             
            $xero             = resolve(\XeroAPI\XeroPHP\Api\AccountingApi::class);
           
            $xeroTenantId = $xeroCredentials->getTenantId();

            
            $xeroInvoice = resolve(\XeroAPI\XeroPHP\Models\Accounting\Invoice::class);
            try{
                if($invoiceStatus == 'cancel'){
                    $xeroInvoice->setStatus(\XeroAPI\XeroPHP\Models\Accounting\Invoice::STATUS_VOIDED);
                }

                $result = $xero->updateInvoice($xeroTenantId, $invoiceId, $xeroInvoice); 
                
                return $result;
            }catch(\throwable $e) {
        
                // This can happen if the credentials have been revoked or there is an error with the organisation (e.g. it's expired)
                $error = $e->getMessage();
                Log::info("Error caught in updating Xero Invoice to Voided/Cancel -  " . $error);
                
            }


        }else{
            return false;
        }
    }

    /**
     * Change status to Paid.
     *
     * 0: Draft 1:Awaiting payment 2: Paid 3: Overdue 4:Canceled
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function paid(Request $request, OauthCredentialManager $xeroCredentials)
    {
        $validated = Validator::make($request->all(), [
            'id_paid' => 'required|numeric|exists:invoices,id',
            'subscriptionExpirationDate' => 'required',
        ])->validate();

        $id = $validated['id_paid'];
        $invoice = Invoice::findOrFail($id);
        Log::info("Marked as paid invoice " . $id);

        // check invoice status
        if (!$invoice->isMarkAsPaidAllowed()) {
            return back()->withErrors($this->action_not_allowed_msg);
        }

        if ($invoice->status != 2) {
            $invoice->status = 2;
            $invoice->paid_date = today();
            $invoice->save();

            if ($invoice->isGracePeriodExpire()) {
                $invoice->to->subscription_expiration_date = $invoice->newSubscriptionExpirationDate();
            }
            Log::info("subscription expired date: " . $invoice->to->subscription_expiration_date);

            // update date_last_paid/last_renewal_date/last_invoice_number
            $invoice->to->last_paid_date = $invoice->paid_date;
            $invoice->to->last_renewal_date = $invoice->paid_date;
            $invoice->to->last_invoice_number = $invoice->invoice_id;
            $invoice->to->save();

            event(new InvoiceStatusChange($invoice));
        }

        if($invoice->xero_invoice_id){
            $payXeroInvoice = $this->payXeroInvoice($invoice->xero_invoice_id, $invoice->total_amount, $xeroCredentials);
        }

        $request->session()->flash('alert-success', 'Invoice marked as paid successfully.');

        return redirect()->route('admin.invoices.index', array("q_to" => $invoice->to->id));
    }

    public function payXeroInvoice($invoiceId, $amount, $xeroCredentials){

        if($xeroCredentials->exists()){
            
           Log::info("XeroCredentials Exists"); 

            $str = '';
            //for demo company
            $accountId = "13918178-849A-4823-9A31-57B7EAC713D7";
            //for AAMT company, Account ID for Bank Account No. "105-034-058004340"
           // $accountId = "94E4EBA9-21B4-4F05-AB59-44CC7D0299CB";

            try{

                $xero = resolve(\XeroAPI\XeroPHP\Api\AccountingApi::class);
                   
                $xeroTenantId = $xeroCredentials->getTenantId();

                //[Payments:Create]
                try{

                    $invoice_id = $invoiceId;

                    $invoice = resolve(\XeroAPI\XeroPHP\Models\Accounting\Invoice::class);
                    $invoice->setInvoiceID($invoice_id);
                }catch(\throwable $e) {
                
                    $error = $e->getMessage();
                    Log::info("Error caught in getting invoice to pay the invoice -  " . $error);
                    
                }

                try{
                    $bankaccount = resolve(\XeroAPI\XeroPHP\Models\Accounting\Account::class);
                    $bankaccount->setAccountID($accountId);
                }catch(\throwable $e) {
                
                    $error = $e->getMessage();
                    Log::info("Error caught in getting Account to pay the invoice -  " . $error);
                }

                try{
                    $payment = resolve(\XeroAPI\XeroPHP\Models\Accounting\Payment::class);
                    $payment->setInvoice($invoice)
                        ->setAccount($bankaccount)
                        ->setAmount($amount);
                }catch(\throwable $e) {
                
                    $error = $e->getMessage();
                    Log::info("Error caught in setting payment to pay the invoice -  " . $error);
                }

                try{
                    $result = $xero->createPayment($xeroTenantId,$payment);
                }catch(\throwable $e) {
                
                    $error = $e->getMessage();
                    Log::info("Error caught in creating payment for a Invoice -  " . $error);
                }
                //[/Payments:Create]
                        
                $str = $str . "Create Payment ID: " . $result->getPayments()[0]->getPaymentID() . "<br>" ;
                
                return $result;
            }catch(\throwable $e) {
                
                $error = $e->getMessage();
                Log::info("Error caught in to pay the invoice -  " . $error);
            }


        }else{
            return false;
        }

    }

    /**
     * Change status to cancel.
     *
     * 0: Draft 1:Awaiting payment 2: Paid 3: Overdue 4:Canceled
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, OauthCredentialManager $xeroCredentials)
    {
        $id = $request->id_cancel;

        $invoice = Invoice::findOrFail($id);
        Log::info("cancel invoice " . $id);

        // check invoice status
        if (in_array($invoice->status, array(2, 4))) {
            return back()->withErrors($this->action_not_allowed_msg);
        }

        $need_email = ($invoice->status != 0);

        if ($invoice->status != 4) {
            $invoice->status = 4;
            $invoice->cancel_date = today();
            $invoice->save();

            if ($need_email) {
                event(new InvoiceStatusChange($invoice));
            }
        }

        if($invoice->xero_invoice_id){
            $updateXeroInvoice = $this->updateXeroInvoice('cancel', $invoice->xero_invoice_id, $xeroCredentials);
        }

        $request->session()->flash('alert-success', 'Invoice cancelled successfully.');

        return redirect()->route('admin.invoices.index', array("q_to" => $invoice->to->id));
    }

    /**
     * manually send reminder
     * @param  \App\Models\Invoice $invoice
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendReminder(Invoice $invoice, Request $request)
    {
        Log::info("send reminder invoice " . $invoice->id);

        // check invoice status
        if (!$invoice->isSendReminderAllowed()) {
            return back()->withErrors($this->action_not_allowed_msg);
        }

        $overdue_weeks = Carbon::parse($invoice->due_date)->copy()->diffInWeeks(today());
        SendInvoiceReminderEmail::overDueReminder($invoice, $overdue_weeks);

        $request->session()->flash('alert-success', 'Invoice send reminder successfully.');

        return redirect()->route('admin.invoices.index', array("q_to" => $invoice->to->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }
}
