# data migration tool
This tool is intended to import data from the old maths300 system to the new system. 

Note: this python script is tested on MacOS with Python 3.7.3

## install requirements 
```shell
pip3 install pandas
pip3 install xlrd
pip3 install mysql-connector-python
```
## run
```shell
python3 maths300_data_import.py
```

## php artisan
```shell
php artisan queue:listen --timeout=0
php artisan data:migration


```

## TBD

### email
- total record: 1877
- 1049 invoicing_email equals teacher email
- teacher as coordinator?
- how to build a finance officer?
- renewal_date == current subscription expiry date?
- paid_date is not reliable?

## data problem
```sql
# query teacher_email appears in more than 1 school
#
SELECT teacher_email, count(*) FROM `history_data` GROUP by teacher_email HAVING count(*) > 1 order by teacher_email

# query teacher_email is empty
# 25 teacher_email is empty
SELECT teacher_email FROM `history_data` where teacher_email is null


SELECT * FROM `history_data` ORDER by renewal_date desc

SELECT * FROM `history_data` ORDER by paid_date desc

SELECT * FROM `history_data` WHERE institution = ''

SELECT membership_status, count(*) FROM `history_data` group by membership_status

membership_status
count(*)
ACTIVE
1244
EXPIRED
633

SELECT renew_status, count(*) FROM `history_data` group by renew_status

renew_status
count(*)
584
Cancel
196
Still Considering
90
Yes
1007

SELECT payment_status, count(*) FROM `history_data` group by payment_status

payment_status
count(*)
13
No Payment Required
15
Overdue
204
Paid
1508
Waiting Payment
137

```

