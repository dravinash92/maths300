<?php

namespace App\Console\Commands;

use App\Jobs\SendInvoices;
use Illuminate\Console\Command;

class SendBatchInvoiceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:sendInvoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send batch invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SendInvoices::dispatch();
        $this->info("Run send batch invoices program successfully.");
    }
}
