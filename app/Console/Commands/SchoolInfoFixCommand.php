<?php

namespace App\Console\Commands;

use App\Jobs\SchoolInfoFix;
use Illuminate\Console\Command;

class SchoolInfoFixCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fix messes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SchoolInfoFix::dispatch();
        $this->info("Run school info fix program successfully.");
    }
}
