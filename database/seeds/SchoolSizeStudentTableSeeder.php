<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SchoolSizeStudent;

class SchoolSizeStudentTableSeeder extends Seeder {

    public function run()
    {
        DB::table('school_size_students')->delete();

        SchoolSizeStudent::create([
            'name' => '1–50 students at campus'
        ]);

        SchoolSizeStudent::create([
            'name' => '51–100 students at campus'
        ]);

        SchoolSizeStudent::create([
            'name' => '101–200 students at campus'
        ]);

        SchoolSizeStudent::create([
            'name' => '201–400 students at campus'
        ]);

        SchoolSizeStudent::create([
            'name' => '401–600 students at campus'
        ]);

        SchoolSizeStudent::create([
            'name' => '601–800 students at campus'
        ]);

        SchoolSizeStudent::create([
            'name' => '801–1200 students at campus'
        ]);

        SchoolSizeStudent::create([
            'name' => '1201–1600 students at campus'
        ]);

        SchoolSizeStudent::create([
            'name' => '1601+ students at campus'
        ]);
    }

}