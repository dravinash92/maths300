<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;
use App\Mail\UserActivated;
use App\Mail\UserResetPass;
use App\Mail\UserDisabled;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;

class User extends Authenticatable
{
    use Notifiable;

    use HasRoles;

    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *
     * generate password
     *
     * @param int $length
     * @param bool $special_chars
     * @param bool $extra_special_chars
     * @return string
     * @throws \Exception
     */
    public static function generate_password($length = 12, $special_chars = true, $extra_special_chars = false)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        if ($special_chars)
            $chars .= '@&*()';
        if ($extra_special_chars)
            $chars .= '-_ []{}<>~`+=,.;:/?|';

        $password = '';
        for ($i = 0; $i < $length; $i++) {
            $password .= substr($chars, random_int(0, strlen($chars) - 1), 1);
        }

        return $password;
    }

    /**
     *  every user may have many schools (except admin)
     *
     */
    public function schools()
    {
        return $this->belongsToMany(School::class);
    }

    /**
     *  every user may have many schools (except admin)
     *
     */
    public function getSchool()
    {
        return count($this->schools) > 0 ? $this->schools[0] : null;
    }

    /**
     * The year levels that belong to the user.
     */
    public function yearLevels()
    {
        return $this->belongsToMany(YearLevel::class);
    }

    /**
     * The favoriteLessons that belong to the user.
     */
    public function favoriteLessons()
    {
        return $this->belongsToMany(Lesson::class, 'user_favorite_lesson');
    }

    public function hasFavoriteLesson($lesson)
    {
        $flag = 0;
        foreach ($this->favoriteLessons as $favlesson) {
            if ($lesson->id == $favlesson->id) {
                $flag = 1;
                break;
            }
        }
        return $flag;
    }

    /**
     * format date_last_paid
     * @return string
     */
    public function lastLoginFormat()
    {
        return $this->last_login ? Carbon::parse($this->last_login)->format('d/m/Y') : "N/A";
    }

    public function getStatus()
    {
        $status = "unknown";

        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status == 0) {
            $status = "inactive";
        }
        if ($this->status == 1) {
            $status = "active";
        }
        if ($this->status == 2) {
            $status = "suspended";
        }
        if ($this->status == 3) {
            $status = "archived";
        }

        return $status;
    }

    public function isActivateAllowed()
    {
        return $this->status == 0 || $this->status == 2;
    }

    public function isSuspendAllowed()
    {
        return $this->status == 1;
    }

    public function isArchiveAllowed()
    {
        return $this->status == 2;
    }

    public function isDeleteAllowed()
    {
        return $this->status == 3;
    }

    public function isReinstateAllowed()
    {
        return $this->status == 3;
    }

    /**
     * Scope a query to only include given keywords.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%')
            ->orWhere('email', 'like', '%' . $keywords . '%')
            ->orWhere('phone', 'like', '%' . $keywords . '%')
            ->orWhere('position', 'like', '%' . $keywords . '%');
    }

    /**
     * Scope a query to only include given school.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $school
     */
    public function scopeSchool($query, $school = NULL)
    {
        Log::info("school: " . $school);
        $query->with('schools')->whereHas('schools', function ($query) use ($school) {
            if($school == NULL) {
                $query;
            } else {
                $query->where('schools.id', $school);
            }
        });
    }

    /**
     * activate user
     */
    public function activate()
    {
        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status != 1) {
            Log::info("Activate user name:" . $this->name . ' email: ' . $this->email);
            $this->status = 1;
            $tmp_pass = Str::random(8);
            $this->password = Hash::make($tmp_pass);
            $this->reset_token = Password::getRepository()->create($this);
            $this->save();
            // send notification emails to user
            Log::info("send UserActivated Email to: " . $this->email);
            Mail::to($this)->send(new UserActivated($this, $tmp_pass));
            // copy emails to maths300 admin
            Log::info("send UserActivated Email to maths300 admin: " . config('app.contact_email'));
            Mail::to(config('app.contact_email'))->send(new UserActivated($this, $tmp_pass));
        }
    }

    public function sendResetPassEmail()
    {
        $tmp_pass = Str::random(8);
        $this->password = Hash::make($tmp_pass);
        $this->reset_token = Password::getRepository()->create($this);
        $this->save();
        Log::info("send ResetPass Email to: " . $this->email);
        // send notification emails to user
        Mail::to($this)->send(new UserResetPass($this, $tmp_pass));
        // copy emails to maths300 admin
        Log::info("send ResetPass Email to maths300 admin: " . config('app.contact_email'));
        Mail::to(config('app.contact_email'))->send(new UserResetPass($this, $tmp_pass));
    }

    /**
     * suspend user account
     */
    public function suspend()
    {
        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status != 2) {
            Log::info("Suspend user name:" . $this->name . ' email: ' . $this->email);
            $this->status = 2;
            $this->save();
            // send notification emails to user
            Mail::to($this)->send(new UserDisabled($this));
        }
    }

    /**
     * archive user
     */
    public function archive()
    {
        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status != 3) {
            $this->status = 3;
            $this->save();
        }
    }

    /**
     * reinstate user
     */
    public function reinstate()
    {
        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status != 0) {
            $this->status = 0;
            $this->save();
        }
    }

    /**
     * Scope a query to only include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $status
     */
    public function scopeStatus($query, $status)
    {
        $query->where('status', $status);
    }

    /**
     * Scope a query to only include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $status
     */
    public function scopeStatusIn($query, $status)
    {
        $query->whereIn('status', $status);
    }

    /**
     * Scope a query to not include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $status
     */
    public function scopeStatusNot($query, $status)
    {
        $query->where('status', '!=', $status);
    }

    /**
     * check if the given email exists in the system
     * @param $email
     * @return bool
     */
    public static function isUserEmailExists($email)
    {
        return User::where('email', $email)->count() > 0;
    }

    /**
     * get a user instance by given email
     * @param $email
     * @return mixed
     */
    public static function getUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    /**
     * @param $data
     * @param $school
     * @param mixed ...$roles
     * @return User
     * @throws \Exception
     */
    public static function createWithRoles($data, $school, ...$roles)
    {
        // check subscription plan user limit first
        $validateSubscriptionUserLimitRet = $school->validateSubscriptionUserLimit();
        if (!$validateSubscriptionUserLimitRet['validated']) {
            throw new \Exception($validateSubscriptionUserLimitRet['msg']);
        }

        $user = new User();
        $tmp_pass = Str::random(10);
        $user->password = Hash::make($tmp_pass); // init random pass
        $user->status = 0; //0:inactive

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->position = !empty($data['position']) ? $data['position'] : "";
        $user->reset_token = Password::getRepository()->create($user);

        $user_year_levels = array();
        if (!empty($data['year_levels'])) {
            $user_year_levels = $data['year_levels'];
        }
        $user->yearLevels()->sync($user_year_levels);

        $user->save();
        // build relationship with school
        $user->schools()->sync([$school->id]);

        // assign roles
        $user->assignRole($roles);

        Log::info("create user of school: " . $school->name . ", user name: " . $user->name . ", email: " . $user->email . ", roles: [" . implode(",", $roles) . "]");

        return $user;
    }

    public static function updateWithoutEmailWithRoles($user, $data, $school, $removeRoles, ...$roles)
    {
        $user->name = $data['name'];
        $user->phone = $data['phone'];
        $user->position = !empty($data['position']) ? $data['position'] : "";
        $user_year_levels = array();
        if (!empty($data['year_levels'])) {
            $user_year_levels = $data['year_levels'];
        }
        $user->yearLevels()->sync($user_year_levels);
        $user->save();
        // build relationship with school
        $user->schools()->sync([$school->id]);

        // remove roles
        if ($removeRoles) {
            $user->removeRole($removeRoles);
        }

        // assign roles
        $user->assignRole($roles);

        Log::info("update user of school: " . $school->name . ", user name: " . $user->name . ", email: " . $user->email . ", roles: [" . implode(",", $roles) . "]");

        return $user;
    }
    /**
     * Get data to generate user reports
     * @param $where
     * @return User
     */
    public static function getusers($where)
    {
        return parent::selectRaw("users.id as userId,users.name as userName,users.email,users.phone,users.status as userStatus,school_user.*,schools.name,schools.status as subscription_status,locations.name as location,school_types.name as type_name,school_sectors.name as sector_name,schools.coordinator_id,schools.finance_officer_id,users.position,users.last_login,user_favorite_lesson.lesson_id,lessons.number,lessons.title")
             ->leftjoin("school_user","school_user.user_id", "=" ,"users.id")
             ->leftjoin("schools","schools.id", "=" ,"school_user.school_id")    
             ->leftjoin("locations","locations.id", "=" ,"schools.address_location_id")                
             ->leftjoin("school_types","school_types.id", "=" ,"schools.school_type_id")                
             ->leftjoin("school_sectors","school_sectors.id", "=" ,"schools.school_sector_id")              
             ->leftjoin("user_favorite_lesson","users.id", "=" ,"user_favorite_lesson.user_id")                
             ->leftjoin("lessons","lessons.id", "=" ,"user_favorite_lesson.lesson_id")                
             ->whereRaw($where)
             ->get();
    }
    public static function gettotusers($id)
    {
        return parent::selectRaw("count(school_user.id) as Total_user")
             ->leftjoin("school_user","school_user.user_id", "=" ,"users.id")        
             ->whereRaw("school_user.school_id = '".$id."'")
             ->get();
    }
    public static function getactusers($id)
    {
        return parent::selectRaw("school_user.user_id as school_user,school_user.school_id as sch_id")
             ->leftjoin("school_user","school_user.user_id", "=" ,"users.id")        
             ->whereRaw("school_user.school_id = '".$id."'")
             ->get();
    }
    public static function getactiveusers($id)
    {
        return parent::selectRaw("count(*) as active_user")
             ->whereRaw("id in (".$id.") and status = 1")
             ->get();
    }

    public static function sendMailToTeachers($id)
    {
        $res = parent::selectRaw("schools.name as school_name, users.name as coordinator_name")
             ->leftjoin("schools","schools.coordinator_id", "=" ,"users.id")->whereRaw("schools.id = '".$id."'")
             ->get();
        $school_coordinator = [];
        foreach($res as $object)
        {
            $result[] = $object->toArray();
            $school_coordinator['school_name'] = $result[0]['school_name'];
            $school_coordinator['coordinator_name'] = $result[0]['coordinator_name'];
        }

        $school_teachers = parent::selectRaw("users.id, users.name, users.email, users.status")
             ->leftjoin("school_user","school_user.user_id", "=" ,"users.id")->leftjoin("schools","schools.id", "=" ,"school_user.school_id")->whereRaw("schools.id = '1' AND `position` LIKE '%teacher%'")
             ->get();
        

        foreach ($school_teachers as $user) 
        {
            $user['school_name'] = $school_coordinator['school_name'];
            $user['coordinator_name'] = $school_coordinator['coordinator_name'];
            echo "<pre>";print_r($user);exit;
            $user->status = 0; //0: disabled
            $user->save();
            // send notification emails to all users
            // Mail sender is in queue by default
            Mail::to($user)->send(new SubscriptionRenewReminderTeacher($user));
        }

        return $result;
    }
}
