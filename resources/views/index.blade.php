@extends('layouts.master')

@section('css')
    <style>
        .switch-layout-container .thim-course-order {
            margin: 10px auto;
            display: flex;
        }

        .search-input-item {
            margin-top: 20px;
            width: 45%;
        }

        .search-input-item select {
            width: 100%;
        }

        .search-input-item select input {
            width: 100%;
        }

        .thim-widget-courses-searching .courses-searching input[type=text] {
            padding-left: 5px;
        }

        .thim-search-light-style .thim-widget-courses-searching .courses-searching {
            margin: 10px 20px 10px 20px;
            width: 100%;
            max-width: 100%;
        }

        .thim-search-light-style .thim-widget-courses-searching .courses-searching input {
            width: 100%;
            border: 1px solid #aaa;
        }

        .thim-search-light-style .thim-widget-courses-searching .courses-searching .search {
            margin-top: 20px;
            width: 40%;
        }

        @media (max-width: 480px) {
            .switch-layout-container .thim-course-order {
                margin: 10px auto;
                display: flex;
            }

            .search-input-item {
                margin-top: 20px;
                width: 100%;
            }

            .search-input-item select {
                width: 100%;
            }

            .thim-search-light-style .thim-widget-courses-searching .courses-searching {
                margin: 10px 0px 10px 0px;
                width: 100%;
                max-width: 100%;
            }

            .thim-search-light-style .thim-widget-courses-searching .courses-searching input {
                width: 100%;
                border: 1px solid #aaa;
            }

            .thim-widget-courses-searching .courses-searching input {
                width: 100%;
                height: 40px !important;
                line-height: 40px !important;
            }

        }

        @media (max-width: 375px) {
            .thim-search-light-style {
                height: 135vh;
            }
        }

        @media (max-width: 320px) {

            .thim-search-light-style {
                height: 130vh;
            }

            .switch-layout-container .thim-course-order {
                margin: 5px auto;
                display: flex;
            }

            .search-input-item {
                margin-top: 10px;
                width: 100%;
            }

            .search-input-item select {
                width: 100%;
            }

            .thim-search-light-style .thim-widget-courses-searching .courses-searching {
                margin: 10px 0px 0px 0px;
                width: 100%;
                max-width: 100%;
            }

            .thim-search-light-style .thim-widget-courses-searching .courses-searching input {
                width: 100%;
                border: 1px solid #aaa;
            }

            .thim-widget-courses-searching .courses-searching input {
                width: 100%;
                height: 40px !important;
                line-height: 40px !important;
            }

            .thim-search-light-style .search-course-list-info li {
                margin: 0 15px;
            }

            .thim-search-light-style .thim-widget-courses-searching .courses-searching .search {
                margin-top: 20px;
                width: 50%;
            }

        }

        #pg-5900-2 {
            margin-bottom: 55px;
        }

        #pg-5900-2.panel-has-style > .panel-row-style {
            -webkit-align-items: flex-start;
            align-items: flex-start;
        }

        #pgc-5900-2-0, #pgc-5900-2-1, #pgc-5900-2-2 {
            width: 33.3333%;
            width: calc(33.3333% - (0.666666666667 * 30px));
        }

        #pl-5900 .so-panel:last-child {
            margin-bottom: 0px;
        }

        @media (max-width: 767px) {
            #pg-5900-2.panel-has-style > .panel-row-style {
                flex-direction: column;
            }

            #pg-5900-2 .panel-grid-cell {
                padding: 0;
                width: 100%;
                margin-right: 0;
                margin-bottom: 30px;
            }
        }

        .thim-box-language-tests .wrapper-box-icon.has_custom_image .inner-icon img {
            height: 293px;
        }


    </style>
@endsection

@section('body')
    <body class="home page-template page-template-page-templates page-template-homepage page-template-page-templateshomepage-php page page-id-5899 checkout become_a_teacher eduma learnpress learnpress-page pmpro-body-has-access siteorigin-panels siteorigin-panels-before-js siteorigin-panels-home woocommerce-no-js thim-body-preload bg-boxed-image wpb-js-composer js-comp-ver-5.6.1 vc_responsive"
          id="thim-body">

    <div id="preload">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
    <h1 class="title">Maths300</h1>
    <div id="wrapper-container" class="wrapper-container">
        <div class="content-pusher">
            @component('layouts.header')
            @endcomponent
            <div id="main-content">
                <div id="main-home-content" class="home-content home-page container" role="main">

                    <div id="pl-5899" class="panel-layout">
                        <div id="pg-5899-0" class="panel-grid panel-has-style">
                            <div class="thim-bg-overlay thim-search-light-style siteorigin-panels-stretch panel-row-style panel-row-style-for-5899-0"
                                 data-stretch-type="full">
                                <div id="pgc-5899-0-0" class="panel-grid-cell">
                                    <div id="panel-5899-0-0-0"
                                         class="so-panel widget widget_courses-searching panel-first-child"
                                         data-index="0">
                                        <div class="thim-widget-courses-searching thim-widget-courses-searching-base">
                                            <h3
                                                    class="search-course-title">Maths Lesson Library</h3>

                                            <form id="lessonSearchForm" method="get"
                                                  action="{{ route('front.lessons.index') }}" style="margin:20px;">
                                                @csrf
                                                <div class="thim-course-top switch-layout-container"
                                                     style="margin-bottom: 10px;">
                                                    <div class="thim-course-order search-input-item">
                                                        <select class="select2-year_levels"
                                                                id="q_year_levels"
                                                                name="q_year_levels[]"
                                                                multiple="multiple"
                                                                data-placeholder="Select year levels">
                                                        </select>
                                                    </div>

                                                    <div class="thim-course-order search-input-item">
                                                        <select data-placeholder="Select content strands"
                                                                class="form-control select2-content_strands"
                                                                id="q_content_strands"
                                                                name="q_content_strands[]"
                                                                multiple="multiple">
                                                        </select>
                                                    </div>

                                                    <div class="thim-course-order search-input-item">
                                                        <select class="form-control select2-pedagogies"
                                                                id="q_pedagogies"
                                                                name="q_pedagogies[]"
                                                                multiple="multiple"
                                                                data-placeholder="Select pedagogies">
                                                        </select>
                                                    </div>

                                                    <div class="thim-course-order search-input-item">
                                                        <select class="form-control select2-curricula"
                                                                id="q_curricula"
                                                                name="q_curricula[]"
                                                                multiple="multiple"
                                                                data-placeholder="Select curricula">
                                                        </select>
                                                    </div>


                                                    <div class="courses-searching">
                                                        <input type="text" value="{{ old('q') }}" name="q" id="q"
                                                               placeholder="Enter search key words (title/number/summary)"
                                                               class="thim-s form-control courses-search-input"
                                                               autocomplete="off"/>
                                                        <button type="submit" class="search">Search
                                                        </button>
                                                        <span class="widget-search-close"></span>

                                                        <ul class="courses-list-search list-unstyled"></ul>
                                                    </div>

                                                </div>
                                            </form>


                                        </div>
                                    </div>
                                    <div id="panel-5899-0-0-1" class="so-panel widget widget_text panel-last-child"
                                         data-index="1">
                                        <div class="textwidget">
                                            <p></p>
                                            <ul class="search-course-list-info">
                                                <li><a href="#">Maths 300: A rich inquiry-based approach to teaching maths.</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="pg-5900-2" class="panel-grid panel-has-style">
                            <div class="thim-box-language-tests panel-row-style panel-row-style-for-5900-2">
                                @if($sample_lesson)
                                    <div id="pgc-5900-2-0" class="panel-grid-cell">
                                        <div id="panel-5900-2-0-0"
                                             class="so-panel widget widget_icon-box panel-first-child panel-last-child"
                                             data-index="4">
                                            <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                <div class="wrapper-box-icon has_custom_image has_read_more text-left   wrapper-line-heading">
                                                    <div class="smicon-box iconbox-top">
                                                        <div class="boxes-icon">
                                                            <a class="icon-box-link" target="_blank"
                                                               href="{{ route('front.lessons.index', array( "q_id" => $sample_lesson->id )) }}">
                                                            <span class="inner-icon">
                                                                <span class="icon icon-images">
                                                                    <img src="{{ $sample_lesson->coverImage? $sample_lesson->coverImage->url : asset('images/maths300_default.png') }}"
                                                                         alt="maths300" title="maths300-lesson"
                                                                         width="480" height="293">
                                                                </span>
                                                            </span>
                                                            </a>
                                                        </div>
                                                        <div class="content-inner">
                                                            <div class="sc-heading article_heading"><h3
                                                                        class="heading__primary"
                                                                        style="font-size: 18px; line-height: 18px;font-weight: 700;margin-top: 28px;margin-bottom: 18px;">
                                                                    Sample Lesson</h3><span
                                                                        class="line-heading"></span></div>
                                                            <div class="desc-icon-box">
                                                                <div class="desc-content">
                                                                    <p>
                                                                    <h3>{{ $sample_lesson->title }}</h3>
                                                                    <span style="font-size:17px">{{ $sample_lesson->desc_front }}</span>
                                                                    <br>
                                                                    </p>
                                                                </div>
                                                                <a class="smicon-read sc-btn" target="_blank"
                                                                   href="{{ route('front.lessons.index', array( "q_id" => $sample_lesson->id )) }}">Learn
                                                                    More<i class="fa fa-chevron-right"></i></a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(count($favorite_lessons))
                                    @foreach($favorite_lessons as $index=>$lesson)
                                        <div id="pgc-5900-2-{{$index+1}}" class="panel-grid-cell">
                                            <div id="panel-5900-2-{{$index+1}}-0"
                                                 class="so-panel widget widget_icon-box panel-first-child panel-last-child"
                                                 data-index="5">
                                                <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                    <div class="wrapper-box-icon has_custom_image has_read_more text-left   wrapper-line-heading">
                                                        <div class="smicon-box iconbox-top">
                                                            <div class="boxes-icon">
                                                                <a class="icon-box-link" target="_blank"
                                                                   href="{{ route('front.lessons.index', array( "q_id" => $lesson->id )) }}">
                                                        <span class="inner-icon">
                                                            <span class="icon icon-images front-images">
                                                                <img src="{{ $lesson->coverImage? $lesson->coverImage->url : asset('images/maths300_default.png') }}"
                                                                     alt="maths300-lesson"
                                                                     title="maths300-lesson" width="480"
                                                                     height="293">
                                                            </span>
                                                        </span>
                                                                </a>
                                                            </div>
                                                            <div class="content-inner">
                                                                <div class="sc-heading article_heading">
                                                                    <h3 class="heading__primary"
                                                                        style="font-size: 18px; line-height: 18px;font-weight: 700;margin-top: 28px;margin-bottom: 18px;">
                                                                        Favorite Lesson {{$index+1}}
                                                                    </h3>
                                                                    <span class="line-heading"></span>
                                                                </div>
                                                                <div class="desc-icon-box">
                                                                    <div class="desc-content">
                                                                        <p>
                                                                        <h3>{{ $lesson->title }}</h3>
                                                                        <span style="font-size:17px">{{ $lesson->desc_front }}</span>
                                                                        <br>
                                                                        </p>
                                                                    </div>
                                                                    <a class="smicon-read sc-btn" target="_blank"
                                                                       href="{{ route('front.lessons.index', array( "q_id" => $lesson->id )) }}">Learn
                                                                        More
                                                                        <i class="fa fa-chevron-right"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div><!-- #main-content -->

                @component('layouts.footer')
                @endcomponent

            </div><!--end main-content-->
        </div><!-- end content-pusher-->

        <a href="#" id="back-to-top">
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </a>
    </div><!-- end wrapper-container -->

    <div class="gallery-slider-content"></div>
    @stop

    @section('js')
        <script type='text/javascript'>
            /* <![CDATA[ */
            var lpQuizSettings = [];
            /* ]]> */

            jQuery(document).ready(function () {
                // Initialize Select2 elements
                let select2_year_levels = jQuery('.select2-year_levels').select2({
                    allowClear: true,
                    data: {!! json_encode($year_levels)!!}
                });

                @if(old('q_year_levels'))
                select2_year_levels.val({!! json_encode( collect(old('q_year_levels')) ) !!}).trigger('change');
                        @endif

                let select2_content_strands = jQuery('.select2-content_strands').select2({
                        allowClear: true,
                        data: {!! json_encode($content_strands)!!}
                    });

                @if(old('q_content_strands'))
                select2_content_strands.val({!! json_encode( collect(old('q_content_strands')) ) !!}).trigger('change');
                        @endif

                let select2_pedagogies = jQuery('.select2-pedagogies').select2({
                        allowClear: true,
                        data: {!! json_encode($pedagogies)!!}
                    });

                @if(old('q_pedagogies'))
                select2_pedagogies.val({!! json_encode( collect(old('q_pedagogies')) ) !!}).trigger('change');
                        @endif

                let select2_curricula = jQuery('.select2-curricula').select2({
                        allowClear: true,
                        data: {!! json_encode($curricula)!!}
                    });

                @if(old('q_curricula'))
                select2_curricula.val({!! json_encode( collect(old('q_curricula')) ) !!}).trigger('change');
                @endif
            });
        </script>
@stop