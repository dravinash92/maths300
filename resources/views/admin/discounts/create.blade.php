@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.discounts.index') }}">Discounts</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add New</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="discountForm" role="form" method="post" action="{{ route('admin.discounts.store') }}">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group {{ $errors->has('discountType') ? 'has-error' : '' }}">
                        <label for="discountType">Discount Type</label>
                        <select data-placeholder="Select a discountType" class="form-control select2_discountType"
                                style="width: 100%;" id="discountType" name="discountType">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('discountType') }}</span>
                    </div>


                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name"
                               value="{{ old('name') }}">
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
                        <label for="value">Value</label>
                        <input type="text" class="form-control" id="value" name="value" placeholder="Enter value"
                               value="{{ old('value') }}">
                        <span class="text-danger">{{ $errors->first('value') }}</span>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            let select2_discountType = $('.select2_discountType').select2({
                allowClear: true,
                data: {!! json_encode($discountTypes)!!}
            });

            @if(old('discountType'))
            select2_discountType.val({!! old('discountType') !!}).trigger('change');
            @endif

            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>
@endpush