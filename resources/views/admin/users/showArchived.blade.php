@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.users.index') }}">Users</a></li>
        <li><a href="{{ route('admin.users.showArchived') }}">Archived</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Users</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="userSearchForm" role="form" method="get" action="{{ route('admin.users.showArchived') }}">
                {{ csrf_field() }}
                <div class="row ">
                    <div class="col-xs-3">
                        <select data-placeholder="Select a school" class="form-control select2_school"
                                style="width: 100%;" id="q_school" name="q_school">
                            <option></option>
                        </select>
                    </div>

                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter keywords (name/email/phone)..."
                                   value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="usersearchBtn" type="submit"
                                        class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>

            <table id="tab_users" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Last login</th>
                    <th>Name</th>
                    <th>User</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th>Menu</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($users as $l)
                    <tr class="item{{ $l->id }}">
                        <td class="col1">{{ $l->lastLoginFormat() }}</td>
                        <td><a href='{{ route('admin.users.show',$l->id) }}'>{{ $l->name }}</a></td>
                        <td>
                            @foreach($l->schools()->orderBy('schools.id')->pluck('schools.name','schools.id') as $k=>$v )
                                <a href='{{ route('admin.schools.show',$k) }}'>{{  $v }}</a>
                            @endforeach
                        </td>
                        <td><a href="mailto:{{ $l->email }}">{{ $l->email }}</a></td>
                        <td><a href="tel:{{ $l->phone }}">{{ $l->phone }}</a></td>
                        <td>{{ $l->getStatus() }}</td>
                        <td>
                            @if( $l->isReinstateAllowed() )
                                <button class="reinstate-modal btn btn-primary" data-id="{{ $l->id }}"
                                        data-name="{{ $l->name }}">Reinstate
                                </button>
                            @endif

                            @if( $l->isDeleteAllowed() )
                                <button class="delete-modal btn btn-primary" data-id="{{ $l->id }}"
                                        data-name="{{ $l->name }}">Delete
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    @modal(['action' => 'reinstate', 'model' => 'user', 'form_action' => '/admin/users/reinstate'])
    @endmodal

    @modal(['action' => 'delete', 'model' => 'user'])
    @endmodal
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {

            // Initialize Select2 elements
            let select2_school = $('.select2_school').select2({
                allowClear: true,
                data: {!! json_encode($schools)!!}
            });

            @if(old('q_school'))
            select2_school.val({!! old('q_school') !!}).trigger('change');
            @endif

            $('#tab_users').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search users......"
                },
                'ordering': true,
                'info': true,
                'columnDefs': [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 6
                    }],
                'autoWidth': false,
                'columns': [{
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "15%"
                }, {
                    "width": "20%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "30%"
                }
                ]
            });

            $(".dataTables_filter").hide();
        });

        // Automatically trigger the loading animation on click
        Ladda.bind('button[type=submit]');
    </script>

    @modaljs(['action' => 'reinstate'])
    @endmodaljs

    @modaljs(['action' => 'delete', 'tab' => 'tab_users', 'submit_url' => 'admin/users'])
    @endmodaljs
@endpush
