@section('css')
    <style>
        table, tr, tbody, td {
            /*border: none;*/
        }

        .table {
            margin-bottom: 0px;
        }

        .table > tbody > tr > td {
            border-top: 0px;
            padding: 0px;
        }

        .lesson-content {
            border: 1px solid #ccc;
            padding: 20px;
        }

        .summary {
            margin-bottom: 0px;
        }

        .item-block {
            min-width: 40px;
            height: 40px;
            border: 1px solid #111;
            text-align: center;
            line-height: 38px;
            display: inline-block;
            overflow: hidden;
            font-size: 13px;
            font-weight: 700;
            color: #333;
            padding: 0 10px 0px 10px;
        }

        .highlight {
            background-color: #b8beba;
            border-color: #FFFFFF;
        }

        .item-curricula-block {
            width: 90px;
            height: 40px;
            border: 1px solid #111;
            text-align: center;
            line-height: 38px;
            display: inline-block;
            overflow: hidden;
            font-size: 13px;
            font-weight: 700;
            color: #333;
            padding: 0 10px 0px 10px;
            background-color: #b8beba;
            border-color: #FFFFFF;
        }

        .thim-course-content ul span a {
            display: inline-block;
            line-height: 38px;
            color: #111;
        }

        article .entry-header .entry-title {
            line-height: 35px;
            margin: 0;
        }

        @media (min-width: 767px) {
            .thim-course-content > p > img {
                max-width: 320px;
            }

            .thim-course-content > p {
                margin-bottom: 20px;
            }

            .thim-course-content > p > img.alignright {
                margin: 7px 0 7px 24px;
            }
        }

        @media (max-width: 420px) {
            img.alignright {
                margin: 7px 0 7px 16px;
            }

            .thim-course-content > p {
                margin-bottom: 5px;
            }
        }

        @media (max-width: 320px) {
            .thim-course-content > p > img {
                max-width: 180px;
            }

            .thim-course-content > p > img.alignright {
                margin: 7px 0 7px 48px;
            }
        }
    </style>
@stop

@extends('layouts.page')

@section('top_heading')
    Lessons
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/lessons" title="Lessons"><span itemprop="name">Lessons</span></a>
    </li>
@stop

@section('content')
    <article id="post-2778"
             class="post-2778 page type-page status-publish hentry pmpro-has-access">
        <div class="entry-content">
            <div id="lp-archive-courses">

            </div>
        </div><!-- .entry-content -->
    </article><!-- #post-## -->

    <article id="post-3696"
             class="col-sm-12 post-3696 post type-post status-publish format-standard has-post-thumbnail hentry category-business tag-wordpress pmpro-has-access">
        <div class="content-inner">
            @if(count($lessons))
                @foreach($lessons as $lesson)
                <?php $salt="MathsLesson300";$encrypted_id = base64_encode($lesson->id . $salt);  ?>
                    <div class="entry-content lesson-content">
                        <header class="entry-header">
                            <h2 class="entry-title">
                                <a href='{{ route('front.lessons.show',$encrypted_id) }}'
                                   rel="bookmark">{{ $lesson->title }}</a>
                            </h2>
                        </header>
                        <!-- .entry-header -->

                        <div class="thim-course-content">
                            <h4>Summary</h4>
                            <p class="summary">
                                {!! $lesson->summary !!}
                            </p>

                            <h4>Year Levels</h4>
                            <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                @foreach($lesson->yearLevelItemsHtml() as $html)
                                    {!! $html !!}
                                @endforeach
                            </ul>

                            <h4>Content Strands</h4>
                            <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                @foreach($lesson->contentStrandsItemsHtml() as $html)
                                    {!! $html !!}
                                @endforeach
                            </ul>

                            <h4>Pedagogies</h4>
                            <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                @foreach($lesson->pedagogiesItemsHtml() as $html)
                                    {!! $html !!}
                                @endforeach
                            </ul>

                            <h4>Curricula</h4>
                            <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                @foreach($lesson->curriculaItemsHtml() as $html)
                                    {!! $html !!}
                                @endforeach
                            </ul>
                        </div>

                        <div class="readmore">
                            <?php $salt="MathsLesson300";
                                 $encrypted_id = base64_encode($lesson->id . $salt);  ?>
                            <a href='{{ $lesson->is_sample? route('front.lessons.sample', $encrypted_id) : route('front.lessons.show', $encrypted_id) }}'
                               target="_blank">Read
                                More</a>
                        </div>
                    </div>
                    <br>
                @endforeach
            @else
                <p class="message message-error">No results found!</p>
            @endif
        </div>
    </article><!-- #post-## -->
@stop

<!-- .pagination -->
@section('pagination')
    {{ $lessons->links() }}
@stop

@section('sidebar')

    <div class="theiaStickySidebar"
         style="padding-top: 0px; padding-bottom: 1px; position: fixed; width: 270px; top: 110px; left: 1035px;">
        <aside class="thim-course-filter-wrapper">

            <form name='lessonSearchForm' id="lessonSearchForm" role="form" method="get"
                  action="{{ route('front.lessons.index') }}">
                {{ csrf_field() }}

                <h4 class="filter-title">Year Levels</h4>
                <select class="select2-year_levels" style="width: 100%;"
                        id="q_year_levels"
                        name="q_year_levels[]"
                        multiple="multiple"
                        data-placeholder="Select year levels">
                </select>

                <h4 class="filter-title">Content Strands</h4>
                <select data-placeholder="Select content strands"
                        class="form-control select2-content_strands"
                        style="width: 100%;" id="q_content_strands"
                        name="q_content_strands[]"
                        multiple="multiple">
                </select>

                <h4 class="filter-title">Pedagogies</h4>
                <select class="form-control select2-pedagogies"
                        style="width: 100%;" id="q_pedagogies"
                        name="q_pedagogies[]"
                        multiple="multiple"
                        data-placeholder="Select pedagogies">
                </select>

                <h4 class="filter-title">Curricula</h4>
                <select class="form-control select2-curricula"
                        style="width: 100%;" id="q_curricula"
                        name="q_curricula[]"
                        multiple="multiple"
                        data-placeholder="Select curricula">
                </select>

                <h4 class="filter-title">Key Words</h4>
                <div class="courses-searching"
                     style="margin-left: 0px; margin-top: 0px;">
                    <input type="text" value="{{ old('q') }}" name="q"
                           placeholder="Enter search key words"
                           class="form-control "
                           style="width: 100%;"
                           autocomplete="off"/>
                    <div class="filter-submit">
                        <button type="submit" style="margin-top: 20px">Search</button>
                    </div>
                </div>
            </form>
        </aside>
    </div>
@stop

@section('js')
    <script type='text/javascript'>
        /* <![CDATA[ */
        var lpQuizSettings = [];
        /* ]]> */

        jQuery(document).ready(function () {
            // Initialize Select2 elements
            let select2_year_levels = jQuery('.select2-year_levels').select2({
                allowClear: true,
                data: {!! json_encode($year_levels)!!}
            });

            @if(old('q_year_levels'))
            select2_year_levels.val({!! json_encode( collect(old('q_year_levels')) ) !!}).trigger('change');
                    @endif

            let select2_content_strands = jQuery('.select2-content_strands').select2({
                    allowClear: true,
                    data: {!! json_encode($content_strands)!!}
                });

            @if(old('q_content_strands'))
            select2_content_strands.val({!! json_encode( collect(old('q_content_strands')) ) !!}).trigger('change');
                    @endif

            let select2_pedagogies = jQuery('.select2-pedagogies').select2({
                    allowClear: true,
                    data: {!! json_encode($pedagogies)!!}
                });

            @if(old('q_pedagogies'))
            select2_pedagogies.val({!! json_encode( collect(old('q_pedagogies')) ) !!}).trigger('change');
                    @endif

            let select2_curricula = jQuery('.select2-curricula').select2({
                    allowClear: true,
                    data: {!! json_encode($curricula)!!}
                });

            @if(old('q_curricula'))
            select2_curricula.val({!! json_encode( collect(old('q_curricula')) ) !!}).trigger('change');
            @endif

            jQuery('#lessonSearchForm').submit(function (event) {
                console.log('search submit......');
                jQuery('#thim-course-archive').addClass('loading');
                return true;
            });

            // remove image inline max-width attribute
            jQuery('.thim-course-content > p > img').removeAttr('style');

            // fix front-end table border color issue start ///
            let border_styled_tables = jQuery('.entry-content table');
            for(let i=0; i<border_styled_tables.length; i++) {
                let table = jQuery(border_styled_tables[i]);
                let border_color = table.css('border-color');
                // console.log('border_color:' + border_color);
                // set tr/td border color
                let rows = table.find('> tbody > tr');
                for(let j=0; j<rows.length; j++) {
                    let row = jQuery(rows[j]);
                    // console.log('row border_color:' + row.css('border-color'));
                    row.css('border', '1px solid '+ border_color);
                }

                let tds = table.find('> tbody > tr > td');
                for(let x=0; x<tds.length; x++) {
                    let td = jQuery(tds[x]);
                    // console.log('td border-color: ' + td.css('border-color'));
                    // temporary workaround
                    if (td.css('border-color') === 'rgb(238, 238, 238)') {
                        td.css('border-color', border_color);
                    }
                }
            }
            // fix front-end table border color issue end ///
        });
    </script>
@stop