<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();

            // 0: closed 1: open
            // adding new topic is not allowed when forum closed
            $table->integer('status')->default(1);

            // 0: hidden 1: public
            $table->integer('visibility')->default(1);

            // 0: no 1: yes
            // stick it to the top of all forums.
            $table->integer('sticky')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forums');
    }
}
