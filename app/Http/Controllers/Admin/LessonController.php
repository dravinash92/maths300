<?php

namespace App\Http\Controllers\Admin;

use App\Models\Advertisement;
use App\Models\ContentStrand;
use App\Models\Lesson;
use App\Models\Tag;
use App\Models\Pedagogy;
use App\Models\Curriculum;
use App\Models\Resource;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\YearLevel;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\LessonRequest;
use Response;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['year_levels'] = data4Select(YearLevel::class);
        $arr['content_strands'] = data4Select(ContentStrand::class);
        $arr['pedagogies'] = data4Select(Pedagogy::class);
        $arr['curricula'] = data4Select(Curriculum::class);
        $arr['tags'] = data4Select(Tag::class);
        
        $query = Lesson::query();


        if ($request->has('q_year_levels')) {
            $query->yearLevels($request->q_year_levels);
        }

        if ($request->has('q_content_strands')) {
            $query->contentStrands($request->q_content_strands);
        }

        if ($request->has('q_pedagogies')) {
            $query->pedagogies($request->q_pedagogies);
        }

        if ($request->has('q_curricula')) {
            $query->curricula($request->q_curricula);
        }
        
        if ($request->has('q_tags')) {
            $query->Tags($request->q_tags);
        }
        

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("lesson search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        if($request->has('sortBy')) {
            $order = $request->has('order') && $request->order=='desc' ? 'desc' : 'asc';
            if($request->sortBy=='upload_date') {
                $query->orderBy('created_at', $order);
            }
            if($request->sortBy=='views') {
                $query->orderBy('number_of_views', $order);
            }
            if($request->sortBy=='number') {
                $query->orderBy('number', $order);
            }
            if($request->sortBy=='title') {
                $query->orderBy('title', $order);
            }
            if($request->sortBy=='strands') {

            }
            if($request->sortBy=='year') {

            }
            if($request->sortBy=='status') {
                $query->orderBy('status', $order);
            }
        }

        $paginator = $query->paginate(config('app.page_number'));

        if ($request->has('q_year_levels')) {
            $paginator->appends(array('q_year_levels' => $request->q_year_levels));
        }

        if ($request->has('q_content_strands')) {
            $paginator->appends(array('q_content_strands' => $request->q_content_strands));
        }

        if ($request->has('q_pedagogies')) {
            $paginator->appends(array('q_pedagogies' => $request->q_pedagogies));
        }

        if ($request->has('q_curricula')) {
            $paginator->appends(array('q_curricula' => $request->q_curricula));
        }
        
        if ($request->has('q_tags')) {
            $paginator->appends(array('q_tags' => $request->q_tags));
        }

        if ($request->has('q') && $request->q != null) {
            $paginator->appends(array('q' => $request->q));
        }

        $arr['lessons'] = $paginator;
        $arr['sortby'] = $request->sortBy;
        $arr['order'] = $request->order;
        $arr['q_year_levels'] = $request->q_year_levels;
        $arr['q_content_strands'] = $request->q_content_strands;
        $arr['q_pedagogies'] = $request->q_pedagogies;
        $arr['q_curricula'] = $request->q_curricula;
        $arr['q'] = $request->q;

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.lessons.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $arr['content_strands'] = ContentStrand::all();
        $arr['year_levels'] = YearLevel::all();
        $arr['pedagogies'] = Pedagogy::all();
        $arr['curricula'] = Curriculum::all();
        $arr['lessons'] = Lesson::all('id', 'number', 'title');
        $arr['tags'] = Tag::all('id', 'name');
        return view('admin.lessons.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\LessonRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(LessonRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $lesson = new Lesson();
        $number_format = sprintf('%03d', $validated['number']);
        $lesson->number = $number_format;
        $lesson->title = $validated['title'];
        // purify html input todo
        $lesson->summary = $validated['summary'];
        $lesson->resources = $validated['resources'];
        $lesson->lesson_notes = $validated['lesson_notes'];
        // handle file upload
        $lesson->extensions = $validated['extensions'];
        $lesson->practical_tips = $validated['practical_tips'];
        $lesson->related_lessons = isset($validated['related_lessons']) ? implode(",", (array)$validated['related_lessons']) : '';
        $lesson->acknowledgements = $validated['acknowledgements'];
        $lesson->attributes = $validated['attributes'];
        $lesson->software_required = $validated['software_required'];
        if (!empty($validated['software_link'])) {
            $lesson->software_link = $validated['software_link'];
        }
        if ($lesson->software_required == 0) {
            $lesson->software_link = '';
        }
        $lesson->partial_video = $validated['partial_video'];
        $lesson->full_video = $validated['full_video'];
        $lesson->notes = $validated['notes'];

        $lesson->is_front = $validated['is_front'];
        $lesson->is_sample = $validated['is_sample'];

        DB::transaction(function () use ($lesson, $validated) {
            $lesson->save();
            $lesson->yearLevels()->sync($validated['year_levels']);
            $lesson->contentStrands()->sync($validated['content_strands']);
            
            if (!empty($validated['pedagogies'])) {
                $lesson->pedagogies()->sync($validated['pedagogies']);
            }
            if (!empty($validated['lesson_pack'])) {
                $lesson->Tags()->sync($validated['lesson_pack']);
            }

            if (!empty($validated['curricula'])) {
                $lesson->curricula()->sync($validated['curricula']);
            }

            // save relationships between resources and lesson.
            $lesson->attachResources($lesson);

            // cover info
            if ($lesson->is_front) {
                if (!empty($validated['desc_front'])) {
                    $lesson->desc_front = $validated['desc_front'];
                }
                if (!empty($validated['cover_image_front'])) {
                    // cover_image_front
                    $cover_image_front = Resource::find($validated['cover_image_front']);
                    $lesson->coverImage()->associate($cover_image_front)->save();
                }
                $lesson->save();
            }

            Log::info("create lesson " . $lesson->id);
        });

        $request->session()->flash('alert-success', 'Lesson added successfully.');

        return redirect()->route('admin.lessons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        $arr['lesson'] = $lesson;
        $arr['tags'] = Tag::all('id', 'name');
        Log::info("show lesson " . $lesson->id);

        return view('admin.lessons.show')->with($arr);
    }

    /**
     * preview in front-end
     *
     * @param  \App\Models\Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $lesson = Lesson::findOrFail($id);
        $arr['lesson'] = $lesson;
        // add latest published lessons (5)
        $query = Lesson::query()->status(1)->orderBy('created_at', 'desc')->limit(5);
        $arr['latest_lessons'] = $query->get();
        // published ads
        $arr['ads'] = Advertisement::status(1)->get();

        Log::info("preview lesson " . $lesson->id);

        return view('front.lessons.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        $arr['content_strands'] = ContentStrand::all();
        $arr['year_levels'] = YearLevel::all();
        $arr['pedagogies'] = Pedagogy::all();
        $arr['curricula'] = Curriculum::all();
        $arr['lessons'] = Lesson::all('id', 'number', 'title')->where('id', '!=', $lesson->id);
        $arr['lesson'] = $lesson;
        $arr['tags'] = Tag::all();
        return view('admin.lessons.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\LessonRequest $request
     * @param  \App\Models\Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(LessonRequest $request, Lesson $lesson)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $number_format = sprintf('%03d', $validated['number']);
        $lesson->number = $number_format;
        $lesson->title = $validated['title'];
        // purify html input todo
        $lesson->summary = $validated['summary'];
        $lesson->resources = $validated['resources'];
        $lesson->lesson_notes = $validated['lesson_notes'];
        // handle file upload
        $lesson->extensions = $validated['extensions'];
        $lesson->practical_tips = $validated['practical_tips'];
        $lesson->related_lessons = isset($validated['related_lessons']) ? implode(",", (array)$validated['related_lessons']) : '';
        $lesson->acknowledgements = $validated['acknowledgements'];
        $lesson->attributes = $validated['attributes'];
        $lesson->software_required = $validated['software_required'];
        if (!empty($validated['software_link'])) {
            $lesson->software_link = $validated['software_link'];
        }
        if ($lesson->software_required == 0) {
            $lesson->software_link = '';
        }
        $lesson->partial_video = $validated['partial_video'];
        $lesson->full_video = $validated['full_video'];
        $lesson->notes = $validated['notes'];
        $lesson->is_front = $validated['is_front'];
        $lesson->is_sample = $validated['is_sample'];

        DB::transaction(function () use ($lesson, $validated) {
            $lesson->save();
            $lesson->yearLevels()->sync($validated['year_levels']);
            $lesson->contentStrands()->sync($validated['content_strands']);

            // check if the input is empty, and an empty input is allowed!!
            $curricula = array();
            if (!empty($validated['curricula'])) {
                $curricula = $validated['curricula'];
            }
            $lesson->curricula()->sync($curricula);

            // check if the input is empty, and an empty input is allowed!!
            $pedagogies = array();
            if (!empty($validated['pedagogies'])) {
                $pedagogies = $validated['pedagogies'];
            }
            $lesson->pedagogies()->sync($pedagogies);
            
            $tags = array();
            if (!empty($validated['lesson_pack'])) {
                $tags = $validated['lesson_pack'];
            }
            $lesson->Tags()->sync($tags);

            // save relationships between resources and lesson.
            $lesson->attachResources();

            // cover info
            if ($lesson->is_front) {
                if (!empty($validated['desc_front'])) {
                    $lesson->desc_front = $validated['desc_front'];
                }
                if (!empty($validated['cover_image_front'])) {
                    // remove previous res
                    $prev_cover_image = $lesson->coverImage;
                    // previous cover image exists
                    if ($prev_cover_image) {
                        // check if image has been replaced
                        if ($prev_cover_image->id != $validated['cover_image_front']) {
                            Log::info("cover_image has been replaced to " . $validated['cover_image_front']);
                            // delete
                            Resource::deleteRes($prev_cover_image->path);
                            $lesson->coverImage()->dissociate()->save();
                            $prev_cover_image->delete();
                        }
                    }
                    // cover_image_front
                    $cover_image_front = Resource::find($validated['cover_image_front']);
                    $lesson->coverImage()->associate($cover_image_front)->save();
                }
                $lesson->save();
            } else {
                $lesson->desc_front = '';
                // cover_image_front
                // change it to not front
                // delete the previous resources
                if (!empty($validated['cover_image_front'])) {
                    $cover_image_front = Resource::find($validated['cover_image_front']);
                    Resource::deleteRes($cover_image_front->path);
                    $lesson->coverImage()->dissociate()->save();
                    $cover_image_front->delete();
                }

                $lesson->save();
            }

            Log::info("update lesson " . $lesson->id);
        });

        $request->session()->flash('alert-success', 'Lesson updated successfully.');

        return redirect()->route('admin.lessons.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lesson = Lesson::findOrFail($id);
        Log::info("delete lesson " . $id);

        // delete operations in one transaction
        DB::transaction(function () use ($lesson) {
            $lesson->deleteAllResources();
            // remove previous res
            $prev_cover_image = $lesson->coverImage;
            // previous cover image exists
            if ($prev_cover_image) {
                // delete
                Resource::deleteRes($prev_cover_image->path);
                $lesson->coverImage()->dissociate()->save();
                $prev_cover_image->delete();
            }
            $lesson->delete();
        });

        return response()->json($lesson);
    }

    /**
     * Change status to published.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request)
    {
        $id = $request->id_publish;

        $lesson = Lesson::findOrFail($id);
        Log::info("publish lesson " . $id);

        $lesson->status = 1;
        $lesson->save();

        $request->session()->flash('alert-success', 'Lesson published successfully.');

        return redirect()->route('admin.lessons.index');
    }

    /**
     * Change status to suspended.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function suspend(Request $request)
    {
        $id = $request->id_suspend;

        $lesson = Lesson::findOrFail($id);
        Log::info("suspend lesson " . $id);

        $lesson->status = 2;
        $lesson->save();

        $request->session()->flash('alert-success', 'Lesson suspended successfully.');

        return redirect()->route('admin.lessons.index');
    }

}
