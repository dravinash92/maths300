@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.schools.index') }}">Schools</a></li>
        <li><a href="{{ route('admin.invoices.index',array("q_to"=> $school_id)) }}">Invoices</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Invoices</h3>
            </div>
            <div class="col-md-4 text-right">
                <form id="form_invoice_gen" method="get" action="{{ route('admin.invoices.create') }}">
                    <input type="hidden" id="school_id" name="school_id" value="{{ old('school_id', $school_id) }}">
                    <button type="submit" id="btn_invoice_gen" class="genInvoice btn btn-primary ladda-button btn-flat"
                            data-style="expand-right" {{ $create_invoice_action }}><span
                                class="ladda-label">Add New</span></button>
                </form>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Error</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

{{--            <div class="col-md-12 text-right">--}}
{{--                <a class="archive btn btn-primary btn-flat" href="{{ route('admin.invoices.showArchived',array("q_to"=> $school_id)) }}">Archived</a>--}}
{{--            </div>--}}

            <table id="tab_invoices" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>To</th>
                    <th>Issue Date</th>
                    <th>Due Date</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Menu</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($invoices as $l)
                    <tr class="item{{ $l->id }}">
                        <td><a href='{{ route('admin.invoices.show',$l->id) }}'>{{ $l->invoice_id }}</a></td>
                        <td>{{ $l->to->name }}</td>
                        <td>{{ $l->issueDateFormat() }}</td>
                        <td>{{ $l->dueDateFormat() }}</td>
                        <td>{{ $l->amount_due }}</td>
                        <td>{{ $l->getStatus() }}</td>
                        <td>
                            @if( $l->isSendAllowed() )
                            <button class="send-modal btn btn-primary" data-id="{{ $l->id }}">Send</button>
                            @endif

                            @if( $l->isMarkAsPaidAllowed() )
                            <button class="paid-modal btn btn-primary"
                                    data-id="{{ $l->id }}"
                                    data-invoiceId="{{ $l->invoice_id }}"
                                    data-issueDate="{{ $l->issueDateFormat() }}"
                                    data-dueDate="{{ $l->dueDateFormat() }}"
                                    data-sendDate="{{ $l->sendDateFormat() }}"
                                    data-subscriptionExpirationDate="{{ $l->subscriptionExpirationDateFormat() }}"
                                    data-newSubscriptionExpirationDate="{{ $l->newSubscriptionExpirationDateFormat() }}"
                                    data-amountDue="{{ $l->amount_due }}"
                                    data-isOverDue="{{ $l->isOverDue() }}"
                                    data-isGracePeriodExpire="{{ $l->isGracePeriodExpire() }}"
                                    data-overDueDays="{{ $l->overDueDays() }}"
                                    data-invoiceStatus="{{ $l->getStatus() }}"
                                    data-schoolStatus="{{ $l->getSchoolStatus() }}"
                            >Mark as Paid</button>
                            @endif

                            @if( $l->isCancelAllowed() )
                            <button class="cancel-modal btn btn-primary" data-id="{{ $l->id }}">Cancel</button>
                            @endif

                            @if( $l->xero_invoice_id )
                            
                            <button class="download-xero-invoice btn btn-primary" data-id="{{ $l->id }}"
                                    data-invoiceId="{{ $l->invoice_id }}" style="margin-top: 1px"
                                >Download Xero Invoice</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <form class="form-horizontal" role="form" id="form_download_xero" method="post"
                                  action="{{ route('admin.invoices.downloadXeroInvoice') }}">
                                {{ csrf_field() }}
        <input type="hidden" class="form-control" id="id_download" name="id_download">
    </form>

    <!-- Modal form to send a invoice -->
    <div id="sendModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Are you sure you want to send this invoice?</h3>
                    <br/>
                    <!-- form start -->
                    <form class="form-horizontal" role="form" id="form_send" method="post"
                          action="{{ route('admin.invoices.send') }}">
                        {{ csrf_field() }}
                        <input type="hidden" class="form-control" id="id_send" name="id_send">
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger send" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-alert'></span> Send
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to paid a invoice -->
    <div id="paidModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Are you sure you want to mark this invoice as paid?</h3>
                    <br/>
                    <!-- form start -->
                    <form class="form-horizontal" role="form" id="form_paid" method="post"
                          action="{{ route('admin.invoices.paid') }}">
                        {{ csrf_field() }}
                        <input type="hidden" class="form-control" id="id_paid" name="id_paid">
                        <input type="hidden" class="form-control" id="isOverDue" name="isOverDue">
                        <input type="hidden" class="form-control" id="isGracePeriodExpire" name="isGracePeriodExpire">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="invoiceId">Invoice ID:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="invoiceId" name="invoiceId"
                                       readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="issueDate">Issue Date:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="issueDate" name="issueDate"
                                       readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="sendDate">Send Date:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="sendDate" name="sendDate"
                                       readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="dueDate">Due Date:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="dueDate" name="dueDate"
                                       readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="subscriptionExpirationDate">Subscription Expiration  Date:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="subscriptionExpirationDate" name="subscriptionExpirationDate"
                                       readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <label class="control-label col-sm-4" for="newSubscriptionExpirationDate">New Subscription Expiration  Date:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="newSubscriptionExpirationDate" name="newSubscriptionExpirationDate"
                                       >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="amountDue">Total Amount:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="amountDue" name="amountDue"
                                       readonly="readonly">
                            </div>
                        </div>
                        <div id='invoice_status_div' class="form-group">
                            <label class="control-label col-sm-4" for="invoice_status">Invoice Status:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control " id="invoiceStatus" name="invoiceStatus"
                                       readonly="readonly">
                            </div>
                        </div>
                        <div id='school_status_div' class="form-group">
                            <label class="control-label col-sm-4" for="school_status">School Status:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="schoolStatus" name="schoolStatus"
                                       readonly="readonly">
                            </div>
                        </div>

                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger paid" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-alert'></span> Paid
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to cancel a invoice -->
    <div id="cancelModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Are you sure you want to cancel this invoice?</h3>
                    <br/>
                    <!-- form start -->
                    <form class="form-horizontal" role="form" id="form_cancel" method="post"
                          action="{{ route('admin.invoices.cancel') }}">
                        {{ csrf_field() }}
                        <input type="hidden" class="form-control" id="id_cancel" name="id_cancel">
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger cancel" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-alert'></span> Cancel
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function () {

            $('#tab_invoices').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search invoices......"
                },
                'ordering': true,
                'info': true,
                'columnDefs': [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 6
                    }],
                'autoWidth': false,
                'columns': [{
                    "width": "10%"
                }, {
                    "width": "20%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "20%"
                }, {
                     "width": "20%"
                    }
                ]
            });

            $(".dataTables_filter").hide();
        });
    </script>

    <script type="text/javascript">

        // send a invoice
        $(document).on('click', '.send-modal', function () {
            $('.modal-title').text('Send');
            $('#id_send').val($(this).data('id'));
            $('#sendModal').modal('show');
        });
        $('.modal-footer').on('click', '.send', function (evt) {
            evt.preventDefault();
            $('#form_send').submit();
        });

        $(document).on('click', '.download-xero-invoice', function (evt) {
            evt.preventDefault();
            $('#form_download_xero #id_download').val($(this).data('id'));
            $('#form_download_xero').submit();
        });

        // paid a invoice
        $(document).on('click', '.paid-modal', function () {
            $('.modal-title').text('Paid');

            console.log('isOverDue:'+$(this).data('isOverDue'.toLowerCase()));
            console.log('isGracePeriodExpire:'+$(this).data('isGracePeriodExpire'.toLowerCase()));

            $('#id_paid').val($(this).data('id'));
            $('#invoiceId').val($(this).data('invoiceId'.toLowerCase()));
            $('#issueDate').val($(this).data('issueDate'.toLowerCase()));
            $('#dueDate').val($(this).data('dueDate'.toLowerCase()));
            $('#sendDate').val($(this).data('sendDate'.toLowerCase()));
            $('#subscriptionExpirationDate').val($(this).data('subscriptionExpirationDate'.toLowerCase()));
            $('#newSubscriptionExpirationDate').val($(this).data('newSubscriptionExpirationDate'.toLowerCase()));
            $('#amountDue').val($(this).data('amountDue'.toLowerCase()));
            $('#isOverDue').val($(this).data('isOverDue'.toLowerCase()));
            $('#isGracePeriodExpire').val($(this).data('isGracePeriodExpire'.toLowerCase()));
            $('#schoolStatus').val($(this).data('schoolStatus'.toLowerCase()));
            $('#invoiceStatus').val($(this).data('invoiceStatus'.toLowerCase()));

            if($(this).data('isOverDue'.toLowerCase()) == '1') {
                let overDueDays = $(this).data('overDueDays'.toLowerCase());
                let day_str = 'day';
                if (parseInt(overDueDays) > 1) {
                    day_str = 'days';
                }
                $('#invoiceStatus').addClass('text-red text-bold');
                $('#invoiceStatus').val('Overdue by ' + overDueDays + ' '+ day_str);
            } else {
                $('#invoiceStatus').removeClass('text-red text-bold');
            }

            if($(this).data('isGracePeriodExpire'.toLowerCase()) == '1') {
                let maxSubscriptionExtensionDate = $(this).data('newSubscriptionExpirationDate'.toLowerCase());
                console.log('maxSubscriptionExtensionDate:'+maxSubscriptionExtensionDate);
                $('#newSubscriptionExpirationDate').show();
                //Date picker
                $('#newSubscriptionExpirationDate').datepicker({
                    autoclose: true,
                    startDate: '0d',
                    enableOnReadonly: false,
                    endDate: maxSubscriptionExtensionDate,
                    format: 'dd/mm/yyyy',
                });
            } else {
                $('#newSubscriptionExpirationDate').hide();
            }

            $('#paidModal').modal('show');
        });
        $('.modal-footer').on('click', '.paid', function (evt) {
            evt.preventDefault();
            $('#form_paid').submit();
        });

        // cancel a invoice
        $(document).on('click', '.cancel-modal', function () {
            $('.modal-title').text('Cancel');
            $('#id_cancel').val($(this).data('id'));
            $('#cancelModal').modal('show');
        });
        $('.modal-footer').on('click', '.cancel', function (evt) {
            evt.preventDefault();
            $('#form_cancel').submit();
        });

        // Automatically trigger the loading animation on click
        Ladda.bind('button[type=submit]');
    </script>
@endpush
