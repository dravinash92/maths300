@extends('layouts.email')

@section('content')
    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        Dear {{ $recipient }},
        <br><br>
        This is a friendly reminder that payment for the Maths 300 subscription invoice #{{ $invoice->invoice_id }}, as sent on {{ $invoice->sendDateFormat() }}, is due next week <strong>{{ $invoice->dueDateFormat() }} </strong>.
        <br><br>
        We would appreciate it if you could arrange for payment to be made. Many thanks for your cooperation.
        <br><br>
    </div>

    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        <table class="buttonwrapper" bgcolor="#ea910b" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center; font-size: 16px; font-family: sans-serif; font-weight: bold; padding: 0px 30px 0px 30px; " height="45">
                    <a href="{{ $invoice->pdf_link }}" style ="color: #ffffff; text-decoration: none;">View invoice</a><br/>
                </td>
            </tr>
        </table>
        <p>If you’re having trouble clicking the "View invoice" button, copy and paste the URL below into your web browser: {{ $invoice->pdf_link }}</p>
    </div>
@stop
