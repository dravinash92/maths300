@extends('adminlte::master')

@section('title', 'Maths300 | Invoice')

@section('adminlte_css')
    <!-- Invoice -->
    <link rel="stylesheet" href="{{ asset('css/invoice.css') }}">
    <style>
        body {
            font-family: "Open Sans", Verdana, Tahoma, serif;
            @if(!empty($is_pdf_generation))
                   background-color: #FFFFFF;
            @endif
        }

        @if(!empty($is_pdf_generation))
        .invoice-container {
            border: 0px solid #fff;
        }
        @endif
    </style>
@stop

@section('body')
    <body style="font-family: 'Open Sans',Verdana,Tahoma,serif;">
    <div class="container-fluid invoice-container">
        <div class="row invoice-header">
            <div class="invoice-head">
                <img src="{{ asset('images/logo-aamt.png') }}"
                     style="margin: 0px; width: 143px; height: auto"
                     alt="{{ 'AAMT_'.config('app.name') }}" title="{{ $invoicefrom['name'] }}">
            </div>

            <div class="invoice-head info">
                <strong>{{ $invoicefrom['name'] }}</strong> <br/>
                ABN: {{ $invoicefrom['abn'] }}<br/>
                {{ $invoicefrom['address'] }}<br/>
                Phone: {{ $invoicefrom['phone'] }}<br/>
                Email: {{ $invoicefrom['email'] }}<br/>
            </div>

            <div class="invoice-head status">
                <div class="invoice-status">
                    <span class="{{ $invoice->getStatus() }}">{{ $invoice->getStatus() }}</span>
                </div>
                @if($invoice->isOverDue())
                    <span class="{{ $invoice->getStatus() }}">{{ $invoice->overDueDiff() }} due date</span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="invoice-col invoice-no">
                <h3>{{ $invoice->tax_apply? 'Tax':'' }} Invoice #{{ $invoice->invoice_id }}</h3>
            </div>
        </div>

        <div class="row">
            <div class="invoice-col">
                <strong>Invoiced To</strong>
                <address class="small-text">
                    {{ $invoice->to->name }} <br/>
                    Attention to: {{ $invoice->to->financeOfficer->name }} <br/>
                    {{ $invoice->to->billing_address_region }} <br/>
                    {{ $invoice->to->billing_address_suburb }} {{ $invoice->to->billingAddressLocation->country->isAustralia()? $invoice->to->billingAddressLocation->name : '' }} {{ $invoice->to->billing_address_postcode }}
                    <br/>
                    {{ $invoice->to->billingAddressLocation->country->name }}
                    <br/>
                </address>
            </div>

            <div class="invoice-col">
                <strong>Issue Date</strong><br>
                <span class="small-text">
                    {{ $invoice->issueDateFormat() }}
                </span>
            </div>

            <div class="invoice-col">
                <strong>Payment Due</strong><br>
                <span class="small-text">
                    {{ $invoice->dueDateFormat() }}
                </span>
            </div>
        </div>

        <div class="row">
            <div class="invoice-col">
                <strong>Subscription Expiry Date</strong><br>
                <span class="small-text">
                    {{ $invoice->subscriptionExpirationDateFormat() }}
                </span>
            </div>

            @if($invoice->to->purchase_order_no)
                <div class="invoice-col">
                    <strong>Purchase Order Number</strong><br>
                    <span class="small-text">
                    {{ $invoice->to->purchase_order_no }}
                    </span>
                </div>
            @endif
        </div>

        <div class="invoice-items panel panel-default invoice-panel">
            <div class="panel-heading">
                <h3 class="panel-title invoice-highlight">Invoice Items</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <td class="invoice-highlight">Description</td>
                            <td width="20%" class="text-center"><strong>Amount</strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoice->orderItems as $item)
                            <tr>
                                <td>{{ $item->name }} 
                                    @if($item->description) 
                                      -  {{ $item->description }}
                                    @endif
                                    </td>
                                <td class="text-center">${{ $item->totalAmountFormat() }} AUD</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td class="total-row text-right"><strong>Subtotal</strong></td>
                            <td class="total-row text-center">${{ $invoice->totalAmountFormat() }} AUD</td>
                        </tr>
                        @if($invoice->discount_apply)
                            <tr>
                                <td class="total-row text-right"><strong>Discount</strong></td>
                                <td class="total-row text-center">${{ $invoice->discountAmountFormat() }} AUD *</td>
                            </tr>
                        @endif
                        @if($invoice->tax_apply)
                            <tr>
                                <td class="total-row text-right"><strong>Includes GST 10%</strong></td>
                                <td class="total-row text-center">${{ $invoice->taxFormat() }} AUD</td>
                            </tr>
                        @endif
                        <tr>
                            <td class="total-row text-right"><strong>Total</strong></td>
                            <td class="total-row text-center">${{ $invoice->amountDueFormat() }} AUD</td>
                        </tr>

                        <tr>
                            <td class="total-row text-right"><strong>Paid</strong></td>
                            <td class="total-row text-center">
                                ${{ $invoice->isPaid()? $invoice->amountDueFormat() : '0.00' }} AUD
                            </td>
                        </tr>

                        <tr>
                            <td class="total-row text-right"></td>
                            <td class="total-row text-center"></td>
                        </tr>

                        <tr>
                            <td class="total-row text-right"><strong>Balance Due</strong></td>
                            <td class="total-row text-center">
                                ${{ $invoice->isPaid()? '0.00' : $invoice->amountDueFormat() }} AUD
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @if($invoice->discount_apply)
            @if(empty($is_pdf_generation))
                <div class="row">
                    <p>*Discount ({{ $invoice->discount->name }}) applied.</p>
                </div>
            @endif
        @endif
        <div class="row">
            <strong>Please do not forget to mention the Tax Invoice # in Description/Reference while paying.</strong>
        </div>
        <br>
        
        <div class="row">
            <div class="invoice-payment-advice">
                <strong>Payment Advice</strong>
                <br>
                <span class="small-text">
                        Account Name: {{ $invoicefrom['accountName'] }}
                        <br>
                        BSB: {{ $invoicefrom['bsb'] }}
                        <br>
                        Account Number: {{ $invoicefrom['accountNumber'] }}
                        <br>
                        Reference: {{ $invoice->invoice_id }}
                        <!-- <br>
                        Please include a copy of this invoice if you pay by cheque. -->
                </span>
                <br>
                <a href="https://www.ipg.stgeorge.com.au/StgWeb/servlet/webpay.website.Simple?token1=x7jQgDq0ugSGvGWbnG2JW2igT&token2=D4nt1JQttxd9WT1TFwv1Y5j3b"
                   class="btn btn-success hidden-print" target="_blank"><i
                        class="fa fa-credit-card"></i> Click to Pay by Credit Card</a>
            </div>
        </div>

        {{--not in pdf generation --}}
        @if(empty($is_pdf_generation))
            <div class="row">
                <div class="pull-right btn-group btn-group-sm hidden-print">
                    <a href="javascript:window.print()" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                    <a href='{{ route('admin.invoices.download', $invoice->id) }}' class="btn btn-default"><i
                            class="fa fa-download"></i> Download</a>
                    @if($invoice->isSendReminderAllowed())
                        <a href='{{ route('admin.invoices.sendReminder', $invoice->id) }}' class="btn btn-default"><i
                                class="fa fa-mail-forward"></i> Send Reminder</a>
                    @endif
                </div>
            </div>

            @if($invoice->isSendReminderAllowed())
                <div class="row">
                    <div class="invoice-col right">
                    <span class="small-text">
                        Last reminder date: {{ $invoice->lastReminderDateFormat() }}<br>
                    </span>
                    </div>
                </div>
            @endif
        @endif
    </div>

    {{--not in pdf generation --}}
    @if(empty($is_pdf_generation))
        <p class="text-center hidden-print">
            <a href="{{ route('admin.invoices.index',array("q_to"=> $invoice->to->id)) }}">« Back</a>
        </p>
    @endif

    </body>
@stop
