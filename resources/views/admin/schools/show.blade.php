@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        /* Invoice Status Colors */

        .awaiting-payment {
            color: #ffcc00;
        }

        .paid {
            color: #00a65a;
        }

        .overdue {
            color: #cc0000;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.schools.index') }}">Schools</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">School Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">School</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Finance Information</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Coordinator Information</a>
                    </li>
                    <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Invoice</a></li>
                    <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">All Users</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <h4>Name</h4>
                        {{ $school->name }}
                        <hr>

                        <h4>Address</h4>
                        {{ $school->address_region }} <br>
                        {{ $school->address_suburb }} {{ $school->addressLocation->name }} {{ $school->address_postcode }}
                        <hr>

                        <h4>School Type</h4>
                        {{ $school->schoolType->name }}
                        <hr>

                        <h4>School Sector</h4>
                        {{ $school->schoolSector->name }}
                        <hr>

                        <h4>School Size</h4>
                        {{ $school->schoolSizeStudent->name }}
                        <br>
                        {{ $school->schoolSizeTeacher->name }}
                        <hr>

                        <h4>Number of campuses</h4>
                        {{ $school->number_of_campuses }}
                        <hr>

                        <h4>Notes</h4>
                        {{ $school->notes ? $school->notes: "N/A" }}
                        <hr>

                        <h4>Expiry date</h4>
                        {{ $school->subscriptionExpirationDateFormat() }}
                        <hr>

                        <h4>Payment Status</h4>
                        <p class="{{ $school->getPaymentStatus() }}">
                            {{ $school->getPaymentStatus() }}
                        </p>
                        <hr>

                        <h4>Status</h4>
                        {{ $school->getStatus() }}
                        <hr>

                        <h4>Updated at</h4>
                        {{ $school->updated_at }}
                        <hr>
                        
                        <h4>Lesson Packs</h4>
                        @if(!empty($school->tags))
                            @foreach($tags as $t)
                                @if(in_array($t->id, explode(',',$school->tags)))
                                    {{ $t->name }},
                                @endif
                            @endforeach
                        @else
                                Default - All Lessons
                        @endif
                        <hr>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <h4>Finance Officer Name</h4>
                        {{ $school->financeOfficer? $school->financeOfficer->name : 'N/A' }}
                        <hr>

                        <h4>Finance Officer Email</h4>
                        {!! $school->getFinanceOfficerEmailHtml() !!}
                        <hr>

                        <h4>Finance Officer Phone</h4>
                        {{ $school->financeOfficer? $school->financeOfficer->phone : 'N/A' }}
                        <hr>

                        <h4>Finance Officer Position</h4>
                        {{ $school->financeOfficer? $school->financeOfficer->position : 'N/A' }}
                        <hr>

                        <h4>Invoice Email</h4>
                        <a href="mailto:{{ $school->finance_invoice_email }}"
                           target="_blank">{{ $school->finance_invoice_email }}</a>
                        <hr>

                        <h4>Billing Address</h4>
                        {{ $school->billing_address_region }} <br>
                        {{ $school->billing_address_suburb }}
                        {{ $school->billingAddressLocation->name }} {{ $school->billing_address_postcode }}
                        <hr>

                        <h4>Subscription Plan</h4>
                        {{ $school->subscriptionPlan? $school->subscriptionPlan->getDesc() : 'N/A' }}
                        <hr>

                        <h4>Last Renewal Date</h4>
                        {{ $school->lastRenewalDateFormat() }}
                        <hr>

                        <h4>Last Paid Date</h4>
                        {{ $school->lastPaidDateFormat() }}
                        <hr>

                        <h4>Subscription Expiration Date</h4>
                        {{ $school->subscriptionExpirationDateFormat() }}
                        <hr>

                        <h4>Purchase Order Number </h4>
                        {{ $school->purchase_order_no? $school->purchase_order_no: 'N/A' }}
                        <hr>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">

                        <h4>Coordinator Name</h4>
                        {{ $school->coordinator? $school->coordinator->name : 'N/A' }}
                        <hr>

                        <h4>Coordinator Email</h4>
                        {!! $school->getCoordinatorEmailHtml() !!}
                        <hr>

                        <h4>Coordinator Phone</h4>
                        {{ $school->coordinator? $school->coordinator->phone : 'N/A' }}
                        <hr>

                        <h4>Coordinator Position</h4>
                        {{ $school->coordinator? $school->coordinator->position : 'N/A' }}
                        <hr>

                        <h4>Year Levels</h4>
                        @if($school->coordinator && count($school->coordinator->yearLevels()->orderBy('year_levels.id')->pluck('name')->toArray()))
                            {{ implode(',', $school->coordinator->yearLevels()->orderBy('year_levels.id')->pluck('name')->toArray()) }}
                        @else
                            N/A
                        @endif
                        <hr>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_4">
{{--                        <div class="col-md-12 text-right">--}}
{{--                            <a class="archive btn btn-primary btn-flat"--}}
{{--                               href="{{ route('admin.invoices.showArchived',array("q_to"=> $school->id)) }}">Archived</a>--}}
{{--                        </div>--}}

                        <table id="tab_invoices" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>To</th>
                                <th>Date</th>
                                <th>Due Date</th>
                                <th>Due</th>
                                <th>Status</th>
                                <th>Menu</th>
                            </tr>
                            {{ csrf_field() }}
                            </thead>
                            <tbody>
                            @foreach($school->invoices as $l)
                                <tr class="item{{ $l->id }}">
                                    <td><a href='{{ route('admin.invoices.show',$l->id) }}'>{{ $l->invoice_id }}</a>
                                    </td>
                                    <td>{{ $l->to->name }}</td>
                                    <td>{{ $l->issueDateFormat() }}</td>
                                    <td>{{ $l->dueDateFormat() }}</td>
                                    <td>{{ $l->amount_due }}</td>
                                    <td>{{ $l->getStatus() }}</td>
                                    <td>
                                        <a class="btn btn-primary"
                                           href='{{ route('admin.invoices.show',$l->id) }}'>View</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="tab_5">
                        <table id="tab_users" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Last login</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($school->users as $l)
                                <tr class="item{{ $l->id }}">
                                    <td class="col1">{{ $l->lastLoginFormat() }}</td>
                                    <td><a href='{{ route('admin.users.show',$l->id) }}' target="_blank">{{ $l->name }}</a></td>
                                    <td><a href="mailto:{{ $l->email }}" target="_blank">{{ $l->email }}</a></td>
                                    <td><a href="tel:{{ $l->phone }}" target="_blank">{{ $l->phone }}</a></td>
                                    <td>{{ $l->getStatus() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.schools.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {

            $('#tab_invoices').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search invoices......"
                },
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'columns': [{
                    "width": "20%"
                }, {
                    "width": "20%"
                }, {
                    "width": "20%"
                }, {
                    "width": "20%"
                }, {
                    "width": "20%"
                }]
            });

            $('#tab_users').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search users......"
                },
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'columns': [{
                    "width": "20%"
                }, {
                    "width": "20%"
                }, {
                    "width": "20%"
                }, {
                    "width": "20%"
                }, {
                    "width": "20%"
                }]
            });

            $(".dataTables_filter").hide();
        });
    </script>
@endpush
