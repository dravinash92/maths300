@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.forums.index') }}">Forums</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Forum Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <h4>Title</h4>
            {{ $forum->title }}
            <hr>

            <h4>Description</h4>
            {{ $forum->description? $forum->description : "N/A" }}
            <hr>

            <h4>Sticky</h4>
            {{ $forum->sticky ? 'Yes':'No' }}
            <hr>

            <h4>Visibility</h4>
            {{ $forum->visibility ? 'Yes':'No' }}
            <hr>

            <h4>Topics</h4>
            {{ $forum->getTopicsCount() }}
            <hr>

            <h4>Replies</h4>
            {{ $forum->getRepliesCount() }}
            <hr>

            <h4>Last reply</h4>
            {{ $forum->lastReply() ? $forum->lastReply()->updated_at : 'N/A' }}
            <hr>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.forums.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
@endpush