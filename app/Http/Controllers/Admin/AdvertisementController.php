<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use Illuminate\Http\Request;
use App\Http\Requests\AdvertisementRequest;
use Illuminate\Support\Facades\Log;
use Auth;

class AdvertisementController extends Controller
{
    const MAX_ADS = 4;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Advertisement::query();

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['advertisements'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.advertisements.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // query published ads count
        if (Advertisement::status(1)->count() >= $this::MAX_ADS) {
            $request->session()->flash('alert-error', 'Only '.$this::MAX_ADS.' published advertisements(Maximum) allowed.');
            return redirect()->route('admin.advertisements.index');
        }
        return view('admin.advertisements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdvertisementRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertisementRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $advertisement = new Advertisement();
        $advertisement->title = $validated['title'];
        $advertisement->content = $validated['content'];
        $advertisement->save();

        $request->session()->flash('alert-success', 'Advertisement added successfully.');

        return redirect()->route('admin.advertisements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Advertisement $advertisement
     * @return \Illuminate\Http\Response
     */
    public function show(Advertisement $advertisement)
    {
        $arr['advertisement'] = $advertisement;

        return view('admin.advertisements.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Advertisement $advertisement
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {
        $arr['advertisement'] = $advertisement;

        return view('admin.advertisements.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdvertisementRequest $request
     * @param  \App\Models\Advertisement $advertisement
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertisementRequest $request, Advertisement $advertisement)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $advertisement->title = $validated['title'];
        $advertisement->content = $validated['content'];
        $advertisement->save();

        $request->session()->flash('alert-success', 'Advertisement updated successfully.');

        return redirect()->route('admin.advertisements.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertisement = Advertisement::findOrFail($id);
        Log::info("delete advertisement " . $id);

        /**
         * Note: all advertisements related will be deleted cascade
         */
        $advertisement->delete();

        return response()->json($advertisement);
    }

    /**
     * Change status to published.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request)
    {
        // query published ads count
        if (Advertisement::status(1)->count() >= $this::MAX_ADS) {
            $request->session()->flash('alert-error', 'Only '.$this::MAX_ADS.' published advertisements(Maximum) allowed.');
            return redirect()->route('admin.advertisements.index');
        }

        $id = $request->id_publish;

        $advertisement = Advertisement::findOrFail($id);
        Log::info("publish advertisement " . $id);

        $advertisement->status = 1;
        $advertisement->save();

        $request->session()->flash('alert-success', 'Advertisement published successfully.');

        return redirect()->route('admin.advertisements.index');
    }

    /**
     * Change status to suspended.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function suspend(Request $request)
    {
        $id = $request->id_suspend;

        $advertisement = Advertisement::findOrFail($id);
        Log::info("suspend advertisement " . $id);

        $advertisement->status = 2;
        $advertisement->save();

        $request->session()->flash('alert-success', 'Advertisement suspended successfully.');

        return redirect()->route('admin.advertisements.index');
    }
}
