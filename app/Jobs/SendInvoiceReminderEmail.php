<?php

namespace App\Jobs;

use App\Mail\InvoiceOverdueReminder;
use App\Mail\BillDueDateReminder;
use App\Mail\SubscriptionRenewReminder;
use App\Mail\SubscriptionRenewReminderTeacher;
use App\Models\Invoice;
use App\Models\School;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Webfox\Xero\OauthCredentialManager;
use App\Http\Controllers\Admin\SchoolController;

class SendInvoiceReminderEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * The number of seconds the job can run before timing out.
     *
     * 6 hours
     *
     * @var int
     */
    public $timeout = 21600;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OauthCredentialManager $xeroCredentials)
    {
        $start = microtime(true);
        Log::info('SendInvoiceReminderEmail job started......');
        $eight_weeks_after_today = Carbon::today()->addWeeks(6); // 6 weeks before expiration
        // echo "<pre>";print_r($eight_weeks_after_today);exit;
        Log::info('Check school expiry date......');
        Log::info("Today: " . Carbon::today() . ", eight weeks after today: " . $eight_weeks_after_today);
        $schools_about_to_expire = School::where('subscription_expiration_date', $eight_weeks_after_today)->get();
        //echo "<pre>";print_r(count($schools_about_to_expire));exit;
        /*
         * Rule for reminder
         * reminder1: 8 weeks before renewal date (including generate invoice)
        */
        if (count($schools_about_to_expire)) 
        {
            Log::info("Found " . count($schools_about_to_expire) . " schools subscription about to expire.");
            foreach ($schools_about_to_expire as $school) 
            {
                $expiration_date = $school->subscription_expiration_date;
                Log::info("Reminder (6 weeks before expiration date), subscription expiry date: " . $expiration_date . ", school: " . $school->name);

                // create a new invoice
                $issue_date = today();
                // create new invoice for the next billin
                $new_invoice = Invoice::newInvoice($school, $issue_date, 0, null, null);
               // echo "<pre>";print_r($new_invoice->to->finance_invoice_email);exit;
                if (!empty($new_invoice)) 
                {
                    $new_invoice->status = 1;
                    $new_invoice->send_date = today();
                    

                    //Invoice::genPDF($new_invoice);

                    Log::info('Send reminder email for subscription renewal with a new invoice#' . $new_invoice->invoice_id);

                    // Send to Invoice Email and CC to School finance officer, School coordinator.
                    if($new_invoice->to->financeOfficer === $new_invoice->to->coordinator)
                    {
                        Mail::to($new_invoice->to->finance_invoice_email)->cc($new_invoice->to->financeOfficer)->send(new SubscriptionRenewReminder($new_invoice, $new_invoice->to->financeOfficer->name, $expiration_date));
                    }
                    else
                    {
                        Mail::to($new_invoice->to->finance_invoice_email)->cc([$new_invoice->to->financeOfficer,$new_invoice->to->coordinator])->send(new SubscriptionRenewReminder($new_invoice, $new_invoice->to->financeOfficer->name, $expiration_date));
                    }
                    
                    //Funtion call to Create Xero Invoice 
                   $xeroInvoiceId = SendInvoiceReminderEmail::CreateXeroRenewInvoice($new_invoice, $xeroCredentials);
                   if($xeroInvoiceId){
                        $new_invoice->xero_invoice_id = $xeroInvoiceId;
                    }
                   $new_invoice->save();

                }
                else
                {
                    Log::error("new invoice error for school " . $school->name);
                }
            }
        } 
        else
        {
            Log::info("No schools subscription about to expire.");
        }
        
        Log::info('Check invoice status......');
        $invoices = Invoice::whereIn('status', [1, 3])->get();

        //echo "<pre>";print_r($invoices);exit;
        $total_invoice_count = 0;
        $awaiting_payment_invoice_count = 0;
        $overdue_invoice_count = 0;
        foreach ($invoices as $invoice) 
        {
            //echo "<pre>";print_r($invoice->to_id);exit;
             User::sendMailToTeachers($invoice->to_id);
            // status = "Awaiting payment";
            if ($invoice->status == 1)
            {
                /*
                 * Rule for reminder
                 * Invoice sent but not get paid, on due date, send reminder email
                 */
                if (Carbon::parse($invoice->due_date)->copy()->isToday())
                {
                    Log::info('send reminder email for unpaid invoice, due today invoice id: ' . $invoice->invoice_id . ', due date: ' . $invoice->due_date);
                    // Send to Invoice Email and CC to School finance officer, School coordinator.
                    Mail::to($invoice->to->finance_invoice_email)->cc([$invoice->to->financeOfficer,$invoice->to->coordinator])->send(new BillDueDateReminder($invoice, $invoice->to->financeOfficer->name));
                    
                    $invoice->last_reminder_date = today();
                    $invoice->save();
                }
                ++$awaiting_payment_invoice_count;
            }
            // status = "Overdue";
            if ($invoice->status == 3)
            {
                /*
                 * Rule for reminder
                 * Invoice overdue by two weeks, send reminder email
                 */
                if (Carbon::parse($invoice->due_date)->copy()->addWeek(2)->isToday())
                {
                    $overdue_weeks = Carbon::parse($invoice->due_date)->copy()->diffInWeeks(today());
                    Log::info('send reminder email for overdue invoice, invoice id: ' . $invoice->invoice_id . ', due date: ' . $invoice->due_date . ', today: ' . today() . ', overdue by ' . $overdue_weeks . ' weeks.');
                    Log::info('next reminder date: ' . $invoice->next_reminder_date);
                    SendInvoiceReminderEmail::overDueReminder($invoice, $overdue_weeks);

                    // set next reminder time: 1 week after
                    $next_reminder_date = today()->copy()->addWeek(1);
                    Log::info('update next_reminder_date to ' . $next_reminder_date);
                    $invoice->next_reminder_date = $next_reminder_date;
                    $invoice->save();
                }

                /*
                 * Rule for reminder
                 * Invoice due date + 3 weeks (3rd reminder will be send after 22 days after invoice due date)
                 * Customer will have 1 final week to pay for the account before account is suspended.
                 */
                if ($invoice->next_reminder_date && Carbon::parse($invoice->next_reminder_date)->copy()->addWeek(1)->isToday())
                {
                    $overdue_weeks = Carbon::parse($invoice->due_date)->copy()->diffInWeeks(today());
                    SendInvoiceReminderEmail::overDueReminder($invoice, $overdue_weeks);
                }

                if (Carbon::parse($invoice->due_date)->copy()->isToday())
                {
                    /* Send mails to teachers */
                    User::sendMailToTeachers($invoice->to_id);
                }

                ++$overdue_invoice_count;
            }
            ++$total_invoice_count;
        }

        $end = microtime(true);
        Log::info('######################################################################################');
        Log::info('SendInvoiceReminderEmail job completed, total invoices count: ' . $total_invoice_count . ', awaiting_payment_invoice_count: ' . $awaiting_payment_invoice_count . ', overdue_invoice_count: ' . $overdue_invoice_count . ', elapsed time: ' . ($end - $start) . ' seconds');
        Log::info('######################################################################################');
    }

    public static function overDueReminder($invoice, $overdue_weeks)
    {
        Log::info('Send reminder email for invoice overdue by ' . $overdue_weeks . ' weeks invoice#' . $invoice->invoice_id);

        // Send to Invoice Email and CC to School finance officer, School coordinator.
        
        if($invoice->to->financeOfficer === $invoice->to->coordinator)
        {
            Mail::to($invoice->to->finance_invoice_email)->cc($invoice->to->financeOfficer)->send(new InvoiceOverdueReminder($invoice, $invoice->to->financeOfficer->name, $overdue_weeks));
        }
        else
        {
            Mail::to($invoice->to->finance_invoice_email)->cc([$invoice->to->financeOfficer,$invoice->to->coordinator])->send(new InvoiceOverdueReminder($invoice, $invoice->to->financeOfficer->name, $overdue_weeks));
        }

        $invoice->last_reminder_date = today();
        $invoice->save();
    }

    public function CreateXeroRenewInvoice($invoice_copy, OauthCredentialManager $xeroCredentials)
    {
        $invoice = $invoice_copy;
        $invoice_copy = $invoice_copy->toArray();

        Log::info("Create Xero Renewal Invoice Started -");
        if ($xeroCredentials->exists())
        {
            Log::info("xeroCredentials exists -  ");

            $xero = resolve(\XeroAPI\XeroPHP\Api\AccountingApi::class);
           
            $xeroTenantId = $xeroCredentials->getTenantId();

            $lineitems = [];        
    
            Log::info("Xero Tenant Id -  ".  $xeroTenantId);
            try
            {
                $order_item = $invoice->orderItems ? $invoice->orderItems->toArray()[0] : '';
            
                $lineitem =  resolve(\XeroAPI\XeroPHP\Models\Accounting\LineItem::class);
                
                $lineitem->setDescription($order_item['name'])
                ->setUnitAmount($invoice_copy['amount_due'])
                //for Demo company
                //->setAccountCode("200")
                // for AAMT company
                ->setAccountCode("40461")
                ->setTaxType("OUTPUT")
                ->setLineAmount($invoice_copy['amount_due']);

               /* if($invoice_copy['tax_apply']){
                    $lineitem->setTaxAmount($invoice_copy['tax']);
                }*/

                array_push($lineitems, $lineitem);
            }
            catch(\throwable $e) 
            {
                $error = $e->getMessage();
                Log::info("Error creating Line Item in xero Invoice - " . $error);
            }
            try
            {
                $contact = resolve(\XeroAPI\XeroPHP\Models\Accounting\Contact::class);

                $where = 'Name=="'.$invoice_copy['to']['name'].'"';

                $contact_result = $xero->getContacts($xeroTenantId, null, $where)->getContacts();

                $count = count($contact_result);

                if($count > 0)
                {
                    $contactId = $contact_result[0]->getContactId();
                }
                else
                {
                    $arr_contacts = [];
                    $contact_1 = resolve(\XeroAPI\XeroPHP\Models\Accounting\Contact::class);

                    $arr_addresses = [];

                    $address  =  resolve(\XeroAPI\XeroPHP\Models\Accounting\Address::class);

                    $address->setAddressType('POBOX')
                    ->setAddressLine1($invoice_copy['to']['billing_address_region'])
                    ->setAddressLine2($invoice_copy['to']['billing_address_suburb'])
                    ->setAddressLine3($invoice_copy['to']['billing_address_postcode']);
                    array_push($arr_addresses, $address);

                    $contact_1->setName($invoice_copy['to']['name'])
                        ->setEmailAddress($invoice_copy['to']['finance_invoice_email'])
                        ->setAddresses($arr_addresses);
                    array_push($arr_contacts, $contact_1);

                    $contacts = resolve(\XeroAPI\XeroPHP\Models\Accounting\Contacts::class);
                    $contacts->setContacts($arr_contacts);

                    $result_contact = $xero->createContacts($xeroTenantId,$contacts);
                    $contactId = $result_contact->getContacts()[0]->getContactId();
                }
                    $contact->setContactId($contactId);  
            }
            catch(\throwable $e) 
            {
                // This can happen if the credentials have been revoked or there is an error with the organisation (e.g. it's expired)
                $error = $e->getMessage();
                Log::info("Error getting Contact in xero Invoice creation -  " . $error);
            }

            $arr_invoices = []; 

            $invoice_1 = resolve(\XeroAPI\XeroPHP\Models\Accounting\Invoice::class);

            try
            {
                $invoice_1->setReference('Ref-Maths300')
                ->setDueDate(new \DateTime($invoice_copy['due_date']))
                ->setContact($contact)
                ->setLineItems($lineitems)
                ->setStatus(\XeroAPI\XeroPHP\Models\Accounting\Invoice::STATUS_AUTHORISED)
                ->setType(\XeroAPI\XeroPHP\Models\Accounting\Invoice::TYPE_ACCREC)
                ->setLineAmountTypes(\XeroAPI\XeroPHP\Models\Accounting\LineAmountTypes::INCLUSIVE)
                ->setInvoiceNumber($invoice_copy['invoice_id']);

                if(isset($invoice_copy['discount_apply']))
                {  
                    $invoice_1->setTotalDiscount($invoice_copy['discount_amount']); 
                }

                if(isset($invoice_copy['tax_apply']))
                {
                    $invoice_1->setTotalTax($invoice_copy['tax']);
                }  
                    
                array_push($arr_invoices, $invoice_1);

                $invoices = resolve(\XeroAPI\XeroPHP\Models\Accounting\Invoices::class);
                $invoices->setInvoices($arr_invoices);

                $result = $xero->createInvoices($xeroTenantId,$invoices); 
                Log::info("Xero Invoice Created -  " );
                //[/Invoices:Create]
                $str = '';
                $str = $str ."Create Invoice 1 total amount: " . $result->getInvoices()[0]->getTotal() . "<br>" ;
                $invoiceId = $result->getInvoices()[0]->getInvoiceId();
                
               Log::info("Create Xero Renewal Invoice End -");
               return $invoiceId;
            }
            catch(\throwable $e)
            {
                // This can happen if the credentials have been revoked or there is an error with the organisation (e.g. it's expired)
                $error = $e->getMessage();

                Log::info("Error caught in xero Renewal Invoice creation -  " . $error);
            }
        }
        Log::info("Create Xero Invoice End -");
    }
}
