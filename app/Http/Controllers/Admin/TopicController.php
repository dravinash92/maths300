<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Forum;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Requests\TopicRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Auth;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['forums'] = Forum::data4Select();
        $query = Topic::query();

        if ($request->has('q_forum') && $request->q_forum != null) {
            $query->forum($request->q_forum);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['topics'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.topics.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['forums'] = Forum::data4Select();
        return view('admin.topics.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TopicRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TopicRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $topic = new Topic();
        $topic->title = $validated['title'];
        $topic->content = $validated['content'];
        $topic->sticky = $validated['sticky'];
        $topic->visibility = $validated['visibility'];

        DB::transaction(function () use ($topic, $validated) {
            $topic->save();

            // save relationships
            // forum
            $forum = Forum::find($validated['forum']);
            $topic->forum()->associate($forum)->save();

            // author: current user
            $author = Auth::user();
            $topic->author()->associate($author)->save();

            Log::info("create topic " . $topic->id);
        });

        $request->session()->flash('alert-success', 'Topic added successfully.');

        return redirect()->route('admin.topics.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        $arr['topic'] = $topic;

        return view('admin.topics.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        $arr['topic'] = $topic;
        $arr['forums'] = Forum::data4Select();

        return view('admin.topics.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TopicRequest  $request
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(TopicRequest $request, Topic $topic)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $topic->title = $validated['title'];
        $topic->content = $validated['content'];
        $topic->sticky = $validated['sticky'];
        $topic->visibility = $validated['visibility'];

        DB::transaction(function () use ($topic, $validated) {
            $topic->save();

            // save relationships
            // forum
            $forum = Forum::find($validated['forum']);
            $topic->forum()->associate($forum)->save();

            // author: current user
            $author = Auth::user();
            $topic->author()->associate($author)->save();

            Log::info("update topic " . $topic->id);
        });

        $request->session()->flash('alert-success', 'Topic updated successfully.');

        return redirect()->route('admin.topics.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::findOrFail($id);
        Log::info("delete topic " . $id);

        /**
         * Note: all replies related will be deleted cascade
         */
        $topic->delete();

        return response()->json($topic);
    }

    /**
     * Change status to opened.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function open(Request $request)
    {
        $id = $request->id_open;

        $topic = Topic::findOrFail($id);
        Log::info("open topic " . $id);

        $topic->status = 1;
        $topic->save();

        $request->session()->flash('alert-success', 'Topic opened successfully.');

        return redirect()->route('admin.topics.index');
    }

    /**
     * Change status to closed.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function close(Request $request)
    {
        $id = $request->id_close;

        $topic = Topic::findOrFail($id);
        Log::info("close topic " . $id);

        $topic->status = 0;
        $topic->save();

        $request->session()->flash('alert-success', 'Topic closed successfully.');

        return redirect()->route('admin.topics.index');
    }
}
