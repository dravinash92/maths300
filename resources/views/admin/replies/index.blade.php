@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.replies.index') }}">Replies</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Replies</h3>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('admin.replies.create') }}" class="btn btn-primary btn-flat">Add Reply</a>
            </div>

        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="replySearchForm" role="form" method="get" action="{{ route('admin.replies.index') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-5">
                        <select data-placeholder="Select a topic" class="form-control select2_topic"
                                style="width: 100%;" id="q_topic" name="q_topic">
                            <option></option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter keywords..." value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="postSearchBtn" type="submit" class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
            <table id="tab_replies" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Forum</th>
                    <th>Topic</th>
                    <th>Author</th>
                    <th>Updated</th>
                    <th>Menu</th>
                </tr>
                </thead>
                <tbody>
                @foreach($replies as $l)
                    <tr class="item{{ $l->id }}">
                        <td><a href='{{ route('admin.replies.show',$l->id) }}'>{{ $l->title }}</a></td>
                        <td>{{ $l->topic->forum->title }}</td>
                        <td>{{ $l->topic->title }}</td>
                        <td>{{ $l->author->name }}</td>
                        <td>{{ $l->getUpdatedAtDate() }}</td>
                        <td>
                            <a href="{{ route('admin.replies.edit',$l->id) }}" class="btn btn-primary">Edit</a>
                            <button class="delete-modal btn btn-danger" data-id="{{ $l->id }}"
                                    data-name="{{ $l->title }}">Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @modal(['action' => 'delete', 'model' => 'reply'])
    @endmodal
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {

            // Initialize Select2 elements
            let select2_topic = $('.select2_topic').select2({
                allowClear: true,
                data: {!! json_encode($topics)!!}
            });

            @if(old('q_topic'))
            select2_topic.val({!! old('q_topic') !!}).trigger('change');
            @endif


            $('#tab_replies').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search replies......"
                },
                'ordering': true,
                'info': true,
                'columnDefs': [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 5
                    }],
                'autoWidth': false,
                'columns': [{
                    "width": "20%"
                }, {
                    "width": "20%"
                }, {
                    "width": "20%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "20%"
                }
                ]
            });

            $(".dataTables_filter").hide();
        });

        // Automatically trigger the loading animation on click
        Ladda.bind('button[type=submit]');
    </script>

    @modaljs(['action' => 'delete', 'tab' => 'tab_replies', 'submit_url' => 'admin/replies'])
    @endmodaljs
@endpush
