@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.schools.index') }}">Schools</a></li>
        <li><a href="{{ route('admin.invoices.index',array("q_to"=> $school_id)) }}">Invoices</a></li>
        <li><a href="{{ route('admin.invoices.showArchived',array("q_to"=> $school_id)) }}">Archived</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Archived</h3>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12 text-right">
                <a class="archive btn btn-primary ladda-button btn-flat" href="{{ route('admin.invoices.index',array("q_to"=> $school_id)) }}">Back</a>
            </div>

            <table id="tab_invoices" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>To</th>
                    <th>Date</th>
                    <th>Due Date</th>
                    <th>Total</th>
                    <th>Status</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($invoices as $l)
                    <tr class="item{{ $l->id }}">
                        <td><a href='{{ route('admin.invoices.show',$l->id) }}'>{{ $l->invoice_id }}</a></td>
                        <td>{{ $l->to->name }}</td>
                        <td>{{ $l->issueDateFormat() }}</td>
                        <td>{{ $l->dueDateFormat() }}</td>
                        <td>{{ $l->amount_due }}</td>
                        <td>{{ $l->getStatus() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#tab_invoices').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search invoices......"
                },
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'columns': [{
                    "width": "15%"
                }, {
                    "width": "15%"
                }, {
                    "width": "15%"
                }, {
                    "width": "15%"
                }, {
                    "width": "15%"
                }, {
                    "width": "25%"
                }
                ]
            });

            $(".dataTables_filter").hide();
        });
    </script>
@endpush