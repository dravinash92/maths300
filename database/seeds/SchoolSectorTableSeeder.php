<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SchoolSector;

class SchoolSectorTableSeeder extends Seeder {

    public function run()
    {
        DB::table('school_sectors')->delete();

        SchoolSector::create([
            'name' => 'Government'
        ]);

        SchoolSector::create([
            'name' => 'Catholic'
        ]);

        SchoolSector::create([
            'name' => 'Independent'
        ]);
    }

}