<?php

namespace App\Exports;

use App\Models\School;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
class SchoolExport implements FromArray, WithHeadings, ShouldAutoSize, WithEvents, WithColumnFormatting
{
    protected $invoices;

    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }
    public function headings(): array
    {
        return ["School ID","School Name", "Address", "Country","Location","School Type","School Sector","School Size (Students)","Number of Campuses","Lesson Pack","School Size (Teachers)","Subscription Status","Join Date","Expiry Date","Last Renewal Date","Coordinator Name","Coordinator Email","Coordinator Phone","Coordinator Position","Finance Officer Name","Finance Officer Email","Finance Officer Phone","Finance Officer Position","Invoice Email","Total Users","Active Users","PO Number","Notes"];
        // return ["Lesson No","Upload date","Views(as on that date)","Title","Content Strands","Year Levels","Status","Pedagogies","Curricula","Related Lessons","Software Required","Software Link","Acknowledgements","Attributes","Partial Video","Full Video","Notes","Set Front","Set Sample"];
        
    }
    // public function map($invoice): array
    // {
    //     return [
    //         Date::dateTimeToExcel($invoice->join_date),
    //     ];
    // }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:AB1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $columnRange = 'Q2:Q10000';
                $alignArray = [
                'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    ],
                ];
                $event->sheet->getDelegate()->getStyle($columnRange)->applyFromArray($alignArray);
                $columnRange = 'U2:U10000';
                $alignArray = [
                'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    ],
                ];
                $event->sheet->getDelegate()->getStyle($columnRange)->applyFromArray($alignArray);



                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ];

                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
            },
        ];
    }
    public function columnFormats(): array
    {
        return [
            'M' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}
