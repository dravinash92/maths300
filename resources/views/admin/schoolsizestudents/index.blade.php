@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.schoolsizestudents.index') }}">School Size Students</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">School Size Students</h3>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('admin.schoolsizestudents.create') }}" class="btn btn-primary btn-flat">Add New</a>
            </div>

        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="schoolsizestudentSearchForm" role="form" method="get" action="{{ route('admin.schoolsizestudents.index') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter keywords..." value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="postSearchBtn" type="submit" class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
            <table id="tab_schoolsizestudents" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Menu</th>
                </tr>
                </thead>
                <tbody>
                @foreach($schoolsizestudents as $l)
                    <tr class="item{{ $l->id }}">
                        <td></td>
                        <td><a href='{{ route('admin.schoolsizestudents.show',$l->id) }}'>{{ $l->name }}</a></td>
                        <td>
                            <a href="{{ route('admin.schoolsizestudents.edit', $l->id) }}" class="btn btn-primary">Edit</a>
                            <button class="delete-modal btn btn-danger" data-id="{{ $l->id }}"
                                    data-name="{{ $l->name }}">Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @modal(['action' => 'delete', 'model' => 'schoolsizestudent'])
    @endmodal
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {

            let t = $('#tab_schoolsizestudents').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search schoolsizestudents......"
                },
                'ordering': true,
                'info': true,
                'columnDefs': [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 2
                    }
                ],
                'autoWidth': false,
                'columns': [{
                    "width": "10%"
                    },{
                    "width": "45%"
                    },{
                    "width": "45%"
                    }
                ]
            });

            $(".dataTables_filter").hide();

            t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();
        });

        // Automatically trigger the loading animation on click
        Ladda.bind('button[type=submit]');
    </script>

    @modaljs(['action' => 'delete', 'tab' => 'tab_schoolsizestudents', 'submit_url' => 'admin/schoolsizestudents'])
    @endmodaljs
@endpush
