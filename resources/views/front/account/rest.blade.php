@extends('layouts.master')

@section('css')
    <style type="text/css" media="all" id="siteorigin-panels-layouts-head">/* Layout 2958 */
        #pgc-2958-0-0, #pgc-2958-1-0 {
            width: 100%;
            width: calc(100% - (0 * 30px))
        }

        #pl-2958 #panel-2958-0-0-0, #pl-2958 #panel-2958-1-0-0 {
        }

        #pg-2958-0, #pl-2958 .so-panel {
            margin-bottom: 30px
        }

        #pl-2958 .so-panel:last-child {
            margin-bottom: 0px
        }

        @media (max-width: 767px) {
            #pg-2958-0.panel-no-style, #pg-2958-0.panel-has-style > .panel-row-style, #pg-2958-1.panel-no-style, #pg-2958-1.panel-has-style > .panel-row-style {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            #pg-2958-0 .panel-grid-cell, #pg-2958-1 .panel-grid-cell {
                margin-right: 0
            }

            #pg-2958-0 .panel-grid-cell, #pg-2958-1 .panel-grid-cell {
                width: 100%
            }

            #pl-2958 .panel-grid-cell {
                padding: 0
            }

            #pl-2958 .panel-grid .panel-grid-cell-empty {
                display: none
            }

            #pl-2958 .panel-grid .panel-grid-cell-mobile-last {
                margin-bottom: 0px
            }
        }</style>
@stop

@section('body')
    <body class="page-template-default page page-id-2958 woocommerce-no-js pmpro-body-has-access siteorigin-panels siteorigin-panels-before-js group-blog thim-body-preload bg-boxed-image thim-popup-feature"
          id="thim-body">
    <div id="preload">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
    <div id="wrapper-container" class="wrapper-container">
        <div class="content-pusher">
            @component('layouts.header')
            @endcomponent
            <div id="main-content">
                <section class="content-area">
                    <div class="top_heading  _out">
                        <div class="top_site_main "
                             style="color: #ffffff;background-image:url({{ asset('images/bg-page.jpg') }});">
                            <span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
                            <div class="page-title-wrapper">
                                <div class="banner-wrapper container"><h1>Account</h1></div>
                            </div>
                        </div>
                    </div>
                    <div class="container site-content">
                        <div class="row">
                            <main id="main" class="site-main col-sm-12 full-width">
                                <article id="post-2958"
                                         class="post-2958 page type-page status-publish hentry pmpro-has-access">
                                    <div class="entry-content">
                                        <div id="pl-2958" class="panel-layout">
                                            <div id="pg-2958-1" class="panel-grid panel-no-style">
                                                <div id="pgc-2958-1-0" class="panel-grid-cell">
                                                    <div id="panel-2958-1-0-0"
                                                         class="so-panel widget widget_login-form panel-first-child panel-last-child"
                                                         data-index="1">

                                                        <div class="thim-widget-login-form thim-widget-login-form-base">
                                                            <div class="thim-login form-submission-reset-password"><h2
                                                                        class="title">Reset Your Password</h2>

                                                                @if(count($errors))
                                                                    @foreach($errors->all() as $error)
                                                                        <p class="message message-error"
                                                                           style="max-width: 370px;">{{ $error }}</p>
                                                                    @endforeach
                                                                @endif

                                                                <form name="restpasswordform" action="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}"
                                                                      method="post">
                                                                    {!! csrf_field() !!}

                                                                    <input type="hidden" name="token"
                                                                           value="{{ $token }}">


                                                                    <p class="login-username">
                                                                        <input placeholder="Email"
                                                                               type="email" name="email"
                                                                               class="input required"
                                                                               value="{{ isset($email) ? $email : old('email') }}">
                                                                    </p>

                                                                    <p class="login-password">
                                                                        <input
                                                                                type="password"
                                                                                name="password"
                                                                                placeholder="Password"
                                                                                class="input required"
                                                                                value=""
                                                                                size="20"/>
                                                                    </p>

                                                                    <p class="login-password">
                                                                        <input
                                                                                type="password"
                                                                                name="password_confirmation"
                                                                                placeholder="Retype password"
                                                                                class="input required"
                                                                                value=""
                                                                                size="20"/>
                                                                    </p>

                                                                    <p>
                                                                        <input type="submit" name="wp-submit"
                                                                               id="wp-submit"
                                                                               class="button button-primary button-large"
                                                                               value="Reset password">
                                                                    </p>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </main>
                        </div>
                    </div>
                </section>
                @component('layouts.footer')
                @endcomponent
            </div>
        </div>
        <a href="#" id="back-to-top"> <i class="fa fa-chevron-up" aria-hidden="true"></i> </a>
    </div>
@stop


