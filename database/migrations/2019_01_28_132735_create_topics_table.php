<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->integer('views')->default(0);

            // relationship
            // belongs to a specific forum (foreign key)
            $table->integer('forum_id')->unsigned()->index()->nullable();
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');

            // belongs to a user (foreign key)
            $table->integer('author_id')->unsigned()->index()->nullable();
            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');

            // 0: closed 1: open
            // adding new reply is not allowed when topic closed
            $table->integer('status')->default(1);
            // 0: no 1: yes
            // stick it to the top of all topics.
            $table->integer('sticky')->default(0);
            // 0: hidden 1: public
            $table->integer('visibility')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
