<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CheckInvoiceStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * The number of seconds the job can run before timing out.
     *
     * 60 minutes
     *
     * @var int
     */
    public $timeout = 3600;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = microtime(true);
        Log::info('CheckInvoiceStatus job started......');
        $invoices = Invoice::all();
        $total_invoice_count = 0;
        $awaiting_payment_invoice_count = 0;
        $overdue_invoice_count = 0;
        foreach ($invoices as $invoice) {
            // status = "Awaiting payment";
            if ($invoice->status == 1) {
                if (Carbon::parse($invoice->due_date)->copy()->isPast()) {
                    Log::info('invoice id: ' . $invoice->invoice_id.' awaiting payment and overdue, school: ' . $invoice->to->name);
                    $invoice->status = 3;
                    $invoice->save();
                }
                ++$awaiting_payment_invoice_count;
            }
            // status = "Overdue";
            if ($invoice->status == 3) {
                if (Carbon::parse($invoice->grace_period_end_date)->copy()->isPast()) {
                    Log::info('invoice id: ' . $invoice->invoice_id.' overdue and grace period expired, school: ' . $invoice->to->name. ', suspend schools/users account automatically');
                    // suspend schools/users account automatically
                    $invoice->to->suspend();
                }
                ++$overdue_invoice_count;
            }
            ++$total_invoice_count;
        }
        $end = microtime(true);
        Log::info('######################################################################################');
        Log::info('CheckInvoiceStatus job completed, total invoices count: ' . $total_invoice_count . ', awaiting_payment_invoice_count: ' . $awaiting_payment_invoice_count . ', overdue_invoice_count: ' . $overdue_invoice_count . ', elapsed time: ' . ($end - $start) . ' seconds');
        Log::info('######################################################################################');
    }
}
