@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.topics.index') }}">Topics</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Topic Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <h4>Forum</h4>
            {{ $topic->forum->title }}
            <hr>

            <h4>Title</h4>
            {{ $topic->title }}
            <hr>

            <h4>Content</h4>
            {!!  $topic->content !!}
            <hr>

            <h4>Sticky</h4>
            {{ $topic->sticky ? 'Yes':'No' }}
            <hr>

            <h4>Visibility</h4>
            {{ $topic->visibility ? 'Yes':'No' }}
            <hr>

            <h4>Views</h4>
            {{ $topic->views }}
            <hr>

            <h4>Replies</h4>
            {{ $topic->getRepliesCount() }}
            <hr>

            <h4>Last reply</h4>
            {{ $topic->lastReply() ? $topic->lastReply()->updated_at : 'N/A' }}
            <hr>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.topics.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
@endpush