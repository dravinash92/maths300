@extends('layouts.pageframe')

@section('css')
    <style type="text/css" media="all" id="siteorigin-panels-layouts-head">/* Layout 2958 */
        #pgc-2958-0-0, #pgc-2958-1-0 {
            width: 100%;
            width: calc(100% - (0 * 30px))
        }

        #pl-2958 #panel-2958-0-0-0, #pl-2958 #panel-2958-1-0-0 {
        }

        #pg-2958-0, #pl-2958 .so-panel {
            margin-bottom: 30px
        }

        #pl-2958 .so-panel:last-child {
            margin-bottom: 0px
        }

        @media (max-width: 767px) {
            #pg-2958-0.panel-no-style, #pg-2958-0.panel-has-style > .panel-row-style, #pg-2958-1.panel-no-style, #pg-2958-1.panel-has-style > .panel-row-style {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            #pg-2958-0 .panel-grid-cell, #pg-2958-1 .panel-grid-cell {
                margin-right: 0
            }

            #pg-2958-0 .panel-grid-cell, #pg-2958-1 .panel-grid-cell {
                width: 100%
            }

            #pl-2958 .panel-grid-cell {
                padding: 0
            }

            #pl-2958 .panel-grid .panel-grid-cell-empty {
                display: none
            }

            #pl-2958 .panel-grid .panel-grid-cell-mobile-last {
                margin-bottom: 0px
            }
        }</style>
@stop

@section('top_heading')
    Account
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/login" title="Lessons"><span itemprop="name">Account</span></a>
    </li>
@stop

@section('site-content')
    <div class="container site-content">
        <div class="row">
            <main id="main" class="site-main col-sm-12 full-width">
                <article id="post-2958"
                         class="post-2958 page type-page status-publish hentry pmpro-has-access">
                    <div class="entry-content">
                        <div id="pl-2958" class="panel-layout">
                            <div id="pg-2958-1" class="panel-grid panel-no-style">
                                <div id="pgc-2958-1-0" class="panel-grid-cell">
                                    <div id="panel-2958-1-0-0"
                                         class="so-panel widget widget_login-form panel-first-child panel-last-child"
                                         data-index="1">
                                        <div class="thim-widget-login-form thim-widget-login-form-base">
                                            <div id="thim-form-login">
                                                <div class="thim-login-container">
                                                    <div class="thim-login form-submission-login"><h2
                                                                class="title">Login with your
                                                            site account</h2>
                                                        @if(count($errors))
                                                            @foreach($errors->all() as $error)
                                                                <p class="message message-error"
                                                                   style="max-width: 370px;">{{ $error }}</p>
                                                            @endforeach
                                                        @endif

                                                        <form name="loginform"
                                                              action="{{ url(config('adminlte.login_url', 'login')) }}"
                                                              method="post">
                                                            @csrf
                                                            <p class="login-username">
                                                                <input
                                                                        type="email" name="email"
                                                                        placeholder="Email"
                                                                        autocomplete="username"
                                                                        class="input required"
                                                                        value="{{ old('email') }}"/>
                                                            </p>
                                                            <p class="login-password">
                                                                <input
                                                                        type="password"
                                                                        name="password"
                                                                        placeholder="Password"
                                                                        class="input required"
                                                                        autocomplete="current-password"
                                                                        value=""
                                                                        size="20"/>
                                                            </p>

                                                            <a class="lost-pass-link"
                                                               href="/password/reset"
                                                               title="Lost Password">Lost your
                                                                password?</a>

                                                            <p class="forgetmenot login-remember">
                                                                <label
                                                                        for="rememberMe"><input
                                                                            name="rememberme"
                                                                            type="checkbox"
                                                                            id="rememberMe"
                                                                            value="forever"/> Remember
                                                                    Me
                                                                </label></p>
                                                            <p class="submit login-submit">
                                                                <input
                                                                        type="submit" name="wp-submit"
                                                                        class="button button-primary button-large"
                                                                        value="Login"/> <input
                                                                        type="hidden"
                                                                        name="redirect_to"/>
                                                        </form>
                                                        {{--<p class="link-bottom">Not a member yet? <a--}}
                                                        {{--href="#">Register--}}
                                                        {{--now</a></p>--}}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </main>
        </div>
    </div>
@stop