<?php

namespace App\Console\Commands;

use App\Jobs\InvoiceReminderFix;
use Illuminate\Console\Command;

class InvoiceReminderFixCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fix reminder invoices generation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        InvoiceReminderFix::dispatch();
        $this->info("Run reminder invoices generation fix program successfully.");
    }
}
