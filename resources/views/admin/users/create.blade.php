@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.users.index') }}">Users</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add User</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="userForm" role="form" method="post" action="{{ route('admin.users.store') }}">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group {{ $errors->has('school') ? 'has-error' : '' }}">
                        <label for="school">School</label>
                        <select data-placeholder="Select a school" class="form-control select2_school"
                                style="width: 100%;" id="school" name="school">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('school') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name"
                               value="{{ old('name') }}">
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email"
                               value="{{ old('email') }}">
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" id="phone" name="phone"
                               value="{{ old('phone') }}">
                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
                        <label for="position">Position</label>
                        <input type="text" class="form-control" id="position" name="position"
                               value="{{ old('position') }}">
                        <span class="text-danger">{{ $errors->first('position') }}</span>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            // Initialize Select2 elements
            let select2_school = $('.select2_school').select2({
                allowClear: true,
                data: {!! json_encode($schools)!!}
            });

            @if(old('school'))
            select2_school.val({!! old('school') !!}).trigger('change');
            @endif

            $("#userForm").submit(function (event) {
                let l = Ladda.create(document.querySelector('.ladda-button'));
                // Start loading
                l.start();
                return true;
            });
        });
    </script>
@endpush