@extends('adminlte::page')

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        Dashboard
    </h1>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $lessons_count }}</h3>

                    <p>Lessons</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="{{ route('admin.lessons.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $schools_count }}</h3>

                    <p>Schools</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('admin.schools.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ $users_count }}</h3>

                    <p>User</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{ route('admin.users.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-9">
            <!-- Recent Lessons List -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Notifications</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(count($recent_notices))
                    <ul class="products-list product-list-in-box">
                        @foreach($recent_notices as $n)
                            <li class="item">
                              <a href='#' onclick="return false">{{ $n->msg }}</a>
                              <span class="direct-chat-timestamp pull-right">{{ '  '.$n->getCreateDateDiffForHumans() }}</span>
                            </li>
                            <!-- /.item -->
                        @endforeach
                    </ul>
                    @else
                        No data available
                    @endif
                </div>
                <!-- /.box-body -->
                <!-- /.box-footer -->
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>

@stop

@push('js')
    <!-- Chart.JS -->
    <script src="{{ asset('vendor/adminlte/vendor/chart.js/Chart.js') }}"></script>
@endpush