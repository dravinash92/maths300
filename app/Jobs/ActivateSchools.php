<?php

namespace App\Jobs;

use App\Models\School;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ActivateSchools implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * The number of seconds the job can run before timing out.
     *
     * 2 hours
     *
     * @var int
     */
    public $timeout = 7200;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('ActivateSchools job started......');
        $all_schools = School::all();
        $total_count = 0;
        foreach ($all_schools as $key => $school) {
            Log::info('ActivateSchools handle ' . ($key + 1) . ' ');
            ++$total_count;
            Log::info("Try to activate school id: " . $school->id. " name: " . $school->name);
            $school->activate();
        }
        Log::info('######################################################################################');
        Log::info('ActivateSchools job completed, processed ' . $total_count);
        Log::info('######################################################################################');
    }
}
