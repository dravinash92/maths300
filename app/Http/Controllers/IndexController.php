<?php

namespace App\Http\Controllers;

use App\Models\ContentStrand;
use App\Models\Lesson;
use App\Models\Pedagogy;
use App\Models\Curriculum;
use App\Models\YearLevel;
use Illuminate\Http\Request;
use App\Models\Setting;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['year_levels'] = data4Select(YearLevel::class);
        $arr['content_strands'] = data4Select(ContentStrand::class);
        $arr['pedagogies'] = data4Select(Pedagogy::class);
        $arr['curricula'] = data4Select(Curriculum::class);

        // query sample lesson
        $query = Lesson::query();
        // published
        $query->status(1);
        $query->front(1);
        $query->sample(1);
        // only 1 sample lesson
        $query->limit(1);
        $arr['sample_lesson'] = $query->first();

        // query favourite lessons (lesson with setFront 1)
        $query = Lesson::query();
        // published
        $query->status(1);
        $query->sample(0);
        $query->front(1);
        // only 2 favourite lessons
        $query->limit(2);

        $arr['favorite_lessons'] = $query->get();
        $arr['footer_email'] = Setting::get('Email');
        return view('index')->with($arr);
    }
}
