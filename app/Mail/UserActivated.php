<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserActivated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var User
     */
    public $user;

    public $tmp_pass;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $tmp_pass)
    {
        $this->user = $user;
        $this->tmp_pass = $tmp_pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.userActivated')->subject('Welcome to Maths300! Let’s get started');
    }
}
