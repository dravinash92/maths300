@extends('adminlte::master')

@section('title', 'Maths300 | Invoice')

@section('adminlte_css')
    <!-- Invoice -->
    <link rel="stylesheet" href="{{ asset('css/invoice.css') }}">
    <style>
        body {
            font-family: "Open Sans", Verdana, Tahoma, serif;
        }
    </style>
@stop

@section('body')
    <body style="font-family: 'Open Sans',Verdana,Tahoma,serif;">
    <div class="container-fluid invoice-container">
        <div class="row invoice-header">
            <div class="invoice-head">
                <img src="{{ asset('images/logo-aamt.png') }}"
                     style="margin: 0px; width: 100px"
                     alt="{{ 'AAMT_'.config('app.name') }}" title="{{ $invoicefrom['name'] }}">
            </div>

            <div class="invoice-head info">
                <strong>{{ $invoicefrom['name'] }}</strong> <br/>
                ABN: {{ $invoicefrom['abn'] }}<br/>
                {{ $invoicefrom['address'] }}<br/>
                Phone: {{ $invoicefrom['phone'] }}<br/>
                Email: {{ $invoicefrom['email'] }}<br/>
            </div>

            <div class="invoice-head status">
                <div class="invoice-status">
                    <span class="{{ $invoice->getStatus() }}">{{ $invoice->getStatus() }}</span>
                </div>
                @if($invoice->isOverDue())
                    <span class="{{ $invoice->getStatus() }}">{{ $invoice->overDueDiff() }} due date</span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="invoice-col">
                <h3>{{ $invoice->tax_apply? 'Tax':'' }} Invoice #{{ $invoice->invoice_id }}</h3>
            </div>
        </div>

        <div class="row">
            <div class="invoice-col">
                <strong>Invoiced To</strong>
                <address class="small-text">
                    {{ $invoice->to->name }} <br/>
                    Attention to: {{ $invoice->to->financeOfficer->name }} <br/>
                    {{ $invoice->to->billing_address_region }} <br/>
                    {{ $invoice->to->billing_address_suburb }} {{ $invoice->to->billingAddressLocation->country->isAustralia()? $invoice->to->billingAddressLocation->name : '' }} {{ $invoice->to->billing_address_postcode }}
                    <br/>
                </address>
            </div>

            <div class="invoice-col">
                <strong>Issue Date</strong><br>
                <span class="small-text">
                    {{ $invoice->issueDateFormat() }}<br><br>
                </span>
            </div>

            <div class="invoice-col">
                <strong>Payment Due</strong><br>
                <span class="small-text">
                    {{ $invoice->dueDateFormat() }}
                </span>
            </div>
        </div>

        <div class="row">
            <div class="invoice-col">
                <strong>Subscription Expiry Date</strong><br>
                <span class="small-text">
                    {{ $invoice->subscriptionExpirationDateFormat() }}<br><br>
                </span>
            </div>

            @if($invoice->order_id)
                <div class="invoice-col">
                    <strong>Order ID</strong><br>
                    <span class="small-text">
                    {{ $invoice->order_id }}
                </span>
                    <br/><br/>
                </div>
            @endif
        </div>

        <div class="invoice-items panel panel-default invoice-panel">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Invoice Items</strong></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <td><strong>Description</strong></td>
                            <td width="20%" class="text-center"><strong>Amount</strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoice->orderItems as $item)
                            <tr>
                                <td>{{ $item->name }} - {{ $item->description }}</td>
                                <td class="text-center">${{ $item->totalAmountFormat() }} AUD</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td class="total-row text-right"><strong>Subtotal</strong></td>
                            <td class="total-row text-center">${{ $invoice->totalAmountFormat() }} AUD</td>
                        </tr>
                        @if($invoice->discount_apply)
                            <tr>
                                <td class="total-row text-right"><strong>Discount</strong></td>
                                <td class="total-row text-center">${{ $invoice->discountAmountFormat() }} AUD *</td>
                            </tr>
                        @endif
                        @if($invoice->tax_apply)
                            <tr>
                                <td class="total-row text-right"><strong>Includes GST 10%</strong></td>
                                <td class="total-row text-center">${{ $invoice->taxFormat() }} AUD</td>
                            </tr>
                        @endif
                        <tr>
                            <td class="total-row text-right"><strong>Total</strong></td>
                            <td class="total-row text-center">${{ $invoice->amountDueFormat() }} AUD</td>
                        </tr>

                        <tr>
                            <td class="total-row text-right"><strong>Paid</strong></td>
                            <td class="total-row text-center">
                                ${{ $invoice->isPaid()? $invoice->amountDueFormat() : '0.00' }} AUD
                            </td>
                        </tr>

                        <tr>
                            <td class="total-row text-right"></td>
                            <td class="total-row text-center"></td>
                        </tr>

                        <tr>
                            <td class="total-row text-right"><strong>Balance Due</strong></td>
                            <td class="total-row text-center">
                                ${{ $invoice->isPaid()? '0.00' : $invoice->amountDueFormat() }} AUD
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @if($invoice->discount_apply)
            <div class="row">
                <p>*Discount ({{ $invoice->discount->name }}) applied.</p>
            </div>
        @endif

        <div class="row">
            <div class="invoice-payment-advice">
                <strong>Payment Advice</strong><br>
                <span class="small-text">
                        Account Name: {{ $invoicefrom['accountName'] }}<br>
                        BSB: {{ $invoicefrom['bsb'] }}<br>
                        Account Number: {{ $invoicefrom['accountNumber'] }}<br>
                        Reference: {{ $invoice->invoice_id }}<br>
                        Please include this copy of invoice if you pay by cheque.<br>
                </span>
                <br>
                <a href="https://tiny.cc/payaamt" class="btn btn-success hidden-print" target="_blank"><i
                            class="fa fa-credit-card"></i> Pay Now</a>
            </div>
        </div>

        <div class="row">
            <div class="pull-right btn-group btn-group-sm hidden-print">
                <a href="javascript:window.print()" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                <a href='{{ route('front.users.downloadInvoice', $invoice->id) }}' class="btn btn-default"><i
                            class="fa fa-download"></i> Download</a>
            </div>
        </div>
    </div>

    <p class="text-center hidden-print">
        <a href="{{ route('front.users.invoices') }}">« Back</a>
    </p>

    </body>
@stop
