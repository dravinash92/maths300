<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\School;
use App\Models\User;
use App\Models\Lesson;
use DB;

class UserController extends Controller
{
    public $successStatus = 200;

    use SendsPasswordResetEmails;

    use AuthenticatesUsers;

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        return array_add($credentials, 'status', '1');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
	 $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $user = Auth::user();
           // $success['accessToken'] = $user->createToken('MyApp')->accessToken;
	    $success['accessToken'] = $user->reset_token;
            $success['id'] = $user->id;
            $success['name'] = $user->name;
            return response()->json(['status'=> 0,'data' => $success], $this->successStatus);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        $data['message'] = 'Email or password error.';

        return response()->json(['status'=> 1,'data' => $data], 401);
    }

    /**
     * user log out
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        if (Auth::check()) {
            $request->user()->token()->delete();
        }

        return response()->json(Status::success(), 200);
    }
    public function getLesson(Request $request)
    {
        
        if(!empty($request->userId)){
            $schquery = User::getusers("school_user.user_id = ".$request->userId)->toArray();
            if($schquery){
                $school = School::where('id',$schquery[0]['school_id'])->first();
                $tagids = explode(",",$school->tags);
            }
        }
        $query = Lesson::query();
        
        if(!empty($school->tags))
        {
            $query->Tags($tagids);
        }

        // published
        $query->status(1);
        
        
        $paginator = $query->get()->pluck('title')->toArray();
        foreach($paginator as $k=>$val){
            $arr[] = str_replace("\u2019","'",$val);
        }
        return response()->json($arr, 200);
    }

}
