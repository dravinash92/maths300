<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SchoolType;

class SchoolTypeTableSeeder extends Seeder {

    public function run()
    {
        DB::table('school_types')->delete();

        SchoolType::create([
            'name' => 'Preschool'
        ]);

        SchoolType::create([
            'name' => 'Primary school'
        ]);

        SchoolType::create([
            'name' => 'High school'
        ]);
    }

}