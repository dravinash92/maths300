<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['dashboard_nav_active'] = 1;
        $user = Auth::user();
        $school = $user->getSchool();

        if ($user->hasRole('super-admin')) {
            return redirect(route('admin.home'));
        } else {
            $arr['school'] = $school;

            return view('front.home')->with($arr);
        }
    }
}
