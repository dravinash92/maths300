<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\ContentStrand;

class ContentStrandTableSeeder extends Seeder {

    public function run()
    {
        DB::table('content_strands')->delete();

        ContentStrand::create([
            'name' => 'Number'
        ]);

        ContentStrand::create([
            'name' => 'Algebra'
        ]);

        ContentStrand::create([
            'name' => 'Measurement'
        ]);

        ContentStrand::create([
            'name' => 'Geometry'
        ]);

        ContentStrand::create([
            'name' => 'Statistics'
        ]);

        ContentStrand::create([
            'name' => 'Probability'
        ]);
    }

}