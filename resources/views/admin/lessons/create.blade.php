@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.lessons.index') }}">Lessons</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add lesson</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="lessonForm" role="form" method="post" action="{{ route('admin.lessons.store') }}">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group {{ $errors->has('number') ? 'has-error' : '' }}">
                        <label for="number">Number</label>
                        <input type="text" class="form-control" id="number" name="number" placeholder="Enter number"
                               value="{{ old('number') }}" autofocus>
                        <span class="text-danger">{{ $errors->first('number') }}</span>
                    </div>


                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter title"
                               value="{{ old('title') }}">
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('summary') ? 'has-error' : '' }}">
                        <label for="summary">Summary</label>
                        <textarea class="form-control" id="summary" name="summary"
                                  rows="8" placeholder="Enter summary">{{ old('summary') }}</textarea>
                        <span class="text-danger">{{ $errors->first('summary') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('resources') ? 'has-error' : '' }}">
                        <label for="resources">Resources</label>
                        <textarea class="form-control" id="resources" name="resources"
                                  rows="8" placeholder="Enter resources">{{ old('resources') }}</textarea>
                        <span class="text-danger">{{ $errors->first('resources') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('lesson_notes') ? 'has-error' : '' }}">
                        <label for="lesson_notes">Lesson Notes</label>
                        <textarea class="form-control " id="lesson_notes"
                                  name="lesson_notes" rows="8">{{ old('lesson_notes') }}</textarea>
                        <span class="text-danger">{{ $errors->first('lesson_notes') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('extensions') ? 'has-error' : '' }}">
                        <label for="extensions">Extensions</label>
                        <textarea class="form-control " id="extensions"
                                  name="extensions" rows="8">{{ old('extensions') }}</textarea>
                        <span class="text-danger">{{ $errors->first('extensions') }}</span>
                    </div>


                    <div class="form-group {{ $errors->has('practical_tips') ? 'has-error' : '' }}">
                        <label for="practical_tips">Practical Tips</label>
                        <textarea class="form-control " id="practical_tips"
                                  name="practical_tips" rows="8">{{ old('practical_tips') }}</textarea>
                        <span class="text-danger">{{ $errors->first('practical_tips') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('content_strands') ? 'has-error' : '' }}">
                        <label for="content_strands">Content strands</label>
                        <select data-placeholder="Select content strands" class="form-control select2-content_strands"
                                style="width: 100%;" id="content_strands" name="content_strands[]"
                                multiple="multiple">
                            @foreach($content_strands as $c)
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('content_strands') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('year_levels') ? 'has-error' : '' }}">
                        <label for="year_levels">Year levels</label>
                        <select class="form-control select2-year_levels" style="width: 100%;" id="year_levels"
                                name="year_levels[]"
                                multiple="multiple"
                                data-placeholder="Select year levels">
                            @foreach($year_levels as $y)
                                <option value="{{ $y->id }}">{{ $y->name }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('year_levels') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('pedagogies') ? 'has-error' : '' }}">
                        <label for="pedagogies">Pedagogies</label>
                        <select class="form-control select2-pedagogies" style="width: 100%;" id="pedagogies"
                                name="pedagogies[]"
                                multiple="multiple"
                                data-placeholder="Select pedagogies">
                            @foreach($pedagogies as $p)
                                <option value="{{ $p->id }}">{{ $p->name }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('pedagogies') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('curricula') ? 'has-error' : '' }}">
                        <label for="curricula">Curricula</label>
                        <select class="form-control select2-curricula" style="width: 100%;" id="curricula"
                                name="curricula[]"
                                multiple="multiple"
                                data-placeholder="Select curricula">
                            @foreach($curricula as $c)
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('curricula') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('related_lessons') ? 'has-error' : '' }}">
                        <label for="related_lessons">Related Lessons</label>
                        <select class="form-control select2-related_lessons" style="width: 100%;"
                                id="related_lessons"
                                name="related_lessons[]"
                                multiple="multiple"
                                data-placeholder="Select related lessons">
                            @foreach($lessons as $l)
                                <option value="{{ $l->id }}">{{ $l->number }} {{ $l->title }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('related_lessons') }}</span>
                    </div>
                    
                    <div class="form-group {{ $errors->has('lesson_pack') ? 'has-error' : '' }}">
                        <label for="lesson_pack">Lesson Pack</label>
                        <select class="form-control select2-lesson_pack" style="width: 100%;"
                                id="lesson_pack"
                                name="lesson_pack[]"
                                multiple="multiple"
                                data-placeholder="Select lesson packs">
                            @foreach($tags as $t)
                                <option value="{{ $t->id }}">{{ $t->name }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('lesson_pack') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('software_required') ? 'has-error' : '' }}">
                        <label for="software_required">Software Required</label><br>
                        <div>
                            <input class="minimal" type="radio" id="software_required_yes"
                                   name="software_required"
                                   value="1" {{ old('software_required') == '1' ? 'checked' : '' }}>
                            <label for="software_required_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="software_required_no"
                                   name="software_required"
                                   value="0" {{ old('software_required', 0) == '0' ? 'checked' : '' }}>
                            <label for="software_required_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('software_required') }}</span>
                    </div>

                    <div id="software_link_info" style="display: {{ old('software_required')? 'block' : 'none' }};">
                        <div class="form-group {{ $errors->has('software_link') ? 'has-error' : '' }}">
                            <label for="software_link">Software Link</label>
                            <input type="text" class="form-control" id="software_link" name="software_link"
                                   placeholder="Enter software link" value="{{ old('software_link') }}">
                            <span class="text-danger">{{ $errors->first('software_link') }}</span>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('acknowledgements') ? 'has-error' : '' }}">
                        <label for="acknowledgements">Acknowledgements</label>
                        <textarea class="form-control" id="acknowledgements"
                                  name="acknowledgements">{{ old('acknowledgements') }}</textarea>
                        <span class="text-danger">{{ $errors->first('acknowledgements') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('attributes') ? 'has-error' : '' }}">
                        <label for="attributes">Attributes</label>
                        <textarea class="form-control" id="attributes"
                                  name="attributes">{{ old('attributes') }}</textarea>
                        <span class="text-danger">{{ $errors->first('attributes') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('partial_video') ? 'has-error' : '' }}">
                        <label for="partial_video">Partial Video</label>
                        <input type="text" class="form-control" id="partial_video" name="partial_video"
                               placeholder="Enter partial video link" value="{{ old('partial_video') }}">
                        <span class="text-danger">{{ $errors->first('partial_video') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('full_video') ? 'has-error' : '' }}">
                        <label for="full_video">Full video</label>
                        <input type="text" class="form-control" id="full_video" name="full_video"
                               placeholder="Enter full video link"
                               value="{{ old('full_video') }}">
                        <span class="text-danger">{{ $errors->first('full_video') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('notes') ? 'has-error' : '' }}">
                        <label for="notes">Notes</label>
                        <textarea class="form-control" id="notes" name="notes">{{ old('notes') }}</textarea>
                        <span class="text-danger">{{ $errors->first('notes') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('is_front') ? 'has-error' : '' }}">
                        <label for="is_front">Set Front</label><br>
                        <div>
                            <input class="minimal" type="radio" id="is_front_yes"
                                   name="is_front"
                                   value="1" {{ old('is_front') == '1' ? 'checked' : '' }}>
                            <label for="is_front_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="is_front_no"
                                   name="is_front"
                                   value="0" {{ old('is_front', 0) == '0' ? 'checked' : '' }}>
                            <label for="is_front_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('is_front') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('is_sample') ? 'has-error' : '' }}">
                        <label for="is_sample">Set As Sample</label><br>
                        <div>
                            <input class="minimal" type="radio" id="is_sample_yes"
                                   name="is_sample"
                                   value="1" {{ old('is_sample') == '1' ? 'checked' : '' }}>
                            <label for="is_sample_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="is_sample_no"
                                   name="is_sample"
                                   value="0" {{ old('is_sample', 0) == '0' ? 'checked' : '' }}>
                            <label for="is_sample_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('is_sample') }}</span>
                    </div>

                    <div id="coverInfo" style="display: {{ old('is_front')? 'block' : 'none' }};">
                        <div class="form-group {{ $errors->has('cover_image_front') ? 'has-error' : '' }}">
                            <label for="cover_image_front">Front Cover image</label>
                            <br>
                            <span class="text-info">480X293(width*height) for best resolution</span>
                            <br>
                            <button id="cover_image_front" type="button" class="button insert-media add_media"
                                    data-category="image" data-editor="cover_image_front"><span></span>Add Image
                            </button>
                            <span class="text-danger">{{ $errors->first('cover_image_front') }}</span>
                        </div>
                        <div id="cover_image_front_place_holder">
                            @if(old('cover_image_front'))
                                <img id={{ old('cover_image_front') }} src="{{  \App\Models\Resource::getURL(old('cover_image_front')) }}"
                                     style="max-width: 320px;"/>
                                <input name="cover_image_front" value="{{ old('cover_image_front') }}" type="hidden">
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('desc_front') ? 'has-error' : '' }}">
                            <label for="desc_front">Description</label>
                            <textarea class="form-control" id="desc_front" name="desc_front" rows="3">{{ old('desc_front') }}</textarea>
                            <span class="text-danger">{{ $errors->first('desc_front') }}</span>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @component('admin.common.fileUpload')
    @endcomponent
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            // Initialize Select2 elements
            let select2_year_levels = $('.select2-year_levels').select2({
                allowClear: true,
            });

            @if(old('year_levels'))
            select2_year_levels.val({!! json_encode( collect(old('year_levels')) ) !!}).trigger('change');
            @endif

            let select2_content_strands = $('.select2-content_strands').select2({
                    allowClear: true,
                });

            @if(old('content_strands'))
            select2_content_strands.val({!! json_encode( collect(old('content_strands')) ) !!}).trigger('change');
            @endif

            let select2_pedagogies = $('.select2-pedagogies').select2({
                    allowClear: true,
                });

            @if(old('pedagogies'))
            select2_pedagogies.val({!! json_encode( collect(old('pedagogies')) ) !!}).trigger('change');
            @endif

            let select2_curricula = $('.select2-curricula').select2({
                    allowClear: true,
                });

            @if(old('curricula'))
            select2_curricula.val({!! json_encode( collect(old('curricula')) ) !!}).trigger('change');
            @endif

            let select2_related_lessons = $('.select2-related_lessons').select2({
                    allowClear: true,
                });

            @if(old('related_lessons'))
            select2_related_lessons.val({!! json_encode( collect(old('related_lessons')) ) !!}).trigger('change');
            @endif
            
            let select2_lesson_pack = $('.select2-lesson_pack').select2({
                    allowClear: true,
                });

            @if(old('lesson_pack'))
            select2_lesson_pack.val({!! json_encode( collect(old('lesson_pack')) ) !!}).trigger('change');
            @endif

            // Initialize editor elements
            editor.init('summary');
            editor.init('resources', true);
            editor.init('lesson_notes');
            editor.init('extensions');
            editor.init('practical_tips');

            $("#lessonForm").submit(function (event) {
                let l = Ladda.create(document.querySelector('.ladda-button'));
                // Start loading
                l.start();
                $("#is_front_yes").prop('disabled', false);
                $("#is_front_no").prop('disabled', false);

                let software_required_val = $("input[name='software_required']:checked").val();
                // no software required
                if (software_required_val === '0' ) {
                    $('#software_link').remove();
                }

                return true;
            });

            // software link on change event start
            $( "#software_required_yes" ).click(function() {
                $('#software_link_info').show();
            });
            $( "#software_required_no" ).click(function() {
                $('#software_link_info').hide();
                // clear the input field
                $('#software_link').val('');

            });
            // software link on change event end


            $( "#is_front_yes" ).click(function() {
                $('#coverInfo').show();
            });

            $( "#is_front_no" ).click(function() {
                $('#coverInfo').hide();
            });

            $( "#is_sample_yes" ).click(function() {
                $("#is_front_yes").prop("checked", "checked");

                $("#is_front_yes").prop('disabled', true);
                $("#is_front_no").prop('disabled', true);

                $('#coverInfo').show();
            });

            $( "#is_sample_no" ).click(function() {
                $("#is_front_yes").prop('disabled', false);
                $("#is_front_no").prop('disabled', false);
            });
        });
    </script>
@endpush