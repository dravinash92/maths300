<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class SubscriptionPlan extends Model
{
    public function schoolSize()
    {
        return $this->belongsTo(SchoolSizeTeacher::class);
    }

    /**
     * The schools that belong to the subscription plan.
     */
    public function schools()
    {
        return $this->hasMany(School::class);
    }

    public function getDesc()
    {
        return 'Joining fee:$' . $this->joining_fee . '  Annual fee:$' . $this->annual_fee . '  Total fee:$' . $this->total_fee . ' [' . $this->schoolSize->name . ']';
    }

    /**
     * build data for frontend [Select2 data format]
     *
     * only get data where [is_visible] is true
     *
     *
     * ref: https://select2.org/data-sources/formats
     * @return array
     */
    public static function data4Select()
    {
        $all_data = SubscriptionPlan::where('is_visible', 1)->get();
        $results = array();
        foreach ($all_data as $d) {
            $tmp['id'] = $d->id;
            $tmp['text'] = $d->getDesc();
            $results[] = $tmp;
        }

        return $results;
    }

    public static function data4SelectWithSchoolTeacherSize($school_size_id)
    {
        $all_data = SubscriptionPlan::where('is_visible', 1)->where('school_size_id', $school_size_id)->get();
        $results = array();
        foreach ($all_data as $d) {
            $tmp['id'] = $d->id;
            $tmp['text'] = $d->getDesc();
            $results[] = $tmp;
        }

        return $results;
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('joining_fee', 'like', '%' . $keywords . '%')
            ->orWhere('annual_fee', 'like', '%' . $keywords . '%')
            ->orWhere('total_fee', 'like', '%' . $keywords . '%');
    }

    /**
     * @param $query
     * @param $schoolSizeTeacher
     */
    public function scopeSchoolSize($query, $schoolSize)
    {
        $query->with('schoolSize')->whereHas('schoolSize', function ($query) use ($schoolSize) {
            $query->where('name', $schoolSize);
        });
    }

    /**
     * joining_fee will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function joiningFeeFormat()
    {
        return number_format($this->joining_fee, 2);
    }

    /**
     * annual_fee will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function annualFeeFormat()
    {
        return number_format($this->annual_fee, 2);
    }
    public static function getdata4Reports($id)
    {
        $all_data = SubscriptionPlan::where('id', $id)->get();
        $results = array();
        foreach ($all_data as $d) {            
            $results = $d->getDesc();
        }

        return $results;
    }
}
