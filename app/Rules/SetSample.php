<?php

namespace App\Rules;

use App\Models\Lesson;
use Illuminate\Contracts\Validation\Rule;

class SetSample implements Rule
{
    private $limit = 1;

    private $lesson_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($lesson_id)
    {
        $this->lesson_id = $lesson_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value == 1) {
            // edit
            if (!empty($this->lesson_id)) {
                $lesson = Lesson::find($this->lesson_id);
                if ($lesson->is_sample) {
                    return true;
                } else {
                    $query = Lesson::query();
                    $query->sample(1);
                    return $query->count() < $this->limit;
                }
            }
            else { // new
                $query = Lesson::query();
                $query->sample(1);
                return $query->count() < $this->limit;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Set sample not allowed as there was '.$this->limit.' lessons has been set as sample lesson already.';
    }
}
