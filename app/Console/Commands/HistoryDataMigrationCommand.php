<?php

namespace App\Console\Commands;

use App\Jobs\HistoryDataMigration;
use Illuminate\Console\Command;

class HistoryDataMigrationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run history data migration program';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        HistoryDataMigration::dispatch();
        $this->info("Run history data migration program successfully.");
    }
}
