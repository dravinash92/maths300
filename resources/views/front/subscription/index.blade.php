@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }

        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        table thead tr {
            background-color: #78BE20;
            color: #333;
        }

        .btn-subscription:hover {
            background-color: #78BE20;
            border-color: #78BE20;
        }

    </style>
@stop

@extends('layouts.page')

@section('top_heading')
    Subscribe
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/subscribe"><span itemprop="name">Subscribe</span></a>
    </li>
@stop

@section('content')
        <article id="post-87" class="post-87 page type-page status-publish hentry pmpro-has-access">
            <div class="entry-content">
                <div id="pl-87" class="panel-layout">
                    {!! $subscriptionpage->content !!}
                </div>

                <br>
                <div id="pl-88" class="panel-layout">
                    <table id="tab_subscription_plan" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Subscription Plan</th>
                            <th>Joining fee</th>
                            <th>Annual fee</th>
                        </tr>
                        @csrf
                        </thead>
                        <tbody>
                        @if(count($subscriptionplans))
                            @foreach($subscriptionplans as $l)
                                <tr class="item{{ $l->id }}">
                                    <td>{{ $l->schoolSize->name }}</td>
                                    <td>{{ $l->joining_fee }}</td>
                                    <td>{{ $l->annual_fee }}*</td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="no-item">
                                <td colspan="3">No data available.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>

                <div class="col-xs-12 text-right">
                    <a href="{{ route('front.subscription.termsAndConditions', 1) }}" class="btn btn-primary btn-subscription">Subscribe</a>
                </div>

            </div>
        </article>
@stop
