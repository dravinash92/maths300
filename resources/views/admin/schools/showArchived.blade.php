@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.schools.index') }}">Schools</a></li>
        <li><a href="{{ route('admin.schools.showArchived') }}">Archived</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Schools</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="schoolSearchForm" role="form" method="get" action="{{ route('admin.schools.showArchived') }}">
                {{ csrf_field() }}
                <div class="row ">
                    <div class="col-xs-3">
                        <select data-placeholder="Select location" class="form-control select2_address_location"
                                style="width: 100%;" id="q_address_location" name="q_address_location">
                            <option></option>
                        </select>
                    </div>

                    <div class="col-xs-3">
                        <select data-placeholder="Select a school type" class="form-control select2_schoolType"
                                style="width: 100%;" id="q_schoolType" name="q_schoolType">
                            <option></option>
                        </select>
                    </div>

                    <div class="col-xs-3">
                        <select data-placeholder="Select a school sector" class="form-control select2_schoolSector"
                                style="width: 100%;" id="q_schoolSector" name="q_schoolSector">
                            <option></option>
                        </select>
                    </div>
                </div>

                <p/>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter school keywords (postcode/address etc.)..."
                                   value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="schoolSearchBtn" type="submit" class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>
                        </div>
                    </div>
                </div><!-- /.row -->
            </form>

            <table id="tab_schools" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Join date</th>
                    <th>Expiry date</th>
                    <th>Status</th>
                    <th>Payment</th>
                    <th>Name</th>
                    <th>Location</th>
                    <th>Menu</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($schools as $l)
                    <tr class="item{{ $l->id }}">
                        <td class="col1">{{ $l->joinDateFormat() }}</td>
                        <td class="col1">{{ $l->subscriptionExpirationDateFormat() }}</td>
                        <td>{{ $l->getStatus() }}</td>
                        <td class="{{ $l->getPaymentStatus() }}">{{ $l->getPaymentStatus() }}</td>
                        <td><a href='{{ route('admin.schools.show',$l->id) }}'>{{ $l->name }}</a></td>
                        <td>{{ $l->addressLocation->name }}</td>
                        <td>
                            @if( $l->isDeleteAllowed() )
                                <button class="delete-modal btn btn-primary" data-id="{{ $l->id }}"
                                        data-name="{{ $l->name }}">Delete
                                </button>
                            @endif

                            @if( $l->isReinstateAllowed() )
                                <button class="reinstate-modal btn btn-primary" data-id="{{ $l->id }}"
                                        data-name="{{ $l->name }}">Reinstate
                                </button>
                            @endif

                            <a class="invoice-modal btn btn-primary" href='{{ 'viewInvoice/'.$l->id }}'>Invoice</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    @modal(['action' => 'reinstate', 'model' => 'school', 'form_action' => '/admin/schools/reinstate'])
    @endmodal

    @modal(['action' => 'delete', 'model' => 'school'])
    @endmodal
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {

            // Initialize Select2 elements
            let select2_address_location = $('.select2_address_location').select2({
                allowClear: true,
                data: {!! json_encode($locations)!!}
            });

            @if(old('q_address_location'))
            select2_address_location.val({!! old('q_address_location') !!}).trigger('change');
                    @endif

            let select2_schoolType = $('.select2_schoolType').select2({
                    allowClear: true,
                    data: {!! json_encode($schoolTypes)!!}
                });

            @if(old('q_schoolType'))
            select2_schoolType.val({!! old('q_schoolType') !!}).trigger('change');
                    @endif

            let select2_schoolSector = $('.select2_schoolSector').select2({
                    allowClear: true,
                    data: {!! json_encode($schoolSectors)!!}
                });

            @if(old('q_schoolSector'))
            select2_schoolSector.val({!! old('q_schoolSector') !!}).trigger('change');
            @endif

            $('#tab_schools').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search schools......"
                },
                'ordering': true,
                'info': true,
                'columnDefs': [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 6
                    }],
                'autoWidth': false,
                'columns': [{
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "25%"
                }, {
                    "width": "15%"
                }, {
                    "width": "20%"
                }
                ]
            });

            $(".dataTables_filter").hide();
        });

        // Automatically trigger the loading animation on click
        Ladda.bind('button[type=submit]');
    </script>

    @modaljs(['action' => 'reinstate'])
    @endmodaljs

    @modaljs(['action' => 'delete', 'tab' => 'tab_schools', 'submit_url' => 'admin/schools'])
    @endmodaljs
@endpush
