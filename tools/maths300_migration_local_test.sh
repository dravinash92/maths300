#!/bin/bash

workspace=/Users/xiuquanw/Workspace/maths300/code/maths300

# applied db data from production
cd ${workspace}/tools
mysql -u"aiden" -p"password" maths300 < mysql-maths300_prod.sql

# create db schema
cd ${workspace}
composer dump-autoload
php artisan migrate --path=database/migrations/2020_01_15_225301_create_history_data_table.php

cd ${workspace}/tools
python3 maths300_data_import.py

cd ${workspace}
php artisan migrate --path=database/migrations/2020_03_08_210359_alter_schools.php
php artisan db:seed --class=SchoolSizeTeacherTableUpdateSeeder

# school data update
php artisan db:seed --class=SchoolTableUpdateSeeder

# subscription data update
# remove foreign key constrains
php artisan db:seed --class=SubscriptionPlanTableUpdateSeeder

php artisan migrate --path=database/migrations/2020_04_04_124129_mod_school_size_to_sub_table.php
php artisan db:seed --class=SubscriptionPlanTableSeeder

php artisan migrate --path=database/migrations/2020_06_28_101102_add_maximum_user_num_to_sub_table.php
php artisan db:seed --class=SubscriptionPlanTableUpdateMaximumUserSeeder

php artisan queue:listen --timeout=0
php artisan data:migration
