<!-- Modal script for action of {{ $action }} -->
<script type="text/javascript">
    $(document).on('click', '.{{ $action }}-modal', function () {
        $('.modal-title').text('{{ ucfirst($action) }}');
        $('#id_{{ $action }}').val($(this).data('id'));
        $('#name_{{ $action }}').val($(this).data('name'));
        $('#{{ $action }}Modal').modal('show');
    });
    $('.modal-footer').on('click', '.{{ $action }}', function (evt) {
        evt.preventDefault();

        @if($action == 'delete')
        id = $('#id_{{ $action }}').val();
        $.ajax({
            type: 'DELETE',
            url: location.origin + '/{{ isset($submit_url)? $submit_url:'' }}/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                toastr.success('Delete request was handled successfully.', 'Success Alert', {timeOut: 5000});
                $('#{{ $tab }}').DataTable().row('.item' + data['id']).remove().draw(false);
            },
            error: function (response) {
                if (response.status == 500) {
                    toastr.error('Server error.', 'Error Alert', {timeOut: 5000});
                } else {
                    if (response.responseJSON.error) {
                        toastr.error(response.responseJSON.error, 'Error Alert', {timeOut: 5000});
                    } else {
                        toastr.error('Bad Request', 'Error Alert', {timeOut: 5000});
                    }
                }
            }
        });
        @else
        $('#form_{{ $action }}').submit();
        @endif
    });
</script>
