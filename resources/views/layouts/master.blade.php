<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="xmlrpc.php">
    <title>Maths300 &#8211; Engaging lessons and professional support</title>
    <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('favicon.ico') }}"/>
    <meta name="msapplication-TileImage"
          content="{{ asset('favicon.ico') }}"/>
    <link rel='stylesheet' id='lp-course-wishlist-style-css'
          href='/wp-content/plugins/learnpress-wishlist/assets/css/wishlist.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='mo-wp-bootstrap-social-css'
          href='/wp-content/plugins/miniorange-login-openid/includes/css/bootstrap-social.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='rs-plugin-settings-css'
          href='/wp-content/plugins/revslider/public/assets/css/settings.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='siteorigin-panels-front-css'
          href='/wp-content/plugins/siteorigin-panels/css/front-flex.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='wpems-owl-carousel-css-css'
          href='/wp-content/plugins/wp-events-manager/inc/libraries/owl-carousel/css/owl.carousel.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='wpems-magnific-popup-css-css'
          href='/wp-content/plugins/wp-events-manager/inc/libraries/magnific-popup/css/magnific-popup.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='wpems-fronted-css-css'
          href='/wp-content/plugins/wp-events-manager/assets/css/frontend/events.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='font-awesome-css'
          href='/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='jquery-scrollbar-css'
          href='/wp-content/plugins/learnpress/assets/js/vendor/jquery-scrollbar/jquery.scrollbar.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='ionicons-css' href='/wp-content/themes/eduma/assets/css/ionicons.min.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='font-pe-icon-7-css' href='/wp-content/themes/eduma/assets/css/font-pe-icon-7.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='flaticon-css' href='/wp-content/themes/eduma/assets/css/flaticon.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='thim-style-css' href='/wp-content/themes/eduma/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='thim-style-options-css' href='/wp-content/uploads/tc_stylesheets/eduma.1544148817.css'
          type='text/css' media='all'/>
    <script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress-wishlist/assets/js/wishlist.js'></script>
    <script type='text/javascript'
            src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript'
            src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
    <script type='text/javascript' src='/wp-includes/js/underscore.min.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress/assets/js/vendor/watch.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress/assets/js/vendor/jquery.alert.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress/assets/js/vendor/circle-bar.js'></script>
    <script type='text/javascript' src='/wp-includes/js/utils.min.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress/assets/js/global.js'></script>
    <script type='text/javascript'
            src='/wp-content/plugins/learnpress/assets/js/vendor/jquery-scrollbar/jquery.scrollbar.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress/assets/js/frontend/learnpress.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress/assets/js/frontend/course.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress/assets/js/vendor/jquery.scrollTo.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/learnpress/assets/js/frontend/become-teacher.js'></script>
    <link rel='https://api.w.org/' href='index.php/wp-json/index.html'/>
    <script type="text/javascript">
        function tc_insert_internal_css(css) {
            var tc_style = document.createElement("style");
            tc_style.type = "text/css";
            tc_style.setAttribute('data-type', 'tc-internal-css');
            var tc_style_content = document.createTextNode(css);
            tc_style.appendChild(tc_style_content);
            document.head.appendChild(tc_style);
        }
    </script>
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css"
          href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
    <style type="text/css" media="all"
           id="siteorigin-panels-layouts-head">/* Layout 5899 */
        #pgc-5899-0-0, #pgc-5899-1-0 {
            width: 100%;
            width: calc(100% - (0 * 30px))
        }

        #pl-5899 #panel-5899-0-0-0, #pl-5899 #panel-5899-0-0-1, #pl-5899 #panel-5899-1-0-0, #pl-5899 #panel-5899-1-0-1 {
        }

        #pg-5899-0 {
            margin-bottom: 80px
        }

        #pg-5899-1 {
            margin-bottom: 60px
        }

        #pl-5899 .so-panel {
            margin-bottom: 30px
        }

        #pl-5899 .so-panel:last-child {
            margin-bottom: 0px
        }

        #pg-5899-0 > .panel-row-style {
            background-image: url({{ asset('images/top-banner.jpg') }});
            background-position: center center;
            background-size: cover;
            padding: 0px 0px 0px 0px
        }

        #pg-5899-0.panel-no-style, #pg-5899-0.panel-has-style > .panel-row-style {
            -webkit-align-items: flex-start;
            align-items: flex-start
        }

        @media (max-width: 767px) {
            #pg-5899-0.panel-no-style, #pg-5899-0.panel-has-style > .panel-row-style, #pg-5899-1.panel-no-style, #pg-5899-1.panel-has-style > .panel-row-style {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            #pg-5899-0 .panel-grid-cell, #pg-5899-1 .panel-grid-cell {
                margin-right: 0
            }

            #pg-5899-0 .panel-grid-cell, #pg-5899-1 .panel-grid-cell {
                width: 100%
            }

            #pl-5899 .panel-grid-cell {
                padding: 0
            }

            #pl-5899 .panel-grid .panel-grid-cell-empty {
                display: none
            }

            #pl-5899 .panel-grid .panel-grid-cell-mobile-last {
                margin-bottom: 0px
            }
        } </style>
    <script type="text/javascript">function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };</script>
    <noscript>
        <style type="text/css"> .wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript>
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    @yield('css')
</head>

@yield('body')

<script data-cfasync="false" type="text/javascript">
    window.onload = function () {
        var thim_preload = document.getElementById('preload');
        if (thim_preload) {
            setTimeout(function () {
                var body = document.getElementById('thim-body'),
                    len = body.childNodes.length,
                    class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(/(?:^|\s)thim-body-load-overlay(?!\S)/, '');

                body.className = class_name;
                if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
                    for (var i = 0; i < len; i++) {
                        if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == 'preload') {
                            body.removeChild(body.childNodes[i]);
                            break;
                        }
                    }
                }
            }, 500);
        }
    };
</script>
<script>
    window.addEventListener('load', function () {
        /**
         * Fix issue there is an empty spacing between image and title of owl-carousel
         */
        setTimeout(function () {
            var $ = jQuery;
            var $carousel = $('.thim-owl-carousel-post').each(function () {
                $(this).find('.image').css('min-height', 0);
                $(window).trigger('resize');
            });
        }, 500);
    });
</script>
<script data-cfasync="true" type="text/javascript">
    (function ($) {
        'use strict';
        $(document).on('click',
            'body:not(".logged-in") .enroll-course .button-enroll-course, body:not(".logged-in") .purchase-course:not(".guest_checkout, .learn-press-pmpro-buy-membership") .button',
            function (e) {
                e.preventDefault();
                // $(this).parent().find('[name="redirect_to"]').val('index.php/account/index5bab.html?redirect_to=/?enroll-course=5899');
                $(this).parent().find('[name="redirect_to"]').val('/');
                var redirect = $(this).parent().find('[name="redirect_to"]').val();
                window.location = redirect;
            });
    })(jQuery);
</script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/core.min.js'></script>
<script type='text/javascript' src='/wp-includes/js/wp-util.min.js'></script>
<script type='text/javascript' src='/wp-includes/js/backbone.min.js'></script>
<script type='text/javascript'
        src='/wp-content/plugins/wp-events-manager/inc/libraries/owl-carousel/js/owl.carousel.min.js'></script>
<script type='text/javascript'
        src='/wp-content/plugins/wp-events-manager/inc/libraries/magnific-popup/js/jquery.magnific-popup.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/wp-events-manager/assets/js/frontend/events.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/eduma/assets/js/main.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/eduma/assets/js/smooth_scroll.min.js'></script>
<script type='text/javascript' src='/wp-content/themes/eduma/assets/js/custom-script-v2.js'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript'
        src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js?ver=3.0.22'></script>
<script type='text/javascript'>
    WebFont.load({google: {families: ['Roboto:300,300i,400,400i,500,500i,700,700i', 'Roboto Slab:300,400,700']}});
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var panelsStyles = {"fullContainer": "body"};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/siteorigin-panels/js/styling-292.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var lpGlobalSettings = {
        "url": null,
        "siteurl": "http:\/\/maths300.com",
        "ajax": "http:\/\/maths300.com",
        "theme": "eduma",
        "localize": {"button_ok": "OK", "button_cancel": "Cancel", "button_yes": "Yes", "button_no": "No"}
    };
    /* ]]> */
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var lpCheckoutSettings = {
        "ajaxurl": "http:\/\/maths300.com",
        "user_waiting_payment": null,
        "user_checkout": "",
        "i18n_processing": "Processing",
        "i18n_redirecting": "Redirecting",
        "i18n_invalid_field": "Invalid field",
        "i18n_unknown_error": "Unknown error",
        "i18n_place_order": "Place order"
    };
    /* ]]> */
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var lpProfileUserSettings = {
        "processing": "Processing",
        "redirecting": "Redirecting",
        "avatar_size": {"width": 150, "height": 150}
    };
    /* ]]> */
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var lpCourseSettings = [""];
    /* ]]> */
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var lpQuizSettings = [];
    /* ]]> */
</script>
<script type="text/javascript">document.body.className = document.body.className.replace("siteorigin-panels-before-js", "");</script>

<div id="thim-popup-login">
    <div class="thim-login-container has-shortcode" style="max-width: 700px;">
        <script>jQuery(".btn-mo").prop("disabled", false);</script>
        <script type="text/javascript">

            function mo_openid_on_consent_change(checkbox, value) {

                if (value == 0) {
                    jQuery('#mo_openid_consent_checkbox').val(1);
                    jQuery(".btn-mo").attr("disabled", true);
                    jQuery(".login-button").addClass("dis");
                }
                else {
                    jQuery('#mo_openid_consent_checkbox').val(0);
                    jQuery(".btn-mo").attr("disabled", false);
                    jQuery(".login-button").removeClass("dis");
                    //jQuery(".btn-mo").removeAttr("disabled");
                }
            }

            function moOpenIdLogin(app_name, is_custom_app) {
                var current_url = window.location.href;
                var cookie_name = "redirect_current_url";
                var d = new Date();
                d.setTime(d.getTime() + (2 * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cookie_name + "=" + current_url + ";" + expires + ";path=/";

                var base_url = 'http://maths300.com';
                var request_uri = '/demo-courses-hub/';
                var http = 'https://';
                var http_host = 'maths300.com';

                if (is_custom_app == 'false') {

                    if (request_uri.indexOf('wp-login.php') != -1) {
                        var redirect_url = base_url + '/?option=getmosociallogin&app_name=';


                    } else {
                        var redirect_url = http + http_host + request_uri;
                        if (redirect_url.indexOf('?') != -1) {
                            redirect_url = redirect_url + '&option=getmosociallogin&app_name=';

                        }
                        else {
                            redirect_url = redirect_url + '?option=getmosociallogin&app_name=';

                        }
                    }

                }
                else {

                    if (request_uri.indexOf('wp-login.php') != -1) {
                        var redirect_url = base_url + '/?option=oauthredirect&app_name=';

                    } else {
                        var redirect_url = http + http_host + request_uri;
                        if (redirect_url.indexOf('?') != -1)
                            redirect_url = redirect_url + '&option=oauthredirect&app_name=';
                        else
                            redirect_url = redirect_url + '?option=oauthredirect&app_name=';
                    }

                }

                window.location.href = redirect_url + app_name;

            }</script>
        <div class="thim-login" style="width: 100%"><h4 class="title">Login with your site account</h4>
            <form name="loginpopopform"
                  action="{{ url(config('adminlte.login_url', 'login')) }}"
                  method="post">
                {!! csrf_field() !!}
                <div class="error-box">
                </div>

                <p class="login-username">
                    <input
                            type="email" name="email"
                            placeholder="Email"
                            class="input required"
                            autocomplete="username"
                            value="{{ old('email') }}"/>
                </p>
                <p class="login-password">
                    <input
                            type="password"
                            name="password"
                            placeholder="Password"
                            class="input required"
                            autocomplete="current-password"
                            value=""
                            size="20"/>
                </p>

                <a class="lost-pass-link"
                   href="/password/reset"
                   title="Lost Password">Lost your
                    password?</a>

                <p class="forgetmenot login-remember">
                    <label
                            for="rememberMe"><input
                                name="rememberme"
                                type="checkbox"
                                id="rememberMe"
                                value="forever"/> Remember
                        Me
                    </label></p>
                <p class="submit login-submit">
                    <input
                            type="submit" name="wp-submit"
                            class="button button-primary button-large"
                            value="Login"/> <input
                            type="hidden"
                            name="redirect_to"/>
            </form>
        </div>
        <span class="close-popup"><i class="fa fa-times" aria-hidden="true"></i></span>
        <div class="cssload-container">
            <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
        </div>
    </div>
</div>
@yield('js')
</body>
</html>
