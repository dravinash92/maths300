@extends('layouts.fronthome')

@section('page-content')
<div id="listTeachers-content" class="myaccount-right">

        @if(count($errors))
        <div class="alert alert-danger message message-error">
            <strong>Whoops!</strong> There were some problems with your input.
            <br/>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <!-- form start -->
        <form id="userForm" role="form" method="post" action="{{ route('front.users.store') }}">
            {{ csrf_field() }}
            <div class="box-body">
                <input type="hidden" id="school" name="school" value="{{ Auth::user()->schools()->allRelatedIds()->toArray()[0] }}">
                
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name"
                           value="{{ old('name') }}">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email"
                           value="{{ old('email') }}">
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>

                <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label for="phone">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone"
                           value="{{ old('phone') }}">
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                </div>

                <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
                    <label for="position">Position</label>
                    <input type="text" class="form-control" id="position" name="position"
                           value="{{ old('position') }}">
                    <span class="text-danger">{{ $errors->first('position') }}</span>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                            class="ladda-label">Submit</span></button>
            </div>
        </form>
</div>
@stop

@push('js')
<script>
        jQuery(document).ready(function () {

        });
</script>
@endpush