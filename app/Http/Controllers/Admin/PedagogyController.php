<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pedagogy;
use Illuminate\Http\Request;
use App\Http\Requests\PedagogyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class PedagogyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Pedagogy::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['pedagogies'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.pedagogies.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pedagogies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PedagogyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PedagogyRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $pedagogy = new Pedagogy();
        $pedagogy->name = $validated['name'];

        $pedagogy->save();

        $request->session()->flash('alert-success', 'Pedagogy added successfully.');

        return redirect()->route('admin.pedagogies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pedagogy $pedagogy
     * @return \Illuminate\Http\Response
     */
    public function show(Pedagogy $pedagogy)
    {
        $arr['pedagogy'] = $pedagogy;

        return view('admin.pedagogies.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pedagogy $pedagogy
     * @return \Illuminate\Http\Response
     */
    public function edit(Pedagogy $pedagogy)
    {
        $arr['pedagogy'] = $pedagogy;
        return view('admin.pedagogies.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PedagogyRequest $request
     * @param  \App\Models\Pedagogy $pedagogy
     * @return \Illuminate\Http\Response
     */
    public function update(PedagogyRequest $request, Pedagogy $pedagogy)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $pedagogy->name = $validated['name'];

        $pedagogy->save();

        $request->session()->flash('alert-success', 'Pedagogy updated successfully.');

        return redirect()->route('admin.pedagogies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedagogy = Pedagogy::findOrFail($id);
        Log::info("delete pedagogy " . $id);
        
        $pedagogy->delete();

        return response()->json($pedagogy);
    }
}
