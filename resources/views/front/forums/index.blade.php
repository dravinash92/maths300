@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }

        @media only screen and (max-width: 480px) {

            #bbpress-forums ul li.bbp-forum-info {
                width: 35%;
            }

            #bbpress-forums ul li.bbp-forum-topic-count {
                width: 20%;
            }

            #bbpress-forums ul li.bbp-forum-reply-count {
                width: 20%;
            }

            #bbpress-forums ul li.bbp-forum-freshness {
                width: 25%;
            }
        }

        @media only screen and (max-width: 320px) {
            #forums-list-0 li.bbp-header li.bbp-forum-freshness {
                width: 50%;
            }

        }

    </style>

    <link rel='stylesheet' href='/wp-content/plugins/bbpress/templates/default/css/bbpress.css' type='text/css'
          media='all'/>
@stop

@extends('layouts.pageframe')

@section('top_heading')
    Community
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/forums" title="Community"><span itemprop="name">Community</span></a>
    </li>
@stop

@section('site-content')
    <div class="container site-content sidebar-right" style="transform: none;">
        <div class="row" style="transform: none;">
            <main id="main" class="site-main col-sm-9 alignleft">
                <article id="post-0"
                         class="post-0 forum type-forum status-publish hentry pmpro-no-access page type-page">
                    <div class="entry-content">
                        <div id="bbpress-forums">
                            <div class="bbp-search-form">
                                    <form name='forumSearchForm' id="bbp-search-form" role="search" method="get"
                                          action="{{ route('front.forums.index') }}">
                                        @csrf
                                    <div class="bbp-search-box">
                                        <label class="screen-reader-text hidden" for="bbp_search">Search for:</label>
                                        <input tabindex="101" type="text" value="{{ old('q') }}" placeholder="Search the forum"
                                               name="q" id="bbp_search">
                                        <input tabindex="102" class="button" type="submit" id="bbp_search_submit"
                                               value="" onclick="event.preventDefault();
                                                     document.getElementById('bbp-search-form').submit();">
                                    </div>
                                </form>
                            </div>

                            <ul id="forums-list-0" class="bbp-forums">
                                <li class="bbp-header">
                                    <ul class="forum-titles">
                                        <li class="bbp-forum-info">Forum</li>
                                        <li class="bbp-forum-topic-count">Topics</li>
                                        <li class="bbp-forum-reply-count">Replies</li>
                                        <li class="bbp-forum-freshness">Last Reply</li>
                                    </ul>
                                </li><!-- .bbp-header -->

                                <li class="bbp-body">
                                    @if(count($forums))
                                        @foreach($forums as $f)
                                        <ul id="bbp-forum-3136"
                                            class="loop-item-1 even bbp-forum-status-open bbp-forum-visibility-publish post-3136 forum type-forum status-publish hentry pmpro-has-access">
                                            <li class="bbp-forum-info">
                                                <a class="bbp-forum-title"
                                                   href="{{ route('front.topics.index',['q_forum'=>$f->id]) }}">
                                                    {{ $f->title }}</a>
                                                <div class="bbp-forum-content">
                                                    {{ $f->description? $f->description : "" }}
                                                </div>
                                            </li>
                                            <li class="bbp-forum-topic-count">{{ $f->getTopicsCount() }}</li>
                                            <li class="bbp-forum-reply-count">{{ $f->getRepliesCount() }}</li>
                                            <li class="bbp-forum-freshness">
                                                <a href="#"
                                                   title="test topic">
                                                    {{ $f->getLastReplyDateDiffForHumans() }}
                                                </a>
                                                <p class="bbp-topic-meta">
                                                    <span class="bbp-topic-freshness-author">
                                                        {{ $f->getLastReplier() }}
                                                    </span>
                                                </p>
                                            </li>
                                        </ul>
                                        @endforeach
                                    @else
                                        <ul id="bbp-forum-3136"
                                            class="loop-item-1 even bbp-forum-status-open bbp-forum-visibility-publish post-3136 forum type-forum status-publish hentry pmpro-has-access">
                                            <li class="bbp-forum-info">
                                                No data available.
                                            </li>
                                        </ul>
                                    @endif
                                </li><!-- .bbp-body -->
                                <li class="bbp-footer">
                                    <div class="tr">
                                        <p class="td colspan4">&nbsp;</p>
                                    </div><!-- .tr -->
                                </li><!-- .bbp-footer -->
                            </ul><!-- .forums-directory -->

                            <div class="bbp-pagination">
                                <div class="bbp-pagination-links">
                                    {{ $forums->links() }}
                                </div>
                            </div>
                        </div>
                    </div><!-- .entry-content -->
                </article><!-- #post-## -->
            </main>
        </div>
    </div>
@stop