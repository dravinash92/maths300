@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.settings.index') }}">Settings</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Settings</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="settingForm" role="form" method="post"
                  action="{{ route('admin.settings.store') }}">
                @csrf
                <div class="box-body">
                    @foreach($settings as $s)
                        <div class="{{ $s->name }}-group {{ $errors->has('forum') ? 'has-error' : '' }}">
                            <label for="{{ $s->name }}">{{ $s->name }}</label>
                            <input type="hidden" id="{{ $s->name }}" name="{{ $s->name }}_id"
                                   value="{{ old($s->id, $s->id) }}">
                            <input type="text" class="form-control" id="{{ $s->name }}" name="{{ $s->name }}"
                                   value="{{ old($s->name, $s->value) }}">
                            <span class="text-danger"></span>
                        </div>
                        <br>
                    @endforeach
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Save</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>
@endpush