<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ModSchoolSizeToSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription_plans', function (Blueprint $table) {
            $table->integer('is_visible')->after('total_fee')->default(1); // 0: FALSE 1: TRUE
            $table->dropForeign('subscription_plans_school_size_id_foreign');
            $table->foreign('school_size_id')->references('id')->on('school_size_teachers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_plans', function (Blueprint $table) {
            $table->dropColumn('is_visible');
            $table->dropForeign('subscription_plans_school_size_id_foreign');
            $table->foreign('school_size_id')->references('id')->on('school_size_students');
        });
    }
}
