<?php

namespace App\Http\Controllers\Admin;

use App\Models\School;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\DB;
use Auth;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users.
    |
    */
    use SendsPasswordResetEmails;

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['schools'] = data4Select(School::class);
        $roles = ['school-admin', 'school-finance-officer', 'school-teacher'];
        $query = User::query()->role($roles);

        if ($request->has('q_school') && $request->q_school != null) {
            Log::info("user search_keyword: " . $request->q_school);
            $query->school($request->q_school);
        } else {
            $query->school();
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("user search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        if($request->has('sortBy')) {   // Do sorting...
            if($request->sortBy=='last_login') { // Sort by last login
                if($request->has('order') && $request->order=='asc') {
                    $query->orderBy('last_login', 'asc');
                } else {
                    $query->orderBy('last_login', 'desc');
                }
            } else if($request->sortBy=='name') {
                if($request->has('order') && $request->order=='desc') {
                    $query->orderBy('name', 'desc');
                } else {
                    $query->orderBy('name', 'asc');
                }
            } else if($request->sortBy=='school') {
                 // todo
            } else if($request->sortBy=='email') {
                if($request->has('order') && $request->order=='desc') {
                    $query->orderBy('email', 'desc');
                } else {
                    $query->orderBy('email', 'asc');
                }
            } else if($request->sortBy=='phone') {
                if($request->has('order') && $request->order=='desc') {
                    $query->orderBy('phone', 'desc');
                } else {
                    $query->orderBy('phone', 'asc');
                }
            } else if($request->sortBy=='status') {
                if($request->has('order') && $request->order=='desc') {
                    $query->orderBy('status', 'desc');
                } else {
                    $query->orderBy('status', 'asc');
                }
            }
        }
        //  Log::info("order: " . $request->sortBy);


        // not archived
//        $query->statusNot(3);
      
        $paginator = $query->paginate(config('app.page_number'));

        if ($request->has('q_school')) {
            $paginator->appends(array('q_school' => $request->q_school));
        } else {
            $paginator->appends(array('q_school' => $request->q_school));
        }
        if ($request->has('q') && $request->q !== null && $request->q !=="") {
            $paginator->appends(array('q' => $request->q));
        }

        $arr['users'] = $paginator;
        $arr['sortby'] = $request->sortBy;
        $arr['order']= $request->order;
        $arr['q']=$request->q;
        $arr['q_school']= $request->q_school;

        // allow using Input::old() function in view to access the data.
        $request->flash();
        Log::info("order: " . json_encode($arr));
        return view('admin.users.index')->with($arr);
    }

    /**
     * Display a listing of archived users.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showArchived(Request $request)
    {
        $arr['schools'] = data4Select(School::class);
        $roles = ['school-admin', 'school-finance-officer', 'school-teacher'];
        $query = User::query()->role($roles);

        if ($request->has('q_school') && $request->q_school != null) {
            $query->school($request->q_school);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        // only show status: archived
        $query->status(3);
        $arr['users'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.users.showArchived')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['schools'] = data4Select(School::class);
        return view('admin.users.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\UserRequest $request
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function store(UserRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        try {
            User::createWithRoles($validated, School::find($validated['school']), 'school-teacher');
            $request->session()->flash('alert-success', 'User added successfully.');
            return redirect()->route('admin.users.index');
        } catch (\Exception $e) {
            Log::error("Create user caught an exception: " . $e->getMessage());
            // allow using Input::old() function in view to access the data.
            $request->flash();
            return back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $arr['user'] = $user;
        Log::info("show user id: " . $user->id . ", name: " . $user->name);

        return view('admin.users.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $arr['schools'] = data4Select(School::class);
        $arr['user'] = $user;
        return view('admin.users.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UserRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function update(UserRequest $request, User $user)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->phone = $validated['phone'];
        $user->position = !empty($validated['position']) ? $validated['position'] : "";
        // when updating email, update reset token as well
        $user->reset_token = Password::getRepository()->create($user);

        DB::transaction(function () use ($user, $validated) {
            $user->save();
            // get users schools
            $current_school = $user->schools[0];
            $current_school_id = $current_school->id;

            $new_school_id = $validated['school'];
            Log::info("user current school: " . $current_school_id);
            Log::info("user new school: " . $new_school_id);

            if ($new_school_id != $current_school_id) {
                if ($user->hasRole('school-finance-officer')) {
                    Log::warning("school-finance-officer change school: " . $current_school_id);
                    Log::warning("school " . $current_school_id . " remove financeOfficer");
                    $current_school->financeOfficer()->associate(null)->save();
                    $user->removeRole('school-finance-officer');
                    $user->assignRole('school-teacher');
                }
                if ($user->hasRole('school-admin')) {
                    Log::warning("school-admin change school: " . $current_school_id);
                    Log::warning("school " . $current_school_id . " remove coordinator");
                    $current_school->coordinator()->associate(null)->save();
                    $user->removeRole('school-admin');
                    $user->assignRole('school-teacher');
                }
            }

            $user->schools()->sync([$new_school_id]);
            Log::info("update user " . $user->id);
        });

        $request->session()->flash('alert-success', 'User updated successfully.');

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->hasRole('school-finance-officer')) {
            return response()->json(['error' => 'School finance officer can not be deleted'], 400);
        }
        if ($user->hasRole('school-admin')) {
            return response()->json(['error' => 'School coordinator can not be deleted'], 400);
        }
        Log::info("delete user " . $user->id);
        $user->delete();

        return response()->json($user);
    }

    /**
     * Change status to active.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        $id = $request->id_activate;

        $user = User::findOrFail($id);
        $user->activate();

        $request->session()->flash('alert-success', 'User activated successfully.');

        return redirect()->route('admin.users.index');
    }

    /**
     * Change status to suspended.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function suspend(Request $request)
    {
        $id = $request->id_suspend;

        $user = User::findOrFail($id);
        $user->suspend();

        $request->session()->flash('alert-success', 'User suspended successfully.');

        return redirect()->route('admin.users.index');
    }


    /**
     * Change status to archived.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request)
    {
        $id = $request->id_archive;

        $user = User::findOrFail($id);
        Log::info("archive user " . $id);
        $user->archive();

        $request->session()->flash('alert-success', 'User archived successfully.');

        return redirect()->route('admin.users.index');
    }

    /**
     * Change status to inactived.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function reinstate(Request $request)
    {
        $id = $request->id_reinstate;

        $user = User::findOrFail($id);
        Log::info("reinstate user " . $id);
        $user->reinstate();

        $request->session()->flash('alert-success', 'User reinstated successfully.');

        return redirect()->route('admin.users.index');
    }

    /**
     * sendResetPassEmail
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetPassEmail(Request $request)
    {
        $email = $request->email;

        $user = User::where('email', $email)->first();
        Log::info("sendResetPassEmail user " . $email);
        $user->sendResetPassEmail();

        return response()->json(['status' => 0, 'data' => 'success'], 200);
    }

    /**
     * Change changePassReq
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changePassReq(Request $request)
    {
        return view('admin.users.changepassword');
    }

    /**
     * Change changePass
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
            'new_password_confirmation' => 'required|string|min:6|same:new_password',
        ]);

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return back()->withErrors('Your current password does not matches with the password you provided.');
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            //Current password and new password are same
            return back()->withErrors('New Password cannot be same as your current password. Please choose a different password.');
        }

        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($validatedData['new_password']);
        $user->save();

        $request->session()->flash('alert-success', 'Password changed successfully.');

        return redirect()->back();
    }
}
