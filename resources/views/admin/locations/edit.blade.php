@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.locations.index') }}">Locations</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Location</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="locationForm" role="form" method="post" action="{{ route('admin.locations.update', $location->id) }}">
                    @method('PUT')
                    @csrf
                <div class="box-body">
                    <input type="hidden" id="id" name="id" value="{{ old('id', $location->id) }}">
                    <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                        <label for="country">Country</label>
                        <select data-placeholder="Select a country" class="form-control select2_country"
                                style="width: 100%;" id="country" name="country">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('country') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name"
                               value="{{ old('name', $location->name) }}">
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            let select2_country = $('.select2_country').select2({
                allowClear: true,
                data: {!! json_encode($countries)!!}
            });

            @if(old('country'))
            select2_country.val({!! old('country') !!}).trigger('change');
            @else
            select2_country.val({!! $location->country->id !!}).trigger('change');
            @endif

            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>
@endpush