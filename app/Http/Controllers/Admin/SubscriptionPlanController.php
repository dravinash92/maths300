<?php

namespace App\Http\Controllers\Admin;

use App\Models\SubscriptionPlan;
use App\Models\SchoolSizeTeacher;
use Illuminate\Http\Request;
use App\Http\Requests\SubscriptionPlanRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;
use DB;

class SubscriptionPlanController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SubscriptionPlan::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['subscriptionplans'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.subscriptionplans.index')->with($arr);
    }

    /**
     * build json response data for select2 component
     * @param $school_size_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function select($school_size_id)
    {
        Log::info("select subscription plan, school_size_id " . $school_size_id);
        $subscriptionplan_select = SubscriptionPlan::data4SelectWithSchoolTeacherSize($school_size_id);
        return response()->json($subscriptionplan_select);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['schoolSizeTeachers'] = data4Select(SchoolSizeTeacher::class);
        return view('admin.subscriptionplans.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SubscriptionPlanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriptionPlanRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $subscriptionplan = new SubscriptionPlan();
        $subscriptionplan->joining_fee = $validated['joining_fee'];
        $subscriptionplan->annual_fee = $validated['annual_fee'];
        $subscriptionplan->total_fee = $validated['total_fee'];
        $subscriptionplan->is_visible = $validated['is_visible'];

        if(!empty($validated['remark'])) {
            $subscriptionplan->remark = $validated['remark'];
        }

        DB::transaction(function () use ($subscriptionplan, $validated) {
            $subscriptionplan->save();

            $schoolSizeTeacher = SchoolSizeTeacher::find($validated['schoolSizeTeacher']);
            $subscriptionplan->schoolSize()->associate($schoolSizeTeacher)->save();

            Log::info("create subscription " . $subscriptionplan->id);
        });

        $request->session()->flash('alert-success', 'Subscription plan added successfully.');

        return redirect()->route('admin.subscriptionplans.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubscriptionPlan $subscriptionplan
     * @return \Illuminate\Http\Response
     */
    public function show(SubscriptionPlan $subscriptionplan)
    {
        $arr['subscriptionplan'] = $subscriptionplan;

        return view('admin.subscriptionplans.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubscriptionPlan $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(SubscriptionPlan $subscriptionplan)
    {
        $arr['subscriptionplan'] = $subscriptionplan;
        $arr['schoolSizeTeachers'] = data4Select(SchoolSizeTeacher::class);
        return view('admin.subscriptionplans.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SubscriptionPlanRequest $request
     * @param  \App\Models\SubscriptionPlan $subscriptionplan
     * @return \Illuminate\Http\Response
     */
    public function update(SubscriptionPlanRequest $request, SubscriptionPlan $subscriptionplan)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $subscriptionplan->joining_fee = $validated['joining_fee'];
        $subscriptionplan->annual_fee = $validated['annual_fee'];
        $subscriptionplan->total_fee = $validated['total_fee'];
        $subscriptionplan->is_visible = $validated['is_visible'];

        if(!empty($validated['remark'])) {
            $subscriptionplan->remark = $validated['remark'];
        } else {
            $subscriptionplan->remark = '';
        }

        DB::transaction(function () use ($subscriptionplan, $validated) {
            $subscriptionplan->save();

            $schoolSizeTeacher = SchoolSizeTeacher::find($validated['schoolSizeTeacher']);
            $subscriptionplan->schoolSize()->associate($schoolSizeTeacher)->save();

            Log::info("update subscription " . $subscriptionplan->id);
        });

        $request->session()->flash('alert-success', 'Subscription plan updated successfully.');

        return redirect()->route('admin.subscriptionplans.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscriptionplan = SubscriptionPlan::findOrFail($id);
        Log::info("delete subscriptionplan " . $id);

        if ($subscriptionplan->schools()->count()) {
            return response()->json(['error' => 'Delete operation is not allowed as schools associated with this subscription plan exist'],400);
        }

        $subscriptionplan->delete();

        return response()->json($subscriptionplan);
    }
}
