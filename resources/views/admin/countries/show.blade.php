@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="{{ route('admin.countries.index') }}">Countries</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Country Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <h4>Name</h4>
            {{ $country->name }}
            <hr>

            <h4>Tax Rate</h4>
            {{ $country->tax_rate }}
            <hr>

            <h4>Updated at</h4>
            {{ $country->updated_at }}
            <hr>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.countries.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
@endpush