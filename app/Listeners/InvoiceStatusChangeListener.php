<?php

namespace App\Listeners;

use App\Events\InvoiceStatusChange;
use App\Models\Invoice;
use App\Models\Setting;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class InvoiceStatusChangeListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    protected function sendMails($invoice)
    {
        $mailClass = '';
        if ($invoice->status == 1) {
            $mailClass = '\App\Mail\InvoiceSent';
        }
        if ($invoice->status == 2) {
            $mailClass = '\App\Mail\InvoicePaid';
        }
        if ($invoice->status == 4) {
            $mailClass = '\App\Mail\InvoiceCancel';
        }

        if(!empty($mailClass)) {
            Log::info("InvoiceStatusChangeListener is_single_account: ".$invoice->to->is_single_account);
            // single account, only send to coordinator
            if ($invoice->to->is_single_account != 1) {
                // send invoices to school finance officer
                Mail::to($invoice->to->financeOfficer)
                    ->send(new $mailClass($invoice, $invoice->to->financeOfficer->name));
            }

            // send invoices to school coordinator
            Mail::to($invoice->to->coordinator)
                ->send(new $mailClass($invoice, $invoice->to->coordinator->name));

            // cc invoices to AAMT finance officer
            // Mail::to(Setting::get('Finance officer email'))
            //     ->send(new $mailClass($invoice, Setting::get('Finance officer name')));
        }
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceStatusChange $event
     * @return void
     */
    public function handle(InvoiceStatusChange $event)
    {
        Log::info("InvoiceStatusChangeListener fired");
        $invoice = $event->invoice;
        Invoice::genPDF($invoice);
        Log::info("InvoiceStatusChangeListener sendMails");
        $this->sendMails($invoice);

        Log::info("InvoiceStatusChangeListener invoice->status: ".$invoice->status);
        // sent
        if ($invoice->status == 1) {
            // inactive
            if ($invoice->to->status == 0) {
                $invoice->to->activate();
            }
        }

        // paid
        if ($invoice->status == 2) {
            // inactive
            if ($invoice->to->status == 0) {
                $invoice->to->activate();
            }
            // suspended
            if ($invoice->to->status == 2) {
                $invoice->to->activate();
            }
        }
    }
}
