@extends('layouts.email')

@section('content')
    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        Hi {{ $user->name }},
        <br><br>
        We're thrilled you’re on this Maths300 journey with us. We'd love to learn about your experience.
        <br><br>
        Please take a moment to let us know
        <br><br>
        a. how well does Maths300 meet your needs, and
        <br><br>
        b. which three lessons have you found most helpful.
        <br><br>
	<p>We would also like your opinion on three most important features you’d like to see in Maths300.</p>
    <p>Please reply to this email with your feedback and help us improve.
        <br><br>
    Thank you.</p>
    </div>
    
@stop
