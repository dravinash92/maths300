<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SubscriptionPageRequest;
use App\Models\SubscriptionPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;

class SubscriptionPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['subscriptionpage'] = SubscriptionPage::first();
        return view('admin.subscriptionpage.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SubscriptionPageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriptionPageRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $subscriptionPage = SubscriptionPage::findOrFail($request->id);
        $subscriptionPage->content = $validated['content'];
        $subscriptionPage->save();

        $request->session()->flash('alert-success', 'Content saved successfully.');

        return redirect()->route('admin.subscriptionpage.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubscriptionPage  $subscriptionPage
     * @return \Illuminate\Http\Response
     */
    public function show(SubscriptionPage $subscriptionPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubscriptionPage  $subscriptionPage
     * @return \Illuminate\Http\Response
     */
    public function edit(SubscriptionPage $subscriptionPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubscriptionPage  $subscriptionPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubscriptionPage $subscriptionPage)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubscriptionPage  $subscriptionPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubscriptionPage $subscriptionPage)
    {
        //
    }
}
