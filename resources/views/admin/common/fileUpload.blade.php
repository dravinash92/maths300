<!-- Modal form to upload a file -->
<div id="fileUploadModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div id="form-errors"></div>
                <!-- form start -->
                <form class="form-horizontal" role="form" id="form_upload_file">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8">
                            <label for="file_upload" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i>Select
                                file</label>

                            <span id="fileName_holder"></span>

                            <button type="button" class="btn btn-warning edit" id="cropper_btn"
                                    style="display: none">
                                <span id="" class='glyphicon glyphicon-edit'></span> Crop
                            </button>

                            <input type="hidden" id="content_id" name="content_id"/>
                            <input type="hidden" id="category" name="category"/>
                            <input type="file" id="file_upload" name="file" style="display: none">
                            <span class="text-danger" id="response"></span>
                            <div id="img_holder"></div>
                        </div>
                    </div><!-- /.row -->
                </form>

                <div class="modal-footer">
                    <div id='loader'><img src="{{ asset('images/loading.gif') }} " style="width:60px;"></div>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary submit"></button>
                </div>
            </div>
        </div>
    </div>
</div>