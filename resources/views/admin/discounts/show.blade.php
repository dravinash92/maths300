@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.discounts.index') }}">Discounts</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Location Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <h4>Discount Type</h4>
            {{ $discount->discountType->name }}
            <hr>

            <h4>Name</h4>
            {{ $discount->name }}
            <hr>

            <h4>Value</h4>
            {{ $discount->value }}
            <hr>

            <h4>Updated at</h4>
            {{ $discount->updated_at }}
            <hr>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.discounts.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
@endpush