@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }

        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        table thead tr {
            background-color: #78BE20;
            color: #333;
        }

        .btn-subscription:hover {
            background-color: #78BE20;
            border-color: #78BE20;
        }

    </style>
@stop

@extends('layouts.page')

@section('top_heading')
    Subscribe
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/subscribe"><span itemprop="name">Subscribe</span></a>
    </li>
@stop

@section('content')
        <article id="post-87" class="post-87 page type-page status-publish hentry pmpro-has-access">
            <div class="entry-content">
                <div id="pl-87" class="panel-layout">
                    Your application is submitted and will be processed by administrator up to 3 business days.
                </div>
                <br>
                <div class="col-xs-12 text-left">
                    <a href="/" class="btn btn-primary btn-subscription">Back to Home</a>
                </div>

            </div>
        </article>
@stop