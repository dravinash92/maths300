@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.topics.index') }}">Topics</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add Topic</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="topicForm" role="form" method="post" action="{{ route('admin.topics.update', $topic->id) }}">
                @method('PUT')
                @csrf
                <div class="box-body">
                    <!-- select forum first -->
                    <div class="form-group {{ $errors->has('forum') ? 'has-error' : '' }}">
                        <label for="forum">Forum</label>
                        <select data-placeholder="Select a forum" class="form-control select2_forum"
                                style="width: 100%;" id="forum" name="forum">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('forum') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title"
                               value="{{ old('title', $topic->title) }}">
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                        <label for="content">Content</label>
                        <textarea class="form-control " id="content"
                                  name="content" rows="3">{{ old('content', $topic->content) }}</textarea>
                        <span class="text-danger">{{ $errors->first('content') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('sticky') ? 'has-error' : '' }}">
                        <label for="sticky">Sticky</label><br>
                        <div>
                            <input class="minimal" type="radio" id="sticky_yes"
                                   name="sticky"
                                   value="1" {{ old('sticky', $topic->sticky) == '1' ? 'checked' : '' }}>
                            <label for="sticky_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="sticky_no"
                                   name="sticky"
                                   value="0" {{ old('sticky', $topic->sticky) == '0' ? 'checked' : '' }}>
                            <label for="sticky_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('sticky') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('visibility') ? 'has-error' : '' }}">
                        <label for="visibility">Visibility</label><br>
                        <div>
                            <input class="minimal" type="radio" id="visibility_yes"
                                   name="visibility"
                                   value="1" {{ old('visibility', $topic->visibility) == '1' ? 'checked' : '' }}>
                            <label for="visibility_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="visibility_no"
                                   name="visibility"
                                   value="0" {{ old('visibility', $topic->visibility) == '0' ? 'checked' : '' }}>
                            <label for="visibility_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('visibility') }}</span>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            // Initialize Select2 elements
            let select2_forum = $('.select2_forum').select2({
                allowClear: true,
                data: {!! json_encode($forums)!!}
            });

            @if(old('forum'))
            select2_forum.val({!! old('forum') !!}).trigger('change');
            @else
            select2_forum.val({!! $topic->forum->id !!}).trigger('change');
            @endif

            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>
@endpush