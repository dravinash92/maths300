@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Change Password</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="changePassForm" role="form" method="post" action="{{ route('admin.users.changePassword') }}">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                        <label for="current_password">Current Password</label>
                        <input type="password" class="form-control" id="current_password" name="current_password"
                               value="{{ old('current_password') }}">
                        <span class="text-danger">{{ $errors->first('current_password') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
                        <label for="new_password">New Password</label>
                        <input type="password" class="form-control" id="new_password" name="new_password"
                               value="{{ old('new_password') }}">
                        <span class="text-danger">{{ $errors->first('new_password') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('new_password_confirmation') ? 'has-error' : '' }}">
                        <label for="new_password_confirmation">Confirm New Password</label>
                        <input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation"
                               value="{{ old('new_password_confirmation') }}">
                        <span class="text-danger">{{ $errors->first('new_password_confirmation') }}</span>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <script>
        $(document).ready(function () {
            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>
@endpush