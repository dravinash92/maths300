<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Country;

class CountryTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('countries')->delete();

        Country::create([
            'name' => 'Australia',
            // (GST) in Australia is a value added tax of 10% on most goods and services sales
            'tax_rate' => 0.1,
        ]);

        Country::create([
            'name' => 'New Zealand',
        ]);

        Country::create([
            'name' => 'NotFound',
        ]);
    }

}