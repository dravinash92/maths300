@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.subscriptionplans.index') }}">Subscription Plans</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add Subscription Plan</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="subscriptionplanForm" role="form" method="post" action="{{ route('admin.subscriptionplans.store') }}">
                @csrf
                <div class="box-body">
                    <div class="form-group {{ $errors->has('schoolSizeTeacher') ? 'has-error' : '' }}">
                        <label for="schoolSizeTeacher">School Size</label>
                        <select data-placeholder="Select a category" class="form-control select2_schoolSizeTeacher"
                                style="width: 100%;" id="schoolSizeTeacher" name="schoolSizeTeacher">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('schoolSizeTeacher') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('joining_fee') ? 'has-error' : '' }}">
                        <label for="joining_fee">Joining Fee</label>
                        <input type="text" class="form-control" id="joining_fee" name="joining_fee"
                                  value="{{ old('joining_fee') }}">
                        <span class="text-danger">{{ $errors->first('joining_fee') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('annual_fee') ? 'has-error' : '' }}">
                        <label for="annual_fee">Annual Fee</label>
                        <input type="text" class="form-control" id="annual_fee" name="annual_fee"
                               value="{{ old('annual_fee') }}">
                        <span class="text-danger">{{ $errors->first('annual_fee') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('total_fee') ? 'has-error' : '' }}">
                        <label for="total_fee">Total Fee</label>
                        <input type="text" class="form-control" id="total_fee" name="total_fee"
                               value="{{ old('total_fee') }}" placeholder="Click to calculate the total" readonly>
                        <span class="text-danger">{{ $errors->first('total_fee') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('is_visible') ? 'has-error' : '' }}">
                        <label for="is_visible">Visible</label><br>
                        <div>
                            <input class="minimal" type="radio" id="is_visible_yes"
                                   name="is_visible"
                                   value="1" {{ old('is_visible') == '1' ? 'checked' : '' }}>
                            <label for="is_visible_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="is_visible_no"
                                   name="is_visible"
                                   value="0" {{ old('is_visible', 0) == '0' ? 'checked' : '' }}>
                            <label for="is_visible_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('is_visible') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('remark') ? 'has-error' : '' }}">
                        <label for="remark">Remark</label>
                        <input type="text" class="form-control" id="remark" name="remark"
                               value="{{ old('remark') }}">
                        <span class="text-danger">{{ $errors->first('remark') }}</span>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            let select2_schoolSizeTeacher = $('.select2_schoolSizeTeacher').select2({
                allowClear: true,
                data: {!! json_encode($schoolSizeTeachers)!!}
            });

            @if(old('schoolSizeTeacher'))
            select2_schoolSizeTeacher.val({!! old('schoolSizeTeacher') !!}).trigger('change');
            @endif

            // calculate total fee automatically
            $( "#total_fee" ).click(function() {
                if( $('#joining_fee').val() && $('#annual_fee').val()) {                      //if it is blank.
                    $('#total_fee').val(Number($('#joining_fee').val()) + Number($('#annual_fee').val()));
                }
            });

            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>
@endpush