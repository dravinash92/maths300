@extends('layouts.master')

@section('body')
{{--<body class="archive search search-results post-type-archive post-type-archive-lp_course eduma learnpress learnpress-page woocommerce-account woocommerce-js thim-body-preload bg-boxed-image wpb-js-composer js-comp-ver-5.6.1 vc_responsive"--}}
<body class="archive post-type-archive post-type-archive-forum eduma learnpress learnpress-page woocommerce-js thim-body-preload bg-boxed-image wpb-js-composer js-comp-ver-5.6.1 vc_responsive forum-archive bbpress woocommerce-page @yield('body-css')"
      id="thim-body">

<div id="preload">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>

<div id="wrapper-container" class="wrapper-container">
    <div class="content-pusher">
        @component('layouts.header')
        @endcomponent
        <div id="main-content">
            <section class="content-area">
                <div class="top_heading  _out">
                    <div class="top_site_main "
                         style="color: #ffffff;background-image:url({{ asset('images/bg-page.jpg') }});">
                        <span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
                        <div class="page-title-wrapper">
                            <div class="banner-wrapper container">
                                <h1>@yield('top_heading')</h1></div>
                        </div>
                    </div>
                    <div class="breadcrumbs-wrapper">
                        <div class="container">
                            <ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList"
                                id="breadcrumbs" class="breadcrumbs">
                                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a
                                            itemprop="item" href="{{ config('app.url') }}" title="Home"><span
                                                itemprop="name">Home</span></a></li>
                                @yield('breadcrumbs')

                                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"></li>
                            </ul>
                        </div>
                    </div>
                </div>

                @yield('site-content')
            </section>

            @component('layouts.footer')
            @endcomponent
        </div><!--end main-content-->


    </div><!-- end content-pusher-->

    <a href="#" id="back-to-top">
        <i class="fa fa-chevron-up" aria-hidden="true"></i>
    </a>


</div><!-- end wrapper-container -->

<!-- Memberships powered by Paid Memberships Pro v1.9.5.6.
-->
<div class="gallery-slider-content"></div>
@stop
