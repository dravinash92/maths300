<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use Log;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // published
        $match = ['id' => $id, 'status' => 1];
        $ad = Advertisement::where($match)->first();

        if (!empty($ad)) {
            $ad->pv += 1;
            $ad->save();
            Log::info("show advertisement " . $ad->id);
        } else {
            abort(404);
        }

        $arr['ad'] = $ad;

        return view('front.ads.show')->with($arr);
    }
}
