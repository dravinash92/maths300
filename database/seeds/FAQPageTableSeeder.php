<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\FAQPage;

class FAQPageTableSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('faqs')->delete();

        FAQPage::create([
            'content' =>
                "<p>Please check our frequent asked questions.</p>",
        ]);

    }
}