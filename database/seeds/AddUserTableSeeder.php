<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Spatie\Permission\Models\Role;

class AddUserTableSeeder extends Seeder {

    public function run()
    {
        $admin = User::create([
            'name' => 'Maths300 Admin',
            'email' => 'superadmin@aamt.edu.au',
            'email_verified_at' => now(),
            'password' => Hash::make('Maths300@2020'),
            'status' => 1, // 1:enabled, 0:disabed
            'remember_token' => str_random(10),
        ]);
        $admin->assignRole('super-admin');
    }
}
