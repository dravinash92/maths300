@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/bootstrap-daterangepicker/daterangepicker.css')}}">
    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }
        .filter-class{
            display: flex;
            align-items: center;
        }
        .download-btn{
            text-align: center;
            padding: 20px 0px 10px 0px;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.reports.invoiceindex') }}">Invoices report</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Invoices report</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">

            <form id="invoiceReportForm" role="form" method="post" action="{{ route('admin.reports.getinvoices') }}">
                {{ csrf_field() }}
                <div class="row filter-class">
                    <div class="col-xs-2">
                        <label>Filter By</label>
                    </div>
                    <div class="col-xs-4">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="q_due_date_range" name="q_due_date_range" value="{{ old('q_due_date_range') }}" placeholder="Enter Due date range">
                        </div>
                    </div>
                    <!-- Date range -->
                      <div class="col-xs-4">
                          <select class="form-control" name="q_due_status" id="q_due_status">
                            <option value="">Select Status</option>
                            <option value="1">Awaiting payment</option>
                            <option value="2">Paid</option>
                            <option value="3">Overdue</option>
                            <option value="4">Cancelled</option>
                          </select>
                        </div>
                        <!-- /.input group -->
                      </div>
                      <div class="col-xs-12 download-btn">
                        <button class="btn btn-primary btn-flat" type="submit">Download report</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset ('vendor/adminlte/vendor/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset ('vendor/adminlte/vendor/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script>
        $(document).ready(function () {
            //Date range picker
            $('#q_due_date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD/MM/YYYY'
                }
            });

            $('#q_due_date_range').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });

            $('#q_due_date_range').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
            });
    </script>
@endpush