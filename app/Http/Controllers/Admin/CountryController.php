<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Requests\CountryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class CountryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Country::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['countries'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.countries.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CountryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $country = new Country();
        $country->name = $validated['name'];

        $country->save();

        $request->session()->flash('alert-success', 'Country added successfully.');

        return redirect()->route('admin.countries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        $arr['country'] = $country;

        return view('admin.countries.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Country $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        $arr['country'] = $country;
        return view('admin.countries.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CountryRequest $request
     * @param  \App\Models\Country $country
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, Country $country)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $country->name = $validated['name'];

        $country->save();

        $request->session()->flash('alert-success', 'Country updated successfully.');

        return redirect()->route('admin.countries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        Log::info("delete country " . $id);
        if ($country->locations()->count()) {
            return response()->json(['error' => 'Delete country is not allowed as locations associated with this country exist'],400);
        }
        
        $country->delete();

        return response()->json($country);
    }
}
