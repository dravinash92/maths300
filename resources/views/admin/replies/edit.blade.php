@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.replies.index') }}">Replies</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Reply</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
                <form id="replyForm" role="form" method="post" action="{{ route('admin.replies.update', $reply->id) }}">
                    @method('PUT')
                    @csrf
                <div class="box-body">
                    <!-- select topic first -->
                    <div class="form-group {{ $errors->has('topic') ? 'has-error' : '' }}">
                        <label for="topic">Topic</label>
                        <select data-placeholder="Select a topic" class="form-control select2_topic"
                                style="width: 100%;" id="topic" name="topic">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('topic') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                        <label for="content">Content</label>
                        <textarea class="form-control " id="content"
                                  name="content" rows="3">{{ old('content', $reply->content) }}</textarea>
                        <span class="text-danger">{{ $errors->first('content') }}</span>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            // Initialize Select2 elements
            let select2_topic = $('.select2_topic').select2({
                allowClear: true,
                data: {!! json_encode($topics)!!}
            });

            @if(old('topic'))
            select2_topic.val({!! old('topic') !!}).trigger('change');
            @else
            select2_topic.val({!! $reply->topic->id !!}).trigger('change');
            @endif

            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>
@endpush