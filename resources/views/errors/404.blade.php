@extends('layouts.pageframe')

@section('css')
    <style type="text/css" media="all" id="siteorigin-panels-layouts-head">/* Layout 2958 */
        .page-404-content {
            padding: 0px 0px;
        }
    </style>
@stop

@section('top_heading')
    Error
@stop

@section('breadcrumbs')
@stop

@section('site-content')
    <div class="container site-content">
        <div class="row">
            <main id="main" class="site-main col-sm-12 full-width">
                <section class="error-404 not-found">
                    <div class="page-404-content">
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="{{ asset('images/404.jpg') }}" alt="404-page">
                            </div>
                            <div class="col-xs-6 text-left">
                                <h2>404 <span class="thim-color">Error!</span></h2>
                                <p>Sorry, we can't find the page you are looking for. Please go to <a
                                            href="/" class="thim-color">Home.</a></p>
                            </div>
                        </div>
                    </div>
                    <!-- .page-content -->
                </section>
            </main>
        </div>
    </div>
@stop