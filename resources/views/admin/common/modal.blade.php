<!-- Modal to {{ $action }} a {{ $model }} -->
<div id="{{ $action }}Modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center">Are you sure you want to {{ $action }} this {{ $model }}?</h3>
                <br/>
                <!-- form start -->
                <form class="form-horizontal" role="form" id="form_{{ $action }}" method="post"
                      action="{{ isset($form_action)? $form_action: "" }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="id">ID:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id_{{ $action }}" name="id_{{ $action }}"
                                   readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name_{{ $action }}" name="name_{{ $action }}"
                                   readonly="readonly">
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger {{ $action }}" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-alert'></span> {{ ucfirst($action) }}
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>