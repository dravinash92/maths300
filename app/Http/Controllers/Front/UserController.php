<?php

namespace App\Http\Controllers\Front;

use App\Models\School;
use App\Models\User;
use App\Models\Lesson;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['sendResetPassEmail','downloadInvoice']]);
    }

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users.
    |
    */
    use SendsPasswordResetEmails;

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::query()->where('id', '!=', Auth::user()->id);

        foreach (Auth::user()->schools as $school) {
            Log::info("front query users, current school: " . $school->name);
            $query->school($school->id);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("front user search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        // not archived
        $query->statusNot(3);
        $arr['users'] = $query->paginate(5);

        // allow using Input::old() function in view to access the data.
        $request->flash();

        $arr['listTeachers_nav_active'] = 1;

        return view('front.users.index')->with($arr);
    }

    /**
     * Display a listing of archived users.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showArchived(Request $request)
    {
        $query = User::query()->where('id', '!=', Auth::user()->id);

        foreach (Auth::user()->schools as $school) {
            Log::info("query school " . $school->id);
            $query->school($school->id);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        // archived
        $query->status(3);
        $arr['users'] = $query->paginate(5);

        // allow using Input::old() function in view to access the data.
        $request->flash();

        $arr['listTeachers_nav_active'] = 1;

        return view('front.users.showArchived')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['listTeachers_nav_active'] = 1;
        return view('front.users.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\UserRequest $request
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function store(UserRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        try {
            User::createWithRoles($validated, School::find($validated['school']), 'school-teacher');
            $request->session()->flash('alert-success', 'User added successfully.');
            return redirect()->route('front.users.index');
        } catch (\Exception $e) {
            Log::error("Create user caught an exception: " . $e->getMessage());
            // allow using Input::old() function in view to access the data.
            $request->flash();
            return back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $current_opt_user = Auth::user();
        if ($user->id == $current_opt_user->id) {
            $arr['accountDetails_nav_active'] = 1;
        } else {
            $arr['listTeachers_nav_active'] = 1;
        }

        $arr['user'] = $user;
        return view('front.users.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UserRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function update(UserRequest $request, User $user)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->phone = $validated['phone'];
        $user->position = !empty($validated['position']) ? $validated['position'] : "";

        DB::transaction(function () use ($user, $validated) {
            $user->save();
            $user->schools()->sync([$validated['school']]);
            Log::info("update user " . $user->id);
        });

        $request->session()->flash('alert-success', 'User updated successfully.');

        $current_opt_user = Auth::user();
        if ($user->id == $current_opt_user->id) {
            return redirect()->back();
        } else {
            return redirect()->route('front.users.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->hasRole('school-finance-officer')) {
            return response()->json(['error' => 'School finance officer can not be deleted'], 400);
        }
        if ($user->hasRole('school-admin')) {
            return response()->json(['error' => 'School coordinator can not be deleted'], 400);
        }
        Log::info("delete user " . $user->id);
        $user->delete();

        return response()->json($user);
    }

    /**
     * Change status to active.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        $id = $request->id_activate;

        $user = User::findOrFail($id);

        $user->activate();

        $request->session()->flash('alert-success', 'User activated successfully.');

        return redirect()->route('front.users.index');
    }

    /**
     * Change status to suspended.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function suspend(Request $request)
    {
        $id = $request->id_suspend;

        $user = User::findOrFail($id);
        $user->suspend();

        $request->session()->flash('alert-success', 'User suspended successfully.');

        return redirect()->route('front.users.index');
    }


    /**
     * Change status to archived.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request)
    {
        $id = $request->id_archive;

        $user = User::findOrFail($id);
        Log::info("archive user " . $id);
        $user->archive();

        $request->session()->flash('alert-success', 'User archived successfully.');

        return redirect()->route('front.users.index');
    }

    /**
     * Change status to inactived.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function reinstate(Request $request)
    {
        $id = $request->id_reinstate;

        $user = User::findOrFail($id);
        Log::info("reinstate user " . $id);
        $user->reinstate();

        $request->session()->flash('alert-success', 'User reinstated successfully.');

        return redirect()->route('front.users.index');
    }

    /**
     * sendResetPassEmail
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetPassEmail(Request $request)
    {
        $email = $request->email;

        $user = User::where('email', $email)->first();
        Log::info("sendResetPassEmail for user " . $email);
        if ($user) {
            $user->sendResetPassEmail();
            return response()->json(['status' => 0, 'data' => 'success'], 200);
        } else {
            Log::info("sendResetPassEmail error, user " . $email . " not found");
            return response()->json(['status' => 1, 'data' => 'user with email ' . $email . ' not found, please check your email.'], 200);
        }
    }

    /**
     * Change changePassReq
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changePassReq(Request $request)
    {
        $arr['changePass_nav_active'] = 1;
        return view('front.users.changepassword')->with($arr);
    }

    /**
     * Change changePass
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
            'new_password_confirmation' => 'required|string|min:6|same:new_password',
        ]);

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return back()->withErrors('Your current password does not matches with the password you provided.');
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            //Current password and new password are same
            return back()->withErrors('New Password cannot be same as your current password. Please choose a different password.');
        }

        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($validatedData['new_password']);
        $user->save();

        $request->session()->flash('alert-success', 'Password changed successfully.');

        return redirect()->back();
    }

    /**
     * add favlesson
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addfavlesson(Request $request)
    {
        $lesson_id = $request->lesson_id;
        $lesson = Lesson::findOrFail($lesson_id);
        Log::info("add lesson to favorite lessons " . $lesson_id);
        $user = Auth::user();
        if (!$user->hasFavoriteLesson($lesson)) {
            $user->favoriteLessons()->attach($lesson);
        } else {
            Log::info("lesson " . $lesson_id . " has been already added.");
        }
        return redirect()->back();
    }

    /**
     * remove favlesson
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function removefavlesson(Request $request)
    {
        $lesson_id = $request->lesson_id;
        Log::debug("request lesson id " . $request->lesson_id);
        $lesson = Lesson::findOrFail($lesson_id);
        $user = Auth::user();
        if ($user->hasFavoriteLesson($lesson)) {
            $user->favoriteLessons()->detach($lesson);
        } else {
            Log::info("lesson " . $lesson_id . " has been already removed.");
        }
        return redirect()->back();
    }

    /**
     * list favlessons
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function listfavlessons(Request $request)
    {
        $user = Auth::user();
        $tags='';
         if(!empty($user->id)){
             
            $schquery = User::getusers("school_user.user_id = ".Auth::user()->id)->toArray();
            if($schquery){
            $school = School::where('id',$schquery[0]['school_id'])->first();
            $tagids = explode(",",$school->tags);
            $tags = $school->tags;
            }
        }
        $query = Lesson::query();
        //$query->userLessons($user->id);
        $query->Tags($tagids);
        
        $lessons = $query->get();
        $user_query = User::query();
        
        $fav = $user->favoriteLessons;
        if($tags != ''){
            
            $lessons_array = $lessons->toArray();
            $fav_array = $fav->toArray();
            
            $lessons = array_column($lessons_array, 'id');
            $fav = array_column($fav_array, 'id');
            $final_array = array_intersect($lessons, $fav);
            
            foreach($fav_array as $key => $lesson){
                if(!in_array($lesson['id'], $final_array)){
                   unset($fav_array[$key]);
                }
            }
            
            $fav_array =  json_decode(json_encode($fav_array), FALSE);
            $arr['lessons'] = $fav_array;
        }else{
            
             $arr['lessons'] = $user->favoriteLessons;
        }
      // print_r($fav);exit;
        $arr['favoriteLessons_nav_active'] = 1;
        return view('front.users.listfavlessons')->with($arr);
    }

    /**
     * list invoices
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function invoices(Request $request)
    {
        $user = Auth::user();
        $arr['invoices'] = $user->getSchool()->invoices()->paginate(5);
        $arr['invoices_nav_active'] = 1;
        return view('front.users.invoices')->with($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function showInvoice(Invoice $invoice)
    {
        $arr['invoice'] = $invoice;
        $arr['invoicefrom'] = Invoice::getOrgInfo();
        return view('front.users.showInvoice')->with($arr);
    }

    /**
     * download invoice
     *
     * @param \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function downloadInvoice(Invoice $invoice)
    {
        Invoice::genPDF($invoice);
        return response()->download(storage_path('app/public/' . $invoice->pdf_file_path), 'Invoice-' . $invoice->invoice_id . '.pdf', [], 'attachment');
    }
}
