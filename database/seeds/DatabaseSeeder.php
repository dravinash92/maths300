<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     *  php artisan db:seed
     * @return void
     */
    public function run()
    {
        $this->call('SettingTableSeeder');
        $this->command->info('Setting table seeded!');

        $this->call('DiscountTypeTableSeeder');
        $this->command->info('DiscountType table seeded!');

        $this->call('RolesAndPermissionsSeeder');
        $this->command->info('Roles/Permissions table seeded!');

        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');

        $this->call('YearLevelTableSeeder');
        $this->command->info('YearLevel table seeded!');

        $this->call('ContentStrandTableSeeder');
        $this->command->info('ContentStrand table seeded!');

        $this->call('PedagogyTableSeeder');
        $this->command->info('Pedagogy table seeded!');

        $this->call('CurriculumTableSeeder');
        $this->command->info('Curriculum table seeded!');

        $this->call('SchoolTypeTableSeeder');
        $this->command->info('SchoolType table seeded!');

        $this->call('SchoolSectorTableSeeder');
        $this->command->info('SchoolSector table seeded!');

        $this->call('CountryTableSeeder');
        $this->command->info('Country table seeded!');

        $this->call('LocationTableSeeder');
        $this->command->info('Location table seeded!');

        $this->call('SchoolSizeStudentTableSeeder');
        $this->command->info('SchoolSizeStudent table seeded!');

        $this->call('SchoolSizeTeacherTableSeeder');
        $this->command->info('SchoolSizeTeacher table seeded!');

        $this->call('SubscriptionPlanTableSeeder');
        $this->command->info('SubscriptionPlan table seeded!');

        $this->call('SubscriptionPageTableSeeder');
        $this->command->info('SubscriptionPage table seeded!');

        $this->call('AboutPageTableSeeder');
        $this->command->info('AboutPage table seeded!');

        $this->call('SchoolTableUpdateSeeder');
        $this->command->info("School table seed updated");

        $this->call('SchoolSizeTeacherTableUpdateSeeder');
        $this->command->info("SchoolSizeTeacher table seed updated");

        
        $this->call('SubscriptionPlanTableUpdateMaximumUserSeeder');
        $this->command->info("SubscriptionPlan table seed updated");

        $this->call('SubscriptionPlanTableUpdateSeeder');
        $this->command->info("SubscriptionPlan table seed updated");

        $this->call('FAQPageTableSeeder');
        $this->command->info('FAQPage table seeded!');

    }

    public function faker()
    {
        factory(App\Models\Lesson::class, 15)->create();
        factory(App\Models\User::class, 15)->create();
    }
}
