@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }
        .bbp-reply-content {
            font-size: 14px;
        }
    </style>

    <link rel='stylesheet' href='/wp-content/plugins/bbpress/templates/default/css/bbpress.css' type='text/css'
          media='all'/>
@stop

@extends('layouts.pageframe')

@section('top_heading')
    Topics
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/forums" title="Community"><span itemprop="name">Community</span></a>
    </li>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{ route('front.topics.index',['q_forum'=>$forum_id]) }}" title="{{ $topic->forum->title }}"><span
                    itemprop="name">{{ $topic->forum->title }}</span></a>
    </li>
@stop

@section('site-content')
    <div class="container site-content sidebar-right" style="transform: none;">
        <div class="row" style="transform: none;">
            <main id="main" class="site-main col-sm-9 alignleft">
                <article id="post-3136" class="post-3136 forum type-forum status-publish hentry pmpro-has-access">
                    <div class="entry-content">
                        <div id="bbpress-forums">

                            <ul id="topic-4207-replies" class="forums bbp-replies">
                                <li class="bbp-header">
                                    <div class="bbp-reply-author">Author</div><!-- .bbp-reply-author -->
                                    <div class="bbp-reply-content">
                                        Posts
                                    </div><!-- .bbp-reply-content -->

                                </li><!-- .bbp-header -->

                                <li class="bbp-body">
                                    <div id="post-8166" class="bbp-reply-header">
                                        <div class="bbp-meta">
                                            <span class="bbp-reply-post-date">{{ $topic->getCreatedAtDate() }}</span>
                                            {{--<a href="#" onclick="return false;"--}}
                                               {{--class="bbp-reply-permalink">#1</a>--}}
                                        </div><!-- .bbp-meta -->

                                    </div><!-- #post-8166 -->

                                    <div class="even bbp-parent-forum-3136 bbp-parent-topic-4207 bbp-reply-position-2 user-id-1 topic-author post-8166 reply type-reply status-publish hentry pmpro-has-access">
                                        <div class="bbp-reply-author">
                                            <div class="bbp-author-role">{{ $topic->author->name }}</div>
                                            <div class="bbp-reply-ip"><span class="bbp-author-ip">({{ $topic->getOrg() }})</span>
                                            </div>
                                        </div><!-- .bbp-reply-author -->
                                        <div class="bbp-reply-content">
                                            <p>{{ $topic->content }}</p>
                                        </div><!-- .bbp-reply-content -->
                                    </div><!-- topic -->

                                    <!-- replies -->
                                    @foreach($replies as $r)
                                    <div id="post-8166" class="bbp-reply-header">
                                        <div class="bbp-meta">
                                            <span class="bbp-reply-post-date">{{ $r->getCreatedAtDate() }}</span>

                                            @if( $r->adminAllowed() )
                                                <span class="bbp-admin-links">
                                                    <a title="Move this item to the Trash"
                                                       href="{{ route('front.replies.destroy', $r->id) }}"
                                                       onclick="event.preventDefault();document.getElementById('destroy-form{{ $r->id }}').submit();"
                                                       class="bbp-reply-trash-link">Trash
                                                    </a>
                                                </span>
                                                <form id="destroy-form{{ $r->id }}" action="{{ route('front.replies.destroy', $r->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            @endif

                                        </div><!-- .bbp-meta -->

                                    </div><!-- #post-8166 -->

                                    <div class="even bbp-parent-forum-3136 bbp-parent-topic-4207 bbp-reply-position-2 user-id-1 topic-author post-8166 reply type-reply status-publish hentry pmpro-has-access">
                                        <div class="bbp-reply-author">
                                            <div class="bbp-author-role">{{ $r->author->name }}</div>
                                            <div class="bbp-reply-ip"><span class="bbp-author-ip">({{ $r->getOrg() }})</span>
                                            </div>
                                        </div><!-- .bbp-reply-author -->
                                        <div class="bbp-reply-content">
                                            <p>{{ $r->content }}</p>
                                        </div><!-- .bbp-reply-content -->
                                    </div><!-- reply -->
                                    @endforeach

                                </li><!-- .bbp-body -->
                            </ul><!-- #topic-4207-replies -->

                            <div class="bbp-pagination">
                                <div class="bbp-pagination-links">
                                    {{ $replies->appends(['q_topic' => $topic_id])->links() }}
                                </div>
                            </div>

                            @if( !$topic_status )
                                <div class="bbp-template-notice">
                                    <p>This topic is marked as closed to new posts.</p>
                                </div>
                            @else
                                <div id="new-reply-4207" class="bbp-reply-form">
                                    @if(count($errors))
                                        <div class="message-error">
                                            <strong>Whoops!</strong> There were some problems with your input.
                                            <br/>
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form id="topicForm" role="form" method="post" action="{{ route('front.replies.store') }}">
                                        @csrf
                                        <input type="hidden" name="topic" value={{ $topic_id }}>
                                        <fieldset class="bbp-form">
                                            <legend>Reply To: {{ $topic->title }}</legend>
                                            <div>
                                                <div class="bbp-the-content-wrapper">
                                                    <div id="wp-bbp_reply_content-wrap"
                                                         class="wp-core-ui wp-editor-wrap html-active">

                                                        <link rel="stylesheet" id="editor-buttons-css"
                                                              href="/wp-includes/css/editor.min.css"
                                                              type="text/css" media="all">
                                                        <div id="wp-bbp_reply_content-editor-container"
                                                             class="wp-editor-container">

                                                            <textarea class="bbp-the-content wp-editor-area" rows="12"
                                                                      tabindex="101" cols="40" name="content"
                                                                      id="bbp_reply_content"></textarea></div>
                                                    </div>

                                                </div>

                                                <div class="bbp-submit-wrapper">
                                                    <button type="submit" tabindex="107"
                                                            class="button submit" style="background: #78BE20;">Reply
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main>
            <div id="sidebar" class="widget-area col-sm-3 sticky-sidebar" role="complementary"
                 style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
            </div><!-- #secondary -->
        </div>
    </div>
@stop