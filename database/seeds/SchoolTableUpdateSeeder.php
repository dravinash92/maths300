<?php

use Illuminate\Database\Seeder;
use App\Models\School;

class SchoolTableUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::where('id', '=', 1)->update(
            [
                'subscription_plan_id' => null,
            ]
        );
    }
}
