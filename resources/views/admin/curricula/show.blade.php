@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.curricula.index') }}">Curricula</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Curriculum Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <h4>Name</h4>
            {{ $curriculum->name }}
            <hr>

            <h4>Link</h4>
            <a href="{{ $curriculum->link }}">{{ $curriculum->link }}</a>
            <hr>

            <h4>Updated at</h4>
            {{ $curriculum->updated_at }}
            <hr>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.curricula.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
@endpush