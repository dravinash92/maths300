<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Location;

class PostCode implements Rule
{
    private $location = "";

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($location)
    {
        $this->location = $location;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!empty($this->location)) {
            $st = Location::find($this->location);
            if ($st->postal_ranges) {
                $postal_ranges = explode(", ", $st->postal_ranges);
                $flag = true;
                foreach ($postal_ranges as $range) {
                    $tmp = explode("–", $range);
                    $flag = $value>=$tmp[0] && $value<=$tmp[1];
                    if ($flag) break;
                }
                return $flag;
            } else {
                return true;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The postcode is invalid.';
    }
}
