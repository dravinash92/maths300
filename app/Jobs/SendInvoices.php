<?php

namespace App\Jobs;

use App\Mail\InvoiceSent;
use App\Models\Invoice;
use App\Models\School;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendInvoices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * The number of seconds the job can run before timing out.
     *
     * 120 minutes
     *
     * @var int
     */
    public $timeout = 7200;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = microtime(true);
        Log::info('SendInvoices job started......');
        $school_list = ["Masada College",
            "Sackville Street Public School",
            "Unley High School",
            "Faith Christian School",
            "Mullumbimby High School",
            "Drouin Primary School",
            "Biloela State High School",
            "Boulder Primary School",
            "Gulfview Heights Primary School",
            "Beaconhills College Berwick Campus",
            "St Catherine's School - Senior Campus - 2024",
            "Knox Grammar School",
            "Mary Immaculate Catholic Parish Primary School",
            "Coorow Primary School",
            "Barton Primary School",
            "Araluen Primary School",
            "Tullamarine Primary School",
            "Oxley Vale Public School",
            "Carramar Public School",
            "Kensington Primary School",
            "North Albany Senior High School",
            "Watsonia Heights Primary School",
            "Kincoppal Rose Bay School",
            "Swan Hill Primary School",
            "St Joseph's Catholic College 0850",
            "Kallista Primary School",
            "Nyngan Public School",
            "Sassafras Primary School",
            "Cedars Christian College",
            "Port Sorell Primary School",
            "Aitken Creek Primary School",
            "Mernda Primary School",
            "St Eugene College",
            "Leopold Primary School",
            "Staughton College",
            "Normanhurst Boys High School",
            "Pennant Hills Public School",
            "University of Sydney",
            "Good Shepherd Lutheran College - 0835",
            "Nowra Public School",
            "Ivanhoe Primary School",
            "Young Public School",
            "Drysdale Primary School",
            "Eltham North Primary School",
            "Devonport High School",
            "Chapman Primary School",
            "Lloyd Street School",
            "St Patrick's Catholic Primary School - 4670",
            "Broken Hill High School",
            "Laguna Street Public School",
            "Kalinda Primary School",
            "McDonald Park School",
            "Newcastle Grammar School",
            "Hampton Park Primary school",
            "St Joseph's Primary School - O'Conner",
            "West End School",
            "St Francis of Assisi Primary School - Tarneit",
            "Riddells Creek Primary School",
            "Redeemer Lutheran College -  Biloela Campus",
            "St Vincent's Catholic Primary - 2131",
            "William Dean Public School",
            "Bellevue Heights Primary School",
            "Santa Sabina College - 2135",
            "Swifts Creek P-12 School",
            "PLC Sydney Junior School",
            "Dowerin District High School",
            "Flinders Island District High School",
            "Dunolly Primary School",
            "Boolarra Primary School",
            "Turramurra Public School",
            "Marysville Primary School",
            "Clarendon Primary school",
            "The Friends School - High School",
            "Lara Secondary College",
            "Footscray Primary School",
            "Essendon North Primary School",
            "Brunswick East Primary School",
            "Windsor Primary School",
            "Hangzhou International School"];
        $schools = DB::table('schools')->whereIn('name', $school_list)->orderBy('id')->get();
        Log::info('query schools count ' . count($schools));
        $total_count = 0;
        foreach ($schools as $key => $item) {
            // 0911 data reverse
            $school = School::findOrFail($item->id);
            Log::info("Delete invoice of school id: " . $school->id. " name: " . $school->name);
            $invoices = $school->invoices;
            foreach ($invoices as $invoice) {
                Log::debug("Delete invoice orderItems");
                $invoice->orderItems()->delete();
            }
            $school->invoices()->delete();
            // update subscription expiry date
            $school->subscription_expiration_date = Carbon::parse($school->last_renewal_date)->copy()->addYear(1);
            $school->save();

            Log::info("Try to send invoice to school id: " . $school->id. " name: " . $school->name);
            SendInvoices::genInvoiceAndSendEmail($school);
            Log::info('SendInvoices handle ' . ($key + 1) . ' ');
            ++$total_count;
            sleep(2);
        }
        $end = microtime(true);
        Log::info('######################################################################################');
        Log::info('SendInvoices job completed, processed number of schools: ' . $total_count . ', elapsed time: ' . ($end - $start) . ' seconds');
        Log::info('######################################################################################');
    }

    public static function genInvoiceAndSendEmail($school) {
        $issue_date = today();
        // create a new invoice
        $new_invoice = Invoice::newInvoice($school, $issue_date, 0, null, null);
        $new_invoice->status = 1;
        $new_invoice->send_date = today();
        $new_invoice->save();

        Invoice::genPDF($new_invoice);
        Log::info('Send email for new invoice#' . $new_invoice->invoice_id);

        // Send to Invoice Email and CC to School finance officer, School coordinator.
        
        if($new_invoice->to->financeOfficer === $new_invoice->to->coordinator)
        {
            Mail::to($new_invoice->to->finance_invoice_email)->cc($new_invoice->to->financeOfficer)->send(new InvoiceSent($new_invoice, $new_invoice->to->financeOfficer->name));
        }
        else
        {
            Mail::to($new_invoice->to->finance_invoice_email)->cc([$new_invoice->to->financeOfficer,$new_invoice->to->coordinator])->send(new InvoiceSent($new_invoice, $new_invoice->to->financeOfficer->name));
        }
    }
}
