<?php

namespace App\Jobs;

use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;

class ImportUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = microtime(true);
        Log::info('ImportUsers job started......');
        $file = storage_path('app/' . 'Laurimar_PS_users.csv');
        Log::info('date file: '.$file);
        $user_list = array_map('str_getcsv', file($file));
        Log::info('user_list count: '.count($user_list));
        array_walk($user_list, function(&$a) use ($user_list) {
            $a = array_combine($user_list[0], $a);
        });
        array_shift($user_list); # remove column header
        $total_count = 0;
        $school = School::where('name', 'Laurimar Primary School')->first();
        if ($school) {
            Log::info('Find school id: '.$school->id.', name: '.$school->name);
            foreach ($user_list as $key => $item) {
                ++$total_count;
                $user = new User();
                $user->name = $item['Name'];
                $user->email = $item['Email'];
                $user->password = Hash::make(User::generate_password());
                $user->status = 0; //0:disabled
                $user->reset_token = Password::getRepository()->create($user);

                DB::transaction(function () use ($user, $school) {
                    $user->save();
                    $user->schools()->sync([$school->id]);
                    $user->assignRole('school-teacher');
                    Log::info("create user name: " . $user->name. ", email". $user->email. " for school ".$school->name);
                });
                $user->activate();
            }
        } else {
            Log::error('school not found');
        }
        $end = microtime(true);
        Log::info('######################################################################################');
        Log::info('ImportUsers job completed, processed number of users: ' . $total_count . ', elapsed time: ' . ($end - $start) . ' seconds');
        Log::info('######################################################################################');
    }


}
