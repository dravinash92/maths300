<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Location;
use App\Models\Country;

class LocationTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('locations')->delete();

        $country_au = Country::where('name', 'Australia')->first()->id;
        $country_nz = Country::where('name', 'New Zealand')->first()->id;
        $country_not_found = Country::where('name', 'NotFound')->first()->id;

        Location::create([
            'country_id' => $country_au,
            'name' => 'NSW',
            'postal_ranges' => '1000–2599, 2620–2899, 2921–2999',
        ]);

        Location::create([
            'country_id' => $country_au,
            'name' => 'VIC',
            'postal_ranges' => '3000–3999, 8000–8999',
        ]);

        Location::create([
            'country_id' => $country_au,
            'name' => 'QLD',
            'postal_ranges' => '4000–4999, 9000–9999',
        ]);

        Location::create([
            'country_id' => $country_au,
            'name' => 'SA',
            'postal_ranges' => '5000–5999',
        ]);

        Location::create([
            'country_id' => $country_au,
            'name' => 'WA',
            'postal_ranges' => '6000–6999',
        ]);

        Location::create([
            'country_id' => $country_au,
            'name' => 'TAS',
            'postal_ranges' => '7000–7999',
        ]);

        Location::create([
            'country_id' => $country_au,
            'name' => 'ACT',
            'postal_ranges' => '0200–0299, 2600–2619, 2900–2920',
        ]);

        Location::create([
            'country_id' => $country_au,
            'name' => 'NT',
            'postal_ranges' => '0800–0999',
        ]);

        // nz
        Location::create([
            'country_id' => $country_nz,
            'name' => 'All regions',
        ]);

        Location::create([
            'country_id' => $country_not_found,
            'name' => 'NotFound',
        ]);


    }
}