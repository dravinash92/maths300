<?php

namespace App\Http\Controllers\API;

use App\Models\LessonInteract;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Log;

class LessonInteractController extends Controller
{
    public function validateAccessCode(Request $request)
    {
        $validated = $request->validate([
            'access_code' => 'required',
            'access_url' => 'required',
        ]);

        $match = [
            'access_code'=> $validated['access_code'],
            'access_url'=> $validated['access_url'],
        ];
        $lessonInteract = LessonInteract::where($match)->first();

        // check if it expires
        if (!empty($lessonInteract) && !Carbon::parse($lessonInteract->expires_at)->copy()->isPast()) {
            $arr['status'] = 0;
            $arr['data'] = [
                'access_url' => $lessonInteract->access_url,
                'access_code' => $lessonInteract->access_code,
                'expires_in' => $lessonInteract->expires_in,
                'expires_at' => $lessonInteract->expires_at,
            ];
            return response()->json($arr, 200);
        }

        $data['message'] = 'Access code or url error.';

        return response()->json(['status'=> 1,'data' => $data], 401);
    }

    public function checkAuth(Request $request)
    {
        // 1st check if it is authenticated
        if (auth('api')->user()) {
            Log::debug('checkAuth user: '.auth('api')->user()->id);
            $data['message'] = 'Authorization successful.';
            return response()->json(['status'=> 0,'data' => $data], 200);
        } else {
            $data['message'] = 'Authorization failed.';
            return response()->json(['status'=> 1,'data' => $data], 401);
        }
    }
}
