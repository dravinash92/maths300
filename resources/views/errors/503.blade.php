@extends('layouts.pageframe')

@section('css')
    <style type="text/css" media="all" id="siteorigin-panels-layouts-head">/* Layout 2958 */
        .page-404-content {
            padding: 0px 0px;
        }
    </style>
@stop

@section('top_heading')
    Error
@stop

@section('breadcrumbs')
@stop

@section('site-content')
    <div class="container site-content">
        <div class="row">
            <main id="main" class="site-main col-sm-12 full-width">
                <section class="error-404 not-found">
                    <div class="page-404-content">
                        <div class="row">
                            <div class="col-xs-12 text-left">
                                <h3>503 Service unavailable</h3>
                                <h3>Sorry, we are doing some maintenance. Please check back soon.</h3>
                            </div>
                        </div>
                    </div>
                    <!-- .page-content -->
                </section>
            </main>
        </div>
    </div>
@stop
