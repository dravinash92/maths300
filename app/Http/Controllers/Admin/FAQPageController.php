<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FAQPageRequest;
use App\Models\FAQPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;

class FAQPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['faqpage'] = FAQPage::first();
        return view('admin.faqpage.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\FAQPageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FAQPageRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $faqPage = FAQPage::findOrFail($request->id);
        $faqPage->content = $validated['content'];
        $faqPage->save();

        $request->session()->flash('alert-success', 'Content saved successfully.');

        return redirect()->route('admin.faqpage.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FAQPage  $faqPage
     * @return \Illuminate\Http\Response
     */
    public function show(FAQPage $faqPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FAQPage  $faqPage
     * @return \Illuminate\Http\Response
     */
    public function edit(FAQPage $faqPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FAQPage  $faqPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FAQPage $faqPage)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FAQPage  $faqPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(FAQPage $faqPage)
    {
        //
    }
}
