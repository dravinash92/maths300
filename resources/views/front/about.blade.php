@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }

        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        table thead tr {
            background-color: #78BE20;
            color: #333;
        }
    </style>
@stop

@extends('layouts.page')

@section('top_heading')
    About
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/about"><span itemprop="name">About</span></a>
    </li>
@stop

@section('content')
        <article id="post-87" class="post-87 page type-page status-publish hentry pmpro-has-access">
            <div class="entry-content">
                {!! $aboutpage->content !!}
            </div>
        </article>
@stop