<?php

use Illuminate\Database\Seeder;
use App\Models\SubscriptionPlan;

class SubscriptionPlanTableUpdateMaximumUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionPlan::where('id', '=', 1)->update(['maximum_user' => 1]);
        SubscriptionPlan::where('id', '=', 2)->update(['maximum_user' => 6]);
        SubscriptionPlan::where('id', '=', 3)->update(['maximum_user' => 16]);
        SubscriptionPlan::where('id', '=', 4)->update(['maximum_user' => 26]);
        SubscriptionPlan::where('id', '=', 5)->update(['maximum_user' => 41]);
        SubscriptionPlan::where('id', '=', 6)->update(['maximum_user' => 201]);
        SubscriptionPlan::where('id', '=', 7)->update(['maximum_user' => 16]);
        SubscriptionPlan::where('id', '=', 8)->update(['maximum_user' => 41]);
    }
}
