<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AboutPageRequest;
use App\Models\AboutPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;

class AboutPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['aboutpage'] = AboutPage::first();
        return view('admin.aboutpage.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AboutPageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutPageRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $aboutPage = AboutPage::findOrFail($request->id);
        $aboutPage->content = $validated['content'];
        $aboutPage->save();

        $request->session()->flash('alert-success', 'Content saved successfully.');

        return redirect()->route('admin.aboutpage.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AboutPage  $aboutPage
     * @return \Illuminate\Http\Response
     */
    public function show(AboutPage $aboutPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AboutPage  $aboutPage
     * @return \Illuminate\Http\Response
     */
    public function edit(AboutPage $aboutPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AboutPage  $aboutPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AboutPage $aboutPage)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AboutPage  $aboutPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(AboutPage $aboutPage)
    {
        //
    }
}
