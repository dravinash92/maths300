<?php

namespace App\Mail;

use App\Models\School;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SchoolSubscribed extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The school instance.
     *
     * @var school
     */
    public $school;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(School $school)
    {
        $this->school = $school;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.schoolSubscribed')->subject('A new school has subscribed to Maths300 service');
    }
}
