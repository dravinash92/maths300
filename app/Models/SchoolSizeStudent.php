<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolSizeStudent extends Model
{
    /**
     * The schools that belong to the school size of student category.
     */
    public function schools()
    {
        return $this->hasMany(School::class);
    }

    public function subscriptionPlan()
    {
        return $this->hasOne(SubscriptionPlan::class,'school_size_id');
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }
}
