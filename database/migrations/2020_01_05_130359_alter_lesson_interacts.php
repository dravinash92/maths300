<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLessonInteracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_interacts', function (Blueprint $table) {
            $table->renameColumn('url', 'access_url');
            $table->renameColumn('pass_code', 'access_code');
            $table->string('expires_in')->after('pass_code');
            $table->string('short_url')->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_interacts', function (Blueprint $table) {
            $table->renameColumn('access_url', 'url');
            $table->renameColumn('access_code', 'pass_code');
            $table->dropColumn('expires_in');
            $table->dropColumn('short_url');
        });
    }
}
