<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Route::get('/file/{filename}', 'FileController@getFile')->where('filename', '^(.+)\/([^\/]+)$')->name('getFile');

Auth::routes();

Route::namespace('Admin')->prefix('admin')->as('admin.')->middleware('auth')->middleware('role:super-admin')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::post('/lessons/publish', 'LessonController@publish')->name('lessons.publish');
    Route::post('/lessons/suspend', 'LessonController@suspend')->name('lessons.suspend');
    Route::get('/lessons/preview/{id}', 'LessonController@preview')->name('lessons.preview');
    Route::resource('/lessons', 'LessonController');

    Route::post('/schools/activate', 'SchoolController@activate')->name('schools.activate');
    Route::post('/schools/suspend', 'SchoolController@suspend')->name('schools.suspend');
    Route::post('/schools/archive', 'SchoolController@archive')->name('schools.archive');
    Route::post('/schools/reinstate', 'SchoolController@reinstate')->name('schools.reinstate');
    Route::get('/schools/showArchived', 'SchoolController@showArchived')->name('schools.showArchived');
    Route::post('/schools/setExpiryDate', 'SchoolController@setExpiryDate')->name('schools.setExpiryDate');
    Route::get('/schools/viewInvoice/{schoolID}', 'SchoolController@viewInvoice')->name('schools.viewInvoice');
    Route::resource('/schools', 'SchoolController');

    Route::post('/invoices/paid', 'InvoiceController@paid')->name('invoices.paid');
    Route::post('/invoices/send', 'InvoiceController@send')->name('invoices.send');
    Route::post('/invoices/cancel', 'InvoiceController@cancel')->name('invoices.cancel');
    Route::get('/invoices/showArchived', 'InvoiceController@showArchived')->name('invoices.showArchived');
    Route::get('/invoices/download/{invoice}', 'InvoiceController@download')->name('invoices.download');
    Route::get('/invoices/sendReminder/{invoice}', 'InvoiceController@sendReminder')->name('invoices.sendReminder');
    Route::post('/invoices/downloadXeroInvoice', 'InvoiceController@downloadXeroInvoice')->name('invoices.downloadXeroInvoice');
    Route::post('/invoices/adhocStore', 'InvoiceController@adhocStore')->name('invoices.adhocStore');
    Route::resource('/invoices', 'InvoiceController');
    

    Route::get('/resources/files', 'ResourceController@listFiles')->name('resources.listFiles');
    Route::get('/resources/images', 'ResourceController@listImages')->name('resources.listImages');
    Route::resource('/resources', 'ResourceController');

    Route::post('/users/activate', 'UserController@activate')->name('users.activate');
    Route::post('/users/suspend', 'UserController@suspend')->name('users.suspend');
    Route::post('/users/archive', 'UserController@archive')->name('users.archive');
    Route::post('/users/reinstate', 'UserController@reinstate')->name('users.reinstate');
    Route::get('/users/showArchived', 'UserController@showArchived')->name('users.showArchived');
    Route::post('/users/sendResetLinkEmail', 'UserController@sendResetLinkEmail')->name('users.sendResetLinkEmail');
    Route::post('/users/sendResetPassEmail', 'UserController@sendResetPassEmail')->name('users.sendResetPassEmail');
    Route::get('/users/changePassReq', 'UserController@changePassReq')->name('users.changePassReq');
    Route::post('/users/changePassword', 'UserController@changePassword')->name('users.changePassword');
    Route::resource('/users', 'UserController');

    Route::get('/reports/users', 'ReportsController@userindex')->name('reports.userindex');
    Route::get('/reports/schools', 'ReportsController@schoolindex')->name('reports.schoolindex');
    Route::get('/reports/invoices', 'ReportsController@invoiceindex')->name('reports.invoiceindex');
    Route::post('/reports/getusers', 'ReportsController@getusers')->name('reports.getusers');
    Route::post('/reports/getschools', 'ReportsController@getschools')->name('reports.getschools');
    Route::post('/reports/getinvoices', 'ReportsController@getinvoices')->name('reports.getinvoices');
    /* 
 * We name this route xero.auth.success as by default the config looks for a route with this name to redirect back to
 * after authentication has succeeded. The name of this route can be changed in the config file.
 */
    Route::get('/manage/xero', 'XeroController@index')->name('xero.auth.success');

    Route::post('/forums/open', 'ForumController@open')->name('forums.open');
    Route::post('/forums/close', 'ForumController@close')->name('forums.close');
    Route::resource('/forums', 'ForumController');

    Route::post('/topics/open', 'TopicController@open')->name('topics.open');
    Route::post('/topics/close', 'TopicController@close')->name('topics.close');
    Route::resource('/topics', 'TopicController');

    Route::resource('/replies', 'ReplyController');

    Route::resource('/yearlevels', 'YearLevelController');
    Route::resource('/contentstrands', 'ContentStrandController');
    Route::resource('/pedagogies', 'PedagogyController');
    Route::resource('/curricula', 'CurriculumController');
    Route::resource('/schooltypes', 'SchoolTypeController');
    Route::resource('/schoolsectors', 'SchoolSectorController');
    Route::resource('/schoolsizestudents', 'SchoolSizeStudentController');
    Route::resource('/schoolsizeteachers', 'SchoolSizeTeacherController');
    Route::get('/subscriptionplans/select/{school_size_id}', 'SubscriptionPlanController@select')->name('subscriptionplans.select');
    Route::resource('/subscriptionplans', 'SubscriptionPlanController');
    Route::resource('/countries', 'CountryController');
    Route::resource('/locations', 'LocationController');
    Route::resource('/discounts', 'DiscountController');

    Route::resource('/subscriptionpage', 'SubscriptionPageController');
    Route::resource('/aboutpage', 'AboutPageController');

    Route::post('/advertisements/publish', 'AdvertisementController@publish')->name('advertisements.publish');
    Route::post('/advertisements/suspend', 'AdvertisementController@suspend')->name('advertisements.suspend');
    Route::resource('/advertisements', 'AdvertisementController');

    Route::resource('/settings', 'SettingController');
    Route::resource('/faqpage', 'FAQPageController');
    //for tags
    Route::resource('/tags', 'TagController');
});

Route::namespace('Front')->as('front.')->group(function () {
    Route::get('/lessons/sample/{id}', 'LessonController@sample')->name('lessons.sample');
    Route::get('/lessons/listall', 'LessonController@listall')->name('lessons.listall');
    Route::post('/lessons/setAccessCode', 'LessonController@setAccessCode')->name('lessons.setAccessCode');

    Route::resource('/lessons', 'LessonController');
    //Route::resource('/faq', 'FAQController');
    Route::resource('/forums', 'ForumController');
    Route::resource('/topics', 'TopicController');
    Route::resource('/replies', 'ReplyController');
    Route::resource('/ads', 'AdvertisementController');

    Route::get('/contact', 'ContactController@index');
    Route::get('/about', 'AboutPageController@index')->name('about');
    Route::get('/subscribe', 'SubscriptionPageController@index')->name('subscription.index');
    Route::get('/terms/{hasAction}', 'SubscriptionPageController@termsAndConditions')->name('subscription.termsAndConditions');
    Route::get('/subscribe/create', 'SubscriptionPageController@create')->name('subscription.create');
    Route::post('/subscribe/store', 'SubscriptionPageController@store')->name('subscription.store');
    Route::get('/subscribe/notice', 'SubscriptionPageController@notice')->name('subscription.notice');
    Route::get('/subscribe/select/{school_size_id}', 'SubscriptionPageController@select')->name('subscription.select');

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/faq', 'FAQPageController@index')->name('faq');


    Route::post('/users/activate', 'UserController@activate')->name('users.activate');
    Route::post('/users/suspend', 'UserController@suspend')->name('users.suspend');
    Route::post('/users/archive', 'UserController@archive')->name('users.archive');
    Route::post('/users/reinstate', 'UserController@reinstate')->name('users.reinstate');
    Route::get('/users/showArchived', 'UserController@showArchived')->name('users.showArchived');
    Route::post('/users/sendResetLinkEmail', 'UserController@sendResetLinkEmail')->name('users.sendResetLinkEmail');
    Route::post('/users/sendResetPassEmail', 'UserController@sendResetPassEmail')->name('users.sendResetPassEmail');
    Route::get('/users/changePassReq', 'UserController@changePassReq')->name('users.changePassReq');
    Route::post('/users/changePassword', 'UserController@changePassword')->name('users.changePassword');
    Route::post('/users/addfavlesson', 'UserController@addfavlesson')->name('users.addfavlesson');
    Route::post('/users/removefavlesson', 'UserController@removefavlesson')->name('users.removefavlesson');
    Route::get('/users/listfavlessons', 'UserController@listfavlessons')->name('users.listfavlessons');
    Route::get('/users/invoices', 'UserController@invoices')->name('users.invoices');
    Route::get('/users/showInvoice/{invoice}', 'UserController@showInvoice')->name('users.showInvoice');
    Route::get('/users/downloadInvoice/{invoice}', 'UserController@downloadInvoice')->name('users.downloadInvoice');
    Route::resource('/users', 'UserController');
});

