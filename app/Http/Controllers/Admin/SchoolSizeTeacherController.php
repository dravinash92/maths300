<?php

namespace App\Http\Controllers\Admin;

use App\Models\SchoolSizeTeacher;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolSizeTeacherRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class SchoolSizeTeacherController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SchoolSizeTeacher::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['schoolsizeteachers'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.schoolsizeteachers.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schoolsizeteachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SchoolSizeTeacherRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolSizeTeacherRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $schoolsizeteacher = new SchoolSizeTeacher();
        $schoolsizeteacher->name = $validated['name'];

        $schoolsizeteacher->save();

        $request->session()->flash('alert-success', 'School size teacher added successfully.');

        return redirect()->route('admin.schoolsizeteachers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SchoolSizeTeacher $schoolsizeteacher
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolSizeTeacher $schoolsizeteacher)
    {
        $arr['schoolsizeteacher'] = $schoolsizeteacher;

        return view('admin.schoolsizeteachers.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SchoolSizeTeacher $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolSizeTeacher $schoolsizeteacher)
    {
        $arr['schoolsizeteacher'] = $schoolsizeteacher;
        return view('admin.schoolsizeteachers.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SchoolSizeTeacherRequest $request
     * @param  \App\Models\SchoolSizeTeacher $schoolsizeteacher
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolSizeTeacherRequest $request, SchoolSizeTeacher $schoolsizeteacher)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $schoolsizeteacher->name = $validated['name'];

        $schoolsizeteacher->save();

        $request->session()->flash('alert-success', 'School size teacher updated successfully.');

        return redirect()->route('admin.schoolsizeteachers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schoolsizeteacher = SchoolSizeTeacher::findOrFail($id);

        $schoolsizeteacher->delete();
        Log::info("delete schoolsizeteacher " . $id);

        return response()->json($schoolsizeteacher);
    }
}
