<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolSector extends Model
{
    /**
     * The schools that belong to the school sector.
     */
    public function schools()
    {
        return $this->hasMany(School::class);
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }
}
