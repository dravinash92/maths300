<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Exports\UserExport;
use App\Exports\SchoolExport;
use App\Exports\InvoiceExport;
use App\Models\School;
use App\Models\User;
use App\Models\Invoice;
use App\Models\Lesson;
use App\Models\Tag;
use Carbon\Carbon;
use App\Models\Country;
use App\Models\SubscriptionPlan;
use App\Models\SchoolSizeTeacher;
use Illuminate\Http\Request;
use App\Http\Requests\TopicRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Excel;
use Auth;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userindex(Request $request)
    {
        $arr['school'] = data4Select(School::class);
        $temp[] = array("id"=>'0',"text"=>'All schools');
        $arr['schools'] = array_merge($temp,$arr['school']);
        //echo '<pre>';print_r($arr['schools']);exit;
        return view('admin.reports.userindex')->with($arr);
    }
    public function getusers(Request $request)
    {
        ini_set('memory_limit', '-1');
        $where = "users.id != '' ";
        $curdate = date('Y-m-d');
        if(isset($request['q_school']) && !empty($request['q_school']) && $request['q_school'] != 0){
            $where .= " and school_user.school_id ='".$request['q_school']."'";
        }
        if(isset($request['q_status']) && $request['q_status'] != ''){
                $where.=" and users.status = '".$request['q_status']."'";
        }
       
        $data = User::getusers($where)->toArray();
        $dataArr = array();
        $dataArrfin = array();
        $dataArrfin = array_unique(array_column($data, 'userId'));
        $newArrTitle = array();
        $newArrNum = array();
        $newArr1 = array();
        foreach ($data as $key => $value) {
            if($value['userStatus'] == 0){
                $value['userStatus'] = 'Inactive';
            }else if($value['userStatus'] == 1){
                $value['userStatus'] = 'Active';
            }else if($value['userStatus'] == 2){
                $value['userStatus'] = 'Suspended';
            }else if($value['userStatus'] == 3){
                $value['userStatus'] = 'Archived';
            }

            if(!empty($value['phone'])){
                $value['phone'] =$value['phone'];
            }else{
                $value['phone'] = "None";
            }

            if(!empty($value['position'])){
                $value['position'] =$value['position'];
            }else{
                $value['position'] = "None";
            }

            if($value['subscription_status'] == 0){
                $value['subscription_status'] = 'Inactive';
            }else if($value['subscription_status'] == 1){
                $value['subscription_status'] = 'Active';
            }else if($value['subscription_status'] == 2){
                $value['subscription_status'] = 'Suspended';
            }else if($value['subscription_status'] == 3){
                $value['subscription_status'] = 'Archived';
            }

            if($value['userId'] === $value['coordinator_id']){
                $value['coordinator_id'] = 'Yes';
            }else{
                $value['coordinator_id'] = 'No';
            }
            if($value['userId'] === $value['finance_officer_id']){
                $value['finance_officer_id'] = 'Yes';
            }else{
                $value['finance_officer_id'] = 'No';
            }
            if($value['last_login'] != ''){
                $value['last_login'] = date('d-m-Y',strtotime($value['last_login']));
            }else{
                $value['last_login'] = 'N/A';
            }

            foreach ($dataArrfin as $k => $v) {
                if($value['userId'] == $v){
                    if($value['number'] != ''){
                        $newArrTitle[$value['userId']][]= $value['number'].' - '.$value['title'];
                        $value['title'] = implode(', ', $newArrTitle[$value['userId']]);
                    }else{
                        $value['title'] = 'None';
                    }
                    
                    // $newArrNum[$value['userId']][]= $value['number'];
                    // $value['number'] = implode(',', $newArrNum[$value['userId']]);
                }
            }
            
            $dataArr[] = array_except($value,['id','user_id','lesson_id','number']);
        }
        $newArr1= array_combine(array_column($dataArr, 'userId'), $dataArr);
        //echo '<pre>';print_r($newArr1);exit;
        $export = new UserExport($newArr1);

    return Excel::download($export, 'Users_Maths300_'.date('dmY').'.xlsx');
    }

    public function schoolindex(Request $request)
    {
        $arr['locations'] = Country::data4Select();
        $arr['SchoolSizeTeacher'] = SchoolSizeTeacher::data4Select();
        return view('admin.reports.schoolindex')->with($arr);;
    }
    public function getschools(Request $request)
    {
        ini_set('memory_limit', '-1');
        $where = "schools.id != '' ";
        $curdate = date('Y-m-d');
        if(isset($request['q_address_location']) && !empty($request['q_address_location'])){
            $where .= " and schools.address_location_id ='".$request['q_address_location']."'";
        }
        if(isset($request['SchoolSizeTeacher']) && $request['SchoolSizeTeacher'] != ''){
                $where.=" and schools.school_size_teacher_id = '".$request['SchoolSizeTeacher']."'";
        }
        if(isset($request['q_sub_status']) && $request['q_sub_status'] != ''){
            $where.=" and schools.status = '".$request['q_sub_status']."'";
        }
        if(isset($request['q_subscription_expiry_date_range']) && $request['q_subscription_expiry_date_range'] != ''){
                $date = explode(' - ', $request['q_subscription_expiry_date_range']);
                /*$start_date = date('Y-m-d',strtotime($date[0]));
                $end_date = date('Y-m-d',strtotime($date[1]));*/
                $from = Carbon::createFromFormat('d/m/Y', $date[0])->format('Y-m-d');
                $to = Carbon::createFromFormat('d/m/Y', $date[1])->format('Y-m-d');
                //$where.=" and schools.subscription_expiration_date between '".$start_date."' and '".$end_date."'";
                $where.=" and schools.subscription_expiration_date between '".$from."' and '".$to."'";
        }
        
        
        $data = School::getschools($where)->toArray();
        $dataArr = array();
        $newArr1 =array();
        $string=array();
    if(count($data) > 0){
        foreach ($data as $key => $value) {
            if($value['exp_status'] == 0){
                $value['exp_status'] = 'Inactive';
            }else if($value['exp_status'] == 1){
                $value['exp_status'] = 'Active';
            }else if($value['exp_status'] == 2){
                $value['exp_status'] = 'Suspended';
            }else if($value['exp_status'] == 3){
                $value['exp_status'] = 'Archived';
            }
            
            if(!empty($value['address_region'])){
                $value['address_region'] =$value['address_region'];
            }else{
                $value['address_region'] = "None";
            }
            if(!empty($value['tags'])){
                $lessonPacks=[];
                foreach(explode(",",$value['tags']) as $k=>$v){
                    $lesson= Tag::find($v);
                    if(!empty($lesson)){
                        $lessonPacks[]=$lesson->name;
                    }
                }
               $value['tags'] =  implode(",",$lessonPacks);
            }else{
                $value['tags'] = 'Default - All Lessons';
            }
            if(!empty($value['join_date'])){
                $value['join_date'] = date("d-m-Y",strtotime($value['join_date']));
            }else{
                $value['join_date'] = "N/A";
            }
            if(!empty($value['subscription_expiration_date'])){
                $value['subscription_expiration_date'] =date("d-m-Y",strtotime($value['subscription_expiration_date']));
            }else{
                $value['subscription_expiration_date'] = "N/A";
            }
            if(!empty($value['last_renewal_date'])){
                $value['last_renewal_date'] =date("d-m-Y",strtotime($value['last_renewal_date']));
            }else{
                $value['last_renewal_date'] = "N/A";
            }
            if(!empty($value['coordinator_phone'])){
                $value['coordinator_phone'] =$value['coordinator_phone'];
            }else{
                $value['coordinator_phone'] = "None";
            }
            if(!empty($value['coordinator_position'])){
                $value['coordinator_position'] =$value['coordinator_position'];
            }else{
                $value['coordinator_position'] = "None";
            }
            if(!empty($value['finance_user_phone'])){
                $value['finance_user_phone'] =$value['finance_user_phone'];
            }else{
                $value['finance_user_phone'] = "None";
            }

            if(!empty($value['finance_user_position'])){
                $value['finance_user_position'] =$value['finance_user_position'];
            }else{
                $value['finance_user_position'] = "None";
            }
            if(!empty($value['purchase_order_no'])){
                $value['purchase_order_no'] =$value['purchase_order_no'];
            }else{
                $value['purchase_order_no'] = "None";
            }
            if(!empty($value['notes'])){
                $value['notes'] =$value['notes'];
            }else{
                $value['notes'] = "None";
            }
            $value['total_user'] = User::gettotusers($value['id'])->toArray()[0]['Total_user'];
            if(!empty($value['total_user']) && $value['total_user'] > 0){
                $value['total_user'] = $value['total_user'];
            }else{
                $value['total_user'] = '0';
            }
            $newArr1[] = User::getactusers($value['id'])->toArray();
            foreach ($newArr1 as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    if($v1['sch_id'] == $value['id']){    
                        $string[] = $v1['school_user'];
                        $value['users_active'] = implode(',', $string);                     
                    }
                }
                unset($string);
                $string = array();                   
            }
            $value['users_active'] = User::getactiveusers($value['users_active'])->toArray()[0]['active_user'];
            if(!empty($value['users_active']) && $value['users_active'] > 0){
                $value['users_active'] = $value['users_active'];
            }else{
                $value['users_active'] = '0';
            }
            $dataArr[] = $value;            
        }
    }else{
        $dataArr[] = array("data"=>'No records to display');
    }
        //echo '<pre>';print_r($dataArr);exit;
        $export = new SchoolExport($dataArr);

    return Excel::download($export, 'Schools_Maths300_'.date('dmY').'.xlsx');
    }
    public function getlessonschools(Request $request)
    {
        ini_set('memory_limit', '-1');
        $where = "lessons.id != '' ";
        $curdate = date('Y-m-d');
        //$data = Lesson::getschools($where)->toArray();
        $dataArr = array();
        $newArr1 =array();
        $string=array();
        $query = Lesson::query();

        $data = $query->paginate(209);
        
        
    if(count($data) > 0){
        foreach($data as $value){
            if(!empty($value['number'])){
                $newArr1['number'] =$value['number'];
            }else{
                $newArr1['number'] = "";
            }
            $newArr1['upload_date'] =$value->getUploadDate();
            
            if(!empty($value['number_of_views'])){
                $newArr1['number_of_views'] =$value['number_of_views'];
            }else{
                $newArr1['number_of_views'] = "None";
            }
            if(!empty($value['title'])){
                $newArr1['title'] =$value['title'];
            }else{
                $newArr1['title'] = "N/A";
            }
            
            $newArr1['strands'] = implode(',', $value->contentStrands()->orderBy('content_strands.id')->pluck('name')->toArray());
            $newArr1['yrlevel'] = implode(',', $value->yearLevels()->orderBy('year_levels.id')->pluck('name')->toArray());
            if($value['status'] == 0){
                $newArr1['status'] = 'Inactive';
            }else if($value['status'] == 1){
                $newArr1['status'] = 'Active';
            }else if($value['status'] == 2){
                $newArr1['status'] = 'Suspended';
            }else if($value['status'] == 3){
                $newArr1['status'] = 'Archived';
            }
            
            $newArr1['pedagogies'] = implode(',', $value->pedagogies()->orderBy('pedagogies.id')->pluck('name')->toArray());
            $newArr1['curricula'] = implode(',', $value->curricula()->orderBy('curricula.id')->pluck('name')->toArray());
            if(!empty($value['related_lessons'])){
                $related_lessons=[];
                foreach(explode(",",$value['related_lessons']) as $k=>$v){
                    $lesson= Lesson::getTitleById($v);
                    if(!empty($lesson)){
                       $related_lessons[]=Lesson::getTitleById($v);
                    }
                }
               $newArr1['related_lessons'] =  implode(",",$related_lessons);
            }else{
                $newArr1['related_lessons'] = 'N/A';
            }
            if($value['software_required'] == 0){
                $newArr1['software_required'] = 'No';
            }else{
                $newArr1['software_required'] = 'Yes';
            }
           if(!empty($value['software_link'])){
                $newArr1['software_link'] =$value['software_link'];
            }else{
                $newArr1['software_link'] = "N/A";
            }
            if(!empty($value['acknowledgements'])){
                $newArr1['acknowledgements'] =$value['acknowledgements'];
            }else{
                $newArr1['acknowledgements'] = "N/A";
            }
            if(!empty($value['attributes'])){
                $newArr1['attributes'] =$value['attributes'];
            }else{
                $newArr1['attributes'] = "N/A";
            }
            if(!empty($value['partial_video'])){
                $newArr1['partial_video'] =$value['partial_video'];
            }else{
                $newArr1['partial_video'] = "N/A";
            }
            if(!empty($value['full_video'])){
                $newArr1['full_video'] =$value['full_video'];
            }else{
                $newArr1['full_video'] = "N/A";
            }
            if(!empty($value['notes'])){
                $newArr1['notes'] =$value['notes'];
            }else{
                $newArr1['notes'] = "N/A";
            }
            if($value['is_front'] == 0){
                $newArr1['is_front'] = 'No';
            }else{
                $newArr1['is_front'] = 'Yes';
            }
            if($value['is_sample'] == 0){
                $newArr1['is_sample'] = 'No';
            }else{
                $newArr1['is_sample'] = 'Yes';
            }
            $dataArr[] = $newArr1;
        }
    }else{
        $dataArr[] = array("data"=>'No records to display');
    }
        //echo '<pre>';print_r($dataArr);exit;
        $export = new SchoolExport($dataArr);

    return Excel::download($export, 'Lessons_Maths300_'.date('dmY').'.xlsx');
    }
    public function invoiceindex(Request $request)
    {
        return view('admin.reports.invoiceindex');
    }
    public function getinvoices(Request $request)
    {
        ini_set('memory_limit', '-1');
        $where = "invoices.id != '' ";
        //$where = "schools.last_invoice_number != '' ";
        $curdate = date('Y-m-d');
        if(isset($request['q_due_status']) && $request['q_due_status'] != ''){
                $where.=" and invoices.status = '".$request['q_due_status']."'";
        }
        if(isset($request['q_due_date_range']) && $request['q_due_date_range'] != ''){
                $date = explode(' - ', $request['q_due_date_range']);
                $from = Carbon::createFromFormat('d/m/Y', $date[0])->format('Y-m-d');
                $to = Carbon::createFromFormat('d/m/Y', $date[1])->format('Y-m-d');
                //$start_date = date('Y-m-d',strtotime($date[0]));
               // $end_date = date('Y-m-d',strtotime($date[1]));
                $where.=" and invoices.due_date between '".$from."' and '".$to."'";
               // $where.=" and invoices.due_date between '".$start_date."' and '".$end_date."'";
        }
       
        $data = Invoice::getinvoices($where)->toArray();
        $dataArr = array();
    if(count($data) > 0){
        foreach ($data as $key => $value) {
            /*if($value['subscription_expiration_date'] < $curdate){
                $value['subscription_expiration_date'] = 'Expired';
            }else{
                $value['subscription_expiration_date'] = 'Active';
            }*/
            if ($value['subscription_status'] == 0) {
                $value['subscription_status'] = "inactive";
            }
            if ($value['subscription_status'] == 1) {
                $value['subscription_status'] = "active";
            }
            if ($value['subscription_status'] == 2) {
                $value['subscription_status'] = "suspended";
            }
            if ($value['subscription_status'] == 3) {
                $value['subscription_status'] = "archived";
            }
            if ($value['subscription_status'] == 4) {
                $value['subscription_status'] = "imported";
            }

            if(!empty($value['address_region'])){
                $value['address_region'] =$value['address_region'];
            }else{
                $value['address_region'] = "None";
            }

            if(!empty($value['issue_date'])){
                $value['issue_date'] = date("d-m-Y",strtotime($value['issue_date']));
            }else{
                $value['issue_date'] = "N/A";
            }
            if(!empty($value['due_date'])){
                $value['due_date'] =date("d-m-Y",strtotime($value['due_date']));
            }else{
                $value['due_date'] = "N/A";
            }
            if(!empty($value['paid_date'])){
                $value['paid_date'] =date("d-m-Y",strtotime($value['paid_date']));
            }else{
                $value['paid_date'] = "N/A";
            }
            if(!empty($value['coordinator_phone'])){
                $value['coordinator_phone'] =$value['coordinator_phone'];
            }else{
                $value['coordinator_phone'] = "None";
            }
            
            if(!empty($value['finance_user_phone'])){
                $value['finance_user_phone'] =$value['finance_user_phone'];
            }else{
                $value['finance_user_phone'] = "None";
            } 
            if($value['status'] == 1){
                $value['status'] = 'Awaiting payment';
            }elseif($value['status'] == 2){
                $value['status'] = 'Paid';
            }elseif($value['status'] == 3){
                $value['status'] = 'Overdue';
            }elseif($value['status'] == 4){
                $value['status'] = 'Cancelled';
            }else{
                $value['status'] = 'Draft';
            }
            if(!empty($value['purchase_order_no'])){
                $value['purchase_order_no'] =$value['purchase_order_no'];
            }else{
                $value['purchase_order_no'] = "None";
            }
            if(empty($value['discount_amount'])){
                $value['discount_amount'] = "N/A";
            }
            if(!empty($value['join_date'])){
                $value['join_date'] = date("d-m-Y",strtotime($value['join_date']));
            }else{
                $value['join_date'] = "N/A";
            }
            $dataArr[] = $value;            
        }
    }else{
        $dataArr[] = array("data"=>'No records to display');
    }
        //echo '<pre>';print_r($dataArr);exit;
        $export = new InvoiceExport($dataArr);

    return Excel::download($export, 'Invoices_Maths300_'.date('dmY').'.xlsx');
    }
}
