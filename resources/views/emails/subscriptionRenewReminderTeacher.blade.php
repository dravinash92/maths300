@extends('layouts.email')

@section('content')
    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        Dear teachers at {{ $school_name }},
        <br><br>
        This is to inform you that your Maths300 account has expired today. Your access to the system will be suspended in the coming days.
        <br><br>
        If you wish to have continued access to Maths300, please contact your Maths300 coordinator {{ $coordinator_name }} as soon as possible.
        <br><br>
    </div>
@stop
