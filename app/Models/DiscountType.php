<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountType extends Model
{
    /**
     * Get the discounts for the DiscountType.
     */
    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }
}
