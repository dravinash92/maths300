/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');
window.Ladda = require('ladda');

/** editor start **/
window.getUserSetting = function () {

};
window.setUserSetting = function () {

}
// default settings
wp.editor.getDefaultSettings = function () {
    return {
        tinymce: {
            theme: "modern",
            skin: "lightgray",
            language: "en",
            formats: {
                alignleft: [{
                    selector: "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
                    styles: {textAlign: "left"}
                }, {selector: "img,table,dl.wp-caption", classes: "alignleft"}],
                aligncenter: [{
                    selector: "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
                    styles: {textAlign: "center"}
                }, {selector: "img,table,dl.wp-caption", classes: "aligncenter"}],
                alignright: [{
                    selector: "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
                    styles: {textAlign: "right"}
                }, {selector: "img,table,dl.wp-caption", classes: "alignright"}],
                strikethrough: {inline: "del"}
            },
            relative_urls: false,
            remove_script_host: false,
            convert_urls: false,
            browser_spellcheck: true,
            fix_list_elements: true,
            entities: "38,amp,60,lt,62,gt",
            entity_encoding: "raw",
            keep_styles: false,
            cache_suffix: "wp-mce-4800-20181219",
            resize: false,
            menubar: false,
            branding: false,
            preview_styles: "font-family font-size font-weight font-style text-decoration text-transform",
            end_container_on_empty_block: true,
            wpeditimage_html5_captions: true,
            wp_lang_attr: "en-US",
            wp_keep_scroll_position: true,
            height: '480',
            wp_shortcut_labels: {
                "Heading 1": "access1",
                "Heading 2": "access2",
                "Heading 3": "access3",
                "Heading 4": "access4",
                "Heading 5": "access5",
                "Heading 6": "access6",
                "Paragraph": "access7",
                "Blockquote": "accessQ",
                "Underline": "metaU",
                "Strikethrough": "accessD",
                "Bold": "metaB",
                "Italic": "metaI",
                "Code": "accessX",
                "Align center": "accessC",
                "Align right": "accessR",
                "Align left": "accessL",
                "Justify": "accessJ",
                "Cut": "metaX",
                "Copy": "metaC",
                "Paste": "metaV",
                "Select all": "metaA",
                "Undo": "metaZ",
                "Redo": "metaY",
                "Bullet list": "accessU",
                "Numbered list": "accessO",
                "Insert\/edit image": "accessM",
                "Remove link": "accessS",
                "Toolbar Toggle": "accessZ",
                "Insert Page Break tag": "accessP",
                "Distraction-free writing mode": "accessW",
                "Keyboard Shortcuts": "accessH"
            },
            // content_css: "{{ asset ('vendor/adminlte/vendor/wp-editor/css/dashicons.min.css')}}",
            plugins: 'charmap colorpicker compat3x directionality fullscreen hr image lists media paste tabfocus textcolor wordpress wpautoresize wpdialogs wpeditimage wpemoji wpgallery wplink wptextpattern wpview table',
            wpautop: false,
            indent: false,
            toolbar1: 'formatselect bold italic | bullist numlist | blockquote | alignleft aligncenter alignright | link unlink | spellchecker',
            toolbar2: "strikethrough underline hr forecolor backcolor | charmap  | outdent,indent | table | undo redo",
            toolbar3: "",
            toolbar4: "",
            tabfocus_elements: "content-html,save-post",
            body_class: "content post-type-post post-status-draft post-format-standard page-template-default locale-en-us insert_block",
            wp_autoresize_on: true,
            add_unload_trigger: false,
            statusbar: false,
            autoScroll: true,
        },
        quicktags: {
            buttons: 'strong,em,link,ul,ol,li,code'
        }
    };
};

editor = {
    init: function (contend_id, add_file_btn_enable) {
        add_file_btn_enable = typeof add_file_btn_enable !== 'undefined' ? add_file_btn_enable : false;
        wp.editor.initialize(
            contend_id,
            {
                mediaButtons: true,
                fileButtons: add_file_btn_enable,
                tinymce: true,
                quicktags: true
            }
        );
    },

    initWithoutImageBtn: function (contend_id) {
        wp.editor.initialize(
            contend_id,
            {
                mediaButtons: false,
                fileButtons: true,
                tinymce: true,
                quicktags: true
            }
        );
    },

    getContent: function (contend_id) {
        return wp.editor.getContent(contend_id);
    },

    getEditor: function () {
        return wp.editor;
    }

};

// cropped new file to be submitted
var newFile;

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type: mimeString});
}

var cropBoxData;
var canvasData;
var cropper;
var _URL = window.URL || window.webkitURL;

// upload image
$("#file_upload").on('change', function () {
    var file = $(this)[0].files[0];
    var category = $('#category').val();
    if (category == 'image') {
        img = new Image();
        var imgwidth = 0;
        var imgheight = 0;
        var maxwidth = 640;
        var maxheight = 640;
        var need_cropper = false;

        img.src = _URL.createObjectURL(file);
        img.onload = function () {
            imgwidth = this.width;
            imgheight = this.height;
            // console.log('load image, imgwidth:' + imgwidth + ' imgheight:' + imgheight);

            if (typeof (FileReader) != "undefined") {
                var image_holder = $("#img_holder");
                image_holder.empty();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("<br />").appendTo(image_holder);
                    $("<img />", {
                        "id": "image",
                        "src": e.target.result,
                        "alt": "maths300 image",
                    }).appendTo(image_holder);

                    // cropper applied if the image size is larger than 640 * 640
                    if (imgwidth >= maxwidth || imgheight >= maxheight) {
                        need_cropper = true;
                    }

                    if (need_cropper) {
                        // cropper
                        var image = document.getElementById('image');
                        var cropper = new Cropper(image, {
                            viewMode: 3,
                        });

                        $("#cropper_btn").click(function () {
                            image_holder.empty();
                            var dataURL = cropper.getCroppedCanvas().toDataURL("image/png");
                            newFile = dataURItoBlob(dataURL);
                            $("<br />").appendTo(image_holder);
                            $("<img />", {
                                "id": "image",
                                "src": dataURL,
                                "alt": "maths300 image",
                            }).appendTo(image_holder);
                            $("#cropper_btn").hide();
                        });
                        $("#cropper_btn").show();
                    }
                }
                image_holder.show();
                reader.readAsDataURL(file);
            } else {
                alert("This browser does not support FileReader. Please use Chrome/Safari/IE 9+");
            }
        }
        img.onerror = function () {
            $("#response").text("not a valid file: " + file.type);
        }
    }

    if (category == 'file') {
        $('#fileName_holder').text(file.name);
    }
});

function clearModal() {
    if (cropper) {
        cropBoxData = cropper.getCropBoxData();
        canvasData = cropper.getCanvasData();
        cropper.destroy();
    }

    if (newFile) {
        newFile = null;
    }

    // fix same file selection issue
    $('#form_upload_file').trigger("reset");
    $("#form-errors").empty();
    $("#fileName_holder").empty();

    if ($("#file_upload")) {
        $("#file_upload").val("");
        $("#file_upload").empty();
    }

    if ($("#img_holder")) {
        $("#img_holder").empty();
    }

    if ($("#cropper_block")) {
        $("#cropper_block").hide();
    }

    if ($("#cropper_btn")) {
        $("#cropper_btn").hide();
    }
    $('#loader').hide();
}

// upload a image
$(document).on('click', '.add_media', function () {
    $('#content_id').val($(this).data('editor'));
    $('#category').val($(this).data('category'));
    $('.modal-title').text('Add image');
    $('.submit').text('Add image');
    $('#file_upload').attr("accept", "image/*");
    $('#fileUploadModal').modal('show');
});

// upload a file
$(document).on('click', '.add_file', function () {
    $('#content_id').val($(this).data('editor'));
    $('#category').val($(this).data('category'));
    $('.modal-title').text('Add file');
    $('.submit').text('Add file');
    $('#file_upload').attr("accept", ".doc, .docx, .xls, .xlsx, .ppt, .pptx, .txt, .pdf");
    $('#fileUploadModal').modal('show');
});

$('#fileUploadModal').on('hidden.bs.modal', function () {
    clearModal();
});

$('#fileUploadModal').on('show.bs.modal', function () {
    clearModal();
});

$('.modal-footer').on('click', '.submit', function (e) {
    e.preventDefault();
    let content_id = $('#content_id').val();
    let form_data = new FormData($('#form_upload_file')[0]);
    if (newFile) {
        $("#file_upload").val("");
        form_data.append("file", newFile, content_id + "_file_upload.jpeg");
    }
    $.ajax({
        type: 'POST',
        url: '/admin/resources',
        processData: false,
        contentType: false,
        async: false,
        cache: false,
        data: form_data,
        success: function (response) {
            toastr.success('File uploaded successfully.', 'Success Alert', {timeOut: 5000});
            let id = response['id'];
            let category = response['category'];
            let name = response['name'];
            let url = response['url'];
            let active_editor = tinymce.get(content_id);

            if (active_editor) {
                let frame = $('#' + content_id + '_ifr');
                let frame_body = frame.contents().find("body");
                var node = active_editor.selection.getNode();
                var dom = active_editor.dom;

                if (category === 'image') {
                    console.log('image selected......');

                    let image = dom.create('img', {
                        "id": id,
                        "src": url,
                        "style": "max-width: 320px;"
                    });

                    let p_wrapper = dom.create( 'p' );
                    p_wrapper.appendChild( image );

                    dom.insertAfter(p_wrapper, node);

                    br = dom.create('br');
                    dom.insertAfter(br, p_wrapper);

                    frame.css({
                        // fixed increment for now
                        height: $('#' + content_id + '_ifr').height() + 180
                    });
                }

                if (category === 'file') {
                    let link = $("<a />", {
                        "id": id,
                        "href": url,
                        "text": name
                    });

                    frame_body.append(link);

                    frame.css({
                        // fixed increment for now
                        height: $('#' + content_id + '_ifr').height() + 10
                    });
                }
            }

            let place_holder = $('#' + content_id + '_place_holder');
            if (place_holder) {
                // empty holder fist
                place_holder.empty();
                if (category == 'image') {
                    let img = $("<img />", {
                        "id": id,
                        "src": url,
                        "style": "max-width: 320px;",
                    });
                    let input = $("<input />", {
                        "name": content_id,
                        "value": id,
                        "type": "hidden"
                    });
                    place_holder.append(img);
                    place_holder.append(input);
                    place_holder.show();
                }
            }
            // modal fadeOut
            $("#fileUploadModal").modal('hide');
        },
        error: function (response) {
            if (response.status === 422) {
                toastr.error('Input validation error.', 'Error Alert', {timeOut: 5000});
                //process validation errors here.
                errorsJson = JSON.parse(response.responseText); //this will get the errors response data.
                //show them somewhere in the markup
                errorsHtml = '<div class="alert alert-danger"><ul>';
                $.each(errorsJson.errors, function (key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                errorsHtml += '</ul></di>';

                $('#form-errors').html(errorsHtml); //appending to a <div id="form-errors"></div> inside form
                $("#cropper_btn").hide();
                newFile = null;
            }
            if (response.status === 500) {
                toastr.error('Server error.', 'Error Alert', {timeOut: 5000});
                // modal fadeOut
                $("#fileUploadModal").modal('hide');
            }
        }
    });
});