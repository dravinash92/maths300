<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address_region');
            $table->string('address_suburb')->nullable();
            $table->string('address_postcode');
            $table->integer('number_of_campuses');
            $table->integer('status');// 0: INACTIVE 1: ACTIVE
            $table->longText('notes')->nullable();

            // Finance: billing information
            $table->string('finance_invoice_email');
            $table->string('billing_address_region');
            $table->string('billing_address_suburb')->nullable();
            $table->string('billing_address_postcode');

            // invoice/payments
            $table->date('renewal_date')->nullable();
            $table->date('last_renewal_date')->nullable();
            $table->string('last_invoice_number')->nullable();
            $table->date('last_paid_date')->nullable();
            $table->date('subscription_expiration_date')->nullable();
            $table->string('payment_method')->nullable();
            $table->date('join_date')->nullable();
            $table->date('activate_date')->nullable();
            $table->date('suspend_date')->nullable();

            // foreign keys
            $table->integer('school_type_id')->unsigned()->index()->nullable();
            $table->foreign('school_type_id')->references('id')->on('school_types');

            $table->integer('school_sector_id')->unsigned()->index()->nullable();
            $table->foreign('school_sector_id')->references('id')->on('school_sectors');

            $table->integer('address_location_id')->unsigned()->index()->nullable();
            $table->foreign('address_location_id')->references('id')->on('locations');

            $table->integer('billing_address_location_id')->unsigned()->index()->nullable();
            $table->foreign('billing_address_location_id')->references('id')->on('locations');

            $table->integer('school_size_student_id')->unsigned()->index()->nullable();
            $table->foreign('school_size_student_id')->references('id')->on('school_size_students');

            $table->integer('school_size_teacher_id')->unsigned()->index()->nullable();
            $table->foreign('school_size_teacher_id')->references('id')->on('school_size_teachers');

            $table->integer('finance_officer_id')->unsigned()->index()->nullable();
            $table->foreign('finance_officer_id')->references('id')->on('users');

            $table->integer('coordinator_id')->unsigned()->index()->nullable();
            $table->foreign('coordinator_id')->references('id')->on('users');

            $table->integer('subscription_plan_id')->unsigned()->index()->nullable();
            $table->foreign('subscription_plan_id')->references('id')->on('subscription_plans');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
