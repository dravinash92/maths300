<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Resource extends Model
{
    /**
     * Get the lessons that owns the resource.
     */
    public function lessons()
    {
        return $this->belongsToMany(Lesson::class);
    }

    /**
     * Delete the given resource from file system
     *
     * @param String $file_path
     */
    public static function deleteRes($file_path)
    {
        // remove file
        if (!empty($file_path) && Storage::disk()->exists($file_path)) {
            Log::alert('delete file: ' . $file_path);
            Storage::delete($file_path);
        }
    }

    /**
     * Delete all unlinked files from file system
     */
    public static function deleteUnlinkedRes()
    {
        // housekeeping work
        $all_res = Resource::all();
        foreach ($all_res as $res) {
            if (empty($res->lesson())) {
                Log::info("housekeeping delete res_id " . $res->id);
                Resource::deleteRes($res->path);
                $res->delete();
            }
        }
    }

    /**
     * get url by id
     */
    public static function getURL($id)
    {
        $res = Resource::find($id);
        $path = $res->path;
        return Resource::getURLByPath($path);
    }

    /**
     * get url by path
     */
    public static function getURLByPath($path)
    {
        $tmp = "public";
        // public dir
        if (substr( $path, 0, strlen($tmp)) === $tmp) {
            $path = substr($path, strlen($tmp)+1);
            $url = config('app.url').Storage::url($path);
        }
        else {
            // other dir (get file by fileController require auth)
            $url = config('app.url').'/file/'.$path;
        }

        Log::debug('URL: ' . $url);

        return $url? $url : "#";
    }
}
