<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SubscriptionPage;

class SubscriptionPageTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('subscription_pages')->delete();

        SubscriptionPage::create([
            'content' =>
                "<p>Maths300 is a subscription service, with a single joining fee and an ongoing annual fee. A subscription lasts for 12 months.</p><p>The licence allows all staff from one campus access the Maths300 lessons. The campus is issued with a user name and a password; staff may access the website from school or home.</p><p>A single teacher licence can be arranged but use of Maths300 is limited to that teacher's students.</p><p>Subscription prices are based on the number of students at the campus. All prices are in AUD and include 10% GST. First-time or lapsed subscribers will be charged a joining fee in addition to the annual fee.</p><p>Download the <a href=\"http://maths300.com/subscribe.pdf\">subscription form</a> and submit the completed form to the <a href=\"/contact\">AAMT office</a>.</p><p>You will be emailed your user name and password once payment has been processed.</p>",
        ]);

    }
}