<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SchoolSizeTeacher;
use App\Models\SubscriptionPlan;

class SubscriptionPlanTableSeeder extends Seeder {

    public function run()
    {
        DB::table('subscription_plans')->delete();
        DB::statement( 'ALTER TABLE subscription_plans AUTO_INCREMENT = 1' );

        SubscriptionPlan::create(
            [
                'joining_fee' => 50.00,
                'annual_fee' => 90.00,
                'total_fee' => 140.00
            ]
        )->schoolSize()->associate(SchoolSizeTeacher::find(1))->save();

        SubscriptionPlan::create(
            [
                'joining_fee' => 150.00,
                'annual_fee' => 190.00,
                'total_fee' => 340.00
            ]
        )->schoolSize()->associate(SchoolSizeTeacher::find(2))->save();

        SubscriptionPlan::create(
            [
                'joining_fee' => 200.00,
                'annual_fee' => 210.00,
                'total_fee' => 410.00
            ]
        )->schoolSize()->associate(SchoolSizeTeacher::find(3))->save();

        SubscriptionPlan::create(
            [
                'joining_fee' => 250.00,
                'annual_fee' => 230.00,
                'total_fee' => 480.00
            ]
        )->schoolSize()->associate(SchoolSizeTeacher::find(4))->save();

        SubscriptionPlan::create(
            [
                'joining_fee' => 270.00,
                'annual_fee' => 240.00,
                'total_fee' => 510.00
            ]
        )->schoolSize()->associate(SchoolSizeTeacher::find(5))->save();

        SubscriptionPlan::create(
            [
                'joining_fee' => 290.00,
                'annual_fee' => 250.00,
                'total_fee' => 540.00
            ]
        )->schoolSize()->associate(SchoolSizeTeacher::find(6))->save();

        SubscriptionPlan::create(
            [
                'joining_fee' => 150.00,
                'annual_fee' => 190.00,
                'total_fee' => 340.00
            ]
        )->schoolSize()->associate(SchoolSizeTeacher::find(7))->save();

        SubscriptionPlan::create(
            [
                'joining_fee' => 250.00,
                'annual_fee' => 230.00,
                'total_fee' => 480.00
            ]
        )->schoolSize()->associate(SchoolSizeTeacher::find(8))->save();
    }

}
