<?php

namespace App\Models;

use App\Mail\UserDisabled;
use App\Mail\SubscriptionRenewReminderTeacher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class School extends Model
{
    /**
     * format join_date
     * @return string
     */
    public function joinDateFormat()
    {
        return Carbon::parse($this->join_date)->format('d/m/Y');
    }

    /**
     * format last_renewal_date
     * @return string
     */
    public function lastRenewalDateFormat()
    {
        return $this->last_renewal_date ? Carbon::parse($this->last_renewal_date)->format('d/m/Y') : "N/A";
    }

    /**
     * format date_last_paid
     * @return string
     */
    public function lastPaidDateFormat()
    {
        return $this->date_last_paid ? Carbon::parse($this->date_last_paid)->format('d/m/Y') : "N/A";
    }

    /**
     * format subscription_expiration_date
     * @return string
     */
    public function subscriptionExpirationDateFormat()
    {
        return $this->subscription_expiration_date ? Carbon::parse($this->subscription_expiration_date)->format('d/m/Y') : "N/A";
    }

    /**
     * joiningFee on paid account will not be applicable
     * @return bool
     */
    public function joiningFeeApplicable()
    {
        $has_subscription_expiration_date = $this->subscription_expiration_date;
        if ($has_subscription_expiration_date) {
            return Carbon::parse($this->subscription_expiration_date)->copy()->isPast();
        }
        return true;
    }

    public function getStatus()
    {
        $status = "unknown";

        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status == 0) {
            $status = "inactive";
        }
        if ($this->status == 1) {
            $status = "active";
        }
        if ($this->status == 2) {
            $status = "suspended";
        }
        if ($this->status == 3) {
            $status = "archived";
        }
        if ($this->status == 4) {
            $status = "imported";
        }

        return $status;
    }

    public function getPaymentStatus()
    {
        $status = "N/A";

        // find the latest invoice
        $latest_invoice = $this->invoices()->orderBy('id', 'desc')->first();
        if (!empty($latest_invoice)) {
            $status = $latest_invoice->getStatus();
        }

        return $status;
    }

    public function hasActiveInvoice()
    {
        $res = 0;
        $active_invoice_count = 0;
        foreach ($this->invoices as $invoice) {
            if (!empty($invoice->send_date) && $invoice->isActivateSchoolAllowed()) {
                $active_invoice_count += 1;
            }
        }

        if ($active_invoice_count >= 1) {
            $res = 1;
        }

        return $res;
    }

    public function isActivateAllowed()
    {
        return $this->status == 0 || $this->status == 2 || $this->status == 4;
    }

    public function isSuspendAllowed()
    {
        return $this->status == 1;
    }

    public function isArchiveAllowed()
    {
        return $this->status == 2;
    }

    public function isDeleteAllowed()
    {
        return $this->status == 3;
    }

    public function isReinstateAllowed()
    {
        return $this->status == 3;
    }

    /**
     * Get the school type that belongs to this school.
     */
    public function schoolType()
    {
        return $this->belongsTo(SchoolType::class);
    }

    /**
     * Get the school sector that belongs to this school.
     */
    public function schoolSector()
    {
        return $this->belongsTo(SchoolSector::class);
    }

    /**
     * Get the state that belongs to this school.
     */
    public function addressLocation()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * Get the state that belongs to this school.
     */
    public function billingAddressLocation()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * Get the size of students that belongs to this school.
     */
    public function schoolSizeStudent()
    {
        return $this->belongsTo(SchoolSizeStudent::class);
    }

    /**
     * Get the size of teachers that belongs to this school.
     */
    public function schoolSizeTeacher()
    {
        return $this->belongsTo(SchoolSizeTeacher::class);
    }

    /**
     * Get the finance Officer that belongs to this school.
     */
    public function financeOfficer()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the $coordinator that belongs to this school.
     */
    public function coordinator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The users that belong to the school.
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get the subscription plan that belongs to this school.
     */
    public function subscriptionPlan()
    {
        return $this->belongsTo(SubscriptionPlan::class);
    }

    /**
     * Get the invoices that belongs to this school.
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'to_id');
    }

    /**
     * Scope a query to only include given address_location.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $address_location
     */
    public function scopeAddressLocation($query, $address_location)
    {
        $query->with('addressLocation')->whereHas('addressLocation', function ($query) use ($address_location) {
            $query->where('id', $address_location);
        });
    }

    /**
     * Scope a query to only include given schoolType.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $schoolType
     */
    public function scopeSchoolType($query, $schoolType)
    {
        $query->with('schoolType')->whereHas('schoolType', function ($query) use ($schoolType) {
            $query->where('id', $schoolType);
        });
    }

    /**
     * Scope a query to only include given schoolSector.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $schoolSector
     */
    public function scopeSchoolSector($query, $schoolSector)
    {
        $query->with('schoolSector')->whereHas('schoolSector', function ($query) use ($schoolSector) {
            $query->where('id', $schoolSector);
        });
    }

    /**
     * Scope a query to only include given $invoice.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $invoice
     */
    public function scopeInvoice($query, $invoice)
    {
        $query->with('invoices')->whereHas('invoices', function ($query) use ($invoice) {
            $query->where('id', $invoice);
        });
    }

    /**
     * Scope a query to only include given expiry date range.
     * @param $query
     * @param $from
     * @param $to
     */
    public function scopeExpiryDate($query, $from, $to)
    {
        if ($from && $to) $query->whereBetween('subscription_expiration_date', [$from, $to]);
    }

    /**
     * Scope a query to only include given keywords.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%')
            ->orWhere('address_region', 'like', '%' . $keywords . '%')
            ->orWhere('address_postcode', 'like', '%' . $keywords . '%');
    }

    /**
     * activate school account and associated users
     */
    public function activate()
    {
        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status != 1) {
            $this->status = 1;
            $this->activate_date = today();

            $this->save();

            // active users in this school
            foreach ($this->users as $user) {
                $user->activate();
            }
            Log::info("Activate school id: " . $this->id . " name: " . $this->name);
        }
    }

    /**
     * suspend school account and associated users
     */
    public function suspend()
    {
        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status != 2) {
            Log::info("Suspend school:" . $this->name);
            $this->status = 2;
            $this->suspend_date = today();
            $this->save();

            // disable users in this school
            foreach ($this->users as $user) {
                $user->status = 0; //0: disabled
                $user->save();
                // send notification emails to all users
                // Mail sender is in queue by default
                Mail::to($user)->send(new UserDisabled($user));
            }
        }
    }

    /**
     * archive school
     */
    public function archive()
    {
        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status != 3) {
            $this->status = 3;
            $this->save();
        }
    }

    /**
     * reinstate school
     */
    public function reinstate()
    {
        // 0:inactive 1:active 2:suspended 3:archived
        if ($this->status != 0) {
            $this->status = 0;
            $this->save();
        }
    }

    /**
     * setExpiryDate school
     */
    public function setExpiryDate($d)
    {
        Log::info("setExpiryDate school id: " . $this->id . ", name:" . $this->name . ", new expiry date: " . $d);
        $this->subscription_expiration_date = $d;
        $this->save();
    }

    /**
     * Scope a query to only include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $status
     */
    public function scopeStatus($query, $status)
    {
        $query->where('status', $status);
    }

    /**
     * Scope a query to only include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $status
     */
    public function scopeStatusIn($query, $status)
    {
        $query->whereIn('status', $status);
    }

    /**
     * Scope a query to not include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $status
     */
    public function scopeStatusNot($query, $status)
    {
        $query->where('status', '!=', $status);
    }

    /**
     * check if user exists in this school
     * @param $email
     * @return bool
     */
    public function isEmailExists($email)
    {
        $exist = false;
        // active users in this school
        foreach ($this->users as $user) {
            if ($user->email == $email) {
                $exist = true;
                break;
            }
        }
        return $exist;
    }

    public function getCoordinatorEmailHtml()
    {
        if ($this->coordinator) {
            return '<a href="mailto:' . $this->coordinator->email . '" target=\'_blank\'">' . $this->coordinator->email . '</a>';
        } else {
            return 'N/A';
        }
    }

    public function getFinanceOfficerEmailHtml()
    {
        if ($this->financeOfficer) {
            return '<a href="mailto:' . $this->financeOfficer->email . '" target=\'_blank\'">' . $this->financeOfficer->email . '</a>';
        } else {
            return 'N/A';
        }
    }

    public function validateSubscriptionUserLimit()
    {
        Log::info("validate subscription plan user limit started......");
        $result = array(
            "validated" => true,
            "msg" => 'OK',
        );
        $current_user_count = count($this->users);
        if ($this->subscriptionPlan) {
            $current_user_limit = $this->subscriptionPlan->maximum_user;
            Log::info("current_user_count: " . $current_user_count . " current_user_limit: " . $current_user_limit);
            if ($current_user_count >= $current_user_limit) {
                $result['validated'] = false;
                $result['msg'] = 'School has reached subscription plan user limit ' . $current_user_limit;
            }
        }
        $converted_res = $result['validated'] ? 'true' : 'false';
        Log::info("validate subscription plan user limit, result validated: " . $converted_res . ", msg: " . $result['msg']);
        return $result;
    }
    /**
     * Get data to generate user reports
     * @param $where
     * @return User
     */
    public static function getschools($where)
    {
        return parent::selectRaw("schools.id,schools.name as school_name,schools.address_region,countries.name as country,locations.name as location,school_types.name as type_name,school_sectors.name as sector_name,school_size_students.name as size_name,schools.number_of_campuses,schools.tags,school_size_teachers.name,schools.status as exp_status,schools.join_date,schools.subscription_expiration_date,schools.last_renewal_date,coordinator.name as coordinator_name,coordinator.email as coordinator_email,coordinator.phone as coordinator_phone,coordinator.position as coordinator_position,finance_user.name as finance_user_name,finance_user.email as finance_user_email,finance_user.phone as finance_user_phone,finance_user.position as finance_user_position,schools.finance_invoice_email,schools.suspend_date as total_user,schools.billing_address_location_id as users_active,schools.purchase_order_no,schools.notes")
             ->leftjoin("users as coordinator","schools.coordinator_id", "=" ,"coordinator.id")
             ->leftjoin("users as finance_user","schools.finance_officer_id", "=" ,"finance_user.id")
             ->leftjoin("locations","locations.id", "=" ,"schools.address_location_id")                
             ->leftjoin("countries","countries.id", "=" ,"locations.country_id")                
             ->leftjoin("school_types","school_types.id", "=" ,"schools.school_type_id")                
             ->leftjoin("school_sectors","school_sectors.id", "=" ,"schools.school_sector_id")                
             ->leftjoin("school_size_teachers","school_size_teachers.id", "=" ,"schools.school_size_teacher_id")
             ->leftjoin("school_size_students","school_size_students.id", "=" ,"schools.school_size_student_id")
             ->whereRaw($where)
             ->get();
    }

    /**
     * update notes
     */
    public function notes($notes)
    {
        $this->notes = $notes;
        $this->save();
    }
}
