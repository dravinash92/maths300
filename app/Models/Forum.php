<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{

    /**
     * The topics that belong to the Forum.
     */
    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    public function getTopicsCount()
    {
        return $this->topics()->count();
    }

    public function getRepliesCount()
    {
        $sum = 0;
        foreach($this->topics as $topic) {
            $sum += $topic->replies()->count();
        }
        return $sum;
    }

    public function lastReply()
    {
        $array = array();
        foreach($this->topics as $topic) {
            $array[] = $topic->lastReply();
        }
        $collection = collect($array);
        $sorted = $collection->sortByDesc('updated_at');
        return $sorted->values()->first();
    }

    public function getLastReplyDateDiffForHumans()
    {
        return $this->lastReply() ? $this->lastReply()->getUpdatedAtDateDiffForHumans() : "";
    }

    public function getLastReplier()
    {
        return $this->lastReply() ? $this->lastReply()->author->name : "";
    }

    public function getStatus()
    {
        $status = "unknown";

        // 0: closed 1: open
        if ($this->status == 0) {
            $status = "closed";
        }
        if ($this->status == 1) {
            $status = "open";
        }

        return $status;
    }

    public function isOpenAllowed()
    {
        return $this->status == 0;
    }

    public function isCloseAllowed()
    {
        return $this->status == 1;
    }

    /**
     * Scope a query to only include given keywords.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('title', 'like', '%' . $keywords . '%');
    }

    /**
     * Scope a query to only include given visibility.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $visibility
     */
    public function scopeVisibility($query, $visibility)
    {
        $query->where('visibility', $visibility);
    }

    /**
     * build data for frontend [Select2 data format]
     *
     * ref: https://select2.org/data-sources/formats
     * @return array
     */
    public static function data4Select() {
        $all_data = Forum::all();
        $results = array();
        foreach ($all_data as $d) {
            $tmp['id'] = $d->id;
            $tmp['text'] = $d->title;
            $results[] = $tmp;
        }

        return $results;
    }

    /**
     * build data for frontend [Select2 data format]
     *
     * ref: https://select2.org/data-sources/formats
     * @return array
     */
    public static function topicData4Select()
    {
        // Grouped data
        $all_data = Forum::all();
        $results = array();
        foreach ($all_data as $forum) {
            $group['text'] = $forum->title;
            $topics = array();
            foreach ($forum->topics as $topic) {
                $tmp['id'] = $topic->id;
                $tmp['text'] = $topic->title;
                $topics[] = $tmp;
            }
            $group['children'] = $topics;
            $results[] = $group;
        }

        return $results;
    }

}
