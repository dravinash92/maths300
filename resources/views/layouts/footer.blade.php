<?php 
use App\Models\Setting;
$footer_email = Setting::get('Email');
?>

<footer id="colophon" class="site-footer">

    <div class="footer" style="font-size:16px">

        <div class="container">
            <div class="row">
                <aside id="siteorigin-panels-builder-11"
                       class="thim-footer-one-course widget widget_siteorigin-panels-builder footer_widget">
                    <div id="pl-w5c08c12d3aaca" class="panel-layout">
                        <div id="pg-w5c08c12d3aaca-0" class="panel-grid panel-no-style">
                            <div id="pgc-w5c08c12d3aaca-0-0" class="panel-grid-cell">
                                <div id="panel-w5c08c12d3aaca-0-0-0"
                                     class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-first-child panel-last-child"
                                     data-index="0">
                                    <div class="textwidget">
                                        <div class="thim-footer-info">
                                            <div class="footer-logo"><img
                                                        class="alignnone size-full wp-image-10"
                                                        src="{{ asset('images/logo.png') }}"
                                                        alt="logo mangoedu" width="145" height="40"/></div>
                                            <div class="info-email" ><i class="fa fa-envelope"></i>
                                                <a href="mailto:{{$footer_email}}">
                                                    <span class="__cf_email__">{{$footer_email}}</span>
                                                </a>
                                            </div>

                                            <div class="info-phone" ><i class="fa fa-phone"></i>
                                                <a href="tel:+02 6201 5265" title="phone">(02) 6201 5265</a>

                                            </div>
                                            <div class="info-phone" >
                                                <a href="{{ route('front.subscription.termsAndConditions', 2) }}">Terms of use</a>
                                            </div>
                                            <div class="info-social"><a
                                                        href="https://www.facebook.com/aamtinc"
                                                        class="fa fa-facebook"
                                                        title="facebook"></a><a
                                                        href="https://twitter.com/aamtinc"
                                                        class="fa fa-twitter"
                                                        title="twitter"></a>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div id="pgc-w5c08c12d3aaca-0-1" class="panel-grid-cell"></div>
                        </div>
                    </div>
                </aside>
            </div>
            {{--<div class="row">--}}
            {{--<div class="col-sm-6"><p class="text-copyright">© 2018 maths300.com All Rights--}}
            {{--Reserved</p></div>--}}
            {{--</div>--}}
        </div>
    </div>
</footer>
