@extends('layouts.fronthome')

@section('sub_breadcrumbs')
<a itemprop="item" href="/home" title="Home"><span itemprop="name">Dashboard</span></a>
@endsection

@section('page-content')
<div id="dashboard-content" class="myaccount-right">
    <p>
        Welcome! <strong>{{ Auth::user()->name }}</strong>
    <p>
        School details: <br>
        <h6>Address</h6>
        {{ $school->address_region }} <br>
        {{ $school->address_suburb }} {{ $school->addressLocation->name }} {{ $school->address_postcode }}
        <hr>

        <h6>School Type</h6>
        {{ $school->schoolType->name }}
        <hr>

        <h6>School Sector</h6>
        {{ $school->schoolSector->name }}
        <hr>

        <h6>School Size</h6>
        {{ $school->schoolSizeTeacher->name }}
        <hr>

        Subscription details: <br>
        <h6>Subscription Plan</h6>
        {{ $school->subscriptionPlan? $school->subscriptionPlan->getDesc() : 'N/A' }}
        <hr>
        <h6>Expiry date</h6>
        {{ $school->subscriptionExpirationDateFormat() }}
        <hr>
    <p>
        If the school information or subscription plan is incorrect, please contact <a href="mailto:maths300@aamt.edu.au">maths300@aamt.edu.au</a>to update school detail
</div>
@stop
