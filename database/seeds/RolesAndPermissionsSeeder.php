<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions

        // lesson
        Permission::create(['name' => 'manage-lesson']); // menu entry control
        Permission::create(['name' => 'add-lesson']);
        Permission::create(['name' => 'edit-lesson']);
        Permission::create(['name' => 'view-lesson']);
        Permission::create(['name' => 'suspend-lesson']);
        Permission::create(['name' => 'publish-lesson']);
        Permission::create(['name' => 'delete-lesson']);

        // school
        Permission::create(['name' => 'manage-school']); // menu entry control
        Permission::create(['name' => 'add-school']);
        Permission::create(['name' => 'edit-school']);
        Permission::create(['name' => 'view-school']);
        Permission::create(['name' => 'activate-school']);
        Permission::create(['name' => 'suspend-school']);
        Permission::create(['name' => 'archive-school']);
        Permission::create(['name' => 'reinstate-school']);
        Permission::create(['name' => 'delete-school']);

        // invoice
        Permission::create(['name' => 'manage-invoice']); // menu entry control
        Permission::create(['name' => 'gen-invoice']);
        Permission::create(['name' => 'view-invoice']);
        Permission::create(['name' => 'download-invoice']);
        Permission::create(['name' => 'send-invoice']);
        Permission::create(['name' => 'markaspaid-invoice']);
        Permission::create(['name' => 'cancel-invoice']);

        // user
        Permission::create(['name' => 'manage-user']); // menu entry control
        Permission::create(['name' => 'add-user']);
        Permission::create(['name' => 'edit-user']);
        Permission::create(['name' => 'view-user']);
        Permission::create(['name' => 'activate-user']);
        Permission::create(['name' => 'suspend-user']);
        Permission::create(['name' => 'archive-user']);
        Permission::create(['name' => 'reinstate-user']);
        Permission::create(['name' => 'delete-user']);
        Permission::create(['name' => 'resetpass-user']);

        // create roles and assign created permissions
        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'school-admin']);
        $role->givePermissionTo(['edit-school', 'view-school', 'manage-user', 'add-user','edit-user','view-user','activate-user','suspend-user','archive-user','reinstate-user','delete-user','resetpass-user']);

        $role = Role::create(['name' => 'school-finance-officer']);
        $role->givePermissionTo(['edit-user','view-user','resetpass-user']);

        $role = Role::create(['name' => 'school-teacher']);
        $role->givePermissionTo(['edit-user','view-user','resetpass-user']);
    }
}