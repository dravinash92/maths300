<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lesson;
use App\Models\School;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['lessons_count'] = Lesson::all()->count();
        $arr['schools_count'] = School::all()->count();
        $arr['users_count'] = User::all()->count();
        $arr['recent_notices'] = array();

        return view('admin.home')->with($arr);
    }
}
