<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name'];
    /**
     * Get the cities for the Country.
     */
    public function locations()
    {
        return $this->hasMany(Location::class);
    }

    /**
     * build data for frontend [Select2 data format]
     *
     * ref: https://select2.org/data-sources/formats
     * @return array
     */
    public static function data4Select()
    {
        // Grouped data
        $all_data = Country::all();
        $results = array();
        foreach ($all_data as $country) {
            $group['text'] = $country->name;
            $cities = array();
            foreach ($country->locations as $l) {
                $tmp['id'] = $l->id;
                $tmp['text'] = $l->name;
                $cities[] = $tmp;
            }
            $group['children'] = $cities;
            $results[] = $group;
        }

        return $results;
    }

    public static function countryData4Select()
    {
        $all_data = Country::all();
        $results = array();
        foreach ($all_data as $country) {
            $group['text'] = $country->name;
            $results[] = $group;
        }

        return $results;
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeName($query, $name)
    {
        return $query->where('name', '=', $name);
    }

    /**
     * check if the current country is Auzz
     */
    public function isAustralia()
    {
        return $this->name == 'Australia';
    }
}
