<?php

namespace App\Http\Controllers\Front;

use App\Models\AboutPage;
use App\Http\Controllers\Controller;

class AboutPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['aboutpage'] = AboutPage::first();
        return view('front.about')->with($arr);
    }
}
