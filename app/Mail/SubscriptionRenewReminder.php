<?php

namespace App\Mail;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionRenewReminder extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @var invoice
     */
    public $invoice;

    public $recipient;

    public $subscription_expiration_date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, $recipient, $subscription_expiration_date)
    {
        $this->invoice = $invoice;
        $this->recipient = $recipient;
        $this->subscription_expiration_date = $subscription_expiration_date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.subscriptionRenewReminder')->subject('Your Maths300 subscription is about to expire');
    }
}
