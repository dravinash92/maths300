@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.users.index') }}">Users</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Users</h3>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('admin.users.create') }}" class="btn btn-primary btn-flat">Add User</a>
            </div>

        </div>
        <!-- /.box-header -->

        <div class="box-body">

            <form id="userSearchForm" role="form" method="get" action="{{ route('admin.users.index') }}">
                {{ csrf_field() }}
                <div class="row "> 
                    <div class="col-xs-3">
                        <select data-placeholder="Select a school" class="form-control select2_school"
                                style="width: 100%;" id="q_school" name="q_school">
                            <option></option>
                        </select>
                    </div>

                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter keywords (name/email/phone)..."
                                   value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="usersearchBtn" type="submit"
                                        class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>

            <div class="col-md-12 text-right">
                <a class="archive btn btn-primary btn-flat" href="{{ route('admin.users.showArchived') }}">Archived</a>
            </div>

            <table id="tab_users" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Last login&nbsp;<a href="{{ route('admin.users.index', ['sortBy'=>'last_login', 'order' => $order == 'asc' ? 'desc' : 'asc', 'q' => $q, 'q_school' => $q_school ]) }}"><i class="fa fa-sort"></i></a></th>
                    <th>Name <a href="{{ route('admin.users.index', ['sortBy'=>'name', 'order' => $order == 'desc' ? 'asc' : 'desc', 'q' => $q, 'q_school' => $q_school ]) }}"><i class="fa fa-sort"></i></a></th>
                    <th>School</th>
                    <th>Email&nbsp;<a href="{{ route('admin.users.index', ['sortBy'=>'email', 'order' => $order == 'desc' ? 'asc' : 'desc', 'q' => $q, 'q_school' => $q_school ]) }}"><i class="fa fa-sort"></i></a></th>
                    <th>Phone&nbsp;<a href="{{ route('admin.users.index', ['sortBy'=>'phone', 'order' => $order == 'desc' ? 'asc' : 'desc', 'q' => $q, 'q_school' => $q_school ]) }}"><i class="fa fa-sort"></i></a></th>
                    <th>Status&nbsp;<a href="{{ route('admin.users.index', ['sortBy'=>'status', 'order' => $order == 'desc' ? 'asc' : 'desc', 'q' => $q, 'q_school' => $q_school ]) }}"><i class="fa fa-sort"></i></a></th>
                    <th>Menu</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($users as $l)
                    <tr class="item{{ $l->id }}">
                        <td class="col1">{{ $l->lastLoginFormat() }}</td>
                        <td><a href='{{ route('admin.users.show',$l->id) }}'>{{ $l->name }}</a></td>
                        <td>
                            @if( count($l->schools) )
                                @foreach($l->schools()->orderBy('schools.id')->pluck('schools.name','schools.id') as $k=>$v )
                                    <a href='{{ route('admin.schools.show',$k) }}'>{{  $v }}</a>
                                @endforeach
                            @else
                                N/A
                            @endif
                        </td>
                        <td><a href="mailto:{{ $l->email }}" target="_blank">{{ $l->email }}</a></td>
                        <td><a href="tel:{{ $l->phone }}">{{ $l->phone }}</a></td>
                        <td>{{ $l->getStatus() }}</td>
                        <td>
                            @if(!$l->hasRole('super-admin'))
                                <a href="{{ route('admin.users.edit',$l->id) }}" class="btn btn-primary">Edit</a>
                                <button class="resetpass-modal btn btn-primary" data-email="{{ $l->email }}"
                                        data-name="{{ $l->name }}">Reset Password
                                </button>
                                @if( $l->isActivateAllowed() )
                                    <button class="activate-modal btn btn-primary" data-id="{{ $l->id }}"
                                            data-name="{{ $l->name }}">Activate
                                    </button>
                                @endif

                                @if( $l->isSuspendAllowed() )
                                    <button class="suspend-modal btn btn-primary" data-id="{{ $l->id }}"
                                            data-name="{{ $l->name }}">Suspend
                                    </button>
                                @endif

                                @if( $l->isArchiveAllowed() )
                                    <button class="archive-modal btn btn-primary" data-id="{{ $l->id }}"
                                            data-name="{{ $l->name }}">Archive
                                    </button>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {{ $users->links() }}
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @modal(['action' => 'activate', 'model' => 'user', 'form_action' => '/admin/users/activate'])
    @endmodal

    @modal(['action' => 'suspend', 'model' => 'user', 'form_action' => '/admin/users/suspend'])
    @endmodal

    @modal(['action' => 'archive', 'model' => 'user', 'form_action' => '/admin/users/archive'])
    @endmodal

    <!-- Modal form to reset user password -->
    <div id="resetpassModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Are you sure you want to reset password?</h3>
                    <br/>
                    <!-- form start -->
                    <form class="form-horizontal" role="form" id="form_paid" method="post"
                          action="{{ route('admin.users.sendResetPassEmail') }}">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name_resetpass" name="name"
                                       readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email_resetpass" name="email"
                                       readonly="readonly">
                                <span class="text-info">Note: A new password will be sent to this email</span>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger resetpass" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-alert'></span> Reset
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/date-uk.js"></script>
    <script>
        $(document).ready(function () {

            // Initialize Select2 elements
            let select2_school = $('.select2_school').select2({
                    allowClear: true,
                    data: {!! json_encode($schools)!!}
                });

            @if(old('q_school'))
            select2_school.val({!! old('q_school') !!}).trigger('change');
            @endif

            $('#tab_users').DataTable({
                "processing": true,
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search users......"
                },
                'ordering': false,
                'info': true,
                'columnDefs': [
                    {
                        "type": "date-uk",
                        "targets": 0
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 6
                    }],
                'autoWidth': false,
                'columns': [{
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "15%"
                }, {
                    "width": "18%"
                }, {
                    "width": "10%"
                }, {
                    "width": "8%"
                }, {
                    "width": "32%"
                }
                ]
            });

            $(".dataTables_filter").hide();
        });

        // Automatically trigger the loading animation on click
        Ladda.bind('button[type=submit]');

        $(document).on('click', '.resetpass-modal', function () {
            $('.modal-title').text('Reset password');
            $('#name_resetpass').val($(this).data('name'));
            $('#email_resetpass').val($(this).data('email'));
            $('#resetpassModal').modal('show');
        });
        $('.modal-footer').on('click', '.resetpass', function (evt) {
            evt.preventDefault();
            $.ajax({
                type: 'POST',
                url: location.origin + '/admin/users/sendResetPassEmail',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'email': $('input[name=email]').val(),
                },
                success: function (response) {
                    if (response.status === 1) {
                        toastr.error(response.data, 'Error Alert', {timeOut: 5000});
                    }
                    if (response.status === 0) {
                        toastr.success('Reset password request was handled successfully.', 'Success Alert', {timeOut: 5000});
                    }
                },
                error: function (response) {
                    console.log(response);
                    if (response.status === 500) {
                        toastr.error('Server error.', 'Error Alert', {timeOut: 5000});
                    } else {
                        if (response.responseJSON.error) {
                            toastr.error(response.responseJSON.error, 'Error Alert', {timeOut: 5000});
                        } else {
                            toastr.error('Bad Request', 'Error Alert', {timeOut: 5000});
                        }
                    }
                }
            });

        });
    </script>

    @modaljs(['action' => 'activate'])
    @endmodaljs

    @modaljs(['action' => 'suspend'])
    @endmodaljs

    @modaljs(['action' => 'archive'])
    @endmodaljs
@endpush
