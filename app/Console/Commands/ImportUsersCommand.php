<?php

namespace App\Console\Commands;

use App\Jobs\ImportUsers;
use Illuminate\Console\Command;

class ImportUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:userimport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ImportUsers::dispatch();
        $this->info("Run user import program successfully.");
    }
}
