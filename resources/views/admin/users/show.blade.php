@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.users.index') }}">Users</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Show user</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <h4>Name</h4>
            {{ $user->name }}
            <hr>

            <h4>Email</h4>
            <a href="mailto:{{ $user->email }}" target="_blank">{{ $user->email }}</a>
            <hr>

            <h4>Phone</h4>
            <a href="tel:{{ $user->phone }}">{{ $user->phone }}</a>
            <hr>

            <h4>Position</h4>
            {{ $user->position }}
            <hr>

            <h4>School</h4>
            @if(count($user->schools()->orderBy('schools.id')->pluck('schools.name')->toArray()))
                @foreach($user->schools()->orderBy('schools.id')->pluck('schools.name','schools.id') as $k=>$v )
                    <a href='{{ route('admin.schools.show',$k) }}'>{{  $v }}</a>
                @endforeach
            @else
                N/A
            @endif
            <hr>

            <h4>Role</h4>
            {{ $user->getRoleNames() }}
            <hr>

            <h4>Status</h4>
            {{ $user->getStatus() }}
            <hr>

            <h4>Last login</h4>
            {{ $user->lastLoginFormat() }}
            <hr>

            <h4>Created at</h4>
            {{ $user->created_at }}
            <hr>

            <h4>Updated at</h4>
            {{ $user->updated_at }}
            <hr>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.users.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
@endpush
