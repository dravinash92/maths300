<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDF;

class Invoice extends Model
{
    /**
     * Get the to that belongs to this invoice.
     */
    public function to()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * Get the discount that belongs to this invoice.
     */
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * The schools that belong to the subscription plan.
     */
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function getStatus()
    {
        $status = "N/A";

        // 0: Draft 1:Awaiting payment 2: Paid 3: Overdue 4:Canceled
        if ($this->status == 0) {
            $status = "draft";
        }
        if ($this->status == 1) {
            $status = "awaiting-payment";
        }
        if ($this->status == 2) {
            $status = "paid";
        }
        if ($this->status == 3) {
            $status = "overdue";
        }
        if ($this->status == 4) {
            $status = "cancelled";
        }

        return $status;
    }

    public static function isCreateAllowed($school_id)
    {
        Log::info('######check isCreateAllowed start######');
        $allow = 0;
        $school = School::findOrFail($school_id);

        $emptyFinanceOfficer = empty($school->financeOfficer);
        $emptyCoordinator = empty($school->coordinator);
        if (!$emptyCoordinator && !$emptyFinanceOfficer) {
            $allow = 1;
        } else {
            Log::error('empty FinanceOfficer or empty Coordinator');
        }

//        $invoices = $school->invoices;
//        $all_invoice_count = count($invoices);
//        // all invoices are cancelled
//        $cancelled_invoice_count = 0;
//        foreach ($invoices as $invoice) {
//            if ($invoice->isCancelled()) {
//                $cancelled_invoice_count += 1;
//            }
//        }
//        Log::info('cancelled_invoice_count:' . $cancelled_invoice_count);
//        Log::info('all_invoice_count:' . $all_invoice_count);
//
//        if ($cancelled_invoice_count == $all_invoice_count) {
//            $allow = 1;
//        }
//
//        // school with paid invoices exists
//        // next payable invoice has not been generated [6 weeks before subscription expire date]
//        if ($school->last_renewal_date) {
//            Log::info('subscription_expiration_date:' . $school->subscription_expiration_date);
//            Log::info('6 weeks before subscription_expiration_date:' . Carbon::parse($school->subscription_expiration_date)->copy()->subWeeks(6));
//            if (Carbon::parse($school->subscription_expiration_date)->copy()->subWeeks(6)->isPast()) {
//                // no new invoice created, then add new invoice allowed
//                $issue_date = Carbon::parse($school->subscription_expiration_date)->copy()->subWeeks(6);
//                $tmp_invoices = Invoice::where('issue_date', '>=', $issue_date)->get();
//                if (count($tmp_invoices) == 0) {
//                    $allow = 1;
//                }
//            }
//        }
        Log::info('allow:' . $allow);
        Log::info('######check isCreateAllowed end######');

        return $allow;
    }

    public function isSendAllowed()
    {
        return !in_array($this->status, array(1, 2, 3, 4));
    }

    public function isMarkAsPaidAllowed()
    {
        return !in_array($this->status, array(0, 2, 4));
    }

    public function isCancelAllowed()
    {
        return !in_array($this->status, array(2, 4));
    }

    public function isActivateSchoolAllowed()
    {
        return in_array($this->status, array(1, 2));
    }

    public function isSendReminderAllowed()
    {
        return $this->isOverDue();
    }

    public function isPaid()
    {
        return $this->status == 2;
    }

    public function isCancelled()
    {
        return $this->status == 4;
    }

    /**
     * format issue_date
     * @return string
     */
    public function issueDateFormat()
    {
        return $this->issue_date ? Carbon::parse($this->issue_date)->format('d/m/Y') : 'N/A';
    }

    /**
     * format due_date
     * @return string
     */
    public function dueDateFormat()
    {
        return $this->due_date ? Carbon::parse($this->due_date)->format('d/m/Y') : 'N/A';
    }

    /**
     * format send_date
     * @return string
     */
    public function sendDateFormat()
    {
        return $this->send_date ? Carbon::parse($this->send_date)->format('d/m/Y') : 'N/A';
    }

    /**
     * format paid_date
     * @return string
     */
    public function paidDateFormat()
    {
        return $this->paid_date ? Carbon::parse($this->paid_date)->format('d/m/Y') : 'N/A';
    }

    /**
     * format last_reminder_date
     * @return string
     */
    public function lastReminderDateFormat()
    {
        return $this->last_reminder_date ? Carbon::parse($this->last_reminder_date)->format('d/m/Y') : 'N/A';
    }

    /**
     * format grace_period_end_date
     * @return string
     */
    public function gracePeriodEndDateFormat()
    {
        return Carbon::parse($this->grace_period_end_date)->format('d/m/Y');
    }

    public function isOverDue()
    {
        return $this->status == 3 ? 1 : 0;
    }

    public function overDueWeeks()
    {
        $overdue_weeks = 0;
        if ($this->status == 3) {
            $overdue_weeks = Carbon::parse($this->due_date)->copy()->diffInWeeks(today());
        }

        return $overdue_weeks;
    }

    public function getSchoolStatus()
    {
        return $this->to->getStatus();
    }

    public function overDueDays()
    {
        $overdue_days = 0;
        if ($this->status == 3) {
            $overdue_days = Carbon::parse($this->due_date)->copy()->diffInDays(today());
        }

        return $overdue_days;
    }

    public function overDueDiff()
    {
        return now()->diffForHumans($this->due_date);
    }

    public function isGracePeriodExpire()
    {
        return Carbon::parse($this->grace_period_end_date)->copy()->isPast() ? 1 : 0;
    }

    public function newSubscriptionExpirationDateFormat()
    {
        return Carbon::parse($this->newSubscriptionExpirationDate())->format('d/m/Y');
    }

    public function newSubscriptionExpirationDate()
    {
        $new_date = $this->to->subscription_expiration_date;
        if ($this->isGracePeriodExpire()) {
            // existing school account
            if ($this->to->last_renewal_date) {
                $new_date = today()->copy()->addYear(1);
            } else {
                // new school account
                $new_date = today()->copy()->addWeeks(46);
            }
        }
        return $new_date;
    }

    /**
     * format subscription_expiration_date
     * @return string
     */
    public function subscriptionExpirationDateFormat()
    {
        return $this->to->subscription_expiration_date ? Carbon::parse($this->to->subscription_expiration_date)->format('d/m/Y') : 'N/A';
    }

    /**
     * build data for frontend [Select2 data format]
     *
     * ref: https://select2.org/data-sources/formats
     * @return array
     */
    public static function data4Select()
    {
        $results = array(
            array(
                "id" => 0,
                "text" => 'Draft',
            ),
            array(
                "id" => 1,
                "text" => 'Awaiting payment',
            ),
            array(
                "id" => 2,
                "text" => 'Paid',
            ),
            array(
                "id" => 3,
                "text" => 'Overdue',
            ),
            array(
                "id" => 4,
                "text" => 'Cancelled',
            )
        );

        return $results;
    }

    /**
     * showInvoiceStatus at print page
     */
    public function showInvoiceStatus()
    {
        $show_flag = 0;

        if ($this->status == 2 || $this->status == 3 || $this->status == 4) {
            $show_flag = 1;
        }

        return $show_flag;

    }

    /**
     * Scope a query to only include given to.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $to
     */
    public function scopeTo($query, $to)
    {
        $query->with('to')->whereHas('to', function ($query) use ($to) {
            $query->where('id', $to);
        });
    }

    /**
     * Scope a query to only include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $status
     */
    public function scopeStatus($query, $status)
    {
        $query->where('status', $status);
    }

    /**
     * Scope a query to only include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $status
     */
    public function scopeStatusIn($query, $status)
    {
        $query->whereIn('status', $status);
    }

    /**
     * Scope a query to not include given status.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $status
     */
    public function scopeStatusNot($query, $status)
    {
        $query->where('status', '!=', $status);
    }

    /**
     * Scope a query to only include given keywords.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('to', 'like', '%' . $keywords . '%');
    }

    /**
     * @param $to
     * @param $issue_date
     * @param $discount_apply
     * @param $discount
     * @return Invoice
     */
    public static function newInvoice($to, $issue_date, $discount_apply, $discount, $order_id)
    {
        $emptyFinanceOfficer = empty($to->financeOfficer);
        $emptyCoordinator = empty($to->coordinator);
        if ($emptyCoordinator || $emptyFinanceOfficer) {
            Log::error('create a new invoice error, empty FinanceOfficer or empty Coordinator');
            return null;
        }

        Log::info("create a new invoice for school [".$to->name."] started.");
        $invoice = new Invoice();
        $invoice->issue_date = $issue_date;
        // payment term is 2 weeks
        $invoice->due_date = Carbon::parse($invoice->issue_date)->copy()->addWeek(2);
        $invoice->grace_period_end_date = Carbon::parse($invoice->due_date)->copy()->addWeek(4);

        $to_country = $to->addressLocation->country;
        $tax_apply = $to_country->isAustralia() ? 1 : 0;
        Log::info("Tax apply: " . ($tax_apply == 1 ? 'true' : 'false') . ' Discount apply: ' . ($discount_apply == 1 ? 'true' : 'false'));
        // get taxRate from country of this school
        $taxRate = $to_country->tax_rate;
        Log::info("country of school invoiced: " . $to_country->name . " taxRate:" . $taxRate);
        if ($discount_apply) {
            // get taxRate from country of this school
            Log::info("Discount id: " . $discount->id . " Discount name:" . $discount->name . " Discount value:" . $discount->value);
            $invoice->discount_apply = $discount_apply;
        }
        // init status is 0:draft
        $invoice->status = 0;
        $invoice->tax_apply = $tax_apply;

        if ($order_id) {
            $invoice->order_id = $order_id;
        }

        DB::transaction(function () use ($invoice, $to, $taxRate, $to_country, $tax_apply, $discount_apply, $discount) {
            $invoice->save();
            // update invoice_id
            $invoice_id_format = sprintf('%05d', $invoice->id);
            $invoice->invoice_id = $invoice_id_format;
            $invoice->save();

            $orderItems = array();

            $sp = $to->subscriptionPlan;
            Log::info("SubscriptionPlan joining_fee:" . $sp->joining_fee . " annual_fee:" . $sp->annual_fee . " total_fee:" . $sp->total_fee);

            if ($to->joiningFeeApplicable()) {
                $orderItem_joining_fee = new OrderItem();
                $orderItem_joining_fee->name = "Maths300 Joining Subscription";
                $orderItem_joining_fee->description = 'Joining fee:$' . $sp->joiningFeeFormat();
                $orderItem_joining_fee->quantity = 1;
                $orderItem_joining_fee->price = $sp->joining_fee;
                $orderItem_joining_fee->total_amount = $orderItem_joining_fee->price * $orderItem_joining_fee->quantity;
                $orderItem_joining_fee->invoice()->associate($invoice)->save();
                $orderItems[] = $orderItem_joining_fee;
            }

            $orderItem_annual_fee = new OrderItem();
            $orderItem_annual_fee->name = "Maths300 Annual Subscription";
            $orderItem_annual_fee->description = 'Annual fee:$' . $sp->annualFeeFormat();
            $orderItem_annual_fee->quantity = 1;
            $orderItem_annual_fee->price = $sp->annual_fee;
            $orderItem_annual_fee->total_amount = $orderItem_annual_fee->price * $orderItem_annual_fee->quantity;
            $orderItem_annual_fee->invoice()->associate($invoice)->save();
            $orderItems[] = $orderItem_annual_fee;

            // extra administrative fee applied to oversea schools
            if (!$to_country->isAustralia()) {
                $administrative_fee = !empty(Setting::get('Oversea school administrative fee')) ? doubleval(Setting::get('Oversea school administrative fee')) : 0;
                Log::info("administrative_fee applied : " . $administrative_fee);
                $orderItem_administrative_fee = new OrderItem();
                $orderItem_administrative_fee->name = "Maths300 Annual Subscription";
                $orderItem_administrative_fee->description = 'Administrative fee:$' . $invoice->administrativeFeeFormat($administrative_fee);
                $orderItem_administrative_fee->quantity = 1;
                $orderItem_administrative_fee->price = $administrative_fee;

                $orderItem_administrative_fee->total_amount = $orderItem_administrative_fee->price * $orderItem_administrative_fee->quantity;
                $orderItem_administrative_fee->invoice()->associate($invoice)->save();
                $orderItems[] = $orderItem_administrative_fee;
            }

            // calculate total amount
            $invoice_total_amount = 0;
            foreach ($orderItems as $item) {
                // item total amount already includes GST
                $invoice_total_amount += $item->total_amount;
            }

            $invoice->total_amount = $invoice_total_amount;

            $invoice_total_amount_includes_tax = $invoice_total_amount;
            Log::info("before discount applied, invoice_total_amount_includes_tax: " . $invoice_total_amount_includes_tax);

            if ($discount_apply) {
                $discount_amount = 0;
                if ($discount->discountType->name == 'Fixed Amount') {
                    $discount_amount = $discount->value;
                    Log::info("Fixed Amount discount applied, discount_amount: " . $discount_amount);
                }

                if ($discount->discountType->name == 'Percentage Based') {
                    // round the result
                    $discount_amount = round(($invoice_total_amount_includes_tax * $discount->value), 2);
                    Log::info("Percentage Based discount applied, discount_amount: " . $discount_amount);
                }
                $invoice->discount_amount = $discount_amount;
                $invoice_total_amount_includes_tax -= $invoice->discount_amount;

                // save relations
                $invoice->discount()->associate($discount)->save();

                Log::info("after discount applied, invoice_total_amount_includes_tax: " . $invoice_total_amount_includes_tax);
            } else {
                Log::info("no discount applied");
            }

            Log::info("invoice_total_amount_includes_tax: " . $invoice_total_amount_includes_tax);

            if ($tax_apply) {
                // round the result
                $invoice->tax = round($invoice_total_amount_includes_tax / 11, 2);
                Log::info("tax in total: " . $invoice->tax);
            } else {
                Log::info("no tax applied");
            }

            $invoice->amount_due = $invoice_total_amount_includes_tax;
            $invoice->save();

            $invoice->orderItems()->saveMany($orderItems);
            // save relationships
            $invoice->to()->associate($to)->save();
            // update subscription_expiration_date
            if ($invoice->to->last_renewal_date) {
                $subscription_start_date = $invoice->to->subscription_expiration_date;
                Log::info("existing school account, renew invoice, subscription start date: " . $subscription_start_date);
            } else {
                $subscription_start_date = $invoice->issue_date;
                Log::info("new school account, new invoice, subscription start date: " . $subscription_start_date);
            }
            $invoice->to->subscription_expiration_date = Carbon::parse($subscription_start_date)->copy()->addYear(1);
            $invoice->to->save();

            Log::info("create a new invoice for school [".$to->name."] completed, school subscription expiry date: ".$invoice->to->subscription_expiration_date.", invoice id: ". $invoice->id ." issue_date:" . $invoice->issue_date . " due_date:" . $invoice->due_date . " amount_due:" . $invoice->amount_due);
        });

        return $invoice;
    }


    /**
     * @param $to
     * @param $issue_date
     * @param $discount_apply
     * @param $discount
     * @return Invoice
     */
    public static function newAdhocInvoice($to, $issue_date, $invoice_amount, $description, $notes, $another_line, $account_code, $description1, $amount1)
    {
        $emptyFinanceOfficer = empty($to->financeOfficer);
        $emptyCoordinator = empty($to->coordinator);
        if ($emptyCoordinator || $emptyFinanceOfficer) {
            Log::error('create a new invoice error, empty FinanceOfficer or empty Coordinator');
            return null;
        }

        Log::info("create a new ahoc invoice for school [".$to->name."] started.");
        $invoice = new Invoice();
        $invoice->issue_date = $issue_date;
        // payment term is 2 weeks
        $invoice->due_date = Carbon::parse($invoice->issue_date)->copy()->addWeek(2);
        $invoice->grace_period_end_date = Carbon::parse($invoice->due_date)->copy()->addWeek(4);

        $to_country = $to->addressLocation->country;
        $tax_apply = $to_country->isAustralia() ? 1 : 0;
        Log::info("Tax apply: " . ($tax_apply == 1 ? 'true' : 'false'));
        // get taxRate from country of this school
        $taxRate = $to_country->tax_rate;
        Log::info("country of school invoiced: " . $to_country->name . " taxRate:" . $taxRate);
        
        // init status is 0:draft
        $invoice->status = 0;
        $invoice->tax_apply = $tax_apply;

        DB::transaction(function () use ($invoice, $to, $taxRate, $to_country, $tax_apply, $invoice_amount, $description, $notes, $another_line, $account_code, $description1, $amount1) {

            $invoice->save();
            // update invoice_id
            $invoice_id_format = sprintf('%05d', $invoice->id);
            $invoice->invoice_id = $invoice_id_format;
            $invoice->save();
            
            //To concat Notes to School notes.
            $school_notes = $to->notes.". $notes";
            $to->notes($school_notes);

            $orderItems = array();

            $sp = $to->subscriptionPlan;
            Log::info("Adhoc Invoice fee:" . $invoice_amount );

            $orderItem_joining_fee = new OrderItem();
            $orderItem_joining_fee->name = $description;
            $orderItem_joining_fee->description = '';
            $orderItem_joining_fee->quantity = 1;
            $orderItem_joining_fee->price = $invoice_amount;
            $orderItem_joining_fee->total_amount = $invoice_amount * $orderItem_joining_fee->quantity;
            $orderItem_joining_fee->invoice()->associate($invoice)->save();
            $orderItems[] = $orderItem_joining_fee;

             // extra administrative fee applied to oversea schools
            if ($another_line) {
                $administrative_fee = $amount1;
                Log::info("administrative_fee applied : " . $administrative_fee);
                $orderItem_administrative_fee = new OrderItem();
                $orderItem_administrative_fee->name = $description1;
                $orderItem_administrative_fee->description ='';
                $orderItem_administrative_fee->quantity = 1;
                $orderItem_administrative_fee->price = $amount1;
                $orderItem_administrative_fee->total_amount = $amount1 * $orderItem_administrative_fee->quantity;
                $orderItem_administrative_fee->invoice()->associate($invoice)->save();
                $orderItems[] = $orderItem_administrative_fee;
            }


            // calculate total amount
            $invoice_total_amount = 0;
            foreach ($orderItems as $item) {
                // item total amount already includes GST
                $invoice_total_amount += $item->total_amount;
            }

            $invoice->total_amount = $invoice_total_amount;

            $invoice_total_amount_includes_tax = $invoice_total_amount;
           

            Log::info("invoice_total_amount_includes_tax: " . $invoice_total_amount_includes_tax);

            if ($tax_apply) {
                // round the result
                $invoice->tax = round($invoice_total_amount_includes_tax / 11, 2);
                Log::info("tax in total: " . $invoice->tax);
            } else {
                Log::info("no tax applied");
            }

            $invoice->amount_due = $invoice_total_amount_includes_tax;

            //Store Account code and as Adhoc Invoice
            $invoice->is_adhoc_invoice = 1;
            $invoice->xero_account_code = $account_code;
            $invoice->save();

            $invoice->orderItems()->saveMany($orderItems);
            // save relationships
            $invoice->to()->associate($to)->save();
            
            $invoice->to->save();

            Log::info("create a new Adhoc invoice for school [".$to->name."] completed, school subscription expiry date: ".$invoice->to->subscription_expiration_date.", invoice id: ". $invoice->id ." issue_date:" . $invoice->issue_date . " due_date:" . $invoice->due_date . " amount_due:" . $invoice->amount_due);
        });

        return $invoice;
    }

    /**
     * Gen PDF associated with an invoice
     * @param $invoice
     */
    public static function genPDF($invoice)
    {
        Log::info("invoice PDF file generation for school [" . $invoice->to->name . '] started');
        $start = microtime(true);

        $arr['invoice'] = $invoice;
        $arr['invoicefrom'] = Invoice::getOrgInfo();
        $arr['is_pdf_generation'] = 1;

        $options = array(
            'margin-left' => 0,
            'margin-right' => 0,
            'margin-top' => 10,
            'margin-bottom' => 0,
            'viewport-size' => '1280x1024',
            'minimum-font-size' => 16
        );

        $pdf = PDF::loadView('admin.invoices.show', $arr);
        $root_path = 'app/public/';
        $pdf_file_path = 'invoices/Invoice-' . $invoice->invoice_id . '.pdf';
        $pdf->setOptions($options);
        $pdf->save(storage_path($root_path . $pdf_file_path), true);

        $invoice->pdf_link = config('app.url') . '/storage/' . $pdf_file_path;
        $invoice->pdf_file_path = $pdf_file_path;
        $invoice->save();

        $end = microtime(true);
        Log::info("invoice PDF file generation for school [" . $invoice->to->name . '] completed, download link: ' . $invoice->pdf_link . ', elapsed time: ' . ($end - $start) . ' seconds');
    }

    /**
     * from information
     * AAMT name, address, abn
     * @return array
     */
    public static function getOrgInfo()
    {
        $settings = Setting::batchGet(array('Organization name', 'Organization address', 'Phone', 'Fax', 'Email', 'ABN', 'Account name', 'Account bsb', 'Account number'));
        $res = array(
            'name' => $settings[0]->value,
            'address' => $settings[1]->value,
            'phone' => $settings[2]->value,
            'fax' => $settings[3]->value,
            'email' => $settings[4]->value,
            'abn' => $settings[5]->value,
            'accountName' => $settings[6]->value,
            'bsb' => $settings[7]->value,
            'accountNumber' => $settings[8]->value,
        );

        return $res;
    }

    /**
     * totalAmount will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function totalAmountFormat()
    {
        return number_format($this->total_amount, 2);
    }

    /**
     * amount_due will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function amountDueFormat()
    {
        return number_format($this->amount_due, 2);
    }

    /**
     * discount_amount will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function discountAmountFormat()
    {
        return number_format($this->discount_amount, 2);
    }

    /**
     * tax will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function taxFormat()
    {
        return number_format($this->tax, 2);
    }

    /**
     * $administrative_fee will be formatted with decimals decimals with a dot (".") in front,
     * and a comma (",") between every group of thousands
     * rounded up
     * @return string
     */
    public function administrativeFeeFormat($administrative_fee)
    {
        return number_format($administrative_fee, 2);
    }

    public static function getinvoices($where)
    {
        return parent::selectRaw("invoices.invoice_id,schools.id,schools.name as school_name,schools.address_region,invoices.issue_date,invoices.due_date,invoices.amount_due,invoices.status,schools.status as subscription_status,invoices.paid_date,finance_user.name as finance_user_name,finance_user.email as finance_user_email,finance_user.phone as finance_user_phone,coordinator.name as coordinator_name,coordinator.email as coordinator_email,coordinator.phone as coordinator_phone, schools.purchase_order_no, invoices.discount_amount, schools.join_date, school_size.name, locations.name as location, countries.name as country")
             ->leftjoin("schools","schools.id", "=" ,"invoices.to_id")
             ->leftjoin("users as coordinator","schools.coordinator_id", "=" ,"coordinator.id")
             ->leftjoin("users as finance_user","schools.finance_officer_id", "=" ,"finance_user.id") 
             ->leftjoin("school_size_teachers as school_size","school_size.id", "=" ,"schools.school_size_teacher_id")
             ->leftjoin("locations","locations.id", "=" ,"schools.address_location_id")                
             ->leftjoin("countries","countries.id", "=" ,"locations.country_id")
             ->whereRaw($where)
             ->orderBy("invoices.invoice_id")
             ->get();
    }

}
