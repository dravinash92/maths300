<?php

use Illuminate\Database\Seeder;
use App\Models\SubscriptionPlan;

class SubscriptionPlanTableUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionPlan::where('id', '=', 1)->update(['school_size_id' => 1]);
        SubscriptionPlan::where('id', '=', 2)->update(['school_size_id' => 2]);
        SubscriptionPlan::where('id', '=', 3)->update(['school_size_id' => 3]);
        SubscriptionPlan::where('id', '=', 4)->update(['school_size_id' => 4]);
        SubscriptionPlan::where('id', '=', 5)->update(['school_size_id' => 5]);
        SubscriptionPlan::where('id', '=', 6)->update(['school_size_id' => 6]);
        SubscriptionPlan::where('id', '=', 7)->update(['school_size_id' => 7]);
        SubscriptionPlan::where('id', '=', 8)->update(['school_size_id' => 8]);
        SubscriptionPlan::where('id', '=', 9)->update(['school_size_id' => 9]);
    }
}
