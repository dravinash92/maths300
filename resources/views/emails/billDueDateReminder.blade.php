@extends('layouts.email')

@section('content')
    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        Dear {{ $recipient }},
        <br><br>
        This is a friendly reminder that Maths300 subscription invoice #{{ $invoice->invoice_id }} (total <strong>${{ $invoice->amount_due }}</strong>)is now due for payment.
        <br><br>
        You may disregard this email if the payment has been recently made. Otherwise, please process the payment as soon as possible to avoid suspension of your Maths300 account.
        <br><br>
    </div>

    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        <table class="buttonwrapper" bgcolor="#ea910b" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center; font-size: 16px; font-family: sans-serif; font-weight: bold; padding: 0px 30px 0px 30px; " height="45">
                    <!-- <a href="{{ $invoice->pdf_link }}" style ="color: #ffffff; text-decoration: none;">Click here to download invoice</a> -->
                    <a href="<?php echo config('app.url').'/users/downloadInvoice/'.$invoice->invoice_id ?>" style ="color: #ffffff; text-decoration: none;">Click here to download invoice</a>

                    <br/>
                </td>
            </tr>
        </table>
    </div>
@stop
