<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TopicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'forum' => 'required',
            'title' => 'bail|required|min:2|max:80',
            'content' => 'required|max:5000',
            'sticky' => 'nullable',
            'visibility' => 'nullable',
        ];
    }
}