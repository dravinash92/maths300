@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }
    </style>

    <link rel='stylesheet' href='/wp-content/plugins/bbpress/templates/default/css/bbpress.css' type='text/css'
          media='all'/>
@stop

@extends('layouts.pageframe')

@section('top_heading')
    Topics
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/forums" title="Community"><span itemprop="name">Community</span></a>
    </li>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{ route('front.topics.index',['q_forum'=>$forum_id]) }}" title="Topics"><span
                    itemprop="name">Topics</span></a>
    </li>
@stop

@section('site-content')
    <div class="container site-content sidebar-right" style="transform: none;">
        <div class="row" style="transform: none;">
            <main id="main" class="site-main col-sm-9 alignleft">
                <article id="post-3136" class="post-3136 forum type-forum status-publish hentry pmpro-has-access">
                    <div class="entry-content">
                        @if(count($errors))
                            <div class="message-error">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form id="topicForm" role="form" method="post" action="{{ route('front.topics.store') }}">
                            @csrf
                            <input type="hidden" name="forum" value={{ $forum_id }}>
                            <div>
                                <p>
                                    <label for="bbp_topic_title">Topic Title</label><br>
                                    <input type="text" id="title" value="" tabindex="101" size="116"
                                           name="title" maxlength="80">
                                </p>

                                <div class="bbp-the-content-wrapper">
                                    <div id="wp-bbp_topic_content-wrap" class="wp-core-ui wp-editor-wrap html-active">
                                        <div id="wp-bbp_topic_content-editor-container" class="wp-editor-container">
                                        <label for="bbp_topic_title">Content</label><br>
                                        <textarea class="bbp-the-content wp-editor-area" rows="12" tabindex="102"
                                                  cols="40" name="content" id="bbp_topic_content"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" tabindex="107"
                                        class="button submit" style="background: #78BE20;">Submit
                                </button>
                            </div>
                        </form>
                    </div><!-- .entry-content -->
                </article><!-- #post-## -->
            </main>
            <div id="sidebar" class="widget-area col-sm-3 sticky-sidebar" role="complementary"
                 style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
            </div><!-- #secondary -->
        </div>
    </div>
@stop