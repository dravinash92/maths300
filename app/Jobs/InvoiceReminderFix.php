<?php

namespace App\Jobs;

use App\Mail\InvoiceOverdueReminder;
use App\Mail\BillDueDateReminder;
use App\Mail\SubscriptionRenewReminder;
use App\Models\Invoice;
use App\Models\School;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class InvoiceReminderFix implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * The number of seconds the job can run before timing out.
     *
     * 6 hours
     *
     * @var int
     */
    public $timeout = 21600;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = microtime(true);
        Log::info('InvoiceReminderFix job started......');
        $check_start = '2020-11-20';
        $check_end = '2020-11-25';
        $currentDate = strtotime($check_start);

        //While the current timestamp is smaller or equal to
        //the timestamp of the end date.
        while ($currentDate <= strtotime($check_end)) 
        {
            $six_weeks_after_currentDate = Carbon::createFromTimestamp($currentDate)->addWeeks(6);
            Log::info('Check school expiry date......');
            Log::info("currentDate: " . Carbon::createFromTimestamp($currentDate) . ", six weeks after current date: " . $six_weeks_after_currentDate);
            $schools_about_to_expire = School::where('subscription_expiration_date', $six_weeks_after_currentDate)->get();

            if (count($schools_about_to_expire)) 
            {
                Log::warning("Found " . count($schools_about_to_expire) . " schools subscription about to expire.");
                foreach ($schools_about_to_expire as $school)
                {
                    $expiration_date = $school->subscription_expiration_date;
                    Log::info("Reminder (6 weeks before expiration date), subscription expiry date: " . $expiration_date . ", school: " . $school->name);

                    // check if renewal invoices were generated (issue_date == currentDate)
                    $expected_issue_date = Carbon::createFromTimestamp($currentDate)->format('Y-m-d');
                    Log::info("check if school [" . $school->name . "] has invoice issued on expected issue date: " . $expected_issue_date);

                    if (!$school->hasInvoiceIssued($expected_issue_date))
                    {
                        Log::info("Found school [" . $school->name . "] invoice was missing, need to re-generate invoice.");
                        Log::info("Start generating invoice..");
                        // create a new invoice
                        // create new invoice for the next billing
                        $new_invoice = Invoice::newInvoice($school, today(), 0, null, null);
                        if (!empty($new_invoice))
                        {
                            $new_invoice->status = 1;
                            $new_invoice->send_date = today();
                            $new_invoice->save();

                            Invoice::genPDF($new_invoice);

                            Log::info('Send reminder email for subscription renewal with a new invoice#' . $new_invoice->invoice_id);
                            // send invoice to school finance officer
                            Mail::to($new_invoice->to->financeOfficer)
                                ->send(new SubscriptionRenewReminder($new_invoice, $new_invoice->to->financeOfficer->name, $expiration_date));

                            // send invoice to school coordinator
                            Mail::to($new_invoice->to->coordinator)
                                ->send(new SubscriptionRenewReminder($new_invoice, $new_invoice->to->coordinator->name, $expiration_date));

                            // cc invoices to AAMT finance officer
                            // Mail::to(Setting::get('Finance officer email'))
                            //     ->send(new SubscriptionRenewReminder($new_invoice, Setting::get('Finance officer name'), $expiration_date));
                        }
                        else
                        {
                            Log::error("new invoice error for school " . $school->name);
                        }
                        Log::info("End generating invoice..");
                    }
                }
            }
            else
            {
                Log::info("No schools subscription about to expire.");
            }

            //Add one day onto the timestamp / counter.
            $currentDate = strtotime("+1 day", $currentDate);
        }

        $end = microtime(true);
        Log::info('######################################################################################');
        Log::info('InvoiceReminderFix job completed, elapsed time: ' . ($end - $start) . ' seconds');
        Log::info('######################################################################################');
    }
}
