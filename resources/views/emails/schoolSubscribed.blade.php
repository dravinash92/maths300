@extends('layouts.email')

@section('content')
    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        Hi Team,
        <br>
        <p><strong>{{ $school->name }}</strong> has subscribed to Maths300 service. Please process the application.</p>
        <p>Follow the link below to see the details.</p>
    </div>

    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        <table class="buttonwrapper" bgcolor="#ea910b" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center; font-size: 16px; font-family: sans-serif; font-weight: bold; padding: 0px 30px 0px 30px; " height="45">
                    <a href="{{ route('admin.schools.index', ['q_schoolId'=>$school->id]) }}" style ="color: #ffffff; text-decoration: none;">Click Here</a><br/>
                </td>
            </tr>
        </table>
    </div>
@stop
