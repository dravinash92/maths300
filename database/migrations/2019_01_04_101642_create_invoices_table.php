<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_id')->nullable();
            // 0: Draft 1:Sent 2:Awaiting payment 3: Paid 4: Overdue
            // create index on status to increase query performance
            $table->integer('status')->unsigned()->index();
            $table->date('issue_date')->nullable();
            $table->date('send_date')->nullable();
            $table->date('paid_date')->nullable();
            $table->date('cancel_date')->nullable();
            $table->date('due_date')->nullable();
            $table->date('grace_period_end_date')->nullable();
            // use for overdue reminder date
            $table->date('next_reminder_date')->nullable();
            $table->date('last_reminder_date')->nullable();

            // discount apply 0:not applied 1:applied
            // not applied by default
            $table->integer('discount_apply')->default(0);
            $table->double('discount_amount')->default(0);
            // discount
            $table->integer('discount_id')->unsigned()->index()->nullable();
            $table->foreign('discount_id')->references('id')->on('discounts');

            // tax apply 0:not applied 1:applied
            // applied by default
            $table->integer('tax_apply')->default(1);
            $table->double('tax')->default(0);

            // exclude tax/discount
            $table->double('total_amount')->nullable();
            // include tax / apply discount_amount
            $table->double('amount_due')->nullable();

            // pdf download link
            $table->string('pdf_link')->nullable();
            // pdf file path in file system
            $table->string('pdf_file_path')->nullable();

            // to
            $table->integer('to_id')->unsigned()->index()->nullable();
            $table->foreign('to_id')->references('id')->on('schools');

            $table->string('order_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
