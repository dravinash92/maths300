@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }

        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        table thead tr {
            background-color: #78BE20;
            color: #333;
        }

        .btn-subscription:hover {
            background-color: #78BE20;
            border-color: #78BE20;
        }

    </style>
@stop

@extends('layouts.page')

@section('top_heading')
    Subscribe
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/subscribe"><span itemprop="name">Subscribe</span></a>
    </li>
@stop

@section('content')
        <article id="post-87" class="post-87 page type-page status-publish hentry pmpro-has-access">
            <div class="entry-content">
                <div class="content">
                    <h1>Terms of use</h1>
                    <p><strong>Copyright</strong></p>
                    <p>All materials on the Maths 300 site are owned by the Australian Association of Mathematics Teachers (AAMT) Inc. and may only be used for the purposes described below, unless written permission is given by AAMT for the content of this site to be  reproduced or used in any other way.</p>
                    <p><strong>Sample  lessons and software</strong></p>
                    <p>All sample material in the publicly accessible section of Maths 300 may be used, copied or reproduced only for non-commercial educational purposes only, with appropriate acknowledgement of the source. This sample material cannot be changed in any way unless permission is given by AAMT. </p>
                    <p><strong>Subscribers </strong></p>
                    <p>Access to the Maths 300 library and software is granted to registered members of staff and students at the single site or campus where the subscribing school is located. Access is not transferrable between sites or campuses at other locations. </p>
                    <p>As a current subscriber of Maths300, a single campus (of a school or other institution) is bound by the following conditions:  </p>
                    <ul>
                        <li>It is the responsibility of the subscribers to register for a subscription category in accordance with the number of teacher who require access.</li>
                        <li>All Maths 300 materials are licensed to the named campus of a school or institution (not individual teachers). Staff or students who leave the campus may no longer access or use materials.</li>
                        <li>Staff who have a Maths 300 account may use, copy or reproduce any material from Maths 300 for the purposes of teaching their students.  Maths 300 materials must not be sold or given to teachers or schools who are not registered subscribers.</li>
                        <li>User names, passwords, access to software or lessons may not be distributed to anyone who is not bound by the campus licence.</li>
                        <li>The lessons and software may not be used if the subscription has lapsed. In this instance, all materials and software must be deleted from all computers onto which they have been copied or installed, and any hard copies destroyed.</li>
                        <li>Maths 300 lesson content may not be edited or modified and retained after the lapse of a subscription. </li>
                    </ul>
                    <p>Under special circumstances, individuals, rather than an institutional campus, may take out a subscription. Individuals agree to be bound by the same conditions as a campus.  </p>
                    <p><strong>No liability </strong></p>
                    <p>
                        The Maths 300 website is provided on an 'as is' and 'as available' basis at the user’s own risk. To the extent lawful, AAMT makes no representation or warranty (promise) as to the suitability or compatibility of the Maths3 00 website to any computer system, or that the Maths 300 website is free of error, defect, virus or other harmful element or fit for the purpose you intend to use it. AAMT will not be responsible for any loss, damage, liability or expense suffered by you or anyone else arising from your use, or inability to use the Maths 300 website.
                    </p>
                    <p>
                        While AAMT endeavours to ensure the Maths 300 site can be accessed at any time, there may be occasions when the site becomes unavailable, for example due to server or other technical issues.  AAMT cannot be held liable for any such downtime.
                    </p>
                    <p>
                        All users agree that they will not take action against AAMT for any monetary, reputational or other loss arising from the use of the Maths 300 product.
                    </p>
                    <p><strong>Acceptance of terms and conditions and licences</strong></p>
                    <p>If you proceed to use the Maths300 website or download materials from it, your use will be taken as acceptance to comply with the conditions of use set out.</p>
                    <p>If you have any doubts about your right to access and/or use Maths300 materials, please contact <a href="mailto:maths300@aamt.edu.au">maths300@aamt.edu.au</a>. </p>
                    <p><strong>Cancellation</strong></p>
                    <p>Subscriptions may be cancelled at any time by <a href="/contact" target="_self">contacting AAMT</a> but no refunds will be issued. Any subscriber who does not renew their annual subscription will have access automatically cancelled. All users agree that upon cancellation or non-renewal, all software and downloaded files lessons, handouts, etc.) will be permanently deleted from all devices.</p>
                    <p>Subscribers are liable to pay a joining fee if their subscription lapses and they subsequently wish to renew their access. </p>
                </div>

                <br>

                @if($hasAction == 1)
                <div class="form-group has-error ">
                    <label>
                    <input type="checkbox" id='agreed_terms' name="confirm_read_terms"/>I agree to the terms and conditions
                    </label>
                    <br>
                    <span class="text-danger" id='agreed_terms_error' >click the checkbox above to continue</span>
                </div>
                <div class="col-xs-12 text-right">
                    <button id="next" class="btn btn-primary ladda-button btn-subscription" data-style="expand-right"><span
                                class="ladda-label">Next</span></button>
                </div>
                @endif

            </div>
        </article>
@stop

@section('js')
    <script>
        jQuery(document).ready(function () {
            jQuery('#agreed_terms_error').hide();
            jQuery('#next').on('click', function (e) {
                console.log('next button clicked');
                if(document.getElementById('agreed_terms').checked) {
                    jQuery('#agreed_terms_error').hide();
                    location.href = '/subscribe/create';
                } else {
                    jQuery('#agreed_terms_error').show();
                }
            });
        });
    </script>
@endsection
