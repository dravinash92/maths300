@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.forums.index') }}">Forums</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Forums</h3>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('admin.forums.create') }}" class="btn btn-primary btn-flat">Add Forum</a>
            </div>

        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="schoolSearchForm" role="form" method="get" action="{{ route('admin.forums.index') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter forum keywords ..."
                                   value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="schoolSearchBtn" type="submit" class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>
                        </div>
                    </div>
                </div><!-- /.row -->
            </form>
            <table id="tab_forums" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Topics</th>
                    <th>Replies</th>
                    <th>Status</th>
                    <th>Menu</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($forums as $l)
                    <tr class="item{{ $l->id }}">
                        <td><a href='{{ route('admin.forums.show',$l->id) }}'>{{ $l->title }}</a></td>
                        <td>{{ $l->getTopicsCount() }}</td>
                        <td>{{ $l->getRepliesCount() }}</td>
                        <td>{{ $l->getStatus() }}</td>
                        <td>
                            <a href="{{ route('admin.forums.edit',$l->id) }}" class="btn btn-primary">Edit</a>
                            <button class="delete-modal btn btn-danger" data-id="{{ $l->id }}"
                                    data-name="{{ $l->title }}">Delete
                            </button>

                            <div class="btn-group">
                                <button type="button" class="btn btn-warning">Change Status</button>
                                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    @if( $l->isOpenAllowed() )
                                    <li>
                                        <a href="{{ route('admin.forums.open') }}" onclick="event.preventDefault();document.getElementById('open-form{{ $l->id }}').submit();">
                                            Open
                                        </a>
                                    </li>
                                    @endif

                                    @if( $l->isCloseAllowed() )
                                    <li>
                                        <a href="{{ route('admin.forums.close') }}" onclick="event.preventDefault();document.getElementById('close-form{{ $l->id }}').submit();">
                                            Close
                                        </a>
                                    </li>
                                    @endif
                                </ul>

                                <form id="open-form{{ $l->id }}" action="{{ route('admin.forums.open') }}" method="POST" style="display: none;">
                                    @csrf
                                    <input type="hidden" name="id_open" value="{{ $l->id }}">
                                </form>

                                <form id="close-form{{ $l->id }}" action="{{ route('admin.forums.close') }}" method="POST" style="display: none;">
                                    @csrf
                                    <input type="hidden" name="id_close" value="{{ $l->id }}">
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @modal(['action' => 'delete', 'model' => 'forum'])
    @endmodal
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#tab_forums').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search forums......"
                },
                'ordering': true,
                'info': true,
                'columnDefs': [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 4
                    }],
                'autoWidth': false,
                'columns': [{
                    "width": "40%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "30%"
                }
                ]
            });

            $(".dataTables_filter").hide();
        });

        // Automatically trigger the loading animation on click
        Ladda.bind('button[type=submit]');
    </script>

    @modaljs(['action' => 'delete', 'tab' => 'tab_forums', 'submit_url' => 'admin/forums'])
    @endmodaljs
@endpush
