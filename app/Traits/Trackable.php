<?php

namespace App\Traits;

use Cache;

trait Trackable
{
    public static function bootTrackable()
    {
        static::creating(function ($model) {
            Cache::forget(str_plural($model));
        });

        static::updating(function ($model) {
            Cache::forget(str_plural($model));
        });

        static::deleting(function ($model) {
            Cache::forget(str_plural($model));
        });
    }

}