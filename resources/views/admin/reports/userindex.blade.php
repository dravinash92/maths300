@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }
        .filter-class{
            display: flex;
            align-items: center;
        }
        .download-btn{
            text-align: center;
            padding: 20px 0px 10px 0px;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.reports.userindex') }}">Users report</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Users report</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">

            <form id="userReportForm" role="form" method="post" action="{{ route('admin.reports.getusers') }}">
                {{ csrf_field() }}
                <div class="row filter-class">
                    <div class="col-xs-2">
                        <label>Filter By</label>
                    </div>
                    <div class="col-xs-4">
                        <select data-placeholder="Select a school" class="form-control select2_school"
                                style="width: 100%;" id="q_school" name="q_school">
                            <option></option>
                        </select>
                    </div>
                    <!-- Date range -->
                      <div class="col-xs-4">
                          <select class="form-control" name="q_status" id="q_status">
                            <option value="">Select Status</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                            <option value="2">Suspended</option>
                            <option value="3">Archived</option>
                          </select>
                        </div>
                        <!-- /.input group -->
                      </div>
                      <div class="col-xs-12 download-btn">
                        <button class="btn btn-primary btn-flat" type="submit">Download report</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <script>
        $(document).ready(function () {

            // Initialize Select2 elements
            let select2_school = $('.select2_school').select2({
                    allowClear: true,
                    data: {!! json_encode($schools)!!}
                });

            @if(old('q_school'))
            select2_school.val({!! old('q_school') !!}).trigger('change');
            @endif
            });
    </script>
@endpush