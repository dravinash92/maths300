<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SchoolSizeTeacher;

class SchoolSizeTeacherTableSeeder extends Seeder {

    public function run()
    {
        DB::table('school_size_teachers')->delete();

        SchoolSizeTeacher::create([
            'name' => '1–50 teachers at campus'
        ]);

        SchoolSizeTeacher::create([
            'name' => '51–100 teachers at campus'
        ]);

        SchoolSizeTeacher::create([
            'name' => '101–200 teachers at campus'
        ]);

        SchoolSizeTeacher::create([
            'name' => '201–400 teachers at campus'
        ]);

        SchoolSizeTeacher::create([
            'name' => '401–600 teachers at campus'
        ]);
    }

}