<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;

class Location extends Model
{
    /**
     * Get the Country that belongs to this Location.
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeName($query, $name)
    {
        return $query->where('name', '=', $name);
    }

    /**
     * Scope a query to only include given country.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $country
     */
    public function scopeCountry($query, $country)
    {
        $query->with('country')->whereHas('country', function ($query) use ($country) {
            $query->where('id', $country);
        });
    }

    public static function newLoc($countryName, $locationName)
    {
        Log::info("create new location with countryName: " . $countryName . " locationName: " . $locationName);
        $country = Country::query()->name($countryName)->first();
        if (!empty($country)) {
            Log::info("country found, try to find if location exists " . $locationName);
            $location = Location::query()->name($locationName)->country($country->id)->first();
            if (empty($location)) {
                Log::info("location not found, try to create it with input " . $locationName);
                $location = new Location();
                $location->name = $locationName;

                DB::transaction(function () use ($location, $country) {
                    $location->save();
                    $location->country()->associate($country)->save();
                });
            }
        } else {
            Log::info("country not found, try to create its with input " . $countryName);
            $country = new Country();
            $country->name = $countryName;
            $country->save();

            $location = new Location();
            $location->name = $locationName;

            DB::transaction(function () use ($location, $country) {
                $location->save();
                $location->country()->associate($country)->save();
            });
        }

        Log::info("new location with countryName: " . $countryName . " locationName: " . $locationName . " locationID: " . $location->id);

        return $location;
    }
}
