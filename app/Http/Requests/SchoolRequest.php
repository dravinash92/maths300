<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'bail',
                'required',
                 Rule::unique('schools')->ignore($this->id),
                'min:2',
                'max:255',
            ],
            'address_region' => 'bail|required|min:2|max:255',
            'address_suburb' => 'required|min:2|max:255',
            'country' => 'required',
            'location' => 'required',
            'purchase_order_no' => 'nullable|max:50',
            'address_country'=> 'nullable|max:50',
            'address_state'=> 'nullable|max:50',
            'address_postcode' => [
                'bail',
                'required',
                'max:10',
            ],

            'schoolType' => 'required',
            'schoolSector' => 'required',
            'schoolSizeStudent' => 'required',
            'schoolSizeTeacher' => 'required',
            'number_of_campuses' => 'numeric',
            'notes' => 'nullable',

            // $finance_officer
            'finance_officer_id' => 'sometimes',
            'finance_officer_name' => 'required|min:2|max:255',
            'finance_officer_email' => ['required','email'],
            'finance_officer_phone' => 'required|min:2|max:20',
            'finance_officer_position' => 'nullable',

            'finance_invoice_email' => 'required|email',

            // billing address
            'billing_address_region' => 'bail|required|min:2|max:255',
            'billing_address_suburb' => 'required|min:2|max:255',
            'billing_address_location' => 'required',
            'billing_address_country'=> 'required',
            'billing_address_state'=> 'nullable|max:50',
            'billing_address_postcode' => [
                'bail',
                'required',
                'max:10',
            ],

            'subscriptionPlan' => 'required',
            'lesson_pack' => 'required',
            // $billing information

            // $coordinator
            'coordinator_id' => 'sometimes',
            'coordinator_name' => 'required|min:2|max:255',
            'coordinator_email' => ['required','email'],
            'coordinator_phone' => 'required|min:2|max:20',
            'coordinator_position' => 'nullable',
            'coordinator_year_levels' => 'nullable'
        ];
    }
}
