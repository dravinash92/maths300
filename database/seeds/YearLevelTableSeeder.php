<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\YearLevel;

class YearLevelTableSeeder extends Seeder {

    public function run()
    {
        DB::table('year_levels')->delete();

        for ($x = 1; $x <= 12; $x++) {
            YearLevel::create([
                'name' => $x
            ]);
        }
    }

}