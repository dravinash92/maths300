@extends('layouts.email')

@section('content')
    <div class="body-text"
         style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        Dear {{ $recipient }},
        <br><br>
        Please find attached your Maths300 subscription invoice for <strong>${{ $invoice->amount_due }}</strong>, due for payment by <strong>{{ $invoice->dueDateFormat() }} </strong>.
        <br><br>
        We accept bank transfer and credit card payments. The details of both are available on the invoice.
        <br><br>
        Thank you for subscribing to Maths300.
        <br><br>
    </div>

    <div class="body-text"
         style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        <table class="buttonwrapper" bgcolor="#ea910b" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center; font-size: 16px; font-family: sans-serif; font-weight: bold; padding: 0px 30px 0px 30px; "
                    height="45">
                    <!-- <a href="{{ $invoice->pdf_link }}"
                       style="color: #ffffff; text-decoration: none;">Click here to download invoice</a> -->
                       <a href="<?php echo config('app.url').'users/downloadInvoice/'.$invoice->invoice_id ?>"
                       style="color: #ffffff; text-decoration: none;">Click here to download invoice</a>

                       <br/>
                </td>
            </tr>
        </table>
    </div>
@stop

