<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Pedagogy;

class PedagogyTableSeeder extends Seeder {

    public function run()
    {
        DB::table('pedagogies')->delete();

        Pedagogy::create([
            'name' => 'application focus'
        ]);

        Pedagogy::create([
            'name' => 'assessment'
        ]);

        Pedagogy::create([
            'name' => 'calculators'
        ]);

        Pedagogy::create([
            'name' => 'communicating mathematics'
        ]);

        Pedagogy::create([
            'name' => 'computers'
        ]);

        Pedagogy::create([
            'name' => 'concept focus'
        ]);

        Pedagogy::create([
            'name' => 'concrete materials'
        ]);

        Pedagogy::create([
            'name' => 'concrete materials'
        ]);

        Pedagogy::create([
            'name' => 'concurrent teaching of topics'
        ]);

        Pedagogy::create([
            'name' => 'cross curriculum'
        ]);

        Pedagogy::create([
            'name' => 'ESL'
        ]);

        Pedagogy::create([
            'name' => 'estimation'
        ]);

        Pedagogy::create([
            'name' => 'first hand data'
        ]);

        Pedagogy::create([
            'name' => 'game'
        ]);

        Pedagogy::create([
            'name' => 'group work'
        ]);

        Pedagogy::create([
            'name' => 'history of maths'
        ]);

        Pedagogy::create([
            'name' => 'investigation sheet'
        ]);

        Pedagogy::create([
            'name' => 'investigative process'
        ]);

        Pedagogy::create([
            'name' => 'mathematical modelling'
        ]);

        Pedagogy::create([
            'name' => 'mixed ability'
        ]);

        Pedagogy::create([
            'name' => 'open-ended'
        ]);

        Pedagogy::create([
            'name' => 'outdoor'
        ]);

        Pedagogy::create([
            'name' => 'physical involvement'
        ]);

        Pedagogy::create([
            'name' => 'problem posing'
        ]);

        Pedagogy::create([
            'name' => 'problem solving'
        ]);

        Pedagogy::create([
            'name' => 'social issues'
        ]);

        Pedagogy::create([
            'name' => 'software'
        ]);

        Pedagogy::create([
            'name' => 'story shell'
        ]);

        Pedagogy::create([
            'name' => 'visualisation'
        ]);

        Pedagogy::create([
            'name' => 'whole class'
        ]);

        Pedagogy::create([
            'name' => 'working mathematically'
        ]);
    }

}