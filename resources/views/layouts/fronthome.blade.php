@section('css')
<style>
    .woocommerce-MyAccount-content {
        margin-top: 0px;
    }
    table.dataTable tbody td {
        word-break: break-word;
        vertical-align: top;
    }

    table thead tr {
        background-color: #78BE20;
        color: #333;
    }

    .btn-primary:hover {
        background-color: #78BE20;
        border-color: #78BE20;
    }

    button {
        text-transform: none;
    }

    .woocommerce-account .myaccount-right {
        float: left;
        width: 70%;
        margin-top: 0px;
        overflow-x:auto;
    }

    #myaccount-navigation.woocommerce-MyAccount-navigation {
        float: left;
        width: 20%;
    }

    @media screen and (max-width: 1024px) {
        .woocommerce-account .myaccount-right {
            margin-left: 40px;
            overflow-x:auto;
        }
    }

    @media screen and (max-width: 959px) {
        .woocommerce-account .myaccount-right {
            margin-left: 92px;
            overflow-x:auto;
        }
    }

    @media screen and (max-width: 768px) {

        #myaccount-navigation.woocommerce-MyAccount-navigation {
            display: none;
        }

        .woocommerce-account .myaccount-right {
            float: left;
            width: 100%;
            margin-top: 0px;
            margin-left: 0px;
            overflow-x:auto;
        }
    }
</style>
<link rel='stylesheet' href='/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css' type='text/css' media='all'/>

<!-- toastr notifications -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

@stack('css')
@stop

@extends('layouts.pageframe')

@section('body-css')
woocommerce-account
@stop

@section('top_heading') 
Home @yield('sub_heading')
@stop 

@section('breadcrumbs')
<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    @yield('sub_breadcrumbs')
</li>
@stop 

@section('site-content')
<div class="container site-content">
    <div class="row">
        <main id="main" class="site-main col-sm-12 full-width">
            <article id="post-1700" class="post-1700 page type-page status-publish hentry pmpro-has-access">
                <div class="entry-content">
                    <div class="woocommerce">
                        <nav id="myaccount-navigation" class="woocommerce-MyAccount-navigation myaccount-left">
                            <ul>
                                <li id='dashboard-nav' class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard {{ !empty($dashboard_nav_active) ? 'is-active' : '' }}">
                                    <a href="#">Dashboard</a>
                                </li>

                                @if(Auth::user()->hasRole('school-admin'))
                                <li id='listTeachers-nav' class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders {{ !empty($listTeachers_nav_active) ? 'is-active' : '' }}">
                                    <a href="#">Teachers</a>
                                </li>
                                @endif

                                @if(Auth::user()->hasRole('school-admin') || Auth::user()->hasRole('school-finance-officer'))
                                    <li id='invoices-nav' class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders {{ !empty($invoices_nav_active) ? 'is-active' : '' }}">
                                        <a href="#">My Invoices</a>
                                    </li>
                                @endif

                                <li id='favoriteLessons-nav' class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--downloads {{ !empty($favoriteLessons_nav_active) ? 'is-active' : '' }}">
                                    <a href="#">Favorite Lessons</a>
                                </li>

                                <li id='accountDetails-nav' class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account {{ !empty($accountDetails_nav_active) ? 'is-active' : '' }}">
                                    <a href="#">Account Details</a>
                                </li>

                                <li id='changePass-nav' class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account {{ !empty($changePass_nav_active) ? 'is-active' : '' }}">
                                        <a href="#">Change Password</a>
                                </li>
                            </ul>
                        </nav>

                        @yield('page-content')
                    </div>
                </div>
                <!-- .entry-content -->

            </article>
            <!-- #post-## -->
        </main>
    </div>
</div>
@stop

@section('js')

    <!-- toastr notifications -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script>
        @foreach (['error', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            toastr.{{ $msg }}("{{ Session::get('alert-' . $msg) }}", '{{ ucfirst($msg) }} Alert', {timeOut: 5000});
        @endif
        @endforeach
    </script>

    <script>
        jQuery(document).ready(function () {

            // nav dashboard event handler
            jQuery("#dashboard-nav a").click(function(e) {
                e.preventDefault();
                window.location = '{{ route('front.home') }}';
            });

            // nav listTeachers event handler
            jQuery("#listTeachers-nav a").click(function(e) {
                e.preventDefault();
                window.location = '{{ route('front.users.index') }}';
            });

            // nav invoices event handler
            jQuery("#invoices-nav a").click(function(e) {
                e.preventDefault();
                window.location = '{{ route('front.users.invoices') }}';
            });

            // nav favoriteLessons event handler
            jQuery("#favoriteLessons-nav a").click(function(e) {
                e.preventDefault();
                window.location = '{{ route('front.users.listfavlessons') }}';
            });

            // nav accountDetails event handler
            jQuery("#accountDetails-nav a").click(function(e) {
                e.preventDefault();
                window.location = '{{ route('front.users.edit', Auth::user()->id) }}';
            });

            // nav changePass event handler
            jQuery("#changePass-nav a").click(function(e) {
                e.preventDefault();
                window.location = '{{ route('front.users.changePassReq') }}';
            });

        });
    </script>

    @stack('js')
@endsection