<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionRenewReminderTeacher extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @var invoice
     */
    public $school_name;

    public $coordinator_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $school_name, $coordinator_name)
    {
        $this->school_name = $school_name;
        $this->coordinator_name = $coordinator_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.subscriptionRenewReminderTeacher')->subject('Your Maths300 subscription has now expired');
    }
}
