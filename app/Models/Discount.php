<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    /**
     * Get the DiscountType that belongs to this Discount.
     */
    public function discountType()
    {
        return $this->belongsTo(DiscountType::class);
    }

    /**
     * The invoices that belong to the Discount.
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }

    /**
     * Scope a query to only include given discountType.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $discountType
     */
    public function scopeDiscountType($query, $discountType)
    {
        $query->with('discountType')->whereHas('discountType', function ($query) use ($discountType) {
            $query->where('id', $discountType);
        });
    }
}
