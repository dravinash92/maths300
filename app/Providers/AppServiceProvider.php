<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use DB;
use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('admin.common.modal', 'modal');
        Blade::component('admin.common.modaljs', 'modaljs');

        // for font-end use
        Blade::component('admin.common.dialog', 'dialog');
        Blade::component('admin.common.dialogjs', 'dialogjs');

        if (env('APP_DEBUG') == true && env('SQL_DEBUG') == true) {
            DB::listen(function($query) {
                Log::debug(
                    $query->sql,
                    $query->bindings,
                    $query->time
                );
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
