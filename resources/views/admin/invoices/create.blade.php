@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.invoices.index',array("q_to"=> $school_id)) }}">Invoices</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add New</h3><br><br><input type="checkbox" id="is_adhoc_invoice" data-toggle="toggle"><span><b> &nbsp;&nbsp;Adhoc Invoice?  </b></span>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="invoiceForm" role="form" method="post" action="{{ route('admin.invoices.store') }}">
                {{ csrf_field() }}
                <div class="box-body">
                    <input type="hidden" id="school_id" name="school_id" value="{{ old('school_id', $school_id) }}">
                    <div id="discount_apply_info" class="form-group {{ $errors->has('discount_apply') ? 'has-error' : '' }}">
                        <label for="discount_apply">Discount Apply</label><br>
                        <div>
                            <input class="minimal" type="radio" id="discount_apply_yes"
                                   name="discount_apply"
                                   value="1" {{ old('discount_apply') == '1' ? 'checked' : '' }}>
                            <label for="discount_apply_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="discount_apply_no"
                                   name="discount_apply"
                                   value="0" {{ old('discount_apply', 0) == '0' ? 'checked' : ''}}>
                            <label for="discount_apply_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('discount_apply') }}</span>
                    </div>

                    <div id="discountInfo" class="form-group {{ $errors->has('discount') ? 'has-error' : '' }}" style="display: {{ old('discount_apply')? 'block' : 'none' }};">
                        <label for="discount">Discount</label>
                        <select data-placeholder="Select a discount" class="form-control select2_discount"
                                style="width: 100%;" id="discount" name="discount">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('discount') }}</span>
                    </div>

                    <div id="order_id_info" class="form-group {{ $errors->has('order_id') ? 'has-error' : '' }}">
                        <label for="order_id">Order Id</label>
                        <input type="text" class="form-control" id="order_id" name="order_id"
                               value="{{ old('order_id') }}" placeholder="optional">
                        <span class="text-danger">{{ $errors->first('order_id') }}</span>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
            <form id="adhocInvoiceForm" style="display: none" role="form" method="post" action="{{ route('admin.invoices.adhocStore') }}">
                {{ csrf_field() }}
                <div class="box-body">
                    <input type="hidden" id="school_id_adhoc" name="school_id" value="{{ old('school_id', $school_id) }}">
                    <div class="form-group {{ $errors->has('invoice_amount') ? 'has-error' : '' }}">
                        <label for="invoice_amount">Amount*</label>
                        <input type="text" class="form-control" id="invoice_amount" name="invoice_amount"
                               value="" >
                        <span class="text-danger">{{ $errors->first('invoice_amount') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                        <label for="description">Description*</label>
                        <input type="text" class="form-control" id="description" name="description"
                               value="" placeholder="">
                        <span class="text-danger">{{ $errors->first('orddescriptioner_id') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('account_code') ? 'has-error' : '' }}">
                        <label for="account_code">Account Code</label>
                        <select class="form-control account_code" style="width: 100%;"
                                id="account_code"
                                name="account_code"
                                placeholder="Select Account code">
                                <option value="" selected disabled>Select Account code</option>
                                <option value="40460">New Maths300</option>
                                <option value="40461">Existing Maths300</option>
                        </select>
                        <span class="text-danger">{{ $errors->first('account_code') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('notes') ? 'has-error' : '' }}">
                        <label for="notes">Notes (to be appended to Notes in School Details)</label>
                        <input type="text" class="form-control" id="notes" name="notes"
                               value="" placeholder="optional">
                        <span class="text-danger">{{ $errors->first('notes') }}</span>
                    </div>

                    <div id="discount_apply_info" class="form-group {{ $errors->has('another_line') ? 'has-error' : '' }}">
                        <label for="another_line">Add another line item to the invoice?</label><br>
                        <div>
                            <input class="minimal another_line" type="radio" id="another_line_yes"
                                   name="another_line"
                                   value="1">
                            <label for="another_line">Yes</label>&nbsp;&nbsp;

                            <input class="minimal another_line" type="radio" checked id="another_line_no"
                                   name="another_line"
                                   value="0">
                            <label for="another_line">No</label>&nbsp;&nbsp;
                        </div>
                        <span class="text-danger">{{ $errors->first('another_line') }}</span>
                    </div>

                    <div class="form-group addline_info {{ $errors->has('description1') ? 'has-error' : '' }}" style="display:none">
                        <label for="description1">Description*</label>
                        <input type="text" class="form-control" id="description1" name="description1"
                               value="Administrative Fee" placeholder="">
                        <span class="text-danger">{{ $errors->first('description1') }}</span>
                    </div>

                    <div class="form-group addline_info {{ $errors->has('amount1') ? 'has-error' : '' }}" style="display:none">
                        <label for="amount1">Amount*</label>
                        <input type="text" class="form-control" id="amount1" name="amount1"
                               value="{{ $administrative_fee }}" placeholder="">
                        <span class="text-danger">{{ $errors->first('amount1') }}</span>
                    </div>
                    
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                     <p>The Purchase Order number in the school records will be reflected in the invoice. Ensure the Purchase Order number is updated before generating the invoice.</p>
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            let select2_discount = $('.select2_discount').select2({
                allowClear: true,
                data: {!! json_encode($discounts)!!}
            });

            @if(old('discount'))
            select2_discount.val({!! old('discount') !!}).trigger('change');
            @endif

            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');

            $( "#discount_apply_yes" ).click(function() {
                $('#discountInfo').show();
            });

            $( "#discount_apply_no" ).click(function() {
                $('#discountInfo').hide();
            });

            if($('input#is_adhoc_invoice').is(':checked')){
                $('form#invoiceForm').hide();
                $('form#adhocInvoiceForm').show();
            }else{
                $('form#adhocInvoiceForm').hide();
                $('form#invoiceForm').show();
                if($( '#discount_apply_yes' ).is(':checked')){
                    $('#discountInfo').show();
                }
            }

            $('input#is_adhoc_invoice').change(function(){
                if($(this).is(':checked')){
                    $('form#invoiceForm').hide();
                    $('form#adhocInvoiceForm').show();
                }
                else{
                    $('form#adhocInvoiceForm').hide();
                    $('form#invoiceForm').show();
                    if($( '#discount_apply_yes' ).is(':checked')){
                        $('#discountInfo').show();
                    }
                }
            });

            if($('form#adhocInvoiceForm .has-error').length > 0){
                $('form#invoiceForm').hide();
                $('form#adhocInvoiceForm').show();
                $('input#is_adhoc_invoice').prop('checked', true);
                $('input#is_adhoc_invoice').click();
            }

            $('input#another_line_yes').click(function(){
                $('form#adhocInvoiceForm .addline_info').show();
            });

            $('input#another_line_no').click(function(){
                $('form#adhocInvoiceForm .addline_info').hide();
            });
        });
    </script>
@endpush