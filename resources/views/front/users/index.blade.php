@extends('layouts.fronthome')

@section('sub_breadcrumbs')
<a itemprop="item" href="{{ route('front.users.index') }}" title="Teachers"><span itemprop="name">Teachers</span></a>
@endsection

@section('page-content')

<div id="listTeachers-content" class="myaccount-right">

        <div class="row" style="margin-bottom:10px;">
            <form id="userSearchForm" role="form" method="get" action="{{ route('front.users.index') }}">
                @csrf
                <div class="col-xs-6" style="padding-right:0px;">
                    <input type="text" class="form-control" id="q" name="q"
                                placeholder="Enter keywords (name/email/phone)..."
                                value="{{ old('q') }}">
                </div>

                <div class="col-xs-2" style="padding-left:1px;">
                    <button id="usersearchBtn" type="submit"
                                class="btn btn-primary btn-flat ladda-button"
                                data-style="expand-right"><span class="ladda-label">Search</span></button>
                </div>
            </form>

            <div class="col-xs-4 text-right">
                <a href="{{ route('front.users.create') }}" class="btn btn-primary">Add New</a>
                <a class="archive btn btn-primary" href="{{ route('front.users.showArchived') }}">Archived</a>
            </div>
        </div>

        <table id="tab_teachers" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th>Last Login</th>
                    <th>Menu</th>
                </tr>
                @csrf
                </thead>
                <tbody>
                        @foreach($users as $l)
                        <tr class="item{{ $l->id }}">
                            <td>{{ $l->name }}</td>
                            <td>{{ $l->email }}</td>
                            <td>{{ $l->phone }}</td>
                            <td>{{ $l->getStatus() }}</td>
                            <td class="col1">{{ $l->lastLoginFormat() }}</td>
                            <td>
                                @if(!$l->hasRole('super-admin'))
                                    <a href="{{ route('front.users.edit',$l->id) }}" class="btn btn-primary">Edit</a>
                                    <button class="resetpass-modal btn btn-primary" data-email="{{ $l->email }}"
                                            data-name="{{ $l->name }}">Reset Password
                                    </button>
                                    @if( $l->isActivateAllowed() )
                                        <button class="activate-modal btn btn-primary" data-id="{{ $l->id }}"
                                                data-name="{{ $l->name }}">Activate
                                        </button>
                                    @endif

                                    @if( $l->isSuspendAllowed() )
                                        <button class="suspend-modal btn btn-primary" data-id="{{ $l->id }}"
                                                data-name="{{ $l->name }}">Suspend
                                        </button>
                                    @endif

                                    @if( $l->isArchiveAllowed() )
                                        <button class="archive-modal btn btn-primary" data-id="{{ $l->id }}"
                                                data-name="{{ $l->name }}">Archive
                                        </button>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="bbp-pagination">
                    <div class="bbp-pagination-links">
                        {{ $users->links() }}
                    </div>
            </div>

            <!-- Modal form to reset user password -->
            <div id="resetpassModal" class="modal fade" role="dialog" style="display:none">
                <h3>Are you sure you want to reset password?</h3>
                <br/>
                <!-- form start -->
                <form class="form-horizontal" role="form" id="form_paid" method="post"
                                  action="{{ route('front.users.sendResetPassEmail') }}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-sm-1" for="name">Name:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="name_resetpass" name="name"
                                               readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-1" for="email">Email:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="email_resetpass" name="email"
                                               readonly="readonly">
                            <span class="text-info">Note: A new password will be sent to this email</span>
                        </div>
                    </div>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger resetpass" data-dismiss="modal">
                    Reset
                    </button>
                    <button type="button" class="btn btn-warning resetpass_cancel" data-dismiss="modal">
                    Cancel
                    </button>
                </div>
            </div>

    @dialog(['action' => 'activate', 'model' => 'user', 'form_action' => '/users/activate'])
    @enddialog

    @dialog(['action' => 'suspend', 'model' => 'user', 'form_action' => '/users/suspend'])
    @enddialog

    @dialog(['action' => 'archive', 'model' => 'user', 'form_action' => '/users/archive'])
    @enddialog
</div>
@stop

@push('js')
<script>
    jQuery(document).ready(function () {

        // resetpass
        jQuery(document).on('click', '.resetpass-modal', function () {
            jQuery('.modal-title').text('Reset password');
            jQuery('#name_resetpass').val(jQuery(this).data('name'));
            jQuery('#email_resetpass').val(jQuery(this).data('email'));
            jQuery('#resetpassModal').show();
        });

        jQuery('.modal-footer').on('click', '.resetpass', function (evt) {
            evt.preventDefault();
            jQuery('#resetpassModal').hide();
            jQuery('#listTeachers-content').addClass('loading');
            jQuery.ajax({
                type: 'POST',
                url: location.origin + '/users/sendResetPassEmail',
                data: {
                    '_token': jQuery('input[name=_token]').val(),
                    'email': jQuery('input[name=email]').val(),
                },
                success: function (response) {
                    if (response.status === 1) {
                        toastr.error(response.data, 'Error Alert', {timeOut: 5000});
                    }
                    if (response.status === 0) {
                        toastr.success('Reset password request was handled successfully.', 'Success Alert', {timeOut: 5000});
                    }
                },
                error: function (response) {
                    console.log(response);
                    if (response.status === 500) {
                        toastr.error('Server error.', 'Error Alert', {timeOut: 5000});
                    } else {
                        if (response.responseJSON.error) {
                            toastr.error(response.responseJSON.error, 'Error Alert', {timeOut: 5000});
                        } else {
                            toastr.error('Bad Request', 'Error Alert', {timeOut: 5000});
                        }
                    }
                }
            });
        });

        jQuery('.modal-footer').on('click', '.resetpass_cancel', function (evt) {
            evt.preventDefault();
            jQuery('#resetpassModal').hide();
        });
    });
</script>

@dialogjs(['action' => 'activate'])
@enddialogjs

@dialogjs(['action' => 'suspend'])
@enddialogjs

@dialogjs(['action' => 'archive'])
@enddialogjs

@endpush
