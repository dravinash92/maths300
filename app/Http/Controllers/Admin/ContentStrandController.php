<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContentStrand;
use Illuminate\Http\Request;
use App\Http\Requests\ContentStrandRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class ContentStrandController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = ContentStrand::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['contentstrands'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.contentstrands.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contentstrands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ContentStrandRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContentStrandRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $contentstrand = new ContentStrand();
        $contentstrand->name = $validated['name'];

        $contentstrand->save();

        $request->session()->flash('alert-success', 'Content strand added successfully.');

        return redirect()->route('admin.contentstrands.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContentStrand $contentstrand
     * @return \Illuminate\Http\Response
     */
    public function show(ContentStrand $contentstrand)
    {
        $arr['contentstrand'] = $contentstrand;

        return view('admin.contentstrands.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContentStrand $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentStrand $contentstrand)
    {
        $arr['contentstrand'] = $contentstrand;
        return view('admin.contentstrands.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ContentStrandRequest $request
     * @param  \App\Models\ContentStrand $contentstrand
     * @return \Illuminate\Http\Response
     */
    public function update(ContentStrandRequest $request, ContentStrand $contentstrand)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $contentstrand->name = $validated['name'];

        $contentstrand->save();

        $request->session()->flash('alert-success', 'Content strand updated successfully.');

        return redirect()->route('admin.contentstrands.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contentstrand = ContentStrand::findOrFail($id);
        Log::info("delete contentstrand " . $id);
        
        $contentstrand->delete();

        return response()->json($contentstrand);
    }
}
