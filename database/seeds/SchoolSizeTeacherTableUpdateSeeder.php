<?php

use Illuminate\Database\Seeder;
use App\Models\SchoolSizeTeacher;

class SchoolSizeTeacherTableUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolSizeTeacher::where('id', '=', 1)->update(['name' => '1 teacher']);
        SchoolSizeTeacher::where('id', '=', 2)->update(['name' => 'Secondary: 2 – 5 teachers']);
        SchoolSizeTeacher::where('id', '=', 3)->update(['name' => 'Secondary: 6 – 15 teachers']);
        SchoolSizeTeacher::where('id', '=', 4)->update(['name' => 'Secondary: 16 - 25 teachers']);
        SchoolSizeTeacher::where('id', '=', 5)->update(['name' => 'Secondary: 26 - 40 teachers']);
        // SchoolSizeTeacher::where('id', '=', 6)->update(['name' => 'Primary and Secondary: more than 40 teachers']);

        // tricky one......
        SchoolSizeTeacher::where('id', '=', 9)->delete();
        
        SchoolSizeTeacher::create([
            'name' => 'Primary and Secondary: more than 40 teachers'
        ]);

        SchoolSizeTeacher::create([
            'name' => 'Primary: 2 – 15 teachers'
        ]);

        SchoolSizeTeacher::create([
            'name' => 'Primary: 16 – 40 teachers'
        ]);
    }
}
