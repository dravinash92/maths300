@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        /* Invoice Status Colors */

        .awaiting-payment {
            color: #ffcc00;
        }
        .paid {
            color: #00a65a;
        }
        .overdue {
            color: #cc0000;
        }
        .filter-class{
            display: flex;
            align-items: center;
        }
        .download-btn{
            text-align: center;
            padding: 20px 0px 10px 0px;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.schools.index') }}">Schools report</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Schools Report</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="schoolSearchForm" role="form" method="post" action="{{ route('admin.reports.getschools') }}">
                @csrf
                <div class="row filter-class">
                    <div class="col-xs-2">
                        <label>Filter By</label>
                    </div>
                    <div class="col-xs-4">
                        <select data-placeholder="Select location" class="form-control select2_address_location"
                                style="width: 100%;" id="q_address_location" name="q_address_location">
                            <option></option>
                        </select>
                    </div>

                    <div class="col-xs-4">
                        <select data-placeholder="Select School Size (Teachers)"
                                class="form-control select2_SchoolSizeTeacher"
                                style="width: 100%;" id="SchoolSizeTeacher" name="SchoolSizeTeacher">
                            <option></option>
                        </select>
                    </div>
                    
                </div>
                <div class="row filter-class" style="margin-top: 15px;">
                    <div class="col-xs-2">
                       
                    </div>
                    <div class="col-xs-4">
                        <select class="form-control" name="q_sub_status" id="q_sub_status">
                            <option value="">Select Subscription Status</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                            <option value="2">Suspended</option>
                            <option value="3">Archived</option>
                          </select>
                    </div>
                    <div class="col-xs-4">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="q_subscription_expiry_date_range" name="q_subscription_expiry_date_range" value="{{ old('q_subscription_expiry_date_range') }}" placeholder="Enter subscription expiry date range">
                        </div>
                    </div>
                </div>
                    <div class="col-xs-12 download-btn">
                        <button class="btn btn-primary btn-flat" type="submit">Download report</button>
                    </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- date-range-picker -->
    <script src="{{ asset ('vendor/adminlte/vendor/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset ('vendor/adminlte/vendor/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset ('vendor/adminlte/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/date-uk.js"></script>
    <script>
        $(document).ready(function () {

            // Initialize Select2 elements
            let select2_address_location = $('.select2_address_location').select2({
                allowClear: true,
                data: {!! json_encode($locations)!!}
            });

            @if(old('q_address_location'))
            select2_address_location.val({!! old('q_address_location') !!}).trigger('change');
                    @endif

            let select2_SchoolSizeTeacher = $('.select2_SchoolSizeTeacher').select2({
                allowClear: true,
                data: {!! json_encode($SchoolSizeTeacher)!!}
            });

            @if(old('SchoolSizeTeacher'))
            select2_SchoolSizeTeacher.val({!! old('SchoolSizeTeacher') !!}).trigger('change');
            @endif
            //Date range picker
            $('#q_subscription_expiry_date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD/MM/YYYY'
                }
            });

            $('#q_subscription_expiry_date_range').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });

            $('#q_subscription_expiry_date_range').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });

    </script>
@endpush
