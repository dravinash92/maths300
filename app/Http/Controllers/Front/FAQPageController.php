<?php

namespace App\Http\Controllers\Front;

use App\Models\FAQPage;
use App\Http\Controllers\Controller;

class FAQPageController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['faqpage'] = FAQPage::first();
        return view('front.faq')->with($arr);
        
    }
}
