<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Setting extends Model
{
    public static function get($key) {
        $match = ['name'=> $key];
        $setting = Setting::where($match)->first();
        $value = '';

        if (!empty($setting)) {
            $value = $setting->value;
        }

        return $value;
    }

    public static function batchGet($keys) {
        return DB::table('settings')
            ->select('name','value')
            ->whereIn('name', $keys)
            ->get();
    }

    /**
     * from information
     * AAMT name, address, abn
     * @return array
     */
    public static function getContactInfo()
    {
        $settings = Setting::batchGet(array('Organization address', 'Phone', 'Email'));
        $res = array(
            'address' => $settings[0]->value,
            'phone' => $settings[1]->value,
            'email' => $settings[2]->value,
        );

        return $res;
    }
}
