<?php

namespace App\Jobs;

use App\Models\HistoryData;
use App\Models\School;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use App\Models\Country;
use App\Models\Location;
use App\Models\SchoolType;
use App\Models\SchoolSector;
use App\Models\SchoolSizeStudent;
use App\Models\SchoolSizeTeacher;
use App\Models\SubscriptionPlan;
use App\Models\User;
use Carbon\Carbon;

class HistoryDataMigration implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public static function getSchoolType($schoolTypeKeywords)
    {
        Log::info('HistoryDataMigration getSchoolType [' . $schoolTypeKeywords . ']');
        $st = SchoolType::where('name', 'like', '%' . $schoolTypeKeywords . '%')->first();
        // set school type to [Others] if not found suitable
        if (!$st) {
            $st = SchoolType::where('name', 'Other')->first();
        }
        return $st;
    }

    public static function getSchoolSector($schoolSectorKeywords)
    {
        Log::info('HistoryDataMigration getSchoolSector [' . $schoolSectorKeywords . ']');
        $ss = SchoolSector::where('name', 'like', '%' . $schoolSectorKeywords . '%')->first();
        return $ss;
    }

    public static function getLocation($locationKeywords)
    {
        Log::info('HistoryDataMigration getLocation [' . $locationKeywords . ']');
        $l = Location::where('name', 'like', '%' . $locationKeywords . '%')->first();
        return $l;
    }

    /**
     * @param $license
     * @return |null
     */
    public static function getSchoolSize($license)
    {
        Log::info('HistoryDataMigration getSchoolSize by $license [' . $license . ']');
        return SchoolSizeTeacher::where('name', $license)->first();
    }

    public function initMetaData()
    {
        // purify [history_data] invalid data
        DB::table('history_data')
            ->where('focus', 'Secondary Primary')
            ->update(['focus' => 'Primary Secondary']);

        // create new school type

        // Primary Secondary
        if (SchoolType::where('name', 'Primary Secondary')->count() == 0) {
            Log::info('HistoryDataMigration create new school type [Primary Secondary]');
            SchoolType::create([
                'name' => 'Primary Secondary'
            ]);
        }

        if (SchoolType::where('name', 'Tertiary school')->count() == 0) {
            Log::info('HistoryDataMigration create new school type [Tertiary school]');
            SchoolType::create([
                'name' => 'Tertiary school'
            ]);
        }

        if (SchoolType::where('name', 'Other')->count() == 0) {
            Log::info('HistoryDataMigration create new school type [Other]');
            SchoolType::create([
                'name' => 'Other'
            ]);
        }

        // create new country if any
        // SELECT DISTINCT country FROM `history_data`
        $d_countries = DB::table('history_data')->select('country')->distinct()->get();
        Log::info('HistoryDataMigration distinct countries ' . $d_countries);
        foreach ($d_countries as $c) {
            $tmp = ucwords(strtolower($c->country));
            if (!empty($tmp)) {
                if (Country::where('name', $tmp)->count() == 0) {
                    Log::info('HistoryDataMigration create new country ' . $tmp);
                    Country::create([
                        'name' => $tmp,
                    ]);
                }
            }
        }
        // create new locations if any
        // SELECT DISTINCT country, state FROM `history_data` order by country
        $d_country_locations = DB::table('history_data')->select('country', 'state')->distinct()->orderBy('country')->get();
        Log::info('HistoryDataMigration distinct country locations ' . $d_country_locations);
        foreach ($d_country_locations as $cl) {
            if (!empty($cl->state) && !empty($cl->country)) {
                $tmp_country = ucwords(strtolower($cl->country));
                if (Location::where('name', $cl->state)->count() == 0) {
                    Log::info('HistoryDataMigration create new location');
                    $location = new Location();
                    $location->name = $cl->state;

                    DB::transaction(function () use ($location, $tmp_country) {
                        $location->save();

                        // save relationships
                        // $location
                        $country = Country::where('name', $tmp_country)->first();
                        $location->country()->associate($country)->save();

                        Log::info("HistoryDataMigration create location " . $location->name);
                    });
                }
            }
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('HistoryDataMigration job started......');
        $this->initMetaData();
        // step1: load all data from table `History_data`
        // step2: create school info and user info (coordinator and finance officer)
        // step3: populate the last active invoice info
        $all_history_school_data = HistoryData::all();
        $total_count = 0;

        // define error info array
        $error_school_name_empty = array();
        $school_expired_subscription = array();
        $school_duplicated_teacher_email = array();
        $school_duplicated_invoice_email = array();
        $school_empty_finance_officer_email = array();
        $school_empty_emails = array();

        foreach ($all_history_school_data as $key => $data) {
            Log::info('HistoryDataMigration handle ' . ($key + 1) . ' ');
            ++$total_count;

            // skip data where institution is empty
            if (empty($data->institution)) {
                Log::info('HistoryDataMigration skip data where institution is empty, id->' . $data->id . ', username [' . $data->usrname . '] ');
                array_push($error_school_name_empty, $data);
                continue;
            }

            // build school model
            // create users
            // associate users and schools
            $school = new School();
            $school->name = $data->institution;
            $school->address_region = $data->address;
            $school->address_suburb = $data->suburb;
            $school->address_postcode = $data->postcode;
            // 1 by default
            $school->number_of_campuses = 1;
            $school->notes = $data->notes;
            $school->finance_invoice_email = $data->invoicing_email;

            // same as address above
            $school->billing_address_region = $data->address;
            $school->billing_address_suburb = $data->suburb;
            $school->billing_address_postcode = $data->postcode;

            if (!empty($data->joining_date)) {
                $school->join_date = $data->joining_date;
            } else {
                Log::warning('HistoryDataMigration empty joining_date.');
            }

            // default 0:inactive
            $school->status = 0;

            // subscription related dates
            $school->subscription_expiration_date = Carbon::parse($data->renewal_date);
            if ($school->subscription_expiration_date->copy()->isPast()) {
                Log::info('HistoryDataMigration school [' . $school->name . '] subscription has expired, set school status to [suspended]');
                $school->status = 2;
                array_push($school_expired_subscription, $school);
            }
            $paid_date_mock = Carbon::parse($data->renewal_date)->copy()->addYear(-1);
            $school->last_paid_date = $paid_date_mock;
            $school->last_renewal_date = $paid_date_mock;

            // mark as historical data
            $school->is_imported = 1;

            DB::transaction(function () use ($school, $data, $school_duplicated_teacher_email, $school_duplicated_invoice_email, $school_empty_finance_officer_email, $school_empty_emails) {
                $school->save();

                // save relationships
                $schoolType = HistoryDataMigration::getSchoolType($data->focus);
                if (!empty($schoolType)) {
                    $school->schoolType()->associate($schoolType)->save();
                }

                $schoolSector = HistoryDataMigration::getSchoolSector($data->sector);
                if (!empty($schoolSector)) {
                    $school->schoolSector()->associate($schoolSector)->save();
                }

                $address_location = HistoryDataMigration::getLocation($data->state);
                if (!empty($address_location)) {
                    $school->addressLocation()->associate($address_location)->save();
                }

                // default 51–100 students at campus
                $schoolSizeStudent = SchoolSizeStudent::find(2);
                $school->schoolSizeStudent()->associate($schoolSizeStudent)->save();

                $schoolSizeTeacher = HistoryDataMigration::getSchoolSize($data->license_new);
                if (!empty($schoolSizeTeacher) && $schoolSizeTeacher->id == 1) {
                    // set as [single account]
                    $school->is_single_account = 1;
                }
                $school->schoolSizeTeacher()->associate($schoolSizeTeacher)->save();

                $billing_address_location = HistoryDataMigration::getLocation($data->state);
                if (!empty($billing_address_location)) {
                    $school->billingAddressLocation()->associate($billing_address_location)->save();
                }

                // reverse query
                $subscriptionPlan = SubscriptionPlan::query()->schoolSize($data->license_new)->first();
                if (!empty($subscriptionPlan)) {
                    $school->subscriptionPlan()->associate($subscriptionPlan)->save();
                }

                // check email first
                if (!empty($data->teacher_email) && !empty($data->invoicing_email)) {
                    // check exists first
                    if (!User::isUserEmailExists($data->teacher_email)) {
                        // create finance_officer/coordinator for this school (with status inactive)
                        $coordinator = new User();
                        $coordinator->name = $data->teacher_first_name . ' ' . $data->teacher_last_name;
                        $coordinator->email = $data->teacher_email;
                        $coordinator->phone = $data->phone;

                        $coordinator->password = Hash::make($data->password);
                        $coordinator->status = 0; //0:disabled
                        $coordinator->reset_token = Password::getRepository()->create($coordinator);
                        $coordinator->save();
                        // build relationship with school
                        $coordinator->schools()->sync([$school->id]);
                        $coordinator->assignRole('school-admin');
                        $school->coordinator()->associate($coordinator)->save();

                        // build a finance officer
                        // check if [invoicing_email] equals to [teacher_email]
                        if (strtolower($data->teacher_email) == strtolower($data->invoicing_email)) {
                            Log::info('HistoryDataMigration set $finance_officer as $coordinator because they have the same email address.');
                            $finance_officer = $coordinator;
                            $finance_officer->assignRole('school-finance-officer', 'school-admin');
                        } else {
                            if (!User::isUserEmailExists($data->invoicing_email)) {
                                Log::info('HistoryDataMigration set $finance_officer as a separate person.');
                                $finance_officer = new User();
                                $finance_officer->name = $school->name . ' Finance Officer';
                                $finance_officer->email = $data->invoicing_email;

                                $finance_officer->password = Hash::make($data->password);
                                $finance_officer->status = 0; //0:disabled
                                $finance_officer->reset_token = Password::getRepository()->create($finance_officer);
                                $finance_officer->save();
                                $finance_officer->assignRole('school-finance-officer');
                            } else {
                                Log::error('HistoryDataMigration data error invoicing_email exists [' . $data->invoicing_email . ']');
                                array_push($school_duplicated_invoice_email, $data);
                            }
                        }
                        if (!empty($finance_officer)) {
                            // build relationship with school
                            $finance_officer->schools()->sync([$school->id]);
                            $school->financeOfficer()->associate($finance_officer)->save();
                            Log::info("create school [" . $school->name . '] successfully');
                        } else {
                            Log::error('HistoryDataMigration data error finance_officer is empty');
                            array_push($school_empty_finance_officer_email, $data);
                        }
                    } else {
                        Log::error('HistoryDataMigration data error teacher_email exists [' . $data->teacher_email . ']');
                        array_push($school_duplicated_teacher_email, $data);
                    }
                } else {
                    Log::error('HistoryDataMigration data error teacher_email [' . $data->teacher_email . ']');
                    Log::error('HistoryDataMigration data error invoicing_email [' . $data->invoicing_email . ']');
                    Log::error('HistoryDataMigration no school users created [' . $school->name . ']');
                    array_push($school_empty_emails, $data);
                }
            });

        }

        // query school data
        $imported_school = School::where('is_imported', 1)->get();
        $imported_school_single_account = School::where('is_imported', 1)->where('is_single_account', 1)->get();

        Log::info('######################################################################################');
        Log::info('HistoryDataMigration job completed, original data count: ' . $total_count);
        Log::info('Imported school count: ' . count($imported_school));
        Log::info('Single account school count: ' . count($imported_school_single_account));
        Log::info('####Warning###');
        Log::info('error_school_name_empty count:' . count($error_school_name_empty) . ', details as follows:');
        foreach ($error_school_name_empty as $data) {
            Log::info('school username: ' . $data->username);
        }
        Log::info('school_expired_subscription count:' . count($school_expired_subscription));
        Log::info('school_duplicated_teacher_email count:' . count($school_duplicated_teacher_email) . ', details as follows:');
        foreach ($school_duplicated_teacher_email as $data) {
            Log::info('school: ' . $data->institution);
        }
        Log::info('school_duplicated_invoice_email count:' . count($school_duplicated_invoice_email) . ', details as follows:');
        foreach ($school_duplicated_invoice_email as $data) {
            Log::info('school: ' . $data->institution);
        }
        Log::info('school_empty_finance_officer_email count:' . count($school_empty_finance_officer_email) . ', details as follows:');
        foreach ($school_empty_finance_officer_email as $data) {
            Log::info('school: ' . $data->institution);
        }
        Log::info('school_empty_emails count:' . count($school_empty_emails) . ', details as follows:');
        foreach ($school_empty_emails as $data) {
            Log::info('school: ' . $data->institution);
        }
        Log::info('######################################################################################');
    }
}
