<?php

namespace App\Http\Controllers\Front;

use App\Models\Advertisement;
use App\Models\ContentStrand;
use App\Models\Lesson;
use App\Models\User;
use App\Models\School;
use App\Models\LessonInteract;
use App\Models\Pedagogy;
use App\Models\Tag;
use App\Models\Curriculum;
use Illuminate\Http\Request;
use App\Models\YearLevel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Response;
use Validator;
use Auth;
class LessonController extends Controller
{
    public $perPage = 3;

    public function __construct()
    {
        $this->middleware('auth', ['except' => [/*'index',*/ 'sample'/*,'listall'*/]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['year_levels'] = data4Select(YearLevel::class);
        $arr['content_strands'] = data4Select(ContentStrand::class);
        $arr['pedagogies'] = data4Select(Pedagogy::class);
        $arr['curricula'] = data4Select(Curriculum::class);
        if(!empty(Auth::user()->id)){
            $schquery = User::getusers("school_user.user_id = ".Auth::user()->id)->toArray();
            if($schquery){
                $school = School::where('id',$schquery[0]['school_id'])->first();
                $tagids = explode(",",$school->tags);
            }
        }
        $query = Lesson::query();
        
        if(!empty($school->tags))
        {
            $query->Tags($tagids);
        }

        if ($request->has('q_id')) {
            $query->id($request->q_id);
        }

        if ($request->has('q_year_levels')) {
            $query->yearLevels($request->q_year_levels);
        }

        if ($request->has('q_content_strands')) {
            $query->contentStrands($request->q_content_strands);
        }

        if ($request->has('q_pedagogies')) {
            $query->pedagogies($request->q_pedagogies);
        }

        if ($request->has('q_curricula')) {
            $query->curricula($request->q_curricula);
        }

        if ($request->has('q') && $request->q !== null && $request->q !=="") {
            // record user input keywords for further analysis in the future
            Log::info("lessons search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        // published
        $query->status(1);
        $query->orderBy('title', 'asc');
        $paginator = $query->paginate($this->perPage);

        if ($request->has('q_year_levels')) {
            $paginator->appends(array('q_year_levels' => $request->q_year_levels));
        }
        if ($request->has('q_content_strands')) {
            $paginator->appends(array('q_content_strands' => $request->q_content_strands));
        }
        if ($request->has('q_pedagogies')) {
            $paginator->appends(array('q_pedagogies' => $request->q_pedagogies));
        }
        if ($request->has('q_curricula')) {
            $paginator->appends(array('q_curricula' => $request->q_curricula));
        }
        if ($request->has('q') && $request->q !== null && $request->q !=="") {
            $paginator->appends(array('q' => $request->q));
        }

        $arr['lessons'] = $paginator;

        // allow using Input::old() function in view to access the data.
        $request->flash();
        return view('front.lessons.index')->with($arr);
    }

    /**
     * list all lessons in terms of table
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listall(Request $request)
    {
        
        $arr['year_levels'] = data4Select(YearLevel::class);
        $arr['content_strands'] = data4Select(ContentStrand::class);
        $arr['pedagogies'] = data4Select(Pedagogy::class);
        $arr['curricula'] = data4Select(Curriculum::class);
        $arr['tags'] = data4Select(Tag::class);
        if(!empty(Auth::user()->id)){
            $schquery = User::getusers("school_user.user_id = ".Auth::user()->id)->toArray();
            if($schquery){
                $school = School::where('id',$schquery[0]['school_id'])->first();
                $tagids = explode(",",$school->tags);
            }
        }
        $query = Lesson::query();
        
        if(!empty($school->tags))
        {
            $query->Tags($tagids);
        }
        
        if ($request->has('q_year_levels')) {
            $query->yearLevels($request->q_year_levels);
        }

        if ($request->has('q_content_strands')) {
            $query->contentStrands($request->q_content_strands);
        }

        if ($request->has('q_pedagogies')) {
            $query->pedagogies($request->q_pedagogies);
        }
        //
        if ($request->has('q_curricula')) {
            $query->curricula($request->q_curricula);
        }

        if ($request->has('q') && $request->q != null && $request->q !=="") {
            // record user input keywords for further analysis in the future
            Log::info("front lesson search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        // published
        $query->status(1);
        if($request->has('orderby') && $request->orderby==="a-z" ) {
            $query->orderBy('title', 'asc');
        } else if($request->has('orderby') && $request->orderby==="z-a") {
            $query->orderBy('title', 'desc');
        } else if($request->has('orderby') && $request->orderby==="number") {
            $query->orderBy('number', 'desc');
        } else {
            $query->orderBy('number', 'asc');
        }
        
        $paginator = $query->paginate(10);
        //echo "<pre>";print_r($paginator);exit;
        if ($request->has('q_year_levels')) {
            $paginator->appends(array('q_year_levels' => $request->q_year_levels));
        }
        if ($request->has('q_content_strands')) {
            $paginator->appends(array('q_content_strands' => $request->q_content_strands));
        }
        if ($request->has('q_pedagogies')) {
            $paginator->appends(array('q_pedagogies' => $request->q_pedagogies));
        }
        if ($request->has('q_curricula')) {
            $paginator->appends(array('q_curricula' => $request->q_curricula));
        }
        if ($request->has('q') && $request->q !== null && $request->q !=="") {
            $paginator->appends(array('q' => $request->q));
        }

        $arr['lessons'] = $paginator;
        $arr['orderby'] = $request->orderby;

        // allow using Input::old() function in view to access the data.
        $request->flash();
        return view('front.lessons.list')->with($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $salt="MathsLesson300";
        $decrypted_id_raw = base64_decode($id);
        $decrypted_id = preg_replace(sprintf('/%s/', $salt), '', $decrypted_id_raw);
        $match = ['id' => $decrypted_id, 'status' => 1];
        $lesson = Lesson::where($match)->first();
        $arr['tagsList'] = Tag::all('id', 'name');
        if (!empty($lesson)) {
            $lesson->number_of_views += 1;
            $lesson->save();
            Log::info("show lesson " . $lesson->id);
        } else {
            abort(404);
        }
        

        $arr['lesson'] = $lesson;

        if(!empty(Auth::user()->id)){
            $schquery = User::getusers("school_user.user_id = ".Auth::user()->id)->toArray();
            if($schquery){
                $school = School::where('id',$schquery[0]['school_id'])->first();
                $tagids = explode(",",$school->tags);
            }
        }
        
        // add latest published lessons (5)
        $query = Lesson::query()->status(1);

        if(!empty($school->tags))
        {
            $query->Tags($tagids);
        }
        $alllessons = $query->get(['id'])->toArray();
        foreach($alllessons as $vl){
            $arr['alllessons'][] = $vl['id'];
        }
        $query->orderBy('created_at', 'desc')->limit(5);
        
        $arr['latest_lessons'] = $query->get();
        
        // published ads
        $arr['ads'] = Advertisement::status(1)->get();
        //echo "<pre>";print_r($arr['alllessons']);exit;
        return view('front.lessons.show')->with($arr);
    }

    /**
     * show sample
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sample($id)
    {
         $salt="MathsLesson300";
        $decrypted_id_raw = base64_decode($id);
        $decrypted_id = preg_replace(sprintf('/%s/', $salt), '', $decrypted_id_raw);;
        $match = ['id' => $decrypted_id, 'status' => 1, 'is_sample' => 1];
        $lesson = Lesson::where($match)->first();

        if (!empty($lesson)) {
            $lesson->number_of_views += 1;
            $lesson->save();
            Log::info("show lesson " . $lesson->id);
        } else {
            abort(404);
        }

        $arr['lesson'] = $lesson;

        if(!empty(Auth::user()->id)){
            $schquery = User::getusers("school_user.user_id = ".Auth::user()->id)->toArray();
            if($schquery){
                $school = School::where('id',$schquery[0]['school_id'])->first();
                $tagids = explode(",",$school->tags);
            }
        }
        
        // add latest published lessons (5)
        $query = Lesson::query()->status(1);

        if(!empty($school->tags))
        {
            $query->Tags($tagids);
        }
        $alllessons = $query->get(['id'])->toArray();
        foreach($alllessons as $vl){
            $arr['alllessons'][] = $vl['id'];
        }
        $query->orderBy('created_at', 'desc')->limit(5);
        
        $arr['latest_lessons'] = $query->get();
        // published ads
        $arr['ads'] = Advertisement::status(1)->get();

        return view('front.lessons.show')->with($arr);
    }

    public function setAccessCode(Request $request)
    {
        $validated = $request->validate([
            'lesson_id' => 'required',
            'user_id' => 'required',
            'access_url' => 'required',
        ]);

        $lessonInteract = new LessonInteract();
        $lessonInteract->lesson_id = $validated['lesson_id'];
        $lessonInteract->user_id = $validated['user_id'];
        $lessonInteract->access_url = $validated['access_url'];
        $lessonInteract->access_code = Str::upper(Str::random(6));
        $expires_hours = config('app.access_code_expires_hours');
        $lessonInteract->expires_in = $expires_hours.' hours';
        // 2 hours expires by default
        $lessonInteract->expires_at = now()->copy()->addHours($expires_hours);
        $lessonInteract->save();

        $arr['status'] = 0;
        $arr['data'] = [
            'access_url' => $lessonInteract->access_url,
            'access_code' => $lessonInteract->access_code,
            'expires_in' => $lessonInteract->expires_in,
            'expires_at' => $lessonInteract->expires_at,
        ];

        return response()->json($arr, 200);
    }
}
