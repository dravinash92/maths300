<?php

namespace App\Http\Controllers\Admin;

use App\Models\SchoolType;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolTypeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class SchoolTypeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SchoolType::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['schooltypes'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.schooltypes.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schooltypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SchoolTypeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolTypeRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $schooltype = new SchoolType();
        $schooltype->name = $validated['name'];

        $schooltype->save();

        $request->session()->flash('alert-success', 'School type added successfully.');

        return redirect()->route('admin.schooltypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SchoolType $schooltype
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolType $schooltype)
    {
        $arr['schooltype'] = $schooltype;

        return view('admin.schooltypes.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SchoolType $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolType $schooltype)
    {
        $arr['schooltype'] = $schooltype;
        return view('admin.schooltypes.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SchoolTypeRequest $request
     * @param  \App\Models\SchoolType $schooltype
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolTypeRequest $request, SchoolType $schooltype)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $schooltype->name = $validated['name'];

        $schooltype->save();

        $request->session()->flash('alert-success', 'School type updated successfully.');

        return redirect()->route('admin.schooltypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schooltype = SchoolType::findOrFail($id);
        Log::info("delete schooltype " . $id);
        
        $schooltype->delete();

        return response()->json($schooltype);
    }
}
