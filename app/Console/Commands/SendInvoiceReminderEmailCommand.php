<?php

namespace App\Console\Commands;

use App\Jobs\SendInvoiceReminderEmail;
use Illuminate\Console\Command;

class SendInvoiceReminderEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:remind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send invoice reminder emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SendInvoiceReminderEmail::dispatch();
        $this->info("Send invoice reminder emails successfully.");
    }
}
