<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentStrand extends Model
{
    /**
     * The lessons that belong to the content strand.
     */
    public function lessons()
    {
        return $this->belongsToMany(Lesson::class,'lesson_content_strand');
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }
}