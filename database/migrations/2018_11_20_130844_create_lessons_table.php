<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->string('title');
            $table->longText('summary');
            $table->longText('resources');
            $table->longText('lesson_notes');
            $table->longText('extensions')->nullable();
            $table->longText('practical_tips')->nullable();
            // reference to lesson ID
            $table->string('related_lessons');
            $table->longText('acknowledgements')->nullable();
            $table->string('attributes')->nullable();
            // 0:no 1:yes
            $table->integer('software_required')->default(0);
            // required if software_required is yes
            $table->string('software_link')->nullable();
            // links only
            $table->string('partial_video')->nullable();
            // links only
            $table->string('full_video')->nullable();
            $table->longText('notes')->nullable();
            // init status: 0: draft/suspend 1 published
            $table->integer('status')->default(0);
            // init the number of views of newly created lesson to 0
            $table->integer('number_of_views')->default(0);

            // 0: FALSE 1: TRUE
            $table->integer('is_sample')->default(0);
            // 0: FALSE 1: TRUE
            $table->integer('is_front')->default(0);

            // required if is_front set true or is_sample set true
            $table->integer('cover_image_id')->unsigned()->index()->nullable();
            $table->foreign('cover_image_id')->references('id')->on('resources');

            // description for front-end
            $table->longText('desc_front')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
