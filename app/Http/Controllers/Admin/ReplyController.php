<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Forum;
use App\Models\Reply;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Requests\ReplyRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Auth;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['topics'] = Forum::topicData4Select();
        $query = Reply::query();

        if ($request->has('q_topic') && $request->q_topic != null) {
            $query->topic($request->q_topic);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['replies'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.replies.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['topics'] = Forum::topicData4Select();
        return view('admin.replies.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ReplyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReplyRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $reply = new Reply();
        $reply->content = $validated['content'];

        DB::transaction(function () use ($reply, $validated) {
            // save relationships
            // topic
            $topic = Topic::find($validated['topic']);
            $reply->title = 'RE: '.$topic->title;
            $reply->save();

            $reply->topic()->associate($topic)->save();

            // author: current user
            $author = Auth::user();
            $reply->author()->associate($author)->save();

            Log::info("create reply " . $reply->id);
        });

        $request->session()->flash('alert-success', 'Reply added successfully.');

        return redirect()->route('admin.replies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        $arr['reply'] = $reply;

        return view('admin.replies.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function edit(Reply $reply)
    {
        $arr['reply'] = $reply;
        $arr['topics'] = Forum::topicData4Select();

        return view('admin.replies.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ReplyRequest  $request
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(ReplyRequest $request, Reply $reply)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $reply->content = $validated['content'];

        DB::transaction(function () use ($reply, $validated) {
            // save relationships
            // topic
            $topic = Topic::find($validated['topic']);
            $reply->title = 'RE: '.$topic->title;
            $reply->save();

            $reply->topic()->associate($topic)->save();

            // author: current user
            $author = Auth::user();
            $reply->author()->associate($author)->save();

            Log::info("update reply " . $reply->id);
        });

        $request->session()->flash('alert-success', 'Reply updated successfully.');

        return redirect()->route('admin.replies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reply = Reply::findOrFail($id);
        Log::info("delete reply " . $id);

        /**
         * Note: all replies related will be deleted cascade
         */
        $reply->delete();

        return response()->json($reply);
    }
}
