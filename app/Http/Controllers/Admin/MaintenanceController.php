<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class MaintenanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show Maintenance page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.maintenance');
    }
}
