@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.replies.index') }}">Replies</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Reply Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <h4>Forum</h4>
            {{ $reply->topic->forum->title }}
            <hr>

            <h4>Topic</h4>
            {{ $reply->topic->title }}
            <hr>

            <h4>Title</h4>
            {{ $reply->title }}
            <hr>

            <h4>Content</h4>
            {!!  $reply->content !!}
            <hr>

            <h4>Updated at</h4>
            {{ $reply->updated_at }}
            <hr>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.replies.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
@endpush