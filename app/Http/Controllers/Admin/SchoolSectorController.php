<?php

namespace App\Http\Controllers\Admin;

use App\Models\SchoolSector;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolSectorRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class SchoolSectorController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SchoolSector::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['schoolsectors'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.schoolsectors.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schoolsectors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SchoolSectorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolSectorRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $schoolsector = new SchoolSector();
        $schoolsector->name = $validated['name'];

        $schoolsector->save();

        $request->session()->flash('alert-success', 'School sector added successfully.');

        return redirect()->route('admin.schoolsectors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SchoolSector $schoolsector
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolSector $schoolsector)
    {
        $arr['schoolsector'] = $schoolsector;

        return view('admin.schoolsectors.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SchoolSector $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolSector $schoolsector)
    {
        $arr['schoolsector'] = $schoolsector;
        return view('admin.schoolsectors.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SchoolSectorRequest $request
     * @param  \App\Models\SchoolSector $schoolsector
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolSectorRequest $request, SchoolSector $schoolsector)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $schoolsector->name = $validated['name'];

        $schoolsector->save();

        $request->session()->flash('alert-success', 'School sector updated successfully.');

        return redirect()->route('admin.schoolsectors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schoolsector = SchoolSector::findOrFail($id);
        Log::info("delete schoolsector " . $id);
        
        $schoolsector->delete();

        return response()->json($schoolsector);
    }
}
