<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pedagogy extends Model
{
    /**
     * The lessons that belong to the pedagogy.
     */
    public function lessons()
    {
        return $this->belongsToMany(Lesson::class,'lesson_pedagogy');
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }
}
