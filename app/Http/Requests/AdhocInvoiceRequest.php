<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdhocInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required',
            'invoice_amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'description' => 'required|min:2|max:100',
            'notes' => 'nullable|min:2|max:255',
            'account_code' => 'required',
            'another_line' => 'required',
            'description1' => 'required_if:another_line,1',
            'amount1' => 'required_if:another_line,1'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'description1.required_if' => 'Another line Description is required',
            'amount1.required_if' => 'Another line Amount is required'
        ];
    }
}