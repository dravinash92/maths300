<header id="masthead" class="site-header bg-custom-sticky sticky-header header_overlay header_v1 affix-top">
    <div class="thim-nav-wrapper container">
        <div class="row">
            <div class="navigation col-sm-12">
                <div class="tm-table">
                    <div class="width-logo table-cell sm-logo">
                        <a href="/" title="Maths300 - Maths300 Online maths lessons" rel="home" class="no-sticky-logo">
                            <img src="{{ asset('images/logo.png') }}" alt="Maths300">
                        </a>
                        <a href="" rel="home" class="sticky-logo">
                            <img src="{{ asset('images/logo.png') }}" alt="Maths300">
                        </a>
                    </div>

                    <nav class="width-navigation table-cell table-right">
                        <ul class="nav navbar-nav menu-main-menu">
                            <li id="menu-item-8066"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8066 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default">
                                <a href="/about" class="tc-menu-inner">About Us</a></li>
                            <li id="menu-item-8066"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8066 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default">
                                <a href="/subscribe" class="tc-menu-inner">Subscribe</a></li>
                            <li id="menu-item-8130"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8130 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
                                <a href="/forums" class="tc-menu-inner">Community</a></li>

                            <li id="menu-item-8129"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8129 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
                                <a href="/lessons/listall" class="tc-menu-inner">Lessons</a></li>

                            <li id="menu-item-8129"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8169 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
                                <a href="https://d19fxk53ry0j4n.cloudfront.net/login" target="_blank" class="tc-menu-inner">Software</a></li>

                            <li id="menu-item-8188" 
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8188 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
                                <a href="/faq" class="tc-menu-inner">FAQ</a></li>

                            <li id="menu-item-8056"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8056 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default">
                                <a href="/contact" class="tc-menu-inner">Contact</a></li>

                            @auth
                                <li id="menu-item-8056"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8056 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default">
                                    <a href="{{ Auth::user()->hasRole('super-admin')? '/admin': '/home' }}"
                                       class="tc-menu-inner">My Account</a>
                                </li>
                            @endauth

                            <li class="menu-right">
                                {{--<a href="/login">Login</a>--}}
                                <ul>
                                    <li id="login-popup-2" class="widget widget_login-popup">
                                        <div class="thim-widget-login-popup thim-widget-login-popup-base">
                                            <div class="thim-link-login thim-login-popup">
                                                @auth {{--
                                                <a href="{{ url('/admin') }}">Home</a>--}}
                                                <a class="logout" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>
                                                @else
                                                    <a class="login js-show-popup" href="{{ route('login') }}">Login
                                                    </a>
                                                    @if (Route::has('register'))
                                                        <a href="{{ route('register') }}"
                                                           class="register js-show-popup">Register</a>                                                @endif @endauth
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>
                                    </li>


                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <div class="menu-mobile-effect navbar-toggle" data-effect="mobile-effect"><span
                                class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Mobile Menu-->
<nav class="mobile-menu-container mobile-effect">
    <ul class="nav navbar-nav">
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8066 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default">
            <a href="/about" class="tc-menu-inner">About Us</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8131 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
            <a href="/subscribe" class="tc-menu-inner">Subscribe</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8130 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
            <a href="/forums" class="tc-menu-inner">Community</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8129 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
            <a href="/lessons" class="tc-menu-inner">Lessons</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8168 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
            <a href="https://lessons.maths300.com/" target="_blank" class="tc-menu-inner">Software</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8169 tc-menu-item tc-menu-depth-0 tc-menu-layout-default">
            <a href="/faq" target="_blank" class="tc-menu-inner">FAQ</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8056 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default">
            <a href="/contact" class="tc-menu-inner">Contact</a></li>

        @auth
            <li id="menu-item-95"
                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-95 tc-menu-item tc-menu-depth-0 tc-menu-align-left tc-menu-layout-default">
                <span class="tc-menu-inner">My Account</span>
                <span class="icon-toggle">
                    <i class="fa fa-angle-down"></i>
                </span>
                <ul class="sub-menu" style="display: none; opacity: 1;">
                    <li id="menu-item-6550"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6550 tc-menu-item tc-menu-depth-1 tc-menu-align-left">
                        <a href="{{ Auth::user()->hasRole('super-admin')? '/admin': '/home' }}"
                           class="tc-menu-inner tc-megamenu-title">Dashboard</a></li>

                    @if(Auth::user()->hasRole('school-admin'))
                        <li id="menu-item-6551"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6550 tc-menu-item tc-menu-depth-1 tc-menu-align-left">
                            <a href="{{ route('front.users.index') }}"
                               class="tc-menu-inner tc-megamenu-title">Teachers</a></li>
                    @endif

                    @if(Auth::user()->hasRole('school-admin') || Auth::user()->hasRole('school-finance-officer'))
                        <li id="menu-item-6552"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6550 tc-menu-item tc-menu-depth-1 tc-menu-align-left">
                            <a href="{{ route('front.users.invoices') }}"
                               class="tc-menu-inner tc-megamenu-title">My Invoices</a></li>
                    @endif

                    <li id="menu-item-6553"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4451 tc-menu-item tc-menu-depth-1 tc-menu-align-left">
                        <a href="{{ route('front.users.listfavlessons') }}"
                           class="tc-menu-inner tc-megamenu-title">Favorite Lessons</a></li>

                    <li id="menu-item-6554"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2924 tc-menu-item tc-menu-depth-1 tc-menu-align-left">
                        <a href="{{ route('front.users.edit', Auth::user()->id) }}"
                           class="tc-menu-inner tc-megamenu-title">Account Details</a></li>

                    <li id="menu-item-6555"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96 tc-menu-item tc-menu-depth-1 tc-menu-align-left">
                        <a href="{{ route('front.users.changePassReq') }}"
                           class="tc-menu-inner tc-megamenu-title">Change Password</a></li>
                </ul>
            </li>
        @endauth

    </ul>
</nav>
