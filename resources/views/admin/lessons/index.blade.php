@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.lessons.index') }}">Lessons</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Lessons</h3>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('admin.lessons.create') }}" class="btn btn-primary btn-flat">Add Lesson</a>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="lessonSearchForm" role="form" method="get" action="{{ route('admin.lessons.index') }}">
                {{ csrf_field() }}
                <div class="row ">
                    <div class="col-xs-3">
                        <select class="form-control select2-year_levels" style="width: 100%;" id="q_year_levels"
                                name="q_year_levels[]"
                                multiple="multiple"
                                data-placeholder="Select year levels">
                        </select>
                    </div>

                    <div class="col-xs-3">
                        <select data-placeholder="Select content strands" class="form-control select2-content_strands"
                                style="width: 100%;" id="q_content_strands" name="q_content_strands[]"
                                multiple="multiple">
                        </select>
                    </div>

                    <div class="col-xs-3">
                        <select class="form-control select2-pedagogies" style="width: 100%;" id="q_pedagogies"
                                name="q_pedagogies[]"
                                multiple="multiple"
                                data-placeholder="Select pedagogies">
                        </select>
                    </div>

                    <div class="col-xs-3">
                        <select class="form-control select2-curricula" style="width: 100%;" id="q_curricula"
                                name="q_curricula[]"
                                multiple="multiple"
                                data-placeholder="Select curricula">
                        </select>
                    </div>
                </div>

                <p/>

                <div class="row">
                    <div class="col-xs-3">
                        <select class="form-control select2-tags" style="width: 100%;" id="q_tags"
                                name="q_tags[]"
                                multiple="multiple"
                                data-placeholder="Select Lesson Pack">
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter lesson keywords (title/number/summary)..." value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="lessonSearchBtn" type="submit" class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>
                        </div>
                    </div>
                </div><!-- /.row -->
            </form>

            <table id="tab_lessons" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Upload date&nbsp;
                        <a href="{{ route('admin.lessons.index', ['sortBy'=>'upload_date', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_year_levels' => $q_year_levels, 'q_content_strands' => $q_content_strands, 'q_pedagogies' => $q_pedagogies , 'q_curricula' => $q_curricula ]) }}">
                        <i class="fa fa-sort"></i></a>
                    </th>
                    <th>Views&nbsp;
                        <a href="{{ route('admin.lessons.index', ['sortBy'=>'views', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_year_levels' => $q_year_levels, 'q_content_strands' => $q_content_strands, 'q_pedagogies' => $q_pedagogies , 'q_curricula' => $q_curricula ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Number&nbsp;
                        <a href="{{ route('admin.lessons.index', ['sortBy'=>'number', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_year_levels' => $q_year_levels, 'q_content_strands' => $q_content_strands, 'q_pedagogies' => $q_pedagogies , 'q_curricula' => $q_curricula ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Title&nbsp;
                        <a href="{{ route('admin.lessons.index', ['sortBy'=>'title', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_year_levels' => $q_year_levels, 'q_content_strands' => $q_content_strands, 'q_pedagogies' => $q_pedagogies , 'q_curricula' => $q_curricula ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Strands</th>
                    <th>Year</th>
                    <th>Status&nbsp;
                        <a href="{{ route('admin.lessons.index', ['sortBy'=>'status', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_year_levels' => $q_year_levels, 'q_content_strands' => $q_content_strands, 'q_pedagogies' => $q_pedagogies , 'q_curricula' => $q_curricula ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Menu</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($lessons as $l)
                    <tr class="item{{ $l->id }}">
                        <td class="col1">{{ $l->getUploadDate() }}</td>
                        <td>{{ $l->number_of_views }}</td>
                        <td>{{ $l->number }}</td>
                        <td><a href='{{ route('admin.lessons.show',$l->id) }}'>{{ $l->title }}</a></td>
                        <td>{{ implode(',', $l->contentStrands()->orderBy('content_strands.id')->pluck('name')->toArray()) }}</td>
                        <td>{{ implode(',', $l->yearLevels()->orderBy('year_levels.id')->pluck('name')->toArray()) }}</td>
                        <td>{{ $l->getStatus() }}</td>
                        <td>
                            <a href="{{ route('admin.lessons.edit',$l->id) }}" class="btn btn-primary">Edit</a>

                            @if( $l->isSuspendAllowed() )
                                <button class="suspend-modal btn btn-info" data-id="{{ $l->id }}"
                                        data-name="{{ $l->title }}">Suspend
                                </button>
                            @endif

                            @if( $l->isPublishAllowed() )
                                <button class="publish-modal btn btn-warning" data-id="{{ $l->id }}"
                                        data-name="{{ $l->title }}">Publish
                                </button>
                            @endif


                            <button class="delete-modal btn btn-danger" data-id="{{ $l->id }}"
                                    data-name="{{ $l->title }}">Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {{ $lessons->appends([
                    'sortBy' => $sortby,
                    'order' => $order
                ])->links() }}
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @modal(['action' => 'publish', 'model' => 'lesson', 'form_action' => '/admin/lessons/publish'])
    @endmodal

    @modal(['action' => 'suspend', 'model' => 'lesson', 'form_action' => '/admin/lessons/suspend'])
    @endmodal

    @modal(['action' => 'delete', 'model' => 'lesson'])
    @endmodal
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/date-uk.js"></script>
    <script>
        $(document).ready(function () {

            // Initialize Select2 elements
            let select2_year_levels = $('.select2-year_levels').select2({
                allowClear: true,
                data: {!! json_encode($year_levels)!!}
            });

            @if(old('q_year_levels'))
            select2_year_levels.val({!! json_encode( collect(old('q_year_levels')) ) !!}).trigger('change');
                    @endif

            let select2_content_strands = $('.select2-content_strands').select2({
                    allowClear: true,
                    data: {!! json_encode($content_strands)!!}
                });

            @if(old('q_content_strands'))
            select2_content_strands.val({!! json_encode( collect(old('q_content_strands')) ) !!}).trigger('change');
                    @endif

            let select2_pedagogies = $('.select2-pedagogies').select2({
                    allowClear: true,
                    data: {!! json_encode($pedagogies)!!}
                });

            @if(old('q_pedagogies'))
            select2_pedagogies.val({!! json_encode( collect(old('q_pedagogies')) ) !!}).trigger('change');
                    @endif
                    
            let select2_tags = $('.select2-tags').select2({
                    allowClear: true,
                    data: {!! json_encode($tags)!!}
                });

            @if(old('q_tags'))
                select2_tags.val({!! json_encode( collect(old('q_tags')) ) !!}).trigger('change');
            @endif

            let select2_curricula = $('.select2-curricula').select2({
                    allowClear: true,
                    data: {!! json_encode($curricula)!!}
                });

            @if(old('q_curricula'))
            select2_curricula.val({!! json_encode( collect(old('q_curricula')) ) !!}).trigger('change');
            @endif

            $('#tab_lessons').DataTable({
                "processing": true,
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search lessons......"
                },
                'ordering': false,
                'info': true,
                'columnDefs': [
                    {
                        "type": "date-uk",
                        "targets": 0
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 7
                    }],
                'autoWidth': false,
                'columns': [{
                    "width": "11%"
                }, {
                    "width": "5%"
                }, {
                    "width": "4%"
                }, {
                    "width": "24%"
                }, {
                    "width": "15%"
                }, {
                    "width": "6%"
                }, {
                    "width": "8%"
                }, {
                    "width": "27%"
                }
                ]
            });

            $(".dataTables_filter").hide();

            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>

    @modaljs(['action' => 'publish'])
    @endmodaljs

    @modaljs(['action' => 'suspend'])
    @endmodaljs

    @modaljs(['action' => 'delete', 'tab' => 'tab_lessons', 'submit_url' => 'admin/lessons'])
    @endmodaljs
@endpush
