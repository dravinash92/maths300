<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Models\Lesson;
use Auth;
use Validator;

class TagController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Tag::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['tags'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.tags.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TagRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $tag = new Tag();
        $tag->name = $validated['name'];

        $tag->save();

        $request->session()->flash('alert-success', 'Lesson Pack added successfully.');

        return redirect()->route('admin.tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag $Tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        $arr['tag'] = $tag;

        return view('admin.tags.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag $Tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $arr['tag'] = $tag;
        
        return view('admin.tags.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TagRequest $request
     * @param  \App\Models\Tag $Tag
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, Tag $tag)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $tag->name = $validated['name'];

        $tag->save();

        $request->session()->flash('alert-success', 'Lesson Pack updated successfully.');

        return redirect()->route('admin.tags.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        Log::info("delete Tag " . $id);
        
        $query = Lesson::query();
        $data = $query->Tags([$id])->get();
        //echo $tag;exit;
        if(count($data) > 0){
            return response()->json(['error' => 'This lesson pack can not be deleted'], 400);
        }else{
            $tag->delete();
            return response()->json($tag);
        }
    }
}
