<?php

namespace App\Http\Controllers\Front;

use App\Models\SubscriptionPage;
use App\Models\SubscriptionPlan;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Mail\SchoolSubscribed;
use App\Models\School;
use App\Models\Location;
use App\Models\SchoolType;
use App\Models\SchoolSector;
use App\Models\SchoolSizeStudent;
use App\Models\SchoolSizeTeacher;
use App\Models\YearLevel;
use App\Models\Tag;
use App\Models\User;
use App\Models\Setting;
use App\Http\Requests\SchoolRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Response;

class SubscriptionPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['subscriptionpage'] = SubscriptionPage::first();
        $arr['subscriptionplans'] = SubscriptionPlan::where('is_visible', 1)->get();
        return view('front.subscription.index')->with($arr);
    }

    public function termsAndConditions($hasAction)
    {
        $arr['hasAction'] = $hasAction;
        return view('front.subscription.termsAndConditions')->with($arr);
    }

    public function create()
    {
        $arr['locations'] = Country::data4Select();
        $arr['schoolTypes'] = data4Select(SchoolType::class);
        $arr['schoolSectors'] = data4Select(SchoolSector::class);
        $arr['schoolSizeStudents'] = data4Select(SchoolSizeStudent::class);
        $arr['schoolSizeTeachers'] = data4Select(SchoolSizeTeacher::class);
        $arr['subscriptionPlans'] = SubscriptionPlan::data4Select();
        $arr['year_levels'] = data4Select(YearLevel::class);
        $arr['tags'] = Tag::all('id', 'name');
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://countriesnow.space/api/v0.1/countries/iso');
        $countries = json_decode($response->getBody(), true);
        $arr['countries'] =$countries['data'];
        return view('front.subscription.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\SchoolRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(SchoolRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        if (User::isUserEmailExists($validated['finance_officer_email'])) {
            // allow using Input::old() function in view to access the data.
            $request->flash();
            return back()->withErrors('Input finance officer email exists in the system');
        }

        if (User::isUserEmailExists($validated['coordinator_email'])) {
            // allow using Input::old() function in view to access the data.
            $request->flash();
            return back()->withErrors('Input coordinator email exists in the system');
        }
        $country_find = Country::where('name','LIKE',$validated['country'])->first();
        $countryId = '';
        $locationId = '';
        if(empty($country_find)){
            $country = new Country();
            $country->name = $validated['country'];
            $country->save();
            $countryId = $country->id;
        }else{
            $countryId = $country_find->id;
        }
        $location_find = Location::where('name','LIKE',$validated['location'])->where('country_id',$countryId)->first();
        if(empty($location_find)){
            $location = new Location();
            $location->name = $validated['location'];
            $location->country_id = $countryId;
            $location->save();
            $locationId = $location->id;
        }else{
            $locationId = $location_find->id;
        }

        $is_single_account = $validated['schoolSizeTeacher'] == 1 ? 1 : 0;

        $school = new School();
        $school->name = $validated['name'];
        $school->address_region = $validated['address_region'];
        $school->address_suburb = $validated['address_suburb'];
        $school->address_postcode = $validated['address_postcode'];
        $school->number_of_campuses = $validated['number_of_campuses'];
        $school->finance_invoice_email = $validated['finance_invoice_email'];
        $school->billing_address_region = $validated['billing_address_region'];
        $school->billing_address_suburb = $validated['billing_address_suburb'];
        $school->billing_address_postcode = $validated['billing_address_postcode'];
        $school->purchase_order_no = $validated['purchase_order_no'];
        $school->address_location_id = $locationId;
        $school->tags = '';
        if(isset($validated['lesson_pack'])){
            if(in_array('all',$validated['lesson_pack']) && count($validated['lesson_pack']) > 1){
                $request->flash();
                return back()->withErrors('No lesson packs can be added with "Default - All Lessons" pack');
            }else{
                $school->tags = $validated['lesson_pack'][0]=='all' ? '' : implode(",", (array)$validated['lesson_pack']);
            }
        }
        // default 0:inactive
        $school->status = 0;
        $school->join_date = today();
        $school->is_single_account = $is_single_account;

        DB::transaction(function () use ($school, $validated) {
            $school->save();

            // save relationships
            $schoolType = SchoolType::find($validated['schoolType']);
            $school->schoolType()->associate($schoolType)->save();

            $schoolSector = SchoolSector::find($validated['schoolSector']);
            $school->schoolSector()->associate($schoolSector)->save();

            // if (!empty($validated['address_country']) && !empty($validated['address_state'])) {
            //     $address_location = Location::newLoc($validated['address_country'], $validated['address_state']);
            // } else {
            //     $address_location = Location::find($validated['address_location']);
            // }
            // $school->addressLocation()->associate($address_location)->save();

            $schoolSizeStudent = SchoolSizeStudent::find($validated['schoolSizeStudent']);
            $school->schoolSizeStudent()->associate($schoolSizeStudent)->save();

            $schoolSizeTeacher = SchoolSizeTeacher::find($validated['schoolSizeTeacher']);
            $school->schoolSizeTeacher()->associate($schoolSizeTeacher)->save();
            
            $select2_billing_address_country_find = Country::where('name','LIKE',$validated['billing_address_country'])->first();
            $select2_billing_address_countryId = '';
            $select2_billing_address_locationId = '';
            if(empty($select2_billing_address_country_find)){
                $select2_billing_address_country = new Country();
                $select2_billing_address_country->name = $validated['billing_address_country'];
                $select2_billing_address_country->save();
                $select2_billing_address_countryId = $select2_billing_address_country->id;
            }else{
                $select2_billing_address_countryId = $select2_billing_address_country_find->id;
            }
            $select2_billing_address_location_find = Location::where('name','LIKE',$validated['billing_address_location'])->where('country_id',$select2_billing_address_countryId)->first();
            if(empty($select2_billing_address_location_find)){
                $select2_billing_address_location = new Location();
                $select2_billing_address_location->name = $validated['billing_address_location'];
                $select2_billing_address_location->country_id = $select2_billing_address_countryId;
                $select2_billing_address_location->save();
                $select2_billing_address_locationId = $select2_billing_address_location->id;
            }else{
                $select2_billing_address_locationId = $select2_billing_address_location_find->id;
            }
        
            if (!empty($validated['billing_address_country']) && !empty($validated['billing_address_state'])) {
                $billing_address_location = Location::newLoc($validated['billing_address_country'], $validated['billing_address_state']);
            } else {
                $billing_address_location = Location::find($select2_billing_address_locationId);
            }
            $school->billingAddressLocation()->associate($billing_address_location)->save();

            $subscriptionPlan = SubscriptionPlan::find($validated['subscriptionPlan']);
            $school->subscriptionPlan()->associate($subscriptionPlan)->save();

            $data_finance_officer = array();
            $data_finance_officer["name"] = $validated['finance_officer_name'];
            $data_finance_officer["email"] = $validated['finance_officer_email'];
            $data_finance_officer["phone"] = $validated['finance_officer_phone'];

            if (!empty($validated['finance_officer_position'])) {
                $data_finance_officer["position"] = $validated['finance_officer_position'];
            }

            $data_coordinator = array();
            $data_coordinator["name"] = $validated['coordinator_name'];
            $data_coordinator["email"] = $validated['coordinator_email'];
            $data_coordinator["phone"] = $validated['coordinator_phone'];

            if (!empty($validated['coordinator_position'])) {
                $data_coordinator["position"] = $validated['coordinator_position'];
            }

            if (!empty($validated['coordinator_year_levels'])) {
                $data_coordinator["year_levels"] = $validated['coordinator_year_levels'];
            }

            $new_financeOfficer_email = $validated['finance_officer_email'];
            $new_coordinator_email = $validated['coordinator_email'];

            Log::info("create new users......");
            // create finance_officer/coordinator for this school (with status inactive)
            $finance_officer = User::createWithRoles($data_finance_officer, $school, 'school-finance-officer');
            $school->financeOfficer()->associate($finance_officer)->save();

            if ($new_financeOfficer_email == $new_coordinator_email) {
                $coordinator = $finance_officer;
                $coordinator->position = !empty($validated['coordinator_position']) ? $validated['coordinator_position'] : "";
                if (!empty($validated['coordinator_year_levels'])) {
                    $coordinator->yearLevels()->sync($validated['coordinator_year_levels']);
                }
                $coordinator->assignRole('school-finance-officer', 'school-admin');
            } else {
                $coordinator = User::createWithRoles($data_coordinator, $school, 'school-admin');
            }
            $school->coordinator()->associate($coordinator)->save();

            Log::info("create school id: " . $school->id . ", name: " . $school->name);
        });

        // send notification emails to admin
        Mail::to(Setting::get('Email'))->send(new SchoolSubscribed($school));

        return redirect()->route('front.subscription.notice');
    }

    public function notice()
    {
        return view('front.subscription.notice');
    }

    /**
     * build json response data for select2 component
     * @param $school_size_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function select($school_size_id)
    {
        Log::info("select subscription plan, school_size_id " . $school_size_id);
        $subscriptionplan_select = SubscriptionPlan::data4SelectWithSchoolTeacherSize($school_size_id);
        return response()->json($subscriptionplan_select);
    }
}
