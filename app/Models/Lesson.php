<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PHPHtmlParser\Dom;

class Lesson extends Model
{
    /**
     * Get excerpt from string
     *
     * @param String $str String to get an excerpt from
     * @param Integer $startPos Position int string to start excerpt from
     * @param Integer $maxLength Maximum length the excerpt may be
     * @return String excerpt
     */
    public static function getExcerpt($str, $startPos = 0, $maxLength = 100)
    {
        if (strlen($str) > $maxLength) {
            $excerpt = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt = substr($excerpt, 0, $lastSpace);
            $excerpt .= ' ...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public function getSummaryExcerpt()
    {
        return $this->getExcerpt($this->summary, 0, 150);
    }

    public static function getNextId()
    {
        $statement = DB::select("SHOW TABLE STATUS LIKE 'lessons'");
        $nextId = $statement[0]->Auto_increment;

        return $nextId;
    }

    public static function getTitleById($ID)
    {
        return Lesson::find($ID)->title;
    }

    public static function getNumById($ID)
    {
        return Lesson::find($ID)->number;
    }

    public function getUploadDate()
    {
        return $this->created_at->format('d/m/Y');
    }

    public function getCreateDateDiffForHumans()
    {
        return $this->created_at->diffForHumans();
    }

    public static function getFileURL($path)
    {
        return Storage::url($path);
    }

    public function deleteAllResources()
    {
        Log::alert("deleteAllResources");
        $resources = $this->resources()->getEager();
        foreach ($resources as $res) {
            Log::info("delete res_id " . $res->id);
            Resource::deleteRes($res->path);
            $res->delete();
        }
    }

    public function getStatus()
    {
        $status = "unknown";

        // init status: 0: draft 1:published  2:suspended
        if ($this->status == 0) {
            $status = "draft";
        }
        if ($this->status == 1) {
            $status = "published";
        }
        if ($this->status == 2) {
            $status = "suspended";
        }

        return $status;
    }

    public function isPublishAllowed()
    {
        return $this->status == 0 || $this->status == 2;
    }

    public function isSuspendAllowed()
    {
        return $this->status == 1;
    }

    /**
     * The year levels that belong to the lesson.
     */
    public function yearLevels()
    {
        return $this->belongsToMany(YearLevel::class);
    }
    
    /**
     * The lessons that belong to the user favourites.
     */
    public function userLessons()
    {
        return $this->belongsToMany(UserFavLesson::class, 'user_favorite_lesson');
    }
    
    /**
     * The tags that belong to the lesson.
     */
    public function Tags()
    {
        return $this->belongsToMany(Tag::class, 'lesson_tags');
    }

    /**
     * The content strands that belong to the lesson.
     */
    public function contentStrands()
    {
        return $this->belongsToMany(ContentStrand::class, 'lesson_content_strand');
    }

    /**
     * The pedagogies that belong to the lesson.
     */
    public function pedagogies()
    {
        return $this->belongsToMany(Pedagogy::class, 'lesson_pedagogy');
    }

    /**
     * The curricula that belong to the lesson.
     */
    public function curricula()
    {
        return $this->belongsToMany(Curriculum::class, 'lesson_curriculum');
    }

    /**
     * The resources that belong to the post.
     * parse resources from content
     */
    public function resources()
    {
        return $this->belongsToMany(Resource::class);
    }

    /**
     * create relationship between a resource and a lesson.
     *
     * @param \PHPHtmlParser\Dom $dom
     * @param $htmlTag
     */
    private function attachResourceByHtmlTag($dom, $htmlTag)
    {
        $resources_tags = $dom->getElementsbyTag($htmlTag);
        $resources_ids = array();
        foreach ($resources_tags as $content) {
            // get the class attr
            $res_id = $content->getAttribute('id');
            $resources_ids[] = $res_id;
        }

        $this->resources()->attach($resources_ids);
    }

    /**
     * create relationship between a resource and a lesson.
     *
     * @param $htmlContent given input html
     */
    private function attachResource($htmlContent)
    {
        $dom = new Dom;
        $dom->load($htmlContent);
        $this->attachResourceByHtmlTag($dom, 'img');
        $this->attachResourceByHtmlTag($dom, 'a');
    }

    /**
     * create relationships between resources and lesson.
     *
     */
    public function attachResources()
    {
        // step1: removing the relationship, use the dissociate method to reset the foreign key as well as the relation
        $previous_res = $this->resources()->getEager();
        $this->resources()->detach($previous_res);
        // step2: add new relationships
        // summary
        $this->attachResource($this->summary);
        // resources
        $this->attachResource($this->resources);
        // lesson notes
        $this->attachResource($this->lesson_notes);
        // extensions
        $this->attachResource($this->extensions);
        // practical tips
        $this->attachResource($this->practical_tips);
    }

    /**
     * Scope a query to only include given year levels.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $year_levels
     */
    public function scopeYearLevels($query, $year_levels)
    {
        $query->with('yearLevels')->whereHas('yearLevels', function ($query) use ($year_levels) {
            $query->whereIn('year_levels.id', $year_levels);
        });
    }
    
    /**
     * Scope a query to only include given tags.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $tags
     */
    public function scopeTags($query, $tags)
    {
        if ($tags) $query->with('tags')->whereHas('tags', function ($query) use ($tags) {
            $query->whereIn('tags.id', $tags);
        });
    }

    /**
     * Scope a query to only include given content strands.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $content_strands
     */
    public function scopeContentStrands($query, $content_strands)
    {
        $query->with('contentStrands')->whereHas('contentStrands', function ($query) use ($content_strands) {
            $query->whereIn('content_strands.id', $content_strands);
        });
    }

    /**
     * Scope a query to only include given pedagogies.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $pedagogies
     */
    public function scopePedagogies($query, $pedagogies)
    {
        if ($pedagogies) $query->with('pedagogies')->whereHas('pedagogies', function ($query) use ($pedagogies) {
            $query->whereIn('pedagogies.id', $pedagogies);
        });
    }

    /**
     * Scope a query to only include given curricula.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $curricula
     */
    public function scopeCurricula($query, $curricula)
    {
        if ($curricula) $query->with('curricula')->whereHas('curricula', function ($query) use ($curricula) {
            $query->whereIn('curricula.id', $curricula);
        });
    }

    /**
     * Scope a query to only include given keywords.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('title', 'like', '%' . $keywords . '%')
            ->orWhere('number', 'like', '%' . $keywords . '%')
            ->orWhere('summary', 'like', '%' . $keywords . '%');
    }

    /**
     * Scope a query to only include given status.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $status
     */
    public function scopeStatus($query, $status)
    {
        $query->where('status', $status);
    }

    /**
     * Scope a query to only include given data.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $is_front
     */
    public function scopeFront($query, $is_front)
    {
        $query->where('is_front', $is_front);
    }

    /**
     * Scope a query to only include given data.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $is_sample
     */
    public function scopeSample($query, $is_sample)
    {
        $query->where('is_sample', $is_sample);
    }

    /**
     * Scope a query to only include given id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $id
     */
    public function scopeId($query, $id)
    {
        $query->where('id', $id);
    }

    public function getYearLevels()
    {
        return $this->yearLevels()->orderBy('year_levels.id')->pluck('name')->toArray();
    }
    
    public function getTags()
    {
        return $this->Tags()->orderBy('tags.id')->pluck('name')->toArray();
    }

    public function getContentStrands()
    {
        return $this->contentStrands()->orderBy('content_strands.id')->pluck('name')->toArray();
    }

    public function getPedagogies()
    {
        return $this->pedagogies()->orderBy('pedagogies.id')->pluck('name')->toArray();
    }

    public function getCurricula()
    {
        return $this->curricula()->orderBy('curricula.id')->pluck('link', 'name')->toArray();
    }

    public function hasYear($year, $year_array)
    {
        return in_array($year, $year_array);
    }
    
    public function hasTag($tag, $tag_array)
    {
        return in_array($tag, $tag_array);
    }

    public function yearLevelItemsHtml()
    {
        $all_data = YearLevel::all();
        $year_array = $this->yearLevels()->allRelatedIds()->toArray();
        $results = array();
        foreach ($all_data as $d) {
            $css = '';
            if ($this->hasYear($d->id, $year_array)) {
                $css = 'highlight';
            }
            $tmp = '<li><span class="item-block ' . $css . '">' . $d->name . '</span></li>';
            $results[] = $tmp;
        }
        return $results;
    }
    

    public function hasStrand($strand, $strand_array)
    {
        return in_array($strand, $strand_array);
    }

    public function contentStrandsItemsHtml()
    {
        $all_data = ContentStrand::all();
        $strand_array = $this->contentStrands()->allRelatedIds()->toArray();
        $results = array();
        foreach ($all_data as $d) {
            $css = '';
            if ($this->hasStrand($d->id, $strand_array)) {
                $css = 'highlight';
            }
            $tmp = '<li style="margin-bottom:10px"><span class="item-block ' . $css . '">' . $d->name . '</span></li>';
            $results[] = $tmp;
        }
        return $results;
    }
    public function tagsItemsHtml()
    {
        $all_data = $this->Tags;
        $results = array();
        foreach ($all_data as $d) {
            $tmp = '<li style="margin-bottom:10px"><span class="item-block highlight">' . $d->name . '</span></li>';
            $results[] = $tmp;
        }
        if (empty($results)) {
            $tmp = '<li>No Data</li>';
            $results[] = $tmp;
        }
        return $results;
    }
    public function pedagogiesItemsHtml()
    {
        $all_data = $this->pedagogies;
        $results = array();
        foreach ($all_data as $d) {
            $tmp = '<li style="margin-bottom:10px"><span class="item-block highlight">' . $d->name . '</span></li>';
            $results[] = $tmp;
        }
        if (empty($results)) {
            $tmp = '<li>No Data</li>';
            $results[] = $tmp;
        }
        return $results;
    }

    public function curriculaItemsHtml()
    {
        $all_data = $this->curricula;
        $results = array();
        foreach ($all_data as $d) {
            $tmp = '<li style="margin-bottom:10px"><span class="item-curricula-block">' . '<a target="_blank" href="' . $d->link . '">' . $d->name . '</a></span></li>';
            $results[] = $tmp;
        }
        if (empty($results)) {
            $tmp = '<li>No Data</li>';
            $results[] = $tmp;
        }
        return $results;
    }

    /**
     * Get the coverImage that belongs to this Guide.
     */
    public function coverImage()
    {
        return $this->belongsTo(Resource::class);
    }

    public function parseVimeoVideoId($url)
    {
        $videoId = null;
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $parsed = parse_url($url);
            // check url is from vimeo
            if (!empty($parsed)) {
                $host = $parsed['host'];
                if (Str::contains($host, 'vimeo.com')) {
                    $videoId = substr($parsed['path'], 1);
                }
            }
        }

        return $videoId;
    }

    public static function isVimeoURL($url)
    {
        $isVimeo = false;
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $parsed = parse_url($url);
            // check url is from vimeo
            if (!empty($parsed)) {
                $host = $parsed['host'];
                if (Str::contains($host, 'vimeo.com')) {
                    $isVimeo = true;
                }
            }
        }

        return $isVimeo;
    }

    public function getPartialVideoEmbedHtml()
    {
        if (Lesson::isVimeoURL($this->partial_video)) {
            return $this->vimeoPartialVideoEmbedHtml();
        } else {
            return '<a href="' . $this->partial_video . '" target=\'_blank\'">' . $this->partial_video . '</a>';
        }
    }

    public function getFullVideoEmbedHtml()
    {
        if (Lesson::isVimeoURL($this->full_video)) {
            return $this->vimeoFullVideoEmbedHtml();
        } else {
            return '<a href="' . $this->full_video . '" target=\'_blank\'">' . $this->full_video . '</a>';
        }
    }

    public function vimeoPartialVideoEmbedHtml()
    {
        return $this->vimeoEmbedHtml($this->parseVimeoVideoId($this->partial_video));
    }

    public function vimeoFullVideoEmbedHtml()
    {
        return $this->vimeoEmbedHtml($this->parseVimeoVideoId($this->full_video));
    }

    public function vimeoEmbedHtml($videoId)
    {
        $html = '';
        if (!empty($videoId)) {
            $html = '<iframe src="https://player.vimeo.com/video/' . $videoId . '" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>'
                . '<p><a href="https://vimeo.com/' . $videoId . '" target=\'_blank\'">' . $this->title . '</a> from <a href="https://vimeo.com/aamtinc" target=\'_blank\'>AAMT Inc.</a>';
        }
        return $html;
    }

}
