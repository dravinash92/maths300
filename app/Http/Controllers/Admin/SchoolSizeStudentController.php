<?php

namespace App\Http\Controllers\Admin;

use App\Models\SchoolSizeStudent;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolSizeStudentRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class SchoolSizeStudentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SchoolSizeStudent::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['schoolsizestudents'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.schoolsizestudents.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schoolsizestudents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SchoolSizeStudentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolSizeStudentRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $schoolsizestudent = new SchoolSizeStudent();
        $schoolsizestudent->name = $validated['name'];

        $schoolsizestudent->save();

        $request->session()->flash('alert-success', 'School size student added successfully.');

        return redirect()->route('admin.schoolsizestudents.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SchoolSizeStudent $schoolsizestudent
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolSizeStudent $schoolsizestudent)
    {
        $arr['schoolsizestudent'] = $schoolsizestudent;

        return view('admin.schoolsizestudents.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SchoolSizeStudent $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolSizeStudent $schoolsizestudent)
    {
        $arr['schoolsizestudent'] = $schoolsizestudent;
        return view('admin.schoolsizestudents.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SchoolSizeStudentRequest $request
     * @param  \App\Models\SchoolSizeStudent $schoolsizestudent
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolSizeStudentRequest $request, SchoolSizeStudent $schoolsizestudent)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $schoolsizestudent->name = $validated['name'];

        $schoolsizestudent->save();

        $request->session()->flash('alert-success', 'School size student updated successfully.');

        return redirect()->route('admin.schoolsizestudents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schoolsizestudent = SchoolSizeStudent::findOrFail($id);

        if ($schoolsizestudent->subscriptionPlan) {
            return response()->json(['error' => 'Delete operation is not allowed as subscription plans associated with this item exist'],400);
        }
        
        $schoolsizestudent->delete();
        Log::info("delete schoolsizestudent " . $id);

        return response()->json($schoolsizestudent);
    }
}
