pwd
php artisan backup:run

composer dump-autoload -o

php artisan clear-compiled
php artisan optimize

php artisan config:clear
php artisan cache:clear
php artisan route:clear

php artisan config:cache
php artisan route:cache
