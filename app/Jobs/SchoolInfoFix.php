<?php

namespace App\Jobs;

use App\Models\School;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SchoolInfoFix implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('SchoolInfoFix job started......');
        $school_list = [[
            "name"=> "Stawell West Primary School",
            "renewal_date"=> "2021-09-15"
        ],[
            "name"=> "St Aloysius Catholic Primary School",
            "renewal_date"=> "2021-09-15"
        ],[
            "name"=> "Academy of Mary Immaculate",
            "renewal_date"=> "2021-09-15"
        ],[
            "name"=> "Notre Dame College - 3630",
            "renewal_date"=> "2021-09-15"
        ],[
            "name"=> "Brunswick North Primary School",
            "renewal_date"=> "2021-09-15"
        ],[
            "name"=> "Westbourne Grammar School",
            "renewal_date"=> "2021-09-15"
        ]
        ];
        foreach ($school_list as $key => $item) {
            Log::info('SchoolInfoFix handle ' . ($key + 1) . ' school name:'. $item["name"]. '  renewal_date:'. $item["renewal_date"]);
            $match = [
                'name'=> $item["name"],
            ];
            $school = School::where($match)->first();
            if ($school) {
                $school->activate_date = today();
                // active
                $school->status = 1;
                $last_paid_date = Carbon::parse($item["renewal_date"])->copy()->subYear(1);
                $school->last_paid_date = $last_paid_date;
                $school->last_renewal_date = $last_paid_date;
                $school->subscription_expiration_date = $item["renewal_date"];
                $school->save();

                Log::info('School info After change name: '. $school->name
                    .' last_paid_date: '. $school->last_paid_date
                    .' last_renewal_date: '. $school->last_paid_date
                    .' subscription_expiration_date: '. $school->subscription_expiration_date
                );
            } else {
                Log::warning('School  '. $school->name
                    .' not found!!'
                );
            }
        }
        Log::info('######################################################################################');
        Log::info('SchoolInfoFix job completed');
        Log::info('######################################################################################');
    }


}
