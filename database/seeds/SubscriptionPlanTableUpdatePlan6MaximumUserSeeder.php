<?php

use Illuminate\Database\Seeder;
use App\Models\SubscriptionPlan;

class SubscriptionPlanTableUpdatePlan6MaximumUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionPlan::where('id', '=', 6)->update(['maximum_user' => 200]);
    }
}
