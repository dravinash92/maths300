<?php

namespace App\Http\Controllers\Admin;

use App\Models\Curriculum;
use Illuminate\Http\Request;
use App\Http\Requests\CurriculumRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class CurriculumController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Curriculum::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['curricula'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.curricula.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.curricula.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CurriculumRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurriculumRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $curriculum = new Curriculum();
        $curriculum->name = $validated['name'];
        $curriculum->link = $validated['link'];

        $curriculum->save();

        $request->session()->flash('alert-success', 'Curriculum added successfully.');

        return redirect()->route('admin.curricula.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Curriculum $curriculum
     * @return \Illuminate\Http\Response
     */
    public function show(Curriculum $curriculum)
    {
        $arr['curriculum'] = $curriculum;

        return view('admin.curricula.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Curriculum $curriculum
     * @return \Illuminate\Http\Response
     */
    public function edit(Curriculum $curriculum)
    {
        $arr['curriculum'] = $curriculum;
        return view('admin.curricula.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CurriculumRequest $request
     * @param  \App\Models\Curriculum $curriculum
     * @return \Illuminate\Http\Response
     */
    public function update(CurriculumRequest $request, Curriculum $curriculum)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $curriculum->name = $validated['name'];
        $curriculum->link = $validated['link'];

        $curriculum->save();

        $request->session()->flash('alert-success', 'Curriculum updated successfully.');

        return redirect()->route('admin.curricula.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curriculum = Curriculum::findOrFail($id);
        Log::info("delete curriculum " . $id);
        
        $curriculum->delete();

        return response()->json($curriculum);
    }
}
