<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Curriculum;

class CurriculumTableSeeder extends Seeder {

    public function run()
    {
        DB::table('curricula')->delete();

        Curriculum::create([
            'name' => 'ACMNA075',
            'link' => 'https://www.australiancurriculum.edu.au/Search/?q=ACMNA075'
        ]);

        Curriculum::create([
            'name' => 'ACMNA076',
            'link' => 'https://www.australiancurriculum.edu.au/Search/?q=ACMNA076'
        ]);

        Curriculum::create([
            'name' => 'ACMNA077',
            'link' => 'https://www.australiancurriculum.edu.au/Search/?q=ACMNA077'
        ]);

        Curriculum::create([
            'name' => 'ACMNA078',
            'link' => 'https://www.australiancurriculum.edu.au/Search/?q=ACMNA078'
        ]);

        Curriculum::create([
            'name' => 'ACMNA079',
            'link' => 'https://www.australiancurriculum.edu.au/Search/?q=ACMNA079'
        ]);

        Curriculum::create([
            'name' => 'ACMNA080',
            'link' => 'https://www.australiancurriculum.edu.au/Search/?q=ACMNA080'
        ]);

    }

}