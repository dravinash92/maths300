@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="{{ route('admin.subscriptionplans.index') }}">Subscription Plans</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Subscription Plan Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <h4>School Size</h4>
            {{ $subscriptionplan->schoolSize->name }}
            <hr>

            <h4>Joining Fee</h4>
            {{ $subscriptionplan->joining_fee }}
            <hr>

            <h4>Annual Fee</h4>
            {{ $subscriptionplan->annual_fee }}
            <hr>

            <h4>Total Fee</h4>
            {{ $subscriptionplan->total_fee }}
            <hr>

            <h4>Visibility</h4>
            {{ $subscriptionplan->is_visible? "✅" : "❎" }}
            <hr>

            <h4>User limit</h4>
            {{ $subscriptionplan->maximum_user }}
            <hr>

            <h4>Remark</h4>
            {{ $subscriptionplan->remark? $subscriptionplan->remark : 'N/A' }}
            <hr>

            <h4>Updated at</h4>
            {{ $subscriptionplan->updated_at }}
            <hr>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.subscriptionplans.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
@endpush
