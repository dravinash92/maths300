<?php

namespace App\Mail;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceOverdueReminder extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @var invoice
     */
    public $invoice;

    public $recipient;

    public $overDueWeeks;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, $recipient, $overDueWeeks)
    {
        $this->invoice = $invoice;
        $this->recipient = $recipient;
        $this->overDueWeeks = $overDueWeeks;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.invoiceOverdueReminder')->subject('Maths300 invoice #'.$this->invoice->invoice_id.' is overdue by '.$this->overDueWeeks.' weeks');
    }
}
