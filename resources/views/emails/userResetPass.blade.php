@extends('layouts.email')

@section('content')
    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        Dear {{ $user->name }},
        <br><br>
        <p>You are receiving this email because we received a password reset request for your account.</p>
        <p>Please see your new login details below.</p>
        <p>User name: <strong>{{ $user->email }}</strong></p>
        <p>Password: <strong>{{ $tmp_pass }}</strong></p>
    </div>
    <div class="body-text"
         style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        <table class="buttonwrapper" bgcolor="#ea910b" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center; font-size: 16px; font-family: sans-serif; font-weight: bold; padding: 0px 30px 0px 30px; "
                    height="45">
                    <a href="{{ config('app.url') }}/login"
                       style="color: #ffffff; text-decoration: none;">Login</a><br/>
                </td>
            </tr>
        </table>
        <p>If you’re having trouble clicking the "Login" button, copy and paste the URL below into your web browser: {{ config('app.url') }}/login</p>
    </div>
@stop
