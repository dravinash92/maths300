<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    /**
     * Get the forum that belongs to this Topic.
     */
    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    /**
     * Get the forum that belongs to this Topic.
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The replies that belong to the Topic.
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function getRepliesCount()
    {
        return $this->replies()->count();
    }

    public function lastReply()
    {
        return $this->replies()->orderBy('updated_at', 'desc')->first();
    }

    public function getLastReplyDateDiffForHumans()
    {
        return $this->lastReply() ? $this->lastReply()->getUpdatedAtDateDiffForHumans() : "";
    }

    public function getLastReplier()
    {
        return $this->lastReply() ? $this->lastReply()->author->name : "";
    }

    public function getStatus()
    {
        $status = "unknown";

        // 0: closed 1: open
        if ($this->status == 0) {
            $status = "closed";
        }
        if ($this->status == 1) {
            $status = "open";
        }

        return $status;
    }

    public function isOpenAllowed()
    {
        return $this->status == 0;
    }

    public function isCloseAllowed()
    {
        return $this->status == 1;
    }

    /**
     * Scope a query to only include given keywords.
     *  query from numner/title/summary
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('title', 'like', '%' . $keywords . '%');
    }

    /**
     * Scope a query to only include given visibility.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $visibility
     */
    public function scopeVisibility($query, $visibility)
    {
        $query->where('visibility', $visibility);
    }

    /**
     * Scope a query to only include given forum.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $forum
     */
    public function scopeForum($query, $forum)
    {
        $query->with('forum')->whereHas('forum', function ($query) use ($forum) {
            $query->where('id', $forum);
        });
    }

    /**
     * build data for frontend [Select2 data format]
     *
     * ref: https://select2.org/data-sources/formats
     * @return array
     */
    public static function data4Select() {
        $all_data = Topic::all();
        $results = array();
        foreach ($all_data as $d) {
            $tmp['id'] = $d->id;
            $tmp['text'] = $d->title;
            $results[] = $tmp;
        }

        return $results;
    }

    public function getUpdatedAtDate()
    {
        return $this->updated_at ? Carbon::parse($this->updated_at)->format('d, F Y h:i A') : "N/A";
    }

    public function getCreatedAtDate()
    {
        return $this->created_at ? Carbon::parse($this->created_at)->format('d, F Y h:i A') : "N/A";
    }

    public function getOrg()
    {
        $user = $this->author;
        $org = "";
        if ($user->hasRole('super-admin')) {
            $org = "Maths300";
        }
        else if ($user->schools()->first()) {
            $org = $user->schools()->first()->name;
        }

        return $org;
    }

    public function adminAllowed()
    {
        $user = $this->author;
        return $user->hasRole('super-admin');
    }
}
