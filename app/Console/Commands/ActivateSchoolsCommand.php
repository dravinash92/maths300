<?php

namespace App\Console\Commands;

use App\Jobs\ActivateSchools;
use Illuminate\Console\Command;

class ActivateSchoolsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:activateSchools';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run school data activation program';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ActivateSchools::dispatch();
        $this->info("Run school data activation program successfully.");
    }
}
