<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('history_data');
        Schema::create('history_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('institution')->nullable();
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('suburb')->nullable();
            $table->string('postcode')->nullable();
            $table->string('administrator_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('teacher_first_name')->nullable();
            $table->string('teacher_last_name')->nullable();
            $table->string('teacher_email')->nullable();
            $table->string('invoicing_email')->nullable();
            $table->string('focus')->nullable();
            $table->string('sector')->nullable();
            $table->string('no_teachers')->nullable();
            $table->string('license_new')->nullable();
            $table->string('license')->nullable();
            $table->string('renewal_date')->nullable();
            $table->string('joining_date')->nullable();
            $table->string('password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_data');
    }
}
