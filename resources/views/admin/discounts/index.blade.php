@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
@endpush

@section('title', 'Maths300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.discounts.index') }}">Discounts</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Discounts</h3>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('admin.discounts.create') }}" class="btn btn-primary btn-flat">Add New</a>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="postSearchForm" role="form" method="get" action="{{ route('admin.discounts.index') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-3">
                        <select data-placeholder="Select a discountType" class="form-control select2_discountType"
                                style="width: 100%;" id="q_discountType" name="q_discountType">
                            <option></option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter keywords..." value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="postSearchBtn" type="submit" class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>
                        </div>
                    </div>
                </div><!-- /.row -->
            </form>

            <table id="tab_discounts" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Discount Type</th>
                    <th>Name</th>
                    <th>Menu</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($discounts as $l)
                    <tr class="item{{ $l->id }}">
                        <td></td>
                        <td>{{ $l->discountType->name }}</td>
                        <td class="col1"><a href='{{ route('admin.discounts.show',$l->id) }}'>{{ $l->name }}</a></td>
                        <td>
                            <a href="{{ route('admin.discounts.edit',$l->id) }}" class="btn btn-primary">Edit</a>
                            <button class="delete-modal btn btn-danger" data-id="{{ $l->id }}"
                                    data-name="{{ $l->name }}">Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @modal(['action' => 'delete', 'model' => 'discount'])
    @endmodal
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {

            let select2_discountType = $('.select2_discountType').select2({
                allowClear: true,
                data: {!! json_encode($discountTypes)!!}
            });

            @if(old('q_discountType'))
            select2_discountType.val({!! old('q_discountType') !!}).trigger('change');
            @endif

            let t = $('#tab_discounts').DataTable({
                "processing": true,
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search discounts......"
                },
                'ordering': true,
                'info': true,
                'columnDefs': [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 3
                    }
                    ],
                'autoWidth': false,
                'columns': [{
                    "width": "10%"
                    },{
                    "width": "30%"
                    },{
                    "width": "30%"
                    }, {
                    "width": "30%"
                  }]
            });

            $(".dataTables_filter").hide();

            t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>

    @modaljs(['action' => 'delete','tab' => 'tab_discounts', 'submit_url' => 'admin/discounts'])
    @endmodaljs
@endpush