@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }
    </style>

    <link rel='stylesheet' href='/wp-content/plugins/bbpress/templates/default/css/bbpress.css' type='text/css'
          media='all'/>
@stop

@extends('layouts.pageframe')

@section('top_heading')
    Topics
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/forums" title="Community"><span itemprop="name">Community</span></a>
    </li>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{ route('front.topics.index',['q_forum'=>$forum_id]) }}" title="Topics"><span itemprop="name">{{ $forum_name }}</span></a>
    </li>
@stop

@section('site-content')
    <div class="container site-content sidebar-right" style="transform: none;padding-top: 40px;">
        <div class="row" style="transform: none;">
            <main id="main" class="site-main col-sm-9 alignleft">
                <article id="post-3136" class="post-3136 forum type-forum status-publish hentry pmpro-has-access">
                    <div class="entry-content">
                        <div id="bbpress-forums">
                            <h5>{{ $forum_name }}</h5>
                            <ul id="bbp-forum-3136" class="bbp-topics">
                                <li class="bbp-header">
                                    <ul class="forum-titles">
                                        <li class="bbp-topic-title">Topic</li>
                                        <li class="bbp-topic-voice-count">Views</li>
                                        <li class="bbp-topic-reply-count">Replies</li>
                                        <li class="bbp-topic-freshness">Last Reply</li>
                                    </ul>
                                </li>

                                <li class="bbp-body">
                                    @if(count($topics))
                                        @foreach($topics as $t)
                                            <ul id="bbp-topic-4207"
                                                class="even bbp-parent-forum-3136 user-id-1 post-4207 topic type-topic status-closed hentry topic-tag-beauty pmpro-has-access">
                                                <li class="bbp-topic-title">
                                                    <a class="bbp-topic-permalink"
                                                       href="{{ route('front.replies.index',['q_topic'=>$t->id]) }}">
                                                        {{ $t->title }}
                                                    </a>
                                                    <p class="bbp-topic-meta">
                                                        <span class="bbp-topic-started-by">Started by: {{ $t->author->name }}</span>
                                                    </p>
                                                </li>

                                                <li class="bbp-topic-voice-count">{{ $t->views }}</li>

                                                <li class="bbp-topic-reply-count">{{ $t->getRepliesCount() }}</li>

                                                <li class="bbp-topic-freshness">
                                                    <a href="#"
                                                       title="test topic">
                                                        {{ $t->getLastReplyDateDiffForHumans() }}
                                                    </a>
                                                    <p class="bbp-topic-meta">
                                                    <span class="bbp-topic-freshness-author">
                                                        {{ $t->getLastReplier() }}
                                                    </span>
                                                    </p>
                                                </li>

                                            </ul><!-- #bbp-topic-4207 -->
                                        @endforeach
                                    @else
                                        <ul id="bbp-forum-3136"
                                            class="loop-item-1 even bbp-forum-status-open bbp-forum-visibility-publish post-3136 forum type-forum status-publish hentry pmpro-has-access">
                                            <li class="bbp-forum-info">
                                                No data available.
                                            </li>
                                        </ul>
                                    @endif
                                </li>

                                <li class="bbp-footer">

                                    <div class="tr">
                                        <p>
                                            <span class="td colspan4">&nbsp;</span>
                                        </p>
                                    </div><!-- .tr -->

                                </li>

                            </ul><!-- #bbp-forum-3136 -->

                            @if( !$forum_status )
                                <div class="bbp-template-notice">
                                    <p>This forum is marked as closed to new topics.</p>
                                </div>
                            @else
                                <form action="{{ route('front.topics.create') }}" method="get">
                                    @csrf
                                    <input type="hidden" name="forum_id" value={{ $forum_id }}>
                                    <button type="submit" tabindex="107"
                                            class="button submit" style="background: #78BE20;">New Topic
                                    </button>
                                </form>
                            @endif

                            <div class="bbp-pagination">
                                <div class="bbp-pagination-links">
                                    {{ $topics->appends(['q_forum' => $forum_id])->links() }}
                                </div>
                            </div>

                            <div id="no-topic-0" class="bbp-no-topic">
                                {{--<div class="bbp-template-notice">--}}

                                {{--</div>--}}
                            </div>

                        </div>
                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main>
            <div id="sidebar" class="widget-area col-sm-3 sticky-sidebar" role="complementary"
                 style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
            </div><!-- #secondary -->
        </div>
    </div>
@stop