@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }
        .sc_heading {
            padding-top: 0px;
        }

        @media (max-width: 767px) {
            #pg-w58292e9b9c43b-0.panel-grid {
                flex-direction: column;
            }

            #pg-w58292e9b9c43b-0 .panel-grid-cell {
                padding: 0;
                width: 100%;
                margin-right: 0;
                margin-bottom: 30px;
            }
        }

    </style>
@stop

@extends('layouts.page')

@section('top_heading')
    Contact
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/contact"><span itemprop="name">Contact</span></a>
    </li>
@stop

@section('content')
        <article id="post-87" class="post-87 page type-page status-publish hentry pmpro-has-access">
            <div class="entry-content">
                <div id="pl-87" class="panel-layout">
                    <div id="pg-87-0" class="panel-grid panel-no-style">
                        <div id="pgc-87-0-0" class="panel-grid-cell" style="width: 100%;">
                            <div id="panel-87-0-0-0" class="so-panel widget widget_heading panel-first-child"
                                 data-index="0">
                                <div class="panel-widget-style panel-widget-style-for-87-0-0-0">
                                    <div class="thim-widget-heading thim-widget-heading-base">
                                        <div class="sc_heading   text-left">
                                            <h3 class="title">Contact Info</h3>
                                            <p class="sub-heading" style="">Welcome to our Website. We are glad to have
                                                you around.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="panel-87-0-0-1" class="so-panel widget widget_siteorigin-panels-builder"
                                 data-index="1">
                                <div class="thim-border-top panel-widget-style panel-widget-style-for-87-0-0-1">
                                    <div id="pl-w58292e9b9c43b" class="panel-layout">
                                        <div id="pg-w58292e9b9c43b-0" class="panel-grid panel-no-style">
                                            <div id="pgc-w58292e9b9c43b-0-0" class="panel-grid-cell">
                                                <div id="panel-w58292e9b9c43b-0-0-0"
                                                     class="so-panel widget widget_icon-box panel-first-child panel-last-child"
                                                     data-index="0">
                                                    <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                        <div class="wrapper-box-icon text-left contact_info ">
                                                            <div class="smicon-box iconbox-left">
                                                                <div class="boxes-icon" style="width: 30px;"><span
                                                                            class="inner-icon"><span class="icon"><i
                                                                                    class="fa fa-phone"
                                                                                    style="color:#78BE20; font-size:24px; line-height:24px; vertical-align: middle;"></i></span></span>
                                                                </div>
                                                                <div class="content-inner"
                                                                     style="width: calc( 100% - 30px - 15px);">
                                                                    <div class="sc-heading article_heading"><h3
                                                                                class="heading__primary">Phone</h3>
                                                                    </div>
                                                                    <div class="desc-icon-box">
                                                                        <div class="desc-content">{{ $contact['phone'] }}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="pgc-w58292e9b9c43b-0-1" class="panel-grid-cell">
                                                <div id="panel-w58292e9b9c43b-0-1-0"
                                                     class="so-panel widget widget_icon-box panel-first-child panel-last-child"
                                                     data-index="1">
                                                    <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                        <div class="wrapper-box-icon text-left contact_info ">
                                                            <div class="smicon-box iconbox-left">
                                                                <div class="boxes-icon" style="width: 30px;"><span
                                                                            class="inner-icon"><span class="icon"><i
                                                                                    class="fa fa-envelope"
                                                                                    style="color:#78BE20; font-size:24px; line-height:24px; vertical-align: middle;"></i></span></span>
                                                                </div>
                                                                <div class="content-inner"
                                                                     style="width: calc( 100% - 30px - 15px);">
                                                                    <div class="sc-heading article_heading"><h3
                                                                                class="heading__primary">Email</h3>
                                                                    </div>
                                                                    <div class="desc-icon-box">
                                                                        <div class="desc-content"><a
                                                                                    href="mailto:{{ $contact['email'] }}">{{ $contact['email'] }}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="panel-87-0-0-2" class="so-panel widget widget_icon-box" data-index="2">
                                <div class="thim-border-top panel-widget-style panel-widget-style-for-87-0-0-2">
                                    <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                        <div class="wrapper-box-icon text-left contact_info ">
                                            <div class="smicon-box iconbox-left">
                                                <div class="boxes-icon" style="width: 30px;"><span
                                                            class="inner-icon"><span class="icon"><i
                                                                    class="fa fa-map-marker"
                                                                    style="color:#78BE20; font-size:24px; line-height:24px; vertical-align: middle;"></i></span></span>
                                                </div>
                                                <div class="content-inner" style="width: calc( 100% - 30px - 15px);">
                                                    <div class="sc-heading article_heading"><h3
                                                                class="heading__primary">Address</h3></div>
                                                    <div class="desc-icon-box">
                                                        <div class="desc-content">{{ $contact['address'] }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
@stop