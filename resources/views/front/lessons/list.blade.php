@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }

        .bbpress #bbpress-forums li.bbp-header {
            padding: 15px 15px;
        }

        .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-number {
            width: 15%;
            text-align: left;
        }

        .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-title {
            width: 25%;
            text-align: left;
        }

        .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-strands {
            width: 30%;
            text-align: left;
        }

        .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-yearlevels {
            width: 30%;
            text-align: left;
        }

        .bbpress #bbpress-forums li.bbp-body .forum li.lesson-number {
            width: 15%;
            text-align: left;
        }

        .bbpress #bbpress-forums li.bbp-body .forum li.lesson-title {
            width: 25%;
            text-align: left;
        }

        .bbpress #bbpress-forums li.bbp-body .forum li.lesson-strands {
            width: 30%;
            text-align: left;
        }

        .bbpress #bbpress-forums li.bbp-body .forum li.lesson-yearlevels {
            width: 30%;
            text-align: left;
        }

        .bbpress #bbpress-forums li.bbp-body ul a {
            color: #f4a938;
        }

        .bbpress #bbpress-forums .bbp-forum-info .bbp-forum-content {
            font-size: 13px;
            margin: 1px 8px 10px;
            text-align: left;
        }

        @media (max-width: 767px) {
            .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-number {
                width: 10%;
                text-align: center;
            }

            .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-title {
                width: 30%;
                text-align: center;
            }

            .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-strands {
                width: 30%;
                text-align: center;
            }

            .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-yearlevels {
                width: 30%;
                text-align: center;
            }
        }

        @media (max-width: 320px) {
            .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-number {
                width: 100%;
                text-align: center;
            }

            .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-title {
                width: 33%;
                text-align: center;
            }

            .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-strands {
                width: 33%;
                text-align: center;
            }

            .bbpress #bbpress-forums li.bbp-header .forum-titles li.lesson-yearlevels {
                width: 34%;
                text-align: center;
            }
        }

    </style>

    <link rel='stylesheet' href='/wp-content/plugins/bbpress/templates/default/css/bbpress.css' type='text/css'
          media='all'/>
@stop

@extends('layouts.pageframe')

@section('top_heading')
    Lessons
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/lessons/listall" title="Lessons"><span itemprop="name">Lessons</span></a>
    </li>
@stop

@section('site-content')
    <div class="container site-content sidebar-right" style="transform: none;">
        <div class="row" style="transform: none;">
            <main id="main" class="site-main col-sm-9 alignleft">
                <article id="post-0"
                         class="post-0 forum type-forum status-publish hentry pmpro-no-access page type-page">
                    <div class="entry-content">
                        <div id="bbpress-forums">
                            <ul id="forums-list-0" class="bbp-forums">
                                <li class="bbp-header">
                                    <ul class="forum-titles">
                                        <li class="bbp-forum-info lesson-number">Number&nbsp;
                                            @if($orderby==="number")
                                                <a href="/lessons/listall"><i class="fa fa-sort-numeric-desc"></i></a>
                                            @elseif($orderby==="a-z" || $orderby==="z-a")
                                                <a href="/lessons/listall"><i class="fa fa-sort"></i></a>
                                            @else
                                                <a href="/lessons/listall?orderby=number"><i
                                                        class="fa fa-sort-numeric-asc"></i></a>
                                            @endif
                                        </li>
                                        <li class="bbp-forum-topic-count lesson-title">Title&nbsp;&nbsp;
                                            @if($orderby==="z-a")
                                                <a href="/lessons/listall?orderby=a-z"><i
                                                        class="fa fa-sort-alpha-desc"></i></a>
                                            @elseif($orderby==="a-z")
                                                <a href="/lessons/listall?orderby=z-a"><i
                                                        class="fa fa-sort-alpha-asc"></i></a>
                                            @else
                                                <a href="/lessons/listall?orderby=a-z"><i class="fa fa-sort"></i></a>
                                            @endif
                                        </li>
                                        <li class="bbp-forum-reply-count lesson-strands">Strands</li>
                                        <li class="bbp-forum-freshness lesson-yearlevels">Year Levels</li>
                                    </ul>
                                </li><!-- .bbp-header -->

                                <li class="bbp-body">
                                    @if(count($lessons))
                                        @foreach($lessons as $lesson)
                                            <ul id="bbp-forum-3136"
                                                class="loop-item-1 even bbp-forum-status-open bbp-forum-visibility-publish post-3136 forum type-forum status-publish hentry pmpro-has-access">
                                                <li class="bbp-forum-info lesson-number">
                                                    {{ $lesson->number }}
                                                </li>

                                                <li class="bbp-forum-topic-count lesson-title">
                                                    <?php $salt="MathsLesson300";
                                                          $encrypted_id = base64_encode($lesson->id . $salt);  ?>
                                                    <a class="bbp-forum-title"
                                                       href='{{ $lesson->is_sample? route('front.lessons.sample', $lesson->id) : route('front.lessons.show', $encrypted_id) }}'
                                                       target="_blank" rel="bookmark">
                                                        {{ $lesson->title }}</a>
                                                </li>

                                                <li class="bbp-forum-info lesson-strands">
                                                    <div class="bbp-forum-content">
                                                        {{ implode(',', $lesson->contentStrands()->orderBy('content_strands.id')->pluck('name')->toArray()) }}
                                                    </div>
                                                </li>

                                                <li class="bbp-forum-info lesson-yearlevels">
                                                    <div class="bbp-forum-content">
                                                        {{ implode(',', $lesson->yearLevels()->orderBy('year_levels.id')->pluck('name')->toArray()) }}
                                                    </div>
                                                </li>
                                            </ul>
                                        @endforeach
                                    @else
                                        <ul id="bbp-forum-3136"
                                            class="loop-item-1 even bbp-forum-status-open bbp-forum-visibility-publish post-3136 forum type-forum status-publish hentry pmpro-has-access">
                                            <li class="bbp-forum-info">
                                                No data available.
                                            </li>
                                        </ul>
                                    @endif
                                </li><!-- .bbp-body -->
                                <li class="bbp-footer">
                                    <div class="tr">
                                        <p class="td colspan4">&nbsp;</p>
                                    </div><!-- .tr -->
                                </li><!-- .bbp-footer -->
                            </ul><!-- .forums-directory -->

                            <div class="bbp-pagination">
                                <div class="bbp-pagination-links">
                                    {{ $lessons->appends(['orderby' => $orderby])->links() }}
                                </div>
                            </div>
                        </div>
                    </div><!-- .entry-content -->
                </article><!-- #post-## -->
            </main>

            <div id="sidebar" class="widget-area col-sm-3 sticky-sidebar" role="complementary">
                <div class="theiaStickySidebar"
                     style="padding-top: 0px; padding-bottom: 1px; position: fixed; width: 270px; top: 110px; left: 1035px;">
                    <aside class="thim-course-filter-wrapper">

                        <form name='lessonSearchForm' id="lessonSearchForm" role="form" method="get"
                              action="{{ route('front.lessons.index') }}">
                            {{ csrf_field() }}

                            <h4 class="filter-title">Year Levels</h4>
                            <select class="select2-year_levels" style="width: 100%;"
                                    id="q_year_levels"
                                    name="q_year_levels[]"
                                    multiple="multiple"
                                    data-placeholder="Select year levels">
                            </select>

                            <h4 class="filter-title">Content Strands</h4>
                            <select data-placeholder="Select content strands"
                                    class="form-control select2-content_strands"
                                    style="width: 100%;" id="q_content_strands"
                                    name="q_content_strands[]"
                                    multiple="multiple">
                            </select>

                            <h4 class="filter-title">Pedagogies</h4>
                            <select class="form-control select2-pedagogies"
                                    style="width: 100%;" id="q_pedagogies"
                                    name="q_pedagogies[]"
                                    multiple="multiple"
                                    data-placeholder="Select pedagogies">
                            </select>

                            <h4 class="filter-title">Curricula</h4>
                            <select class="form-control select2-curricula"
                                    style="width: 100%;" id="q_curricula"
                                    name="q_curricula[]"
                                    multiple="multiple"
                                    data-placeholder="Select curricula">
                            </select>

                            <h4 class="filter-title">Key Words</h4>
                            <div class="courses-searching"
                                 style="margin-left: 0px; margin-top: 0px;">
                                <input type="text" value="{{ old('q') }}" name="q" id="q"
                                       placeholder="Enter search key words"
                                       class="form-control "
                                       style="width: 100%;"
                                       autocomplete="off"/>
                                <div class="filter-submit">
                                    <button type="submit" style="margin-top: 20px">Search</button>
                                </div>
                            </div>
                        </form>
                    </aside>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <article id="post-2778"
             class="post-2778 page type-page status-publish hentry pmpro-has-access">
        <div class="entry-content">
            <div id="lp-archive-courses">

            </div>
        </div><!-- .entry-content -->
    </article><!-- #post-## -->

    <article id="post-3696"
             class="col-sm-12 post-3696 post type-post status-publish format-standard has-post-thumbnail hentry category-business tag-wordpress pmpro-has-access">
        <div class="content-inner">
            @if(count($lessons))
                @foreach($lessons as $lesson)
                    <div class="entry-content lesson-content">
                        <header class="entry-header">
                            <h2 class="entry-title">
                                <a href='{{ route('front.lessons.show',$lesson->id) }}'
                                   rel="bookmark">{{ $lesson->title }}</a>
                            </h2>
                        </header>
                        <!-- .entry-header -->

                        <div class="thim-course-content">
                            <h4>Summary</h4>
                            <p class="summary">
                                {!! $lesson->summary !!}
                            </p>

                            <h4>Year Levels</h4>
                            <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                @foreach($lesson->yearLevelItemsHtml() as $html)
                                    {!! $html !!}
                                @endforeach
                            </ul>

                            <h4>Content Strands</h4>
                            <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                @foreach($lesson->contentStrandsItemsHtml() as $html)
                                    {!! $html !!}
                                @endforeach
                            </ul>

                            <h4>Pedagogies</h4>
                            <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                @foreach($lesson->pedagogiesItemsHtml() as $html)
                                    {!! $html !!}
                                @endforeach
                            </ul>

                            <h4>Curricula</h4>
                            <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                @foreach($lesson->curriculaItemsHtml() as $html)
                                    {!! $html !!}
                                @endforeach
                            </ul>
                        </div>

                        <div class="readmore">
                            <a href='{{ $lesson->is_sample? route('front.lessons.sample', $lesson->id) : route('front.lessons.show', $lesson->id) }}'
                               target="_blank">Read
                                More</a>
                        </div>
                    </div>
                    <br>
                @endforeach
            @else
                <p class="message message-error">No results found!</p>
            @endif
        </div>
    </article><!-- #post-## -->
@stop

<!-- .pagination -->
@section('pagination')
    {{ $lessons->links() }}
@stop

@section('sidebar')


@stop

@section('js')
    <script type='text/javascript'>
        /* <![CDATA[ */
        var lpQuizSettings = [];
        /* ]]> */

        jQuery(document).ready(function () {
            // Initialize Select2 elements
            let select2_year_levels = jQuery('.select2-year_levels').select2({
                allowClear: true,
                data: {!! json_encode($year_levels)!!}
            });

            @if(old('q_year_levels'))
            select2_year_levels.val({!! json_encode( collect(old('q_year_levels')) ) !!}).trigger('change');
            @endif

            let select2_content_strands = jQuery('.select2-content_strands').select2({
                allowClear: true,
                data: {!! json_encode($content_strands)!!}
            });

            @if(old('q_content_strands'))
            select2_content_strands.val({!! json_encode( collect(old('q_content_strands')) ) !!}).trigger('change');
            @endif

            let select2_pedagogies = jQuery('.select2-pedagogies').select2({
                allowClear: true,
                data: {!! json_encode($pedagogies)!!}
            });

            @if(old('q_pedagogies'))
            select2_pedagogies.val({!! json_encode( collect(old('q_pedagogies')) ) !!}).trigger('change');
            @endif

            let select2_curricula = jQuery('.select2-curricula').select2({
                allowClear: true,
                data: {!! json_encode($curricula)!!}
            });

            @if(old('q_curricula'))
            select2_curricula.val({!! json_encode( collect(old('q_curricula')) ) !!}).trigger('change');
            @endif

            jQuery('#lessonSearchForm').submit(function (event) {
                console.log('search submit......');
                jQuery('#thim-course-archive').addClass('loading');
                return true;
            });
        });
    </script>
@stop
