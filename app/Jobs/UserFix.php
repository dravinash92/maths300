<?php

namespace App\Jobs;

use App\Models\School;
use App\Models\HistoryData;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use App\Models\User;

class UserFix implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * The number of seconds the job can run before timing out.
     *
     * 60 minutes
     *
     * @var int
     */
    public $timeout = 3600;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = microtime(true);
        Log::info('UserFix job started......');
        $schools = DB::table('schools')->whereNull('finance_officer_id')->orWhereNull('coordinator_id')->orderBy('id')->get();
        Log::info('query count ' . count($schools));
        $total_count = 0;
        foreach ($schools as $key => $item) {
            $school = School::findOrFail($item->id);
            $emptyFinanceOfficer = empty($school->financeOfficer);
            $emptyCoordinator = empty($school->coordinator);
            Log::info("school id: " . $school->id. ", name: " . $school->name. ", emptyFinanceOfficer: " .$emptyFinanceOfficer. ", emptyCoordinator: " .$emptyCoordinator);
            if (!$emptyCoordinator) {
                Log::info("school coordinator name: " . $school->coordinator->name. ", email: " . $school->coordinator->email);
            }
            if (!$emptyFinanceOfficer) {
                Log::info("school financeOfficer name: " . $school->financeOfficer->name. ", email: " . $school->financeOfficer->email);
            }

            $data = HistoryData::where('institution', $school->name)->first();

            // emptyCoordinator/emptyFinanceOfficer
            if ($emptyCoordinator && $emptyFinanceOfficer) {
                Log::info('emptyCoordinator/emptyFinanceOfficer');
                if ($data) {
                    Log::info("HistoryData teacher_email: " . $data->teacher_email. ", invoicing_email: " . $data->invoicing_email. ", teacher_first_name: " .$data->teacher_first_name. ", teacher_last_name: " .$data->teacher_last_name);
                    // case 0: teacher_email is not empty && invoicing_email is not empty
                    if (!empty($data->teacher_email) && !empty($data->invoicing_email)) {
                        Log::info('case 0: teacher_email is not empty, invoicing_email is not empty');
                        // check exists first
                        if (!User::isUserEmailExists($data->teacher_email)) {
                            // create finance_officer/coordinator for this school (with status inactive)
                            $coordinator = new User();
                            $coordinator->name = $data->teacher_first_name . ' ' . $data->teacher_last_name;
                            $coordinator->email = $data->teacher_email;
                            $coordinator->phone = $data->phone;

                            $coordinator->password = Hash::make($data->password);
                            $coordinator->status = 1;
                            $coordinator->reset_token = Password::getRepository()->create($coordinator);
                            $coordinator->save();
                            // build relationship with school
                            $coordinator->schools()->sync([$school->id]);
                            $coordinator->assignRole('school-admin');
                            $school->coordinator()->associate($coordinator)->save();

                            // build a finance officer
                            // check if [invoicing_email] equals to [teacher_email]
                            if (strtolower($data->teacher_email) == strtolower($data->invoicing_email)) {
                                Log::info('set finance_officer as coordinator because they have the same email address.');
                                $finance_officer = $coordinator;
                                $finance_officer->assignRole('school-finance-officer', 'school-admin');
                            } else {
                                if (!User::isUserEmailExists($data->invoicing_email)) {
                                    Log::info('set finance_officer as a separate person.');
                                    $finance_officer = new User();
                                    $finance_officer->name = $school->name . ' Finance Officer';
                                    $finance_officer->email = $data->invoicing_email;

                                    $finance_officer->password = Hash::make($data->password);
                                    $finance_officer->status = 1;
                                    $finance_officer->reset_token = Password::getRepository()->create($finance_officer);
                                    $finance_officer->save();
                                    $finance_officer->assignRole('school-finance-officer');
                                } else {
                                    Log::error('data error invoicing_email exists [' . $data->invoicing_email . ']');
                                    Log::info('set coordinator as finance_officer.');
                                    $finance_officer = $coordinator;
                                    $finance_officer->assignRole('school-finance-officer', 'school-admin');
                                }
                            }
                            if (!empty($finance_officer)) {
                                // build relationship with school
                                $finance_officer->schools()->sync([$school->id]);
                                $school->financeOfficer()->associate($finance_officer)->save();
                                Log::info("set finance_officer [" . $finance_officer->name . '] successfully');
                            } else {
                                // rare case!!
                                Log::error('set finance_officer error, finance_officer is empty');
                            }
                        } else {
                            $existedUser = User::where('email', $data->teacher_email)->first();
                            if ($existedUser) {
                                Log::error('data error teacher_email [' . $data->teacher_email . '] exists in school '.$existedUser->getSchool()->name.', current school: '.$school->name);
                            }
                        }
                    } else {
                        // case 1: teacher_email is empty
                        // case 2: invoicing_email is empty
                        if ((empty($data->teacher_email) && !empty($data->invoicing_email)) ||
                            (!empty($data->teacher_email) && empty($data->invoicing_email))
                        ) {
                            $tmp_email = "";
                            if (!empty($data->invoicing_email)) {
                                $tmp_email = $data->invoicing_email;
                                Log::warning('case 1: teacher_email is empty, invoicing_email is not empty');
                            }
                            if (!empty($data->teacher_email)) {
                                $tmp_email = $data->teacher_email;
                                Log::warning('case 2: teacher_email is not empty, invoicing_email is empty');
                            }
                            $existedUser = User::where('email', $tmp_email)->first();
                            if ($existedUser) {
                                Log::error('data error email [' . $tmp_email . '] exists in school '.$existedUser->getSchool()->name.', current school: '.$school->name);
                            } else {
                                $u = new User();
                                $u->name = $data->teacher_first_name . ' ' . $data->teacher_last_name;
                                $u->email = $tmp_email;
                                $u->phone = $data->phone;

                                $u->password = Hash::make($data->password);
                                $u->status = 1;
                                $u->reset_token = Password::getRepository()->create($u);
                                $u->save();

                                // build relationship with school
                                $u->schools()->sync([$school->id]);
                                $u->assignRole('school-finance-officer', 'school-admin');
                                $school->coordinator()->associate($u)->save();
                                $school->financeOfficer()->associate($u)->save();
                            }
                        }
                        // case 3: teacher_email is empty && invoicing_email is empty
                        if (empty($data->teacher_email) && empty($data->invoicing_email)) {
                            Log::error('case 3: teacher_email is empty, invoicing_email is empty');
                        }
                    }
                } else {
                    Log::warning('School  '. $school->name
                        .' not found!!'
                    );
                }
            }
            if (!$emptyCoordinator && $emptyFinanceOfficer) {
                Log::info('!emptyCoordinator/emptyFinanceOfficer');
                $school->coordinator->assignRole('school-finance-officer', 'school-admin');
                $school->financeOfficer()->associate($school->coordinator)->save();
            }
            ++$total_count;
        }
        $end = microtime(true);
        Log::info('######################################################################################');
        Log::info('UserFix job completed, processed number of schools: ' . $total_count . ', elapsed time: ' . ($end - $start) . ' seconds');
        Log::info('######################################################################################');
    }
}
