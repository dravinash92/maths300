@extends('adminlte::page')

@push('css')
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.lessons.index') }}">Lessons</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Show lesson</h3>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('admin.lessons.preview', $lesson->id) }}" target="_blank" class="btn btn-primary btn-flat">Preview</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <h3>Title</h3>
            {{ $lesson->number }}  {{ $lesson->title }}
            <hr>

            <h3>Summary</h3>
            {!! $lesson->summary !!}
            <hr>

            <h3>Resources</h3>
            {!! $lesson->resources !!}
            <hr>

            <h3>Lesson Notes</h3>
            {!! $lesson->lesson_notes !!}
            <hr>

            <h3>Extensions</h3>
            {!! $lesson->extensions !!}
            <hr>

            <h3>Practical Tips</h3>
            {!! $lesson->practical_tips !!}
            <hr>

            <h3>Content strands</h3>
            {{ implode(',', $lesson->getContentStrands()) }}
            <hr>

            <h3>Year Levels</h3>
            {{ implode(',', $lesson->getYearLevels()) }}
            <hr>

            <h3>Pedagogies</h3>
            {{ implode(',', $lesson->getPedagogies()) }}
            <hr>

            <h3>Curricula</h3>
            @if(count($lesson->getCurricula()))
                <ul>
                    @foreach($lesson->getCurricula() as $k=>$v )
                        <li>
                            <a href='{{ $v }}'>{{  $k }}</a>
                        </li>
                    @endforeach
                </ul>
            @else
                N/A
            @endif
            <hr>

            <h3>Related Lessons</h3>
            @if($lesson->related_lessons)
                <ul>
                    @foreach(explode(",", $lesson->related_lessons) as $l)
                        <li>
                            <a href='{{ route('admin.lessons.show',$l) }}'>{{ $lesson->getNumById($l) }} {{ $lesson->getTitleById($l) }}</a>
                        </li>
                    @endforeach
                </ul>
            @else
                N/A
            @endif
            <hr>
            <h4>Lesson Packs</h4>
            @if($lesson->Tags()->allRelatedIds())
                <ul>
                    @foreach($tags as $t)
                        <li>
                            @foreach($lesson->Tags()->allRelatedIds() as $tt)
                                @if($t->id == $tt)
                                    {{ $t->name }}
                                @endif
                            @endforeach
                        </li>
                    @endforeach
                </ul>
            @else
                N/A
            @endif
            <hr>
            <h3>Software Required</h3>
            {{ $lesson->software_required ? 'Yes':'No' }}
            <hr>

            <h3>Software Link</h3>
            @if($lesson->software_link)
                <a href='{{ $lesson->software_link }}' target="_blank">{{ $lesson->software_link }}</a>
            @else
                N/A
            @endif
            <hr>

            <h3>Acknowledgements</h3>
            {{ $lesson->acknowledgements ? $lesson->acknowledgements: "N/A" }}
            <hr>

            <h3>Attributes</h3>
            {{ $lesson->attributes ? $lesson->attributes: "N/A" }}
            <hr>

            <h3>Partial Video</h3>
            @if($lesson->partial_video)
                {!! $lesson->getPartialVideoEmbedHtml()  !!}
            @else
                N/A
            @endif
            <hr>

            <h3>Full Video</h3>
            @if($lesson->full_video)
                {!! $lesson->getFullVideoEmbedHtml() !!}
            @else
                N/A
            @endif
            <hr>

            <h3>Notes</h3>
            {{ $lesson->notes ? $lesson->notes: "N/A" }}
            <hr>

            <h4>Set Front</h4>
            {{ $lesson->is_front ? 'Yes':'No' }}
            <hr>

            <h4>Set Sample</h4>
            {{ $lesson->is_sample ? 'Yes':'No' }}
            <hr>

            <h4>Cover Info</h4>
            @if($lesson->is_front)
                <img id={{ $lesson->coverImage->id }} src="{{ $lesson->coverImage->url }}" style="max-width: 320px;"/>
                <br>
                {{ $lesson->desc_front }}
            @else
                N/A
            @endif
            <hr>

            <h4>Status</h4>
            {{ $lesson->getStatus() }}
            <hr>

            <h4>Updated at</h4>
            {{ $lesson->updated_at }}
            <hr>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="{{ route('admin.lessons.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <!-- /.box -->
@stop

@push('js')
@endpush