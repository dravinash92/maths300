@extends('adminlte::page')

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.forums.index') }}">Forums</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add forum</h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- form start -->
            <form id="forumForm" role="form" method="post" action="{{ route('admin.forums.store') }}">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title"
                               value="{{ old('title') }}">
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                        <label for="description">Description</label>
                        <textarea class="form-control " id="description"
                                  name="description" rows="3">{{ old('description') }}</textarea>
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('sticky') ? 'has-error' : '' }}">
                        <label for="sticky">Sticky</label><br>
                        <div>
                            <input class="minimal" type="radio" id="sticky_yes"
                                   name="sticky"
                                   value="1" {{ old('sticky') == '1' ? 'checked' : '' }}>
                            <label for="sticky_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="sticky_no"
                                   name="sticky"
                                   value="0" {{ old('sticky', 0) == '0' ? 'checked' : '' }}>
                            <label for="sticky_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('sticky') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('visibility') ? 'has-error' : '' }}">
                        <label for="visibility">Visibility</label><br>
                        <div>
                            <input class="minimal" type="radio" id="visibility_yes"
                                   name="visibility"
                                   value="1" {{ old('visibility',1) == '1' ? 'checked' : '' }}>
                            <label for="visibility_yes">Yes</label>&nbsp;&nbsp;

                            <input class="minimal" type="radio" id="visibility_no"
                                   name="visibility"
                                   value="0" {{ old('visibility') == '0' ? 'checked' : '' }}>
                            <label for="visibility_no">No</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('visibility') }}</span>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop

@push('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            // Automatically trigger the loading animation on click
            Ladda.bind('button[type=submit]');
        });
    </script>
@endpush