<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        $admin = User::create([
            'name' => 'Maths300',
            'email' => 'maths300@aamt.edu.au',
            'email_verified_at' => now(),
            'password' => Hash::make('Maths300@Y1XVOxcts'),
            'status' => 1, // 1:enabled, 0:disabed
            'remember_token' => str_random(10),
        ]);
        $admin->assignRole('super-admin');

        $tester = User::create([
            'name' => 'Mr Tester',
            'email' => 'test@aamt.edu.au',
            'email_verified_at' => now(),
            'password' => Hash::make('Maths300@Lp0LSQpy8'),
            'status' => 1, // 1:enabled, 0:disabed
            'remember_token' => str_random(10),
        ]);
        $tester->assignRole('super-admin');

        $mskoss = User::create([
            'name' => 'Matt Skoss',
            'email' => 'mskoss@aamt.edu.au',
            'email_verified_at' => now(),
            'password' => Hash::make('Maths300@wBOk3rkHd'),
            'status' => 1, // 1:enabled, 0:disabed
            'remember_token' => str_random(10),
        ]);
        $mskoss->assignRole('super-admin');

        $drayner = User::create([
            'name' => 'Duncan Rayner',
            'email' => 'drayner@aamt.edu.au',
            'email_verified_at' => now(),
            'password' => Hash::make('Maths300@yEwXjvnc1'),
            'status' => 1, // 1:enabled, 0:disabed
            'remember_token' => str_random(10),
        ]);
        $drayner->assignRole('super-admin');
    }

}