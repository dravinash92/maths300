<?php

namespace App\Http\Controllers\Admin;

use App\Models\Discount;
use App\Models\DiscountType;
use App\Models\School;
use Illuminate\Http\Request;
use App\Http\Requests\DiscountRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;
use DB;

class DiscountController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['discountTypes'] = data4Select(DiscountType::class);
        $query = Discount::query();

        if ($request->has('q_discountType') && $request->q_discountType != null) {
            $query->discountType($request->q_discountType);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['discounts'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.discounts.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['discountTypes'] = data4Select(DiscountType::class);

        return view('admin.discounts.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\DiscountRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiscountRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $discount = new Discount();
        $discount->name = $validated['name'];
        $discount->value = $validated['value'];

        DB::transaction(function () use ($discount, $validated) {
            $discount->save();

            // save relationships
            // $discount
            $discountType = DiscountType::find($validated['discountType']);
            $discount->discountType()->associate($discountType)->save();

            Log::info("create discount " . $discount->id);
        });

        $request->session()->flash('alert-success', 'Discount added successfully.');

        return redirect()->route('admin.discounts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Discount $discount
     * @return \Illuminate\Http\Response
     */
    public function show(Discount $discount)
    {
        $arr['discount'] = $discount;

        return view('admin.discounts.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Discount $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount)
    {
        $arr['discount'] = $discount;
        $arr['discountTypes'] = data4Select(DiscountType::class);

        return view('admin.discounts.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\DiscountRequest $request
     * @param  \App\Models\Discount $discount
     * @return \Illuminate\Http\Response
     */
    public function update(DiscountRequest $request, Discount $discount)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $discount->name = $validated['name'];
        $discount->value = $validated['value'];

        DB::transaction(function () use ($discount, $validated) {
            $discount->save();

            // save relationships
            // $discount
            $discountType = DiscountType::find($validated['discountType']);
            $discount->discountType()->associate($discountType)->save();

            Log::info("update discount " . $discount->id);
        });

        $request->session()->flash('alert-success', 'Discount updated successfully.');

        return redirect()->route('admin.discounts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $discount = Discount::findOrFail($id);
        Log::info("delete discount " . $id);

        if ($discount->invoices()->count()) {
            return response()->json(['error' => 'Delete operation is not allowed as invoices associated with this item exist'],400);
        }

        $discount->delete();

        return response()->json($discount);
    }
}
