<?php 
use App\Models\Setting;
$footer_email = Setting::get('Email');
?>

<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
    <tr>
        <td align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff;">
            <br>

            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container"
                   style="width:600px;max-width:600px">
                <tr>
                    <td class="container-padding header" align="left"
                        style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#DF4726;padding-left:0px;padding-right:24px; background-color:#FFFFFF">
                        <a href="{{ config('app.url') }}" target="_blank">
                            <img src="{{ asset('images/logo-aamt.png') }}" style="margin: 15px; width: 120px; height: auto;" alt="{{ config('app.name') }}">
                        </a>
                    </td>
                </tr>

                <tr>
                    <td class="container-padding content" align="left"
                        style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">

                        @yield('content')

                        <div class="body-text"
                             style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                            <br>
                            Best Regards,<br>
                            AAMT
                        </div>

                    </td>
                </tr>

                <tr>
                    <td class="container-padding footer-text" align="left"
                        style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                        <br><br>
                        For all enquiries related to Maths300 and professional learning opportunities contact us at <a style="color:#666d74;text-decoration:none" href="mailto:{{$footer_email}}">{{ $footer_email }}</a> or {{ config('app.contact_phone') }}
                        <br><br>
                </tr>
            </table>
        </td>
    </tr>
</table>
