<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Reply extends Model
{
    /**
     * Get the topic that belongs to this Reply.
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    /**
     * Get the forum that belongs to this Topic.
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope a query to only include given topic.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $topic
     */
    public function scopeTopic($query, $topic)
    {
        $query->with('topic')->whereHas('topic', function ($query) use ($topic) {
            $query->where('id', $topic);
        });
    }

    /**
     * Scope a query to only include given keywords.
     *  query from title
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('title', 'like', '%' . $keywords . '%');
    }

    public function getUpdatedAtDate()
    {
        return $this->updated_at ? Carbon::parse($this->updated_at)->format('d/m/Y') : "N/A";
    }

    public function getUpdatedAtDateDiffForHumans()
    {
        return $this->updated_at ? Carbon::parse($this->updated_at)->diffForHumans() : "";
    }

    public function getCreatedAtDate()
    {
        return $this->created_at ? Carbon::parse($this->created_at)->format('d, F Y h:i A') : "N/A";
    }

    public function getOrg()
    {
        $user = $this->author;
        $org = "";
        if ($user->hasRole('super-admin')) {
            $org = "Maths300";
        }
        else if ($user->schools()->first()) {
            $org = $user->schools()->first()->name;
        }

        return $org;
    }

    public function adminAllowed()
    {
        return Auth::check() && Auth::user()->hasRole('super-admin');
    }
}
