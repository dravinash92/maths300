<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->double('joining_fee');
            $table->double('annual_fee');
            $table->double('total_fee');
            $table->string('remark')->nullable();

            // foreign keys
            $table->integer('school_size_id')->unsigned()->index()->nullable();
            $table->foreign('school_size_id')->references('id')->on('school_size_students');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_plans');
    }
}
