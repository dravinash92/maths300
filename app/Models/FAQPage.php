<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FAQPage extends Model
{
    protected $table = 'faqs';
}
