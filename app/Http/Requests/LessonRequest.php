<?php

namespace App\Http\Requests;

use App\Rules\SetFront;
use App\Rules\SetSample;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => [
                'bail',
                'required',
                 Rule::unique('lessons')->ignore($this->id),
                'numeric',
                'min:1',
                'max:999',
            ],
            'title' => 'bail|required|min:2|max:255',
            'summary' => 'required|min:2|max:50000',
            'resources' => 'required|min:2|max:50000',
            'lesson_notes' => 'required|min:2|max:50000',
            'extensions' => 'nullable|max:50000',
            'practical_tips' => 'nullable|max:50000',
            'related_lessons' => 'nullable',
            'acknowledgements' => 'nullable',
            'content_strands' => 'required',
            'year_levels' => 'required',
            'pedagogies' => 'nullable',
            'curricula' => 'nullable',
            'attributes' => 'nullable',
            'software_required' => 'required|in:0,1',
            'software_link' => 'required_if:software_required,1|url|max:5000',
            'partial_video' => 'nullable|url',
            'full_video' => 'nullable|url',
            'notes' => 'nullable',
            'lesson_pack' => 'nullable',

            'is_front' => ['required', new SetFront($this->id)],
            'is_sample' => ['required',new SetSample($this->id)],
            'cover_image_front' => 'required_if:is_front,1',
            'desc_front' => 'required_if:is_front,1|max:500',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'is_sample.unique'  => 'Only one sample lesson allowed in this system.',
        ];
    }
}