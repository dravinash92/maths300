<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Reply;
use Illuminate\Http\Request;
use App\Http\Requests\ReplyRequest;
use App\Models\Topic;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Auth;

class ReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Reply::query();

        if ($request->has('q_topic') && $request->q_topic != null) {
            Log::info("query replies of topic " . $request->q_topic);
            $arr['topic_id'] = $request->q_topic;
            $query->topic($request->q_topic);

            // check topic status
            $topic = Topic::findOrFail($request->q_topic);
            $arr['topic_status'] = $topic->status;
            $arr['topic'] = $topic;
            $arr['forum_id'] = $topic->forum->id;
            $topic->views += 1;
            $topic->save();
        } else {
            // access control:
            // list all replies are not allowed, redirect to forums index page automatically.
            return redirect(route('front.forums.index'));
        }

        $query->orderBy('id', 'asc');

        $arr['replies'] = $query->paginate(10);

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('front.replies.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ReplyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReplyRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $reply = new Reply();
        $reply->content = $validated['content'];
        $topic_id = $validated['topic'];

        DB::transaction(function () use ($topic_id, $reply, $validated) {
            // save relationships
            // topic
            $topic = Topic::find($topic_id);
            $reply->title = 'RE: '.$topic->title;
            $reply->save();

            $reply->topic()->associate($topic)->save();

            // author: current user
            $author = Auth::user();
            $reply->author()->associate($author)->save();

            Log::info("create reply from frontend " . $reply->id);
        });

        return redirect()->route('front.replies.index', array("q_topic" => $topic_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function edit(Reply $reply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reply $reply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function destroy(Reply $reply)
    {
        $topic_id = $reply->topic->id;

        Log::info("delete reply front end replyID:" . $reply->id . " TopicID:". $topic_id);

        $reply->delete();

        return redirect()->route('front.replies.index', array("q_topic" => $topic_id));
    }
}
