<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('API')->prefix('v1')->group(function () {
    Route::post('auth/login', 'UserController@login');
    Route::post('auth/validateAccessCode', 'LessonInteractController@validateAccessCode');
    Route::get('checkauth', 'LessonInteractController@checkAuth');
    Route::post('auth/getLesson', 'UserController@getLesson');
    Route::middleware('auth:api')->group(function () {
        Route::post('logout', 'UserController@logout');
    });
    

});