@extends('layouts.fronthome')

@section('sub_breadcrumbs')
    <a itemprop="item" href="{{ route('front.users.listfavlessons') }}" title="Favorite Lessons"><span itemprop="name">Favorite Lessons</span></a>
@endsection

@section('page-content')

    <div id="listfavlessons-content" class="myaccount-right">
        <table id="tab_teachers" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Number</th>
                <th>Title</th>
                <th>Menu</th>
            </tr>
            @csrf
            </thead>
            <tbody>
            @if(count($lessons))
                @foreach($lessons as $l)
                    <tr class="item{{ $l->id }}">
                        <td>{{ $l->number }}</td>
                        <?php $salt="MathsLesson300";
                                $encrypted_id = base64_encode($l->id . $salt);  ?>
                        <td><a class="bbp-forum-title"
                               href='{{ $l->is_sample? route('front.lessons.sample', $encrypted_id) : route('front.lessons.show', $encrypted_id) }}'
                               target="_blank" rel="bookmark">{{ $l->title }}</a></td>
                        <td>
                            <a href="{{ route('front.users.removefavlesson') }}"
                               onclick="event.preventDefault();document.getElementById('remove-form{{ $l->id }}').submit();"
                               class="btn btn-primary">Remove</a>
                        </td>
                        <form id="remove-form{{ $l->id }}" action="{{ route('front.users.removefavlesson') }}"
                              method="POST" style="display: none;">
                            @csrf
                            <input type="hidden" name="lesson_id" value="{{ $l->id }}">
                        </form>
                    </tr>
                @endforeach
            @else
                <tr class="no-item">
                    <td colspan="3">No data available.</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@stop

@push('js')
    <script>
        jQuery(document).ready(function () {

        });
    </script>

@endpush