@extends('adminlte::page')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/select2/dist/css/select2.min.css')}}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset ('vendor/adminlte/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        /* Invoice Status Colors */

        .awaiting-payment {
            color: #ffcc00;
        }
        .paid {
            color: #00a65a;
        }
        .overdue {
            color: #cc0000;
        }

    </style>
@endpush

@section('title', 'Maths 300')

@section('content_header')
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.schools.index') }}">Schools</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title">Schools</h3>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('admin.schools.create') }}" class="btn btn-primary btn-flat">Add School</a>
            </div>

        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <form id="schoolSearchForm" role="form" method="get" action="{{ route('admin.schools.index') }}">
                @csrf
                <div class="row ">
                    <div class="col-xs-3">
                        <select data-placeholder="Select location" class="form-control select2_address_location"
                                style="width: 100%;" id="q_address_location" name="q_address_location">
                            <option></option>
                        </select>
                    </div>

                    <div class="col-xs-3">
                        <select data-placeholder="Select a school type" class="form-control select2_schoolType"
                                style="width: 100%;" id="q_schoolType" name="q_schoolType">
                            <option></option>
                        </select>
                    </div>

                    <div class="col-xs-3">
                        <select data-placeholder="Select a school sector" class="form-control select2_schoolSector"
                                style="width: 100%;" id="q_schoolSector" name="q_schoolSector">
                            <option></option>
                        </select>
                    </div>
                </div>

                <p/>

                <div class="row">
                    <div class="col-xs-3">
                        <input type="text" class="form-control" id="q_invoice" name="q_invoice"
                               placeholder="Enter invoice number..."
                               value="{{ old('q_invoice') }}">
                    </div>
                    <div class="col-xs-3">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="q_subscription_expiry_date_range" name="q_subscription_expiry_date_range" value="{{ old('q_subscription_expiry_date_range') }}" placeholder="Enter subscription expiry date range">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="input-group">
                            <input type="text" class="form-control" id="q" name="q"
                                   placeholder="Enter school keywords (postcode/address etc.)..."
                                   value="{{ old('q') }}">
                            <span class="input-group-btn">
                                <button id="schoolSearchBtn" type="submit" class="btn btn-primary btn-flat ladda-button"
                                        data-style="expand-right"><span class="ladda-label">Search</span></button>
                            </span>

                        </div>

                    </div>

                </div><!-- /.row -->
            </form>
            <div class="col-md-12 text-right">
                <a class="archive btn btn-primary btn-flat" href="{{ route('admin.schools.showArchived') }}">Archived</a>
            </div>

            <table id="tab_schools" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Join date
                        <a href="{{ route('admin.schools.index', ['sortBy'=>'join_date', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_address_location' => $q_address_location, 'q_schoolType' => $q_schoolType, 'q_schoolSector' => $q_schoolSector, 'q_invoid' => $q_invoice, 'q_subscription_expiry_date_range' => $q_subscription_expiry_date_range ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Expiry date&nbsp;
                        <a href="{{ route('admin.schools.index', ['sortBy'=>'expiry_date', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_address_location' => $q_address_location, 'q_schoolType' => $q_schoolType, 'q_schoolSector' => $q_schoolSector, 'q_invoid' => $q_invoice, 'q_subscription_expiry_date_range' => $q_subscription_expiry_date_range ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Status&nbsp;
                        <a href="{{ route('admin.schools.index', ['sortBy'=>'status', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_address_location' => $q_address_location, 'q_schoolType' => $q_schoolType, 'q_schoolSector' => $q_schoolSector, 'q_invoid' => $q_invoice, 'q_subscription_expiry_date_range' => $q_subscription_expiry_date_range ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Payment</th>
                    <th>Name&nbsp;
                        <a href="{{ route('admin.schools.index', ['sortBy'=>'name', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_address_location' => $q_address_location, 'q_schoolType' => $q_schoolType, 'q_schoolSector' => $q_schoolSector, 'q_invoid' => $q_invoice, 'q_subscription_expiry_date_range' => $q_subscription_expiry_date_range ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Location&nbsp;
                        <a href="{{ route('admin.schools.index', ['sortBy'=>'location', 'order' => $order=='desc' ? 'asc' : 'desc', 'q'=> $q, 'q_address_location' => $q_address_location, 'q_schoolType' => $q_schoolType, 'q_schoolSector' => $q_schoolSector, 'q_invoid' => $q_invoice, 'q_subscription_expiry_date_range' => $q_subscription_expiry_date_range ]) }}">
                        <i class="fa fa-sort"></i></a></th>
                    <th>Menu</th>
                </tr>
                {{ csrf_field() }}
                </thead>
                <tbody>
                @foreach($schools as $l)
                    <tr class="item{{ $l->id }}">
                        <td class="col1">{{ $l->joinDateFormat() }}</td>
                        <td class="col1">{{ $l->subscriptionExpirationDateFormat() }}</td>
                        <td>{{ $l->getStatus() }}</td>
                        <td class="{{ $l->getPaymentStatus() }}">{{ $l->getPaymentStatus() }}</td>
                        <td><a href='{{ route('admin.schools.show',$l->id) }}'>{{ $l->name }}</a></td>
                        <td>{{ $l->addressLocation? $l->addressLocation->name : '' }}</td>

                        <td>
                            <a href="{{ route('admin.schools.edit',$l->id) }}" class="btn btn-primary">Edit</a>

                            @if( $l->isActivateAllowed() )
                            <button class="activate-modal btn btn-primary" data-id="{{ $l->id }}"
                                    data-name="{{ $l->name }}">Activate
                            </button>
                            @endif

                            @if( $l->isSuspendAllowed() )
                            <button class="suspend-modal btn btn-primary" data-id="{{ $l->id }}"
                                    data-name="{{ $l->name }}">Suspend
                            </button>
                            @endif

                            @if( $l->isArchiveAllowed() )
                            <button class="archive-modal btn btn-primary" data-id="{{ $l->id }}"
                                    data-name="{{ $l->name }}">Archive
                            </button>
                            @endif

                            <button class="set-expiry-date-modal btn btn-primary" data-id="{{ $l->id }}"
                                    data-name="{{ $l->name }}">Set Expiry Date
                            </button>

                            <a class="invoice-modal btn btn-primary" href='{{ 'schools/viewInvoice/'.$l->id }}'>Invoice</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {{ $schools->appends([
                    'sortBy' => $sortby,
                    'order' => $order
                ])->links() }}
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @modal(['action' => 'activate', 'model' => 'school', 'form_action' => '/admin/schools/activate'])
    @endmodal

    @modal(['action' => 'suspend', 'model' => 'school', 'form_action' => '/admin/schools/suspend'])
    @endmodal

    @modal(['action' => 'archive', 'model' => 'school', 'form_action' => '/admin/schools/archive'])
    @endmodal

    <!-- Modal form to set subscription expiry date -->
    <div id="setExpiryDateModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Are you sure you want to set subscription expiry date?</h3>
                    <br/>
                    <!-- form start -->
                    <form class="form-horizontal" role="form" id="form_set_expiry_date" method="post"
                          action="{{ route('admin.schools.setExpiryDate') }}">
                        @csrf
                        <input type="hidden" class="form-control" id="id_school" name="id_school">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="name">School:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name_school" name="name_school"
                                       readonly="readonly">
                            </div>
                        </div>

                        <div class="form-group" id="expiry_date_school_form_group">
                            <label class="control-label col-sm-3" for="email">Expiry date:</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="expiry_date_school" name="expiry_date_school" placeholder="Enter subscription expiry date">
                                </div>
                                <span class="text-danger" id="expiry_date_school_error"></span>
                            </div>

                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger set-expiry-date-submit" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-alert'></span> Submit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    <!-- DataTables -->
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- date-range-picker -->
    <script src="{{ asset ('vendor/adminlte/vendor/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset ('vendor/adminlte/vendor/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset ('vendor/adminlte/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/date-uk.js"></script>
    <script>
        $(document).ready(function () {

            // Initialize Select2 elements
            let select2_address_location = $('.select2_address_location').select2({
                allowClear: true,
                data: {!! json_encode($locations)!!}
            });

            @if(old('q_address_location'))
            select2_address_location.val({!! old('q_address_location') !!}).trigger('change');
                    @endif

            let select2_schoolType = $('.select2_schoolType').select2({
                    allowClear: true,
                    data: {!! json_encode($schoolTypes)!!}
                });

            @if(old('q_schoolType'))
            select2_schoolType.val({!! old('q_schoolType') !!}).trigger('change');
                    @endif

            let select2_schoolSector = $('.select2_schoolSector').select2({
                    allowClear: true,
                    data: {!! json_encode($schoolSectors)!!}
                });

            @if(old('q_schoolSector'))
            select2_schoolSector.val({!! old('q_schoolSector') !!}).trigger('change');
            @endif

            //Date range picker
            $('#q_subscription_expiry_date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD/MM/YYYY'
                }
            });

            $('#q_subscription_expiry_date_range').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });

            $('#q_subscription_expiry_date_range').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });


            $('#tab_schools').DataTable({
                "processing": true,
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'language': {
                    'searchPlaceholder': "Search schools......"
                },
                'ordering': false,
                'info': true,
                'columnDefs': [
                    {
                        "type": "date-uk",
                        "targets": [0,1]
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 6
                    }],
                'autoWidth': false,
                'columns': [{
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "10%"
                }, {
                    "width": "25%"
                }, {
                    "width": "10%"
                }, {
                    "width": "25%"
                }
                ]
            });

            $(".dataTables_filter").hide();
        });

        // Automatically trigger the loading animation on click
        Ladda.bind('button[type=submit]');

        // set expiry date
        $(document).on('click', '.set-expiry-date-modal', function () {
            $('.modal-title').text('Set subscription expiry date');
            $('#id_school').val($(this).data('id'));
            $('#name_school').val($(this).data('name'));
            $('#setExpiryDateModal').modal('show');

            //Date picker
            $('#expiry_date_school').val('');
            $('#expiry_date_school').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $('#expiry_date_school_form_group').removeClass('has-error');
            $('#expiry_date_school_error').text('');
        });
        $('.modal-footer').on('click', '.set-expiry-date-submit', function (evt) {
            evt.preventDefault();
            // Get the expiry_date value and trim it
            const expiry_date = $.trim($('#expiry_date_school').val());
            // Check if empty of not
            if (expiry_date  === '') {
                $('#expiry_date_school_form_group').addClass('has-error');
                $('#expiry_date_school_error').text('Input expiry date required');
                return false;
            } else {
                $('#form_set_expiry_date').submit();
            }
        });

    </script>

    @modaljs(['action' => 'activate'])
    @endmodaljs

    @modaljs(['action' => 'suspend'])
    @endmodaljs

    @modaljs(['action' => 'archive'])
    @endmodaljs
@endpush
