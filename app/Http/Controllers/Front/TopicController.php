<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\TopicRequest;
use App\Models\Forum;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Auth;

class TopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Topic::query();

        if ($request->has('q_forum') && $request->q_forum != null) {
            Log::info("query topics of forum " . $request->q_forum);
            $arr['forum_id'] = $request->q_forum;
            $query->forum($request->q_forum);

            // check forum status
            $forum = Forum::findOrFail($request->q_forum);
            $arr['forum_status'] = $forum->status;
            $arr['forum_name'] = $forum->title;
        } else {
            // access control:
            // list all topics are not allowed, redirect to forum index page automatically.
            return redirect(route('front.forums.index'));
        }

        // show topics with visibility of public
        $query->visibility(1);
        // show topics with sticky 1 first
        $query->orderBy('sticky', 'desc');
        $query->orderBy('id', 'asc');

        $arr['topics'] = $query->paginate(10);

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('front.topics.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $forum_id = $request->forum_id;

        $forum = Forum::findOrFail($forum_id);
        if (!$forum->status) {
            return back()->withErrors('Creating topic is not allowed as this forum is closed');
        }

        $arr['forum_id'] = $forum_id;

        return view('front.topics.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TopicRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TopicRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $topic = new Topic();
        $topic->title = $validated['title'];
        $topic->content = $validated['content'];
        $topic->sticky = 1;
        $topic->visibility = 1;
        $forum_id = $validated['forum'];

        DB::transaction(function () use ($forum_id, $topic, $validated) {
            $topic->save();

            // save relationships
            // forum
            $forum = Forum::find($forum_id);
            $topic->forum()->associate($forum)->save();

            // author: current user
            $author = Auth::user();
            $topic->author()->associate($author)->save();

            Log::info("create topic from front-end " . $topic->id);
        });

        return redirect()->route('front.topics.index', array("q_forum" => $forum_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $topic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        //
    }
}
