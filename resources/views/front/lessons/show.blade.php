@section('css')
    <style>

        #learn-press-course-curriculum ul.curriculum-sections .section-header {
            margin: 15px 0 0px;
        }

        #learn-press-course-curriculum ul.curriculum-sections .section {
            border-bottom: 1px solid #eee;
        }

        #learn-press-course-curcurriculum-scrollablericulum ul.curriculum-sections .section .section:last-child {
            border-bottom: 0px solid #eee;
        }

        #learn-press-course-curriculum ul.curriculum-sections .section-content .course-item-lp_lesson, #learn-press-course-curriculum ul.curriculum-sections .section-content .course-lesson {
            border-top: 0px;
            overflow: hidden;
        }

        .learn-press .entry-title {
            font-size: 30px;
            font-weight: 700;
            margin: 0px 0 0px;
            line-height: 40px;
            text-transform: none;
        }

        p {
            margin: 0 0 0px;
        }

        .pmpro-has-access .entry-content ul {
            list-style: disc;
        }

        #learn-press-course-curriculum p {
            overflow: hidden;
        }

        #learn-press-course-curriculum ul.curriculum-sections .section-header {
            text-transform: none;
        }

        .lesson-favoritelist-box {
            line-height: 30px;
            padding: 5px 0;
            text-align: center;
            background-color: #78BE20;
            width: 15%;
            color: #fff;
        }

        .lesson-favoritelist-box span.fa {
            cursor: pointer;
        }

        .lesson-favoritelist-box span {
            line-height: 30px;
            display: inline-block;
            color: #fff;
        }

        .lesson-favoritelist-box span.fa.lesson-favoritelisted {
            color: #ffb606;
        }

        /** getaccesscode **/
        .lesson-getaccesscode-box {
            line-height: 30px;
            padding: 5px 0;
            text-align: center;
            background-color: #78BE20;
            width: 15%;
            color: #fff;
        }

        .lesson-getaccesscode-box span.fa {
            cursor: pointer;
        }

        .lesson-getaccesscode-box span {
            line-height: 30px;
            display: inline-block;
            color: #fff;
        }

        .lesson-getaccesscode-box span.fa.accesscodegeted {
            color: #ffb606;
        }

        div.figure {
            float: left;
            /*width: 30%;*/
            overflow: hidden;
            border: thin silver solid;
            margin: 0.5em;
            padding: 0.5em;
            text-align: center;
            font-style: italic;
            font-size: smaller;
            text-indent: 0;
        }

        .item-block {
            min-width: 40px;
            height: 40px;
            border: 1px solid #111;
            text-align: center;
            line-height: 38px;
            display: inline-block;
            overflow: hidden;
            font-size: 13px;
            font-weight: 700;
            color: #333;
            padding: 0 10px 0px 10px;
        }

        .highlight {
            background-color: #b8beba;
            border-color: #FFFFFF;
        }

        .item-curricula-block {
            width: 90px;
            height: 40px;
            border: 1px solid #111;
            text-align: center;
            line-height: 38px;
            display: inline-block;
            overflow: hidden;
            font-size: 13px;
            font-weight: 700;
            color: #333;
            padding: 0 10px 0px 10px;
            background-color: #b8beba;
            border-color: #FFFFFF;
        }

        #learn-press-course-curriculum ul.curriculum-sections .section-content .course-item-lp_lesson span, #learn-press-course-curriculum ul.curriculum-sections .section-content .course-lesson span a {
            display: inline-block;
            line-height: 38px;
            color: #111;
        }

        .adbox {
            width: 270px;
            height: 80px;
            background-color: #78be20;
            text-align: center;
            line-height: 80px;
            color: #FFFFFF;
            font-size: 1em;
            font-weight: 900;
        }

        @media (max-width: 768px) {
            .lesson-favoritelist-box {
                width: 45%;
            }
        }

        .entry-content table[style*="border-style: none"] {
            border: 0;
        }

        .entry-content table[style*="border-style: none"] tr {
            border: 0;
        }

        .entry-content table[style*="border-style: none"] td {
            border: 0;
        }
    </style>
@stop

@extends('layouts.page')

@section('top_heading')
    Lessons
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/lessons" title="Lessons"><span itemprop="name">Lessons</span></a>
    </li>
@stop

@section('content')
    <article id="post-5299"
             class="post-5299 lp_course type-lp_course status-publish has-post-thumbnail hentry course_category-technology course_tag-node course_tag-tutorial pmpro-has-access course">
        <div class="entry-content">
            <div id="lp-single-course" class="lp-single-course">
                <div id="learn-press-course" class="course-summary learn-press">
                    <h3 class="entry-title" itemprop="name">{{ $lesson->title }}</h3>
                </div>
            </div>
            <br/>

            <div class="course-curriculum" id="learn-press-course-curriculum">
                <div class="curriculum-scrollable" style="padding: 0">

                    <ul class="curriculum-sections">
                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Summary
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    {!! $lesson->summary !!}
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Lesson plan
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    {!! $lesson->lesson_notes !!}
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Resources
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5418 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    {!! $lesson->resources !!}
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Extensions
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    {!! $lesson->extensions !!}
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Practical Tips
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    {!! $lesson->practical_tips !!}
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Related Lessons
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    @if($lesson->related_lessons)
                                        <ul>
                                            @foreach(explode(",", $lesson->related_lessons) as $l)
                                            <?php $salt="MathsLesson300";
                                                          $encrypted_id = base64_encode($l . $salt);  ?>
                                                @if(in_array($l,$alllessons))
                                                    <li>
                                                        <a target="_blank"
                                                           href='{{ route('front.lessons.show',$encrypted_id) }}'>{{ $lesson->getNumById($l) }} {{ $lesson->getTitleById($l) }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    @else
                                        N/A
                                    @endif
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Acknowledgements
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    {{ $lesson->acknowledgements ? $lesson->acknowledgements: "N/A" }}
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Year Levels
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                        @foreach($lesson->yearLevelItemsHtml() as $html)
                                            {!! $html !!}
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Content Strands
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                        @foreach($lesson->contentStrandsItemsHtml() as $html)
                                            {!! $html !!}
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Pedagogies
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                        @foreach($lesson->pedagogiesItemsHtml() as $html)
                                            {!! $html !!}
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Curricula
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    @if(count($lesson->getCurricula()))
                                        <ul class="pagination page-numbers" role="navigation" style="margin: 0 0 0">
                                            @foreach($lesson->curriculaItemsHtml() as $html)
                                                {!! $html !!}
                                            @endforeach
                                        </ul>
                                    @else
                                        N/A
                                    @endif
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Partial Video
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    @if($lesson->partial_video)
                                        {!! $lesson->getPartialVideoEmbedHtml() !!}
                                    @else
                                        N/A
                                    @endif
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Full Video
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson"
                                    data-type="lp_lesson">
                                    @if($lesson->full_video)
                                        {!! $lesson->getFullVideoEmbedHtml() !!}
                                    @else
                                        N/A
                                    @endif
                                </li>
                            </ul>
                        </li>

                        <li class="section">
                            <h4 class="section-header">
                                <span class="collapse"></span>
                                Software Link
                            </h4>
                            <ul class="section-content" style="display: none;">
                                <li class="course-item course-item-lp_lesson course-item-5417 item-locked course-lesson" data-type="lp_lesson">
                                @if($lesson->software_required && $lesson->software_link)
                                   <a href='{{ $lesson->software_link }}' target="_blank">{{ $lesson->software_link }}</a>
                                    @auth
                                    <div class="lesson-getaccesscode-box" style="margin-top:5px;">
                                     <span class="fa lesson-getaccesscode"
                                           data-lessonid="{{ $lesson->id }}"
                                           data-userid="{{ Auth::user()->id }}"
                                           data-accessurl="{{ $lesson->software_link }}"
                                           title="Get Access Code for the software link above">
                                      <span class="text">Get Access Code</span>
                                     </span>
                                    @csrf
                                    </div>
                                        <div id="lesson-getaccesscode-result">
                                        </div>
                                    @endauth
                                @else
                                   N/A
                                @endif
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>

            @auth
                <div class="lesson-favoritelist-box" style="margin-top:20px;">
<span class="fa fa-heart lesson-favoritelist" data-lessonid="{{ $lesson->id }}"
      title="Add this course to your favorite lessons list">
<span class="text">Favorite List</span>
</span>
                    @csrf
                </div>
            @endauth
        </div><!-- .entry-content -->
    </article>
@stop

@section('sidebar')
    <aside id="courses-6" class="widget widget_courses">
        <div class="thim-widget-courses thim-widget-courses-base"><h4 class="widget-title">
                Latest Lessons</h4>
            @foreach($latest_lessons as $lesson)
                <div class="thim-course-list-sidebar">
                    <div class="lpr_course">
                        <div class="thim-course-content">
                            <h3 class="course-title">
                                <?php $salt="MathsLesson300";
                                    $encrypted_id = base64_encode($lesson->id . $salt);  ?>
                                <a href='{{ route('front.lessons.show', $encrypted_id) }}'
                                   rel="bookmark">{{ $lesson->title }}</a>
                            </h3>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </aside>
    <aside id="single-images-6" class="widget widget_single-images">
        @foreach($ads as $ad)
            <div class="thim-widget-single-images thim-widget-single-images-base">
                <div class="single-image text-left">
                    <a target=" _self" href="{{ route('front.ads.show', $ad->id) }}" title="{{ $ad->title }}">
                        <div class="adbox">
                            {{ $ad->getTitle() }}
                        </div>
                    </a>
                </div>
            </div>
            <br>
        @endforeach
    </aside>
    <aside id="list-post-8" class="widget widget_list-post">
        <div class="thim-widget-list-post thim-widget-list-post-base"></div>
    </aside>
@stop

@section('js')
    <script>
        // fix front-end table border color issue start ///
        let border_styled_tables = jQuery('.entry-content table');
        for(let i=0; i<border_styled_tables.length; i++) {
            let table = jQuery(border_styled_tables[i]);
            let border_color = table.css('border-color');
            // console.log('border_color:' + border_color);
            // set tr/td border color
            let rows = table.find('> tbody > tr');
            for(let j=0; j<rows.length; j++) {
                let row = jQuery(rows[j]);
                // console.log('row border_color:' + row.css('border-color'));
                row.css('border', '1px solid '+ border_color);
            }

            let tds = table.find('> tbody > tr > td');
            for(let x=0; x<tds.length; x++) {
                let td = jQuery(tds[x]);
                // console.log('td border-color: ' + td.css('border-color'));
                // temporary workaround
                if (td.css('border-color') === 'rgb(238, 238, 238)') {
                    td.css('border-color', border_color);
                }
            }
        }
        // fix front-end table border color issue end ///

        jQuery('.lesson-favoritelist-box [class*=\'lesson-favoritelist\']').on('click', function (event) {
            event.preventDefault();
            var $this = jQuery(this);
            if ($this.hasClass('loading')) return;
            $this.addClass('loading');
            $this.toggleClass('lesson-favoritelist');
            $this.toggleClass('lesson-favoritelisted');
            $class = $this.attr('class');
            if ($this.hasClass('lesson-favoritelisted')) {
                $this.attr("title", "Remove this course from your favorite lessons list");
                jQuery.ajax({
                    type: 'POST',
                    url: '{{ route('front.users.addfavlesson') }}',
                    data: {
                        'lesson_id': $this.data('lessonid'),
                        '_token': jQuery('input[name=_token]').val(),
                    },
                    success: function () {
                        $this.removeClass('loading');
                    },
                    error: function () {
                        $this.removeClass('loading');
                    },
                });
            }
            if ($this.hasClass('lesson-favoritelist')) {
                $this.attr("title", "Add this course to your favorite lessons list");
                jQuery.ajax({
                    type: 'POST',
                    url: '{{ route('front.users.removefavlesson') }}',
                    data: {
                        'lesson_id': $this.data('lessonid'),
                        '_token': jQuery('input[name=_token]').val(),
                    },
                    success: function () {
                        $this.removeClass('loading');
                    },
                    error: function () {
                        $this.removeClass('loading');
                    },
                });
            }
        });

        ///////////////// get access code   ////////////////////
        jQuery('.lesson-getaccesscode-box [class*=\'lesson-getaccesscode\']').on('click', function (event) {
            event.preventDefault();
            let $this = jQuery(this);
            jQuery.ajax({
                type: 'POST',
                url: '{{ route('front.lessons.setAccessCode') }}',
                data: {
                    'lesson_id': $this.data('lessonid'),
                    'user_id': $this.data('userid'),
                    'access_url': $this.data('accessurl'),
                    '_token': jQuery('input[name=_token]').val(),
                },
                success: function (data) {
                    if (data !== undefined) {
                        jQuery('#lesson-getaccesscode-result').empty();
                        jQuery('#lesson-getaccesscode-result').append('Access Code: ' + data.data.access_code);
                        jQuery('#lesson-getaccesscode-result').append('<br>');
                        jQuery('#lesson-getaccesscode-result').append('This code will expire in ' + data.data.expires_in);
                    }
                    $this.removeClass('loading');
                },
                error: function (data) {
                    if (data !== undefined) {
                        console.log(data);
                    }
                    $this.removeClass('loading');
                },
            });

        });
    </script>
@endsection