<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolSizeTeacher extends Model
{
    /**
     * The schools that belong to the school size of teacher category.
     */
    public function schools()
    {
        return $this->hasMany(School::class);
    }

    /**
     * Scope a query to only include given keywords.
     *  query from name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('name', 'like', '%' . $keywords . '%');
    }

    public static function data4Select()
    {
        $all_data = SchoolSizeTeacher::get();
        $results = array();
        foreach ($all_data as $d) {
            $tmp['id'] = $d->id;
            $tmp['text'] = $d->name;
            $results[] = $tmp;
        }

        return $results;
    }
}
