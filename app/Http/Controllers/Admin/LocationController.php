<?php

namespace App\Http\Controllers\Admin;

use App\Models\Location;
use App\Models\Country;
use App\Models\School;
use Illuminate\Http\Request;
use App\Http\Requests\LocationRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;
use DB;

class LocationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['countries'] = data4Select(Country::class);
        $query = Location::query();

        if ($request->has('q_country') && $request->q_country != null) {
            $query->country($request->q_country);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['locations'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.locations.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['countries'] = data4Select(Country::class);

        return view('admin.locations.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\LocationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $location = new Location();
        $location->name = $validated['name'];

        DB::transaction(function () use ($location, $validated) {
            $location->save();

            // save relationships
            // $location
            $country = Country::find($validated['country']);
            $location->country()->associate($country)->save();

            Log::info("create location " . $location->id);
        });

        $request->session()->flash('alert-success', 'Location added successfully.');

        return redirect()->route('admin.locations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        $arr['location'] = $location;

        return view('admin.locations.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Location $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        $arr['location'] = $location;
        $arr['countries'] = data4Select(Country::class);

        return view('admin.locations.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\LocationRequest $request
     * @param  \App\Models\Location $location
     * @return \Illuminate\Http\Response
     */
    public function update(LocationRequest $request, Location $location)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $location->name = $validated['name'];

        DB::transaction(function () use ($location, $validated) {
            $location->save();

            // save relationships
            // $location
            $country = Country::find($validated['country']);
            $location->country()->associate($country)->save();

            Log::info("update location " . $location->id);
        });

        $request->session()->flash('alert-success', 'Location updated successfully.');

        return redirect()->route('admin.locations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::findOrFail($id);
        Log::info("delete location " . $id);

        if (School::query()->addressLocation($id)->count()) {
            return response()->json(['error' => 'Delete location is not allowed as schools associated with this location exist'],400);
        }
        
        $location->delete();

        return response()->json($location);
    }
}
