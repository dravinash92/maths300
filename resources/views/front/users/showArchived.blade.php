@extends('layouts.fronthome')

@section('sub_breadcrumbs')
<a itemprop="item" href="{{ route('front.users.index') }}" title="Teachers"><span itemprop="name">Teachers</span></a>
<a itemprop="item" href="{{ route('front.users.showArchived') }}" title="showArchived"><span itemprop="name">Archived</span></a>
@endsection

@section('page-content')

<div id="listTeachers-content" class="myaccount-right">

        <div class="row" style="margin-bottom:10px;">
            <form id="userSearchForm" role="form" method="get" action="{{ route('front.users.showArchived') }}">
                @csrf
                <div class="col-xs-6" style="padding-right:0px;">
                    <input type="text" class="form-control" id="q" name="q"
                                placeholder="Enter keywords (name/email/phone)..."
                                value="{{ old('q') }}">
                </div>

                <div class="col-xs-2" style="padding-left:1px;">
                    <button id="usersearchBtn" type="submit"
                                class="btn btn-primary btn-flat ladda-button"
                                data-style="expand-right"><span class="ladda-label">Search</span></button>
                </div>
            </form>

            <div class="col-xs-4 text-right">
                <a href="{{ route('front.users.create') }}" class="btn btn-primary">Add New</a>
                <a class="archive btn btn-primary" href="{{ route('front.users.showArchived') }}">Archived</a>
            </div>
        </div>

        <table id="tab_teachers" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th>Last Login</th>
                    <th>Menu</th>
                </tr>
                @csrf
                </thead>
                <tbody>
                        @foreach($users as $l)
                        <tr class="item{{ $l->id }}">
                            <td>{{ $l->name }}</td>
                            <td>{{ $l->email }}</td>
                            <td>{{ $l->phone }}</td>
                            <td>{{ $l->getStatus() }}</td>
                            <td class="col1">{{ $l->lastLoginFormat() }}</td>
                            <td>
                                @if( $l->isReinstateAllowed() )
                                    <button class="reinstate-modal btn btn-primary" data-id="{{ $l->id }}"
                                            data-name="{{ $l->name }}">Reinstate
                                    </button>
                                @endif
    
                                @if( $l->isDeleteAllowed() )
                                    <button class="delete-modal btn btn-primary" data-id="{{ $l->id }}"
                                            data-name="{{ $l->name }}">Delete
                                    </button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
            <div class="bbp-pagination">
                    <div class="bbp-pagination-links">
                        {{ $users->links() }}
                    </div>
            </div>

    
    @dialog(['action' => 'reinstate', 'model' => 'user', 'form_action' => '/users/reinstate'])
    @enddialog

    @dialog(['action' => 'delete', 'model' => 'user'])
    @enddialog
</div>
@stop

@push('js')
<script>
    jQuery(document).ready(function () {

        
    });
</script>

@dialogjs(['action' => 'reinstate'])
@enddialogjs

@dialogjs(['action' => 'delete', 'tab' => 'tab_teachers', 'submit_url' => 'users'])
@enddialogjs

@endpush