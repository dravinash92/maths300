<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class SettingTableSeeder extends Seeder {

    public function run()
    {
        DB::table('settings')->delete();

        Setting::create([
            'name' => 'Organization name',
            'value' => 'The Australian Association of Mathematics Teachers Inc.',
        ]);

        Setting::create([
            'name' => 'Organization address',
            'value' => 'Building D 80 Payneham Road STEPNEY SA 5069',
        ]);

        Setting::create([
            'name' => 'POST address',
            'value' => 'GPO Box 1729, Adelaide SA 5001',
        ]);

        Setting::create([
            'name' => 'Phone',
            'value' => '(02) 6201 5265',
        ]);

        Setting::create([
            'name' => 'Fax',
            'value' => '(08) 8362 9288',
        ]);

        Setting::create([
            'name' => 'Email',
            'value' => 'maths300@aamt.edu.au',
        ]);

        Setting::create([
            'name' => 'Finance officer name',
            'value' => 'Yoko',
        ]);

        Setting::create([
            'name' => 'Finance officer email',
            'value' => 'finance@aamt.edu.au',
        ]);

        Setting::create([
            'name' => 'ABN',
            'value' => '76 515 756 909',
        ]);

        Setting::create([
            'name' => 'Account name',
            'value' => 'Australian Association of Mathematics Teachers Inc',
        ]);

        Setting::create([
            'name' => 'Account bsb',
            'value' => '105-034',
        ]);

        Setting::create([
            'name' => 'Account number',
            'value' => '058004340',
        ]);

        Setting::create([
            'name' => 'Oversea school administrative fee',
            'value' => '25',
        ]);

    }

}