@extends('layouts.fronthome')

@section('sub_breadcrumbs')
<a itemprop="item" href="{{ route('front.users.changePassReq') }}" title="changePassword"><span itemprop="name">Change Password</span></a>
@endsection
 
@section('page-content')
<div id="changePassReq-content" class="myaccount-right">
    @if(count($errors))
    <div class="alert alert-danger message message-error">
        <strong>Whoops!</strong> There were some problems with your input.
        <br/>
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <!-- form start -->
    <form id="changePassForm" role="form" method="post" action="{{ route('front.users.changePassword') }}">
        {{ csrf_field() }}
        <div class="box-body">
            <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                <label for="current_password">Current Password</label>
                <input type="password" class="form-control" id="current_password" name="current_password" value="{{ old('current_password') }}">
                <span class="text-danger">{{ $errors->first('current_password') }}</span>
            </div>

            <div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
                <label for="new_password">New Password</label>
                <input type="password" class="form-control" id="new_password" name="new_password" value="{{ old('new_password') }}">
                <span class="text-danger">{{ $errors->first('new_password') }}</span>
            </div>

            <div class="form-group {{ $errors->has('new_password_confirmation') ? 'has-error' : '' }}">
                <label for="new_password_confirmation">Confirm New Password</label>
                <input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation" value="{{ old('new_password_confirmation') }}">
                <span class="text-danger">{{ $errors->first('new_password_confirmation') }}</span>
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary ladda-button" data-style="expand-right"><span
                                class="ladda-label">Submit</span></button>
        </div>
    </form>
</div>
@stop 

@push('js')
@endpush