<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    /**
     * Scope a query to only include given keywords.
     *  query from title
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $keywords
     */
    public function scopeKeyWords($query, $keywords)
    {
        if ($keywords) $query->where('title', 'like', '%' . $keywords . '%');
    }

    /**
     * Scope a query to only include given status.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  $status
     */
    public function scopeStatus($query, $status)
    {
        $query->where('status', $status);
    }

    public function getUpdatedAtDate()
    {
        return $this->updated_at ? Carbon::parse($this->updated_at)->format('d/m/Y') : "N/A";
    }

    public function getUpdatedAtDateDiffForHumans()
    {
        return $this->updated_at ? Carbon::parse($this->updated_at)->diffForHumans() : "";
    }

    public function getCreatedAtDate()
    {
        return $this->created_at ? Carbon::parse($this->created_at)->format('d, F Y h:i A') : "N/A";
    }

    /**
     * Get excerpt from string
     *
     * @param String $str String to get an excerpt from
     * @param Integer $startPos Position int string to start excerpt from
     * @param Integer $maxLength Maximum length the excerpt may be
     * @return String excerpt
     */
    public static function getExcerpt($str, $startPos = 0, $maxLength = 20)
    {
        if (mb_strlen($str) > $maxLength) {
            $excerpt = mb_substr($str, $startPos, $maxLength - 3);
            $excerpt .= ' ...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public function getTitle()
    {

        return $this::getExcerpt($this->title);
    }

    public function getStatus()
    {
        $status = "unknown";

        // init status: 0: draft 1:published  2:suspended
        if ($this->status == 0) {
            $status = "draft";
        }
        if ($this->status == 1) {
            $status = "published";
        }
        if ($this->status == 2) {
            $status = "suspended";
        }

        return $status;
    }

    public function isPublishAllowed()
    {
        return $this->status == 0 || $this->status == 2;
    }

    public function isSuspendAllowed()
    {
        return $this->status == 1;
    }
}
