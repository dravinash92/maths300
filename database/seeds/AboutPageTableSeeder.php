<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\AboutPage;

class AboutPageTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('about_pages')->delete();

        AboutPage::create([
            'content' =>
                "<div id=\"pg-2901-3\" class=\"panel-grid panel-no-style\"><div id=\"pgc-2901-3-0\" class=\"panel-grid-cell\"><div id=\"panel-2901-3-0-0\" class=\"so-panel widget widget_text panel-first-child panel-last-child\" data-index=\"7\"><div class=\"textwidget\"><h3 class=\"widget-title\">ABOUT MATHS300</h3><p>Maths300 is focussed on finding the most interesting maths lessons from classrooms across Australia and beyond. The lessons are the stimulus to generate professional discussions about:</p><ul><li>open-ended inquiry</li><li>investigative, problem-based approaches</li><li>the role of context to give meaning and purpose</li><li>genuine understanding</li><li>thinking, reasoning and communication</li><li>developing mathematical interconnections</li><li>broadening teachers' pedagogical repertoire</li><li>differentiation and equity</li><li>enriching teachers' assessment repertoire</li><li>the role of technology</li><li>non-threatening learning environments</li><li>a level of success for all</li></ul><p>Maths300 lessons include notes, a range of investigation guides, worksheets, extension problems, software, helpful hints and reflective comments.</p><p>&nbsp;</p></div></div></div></div><div id=\"pg-2901-3\" class=\"panel-grid panel-no-style\"><div id=\"pgc-2901-3-2\" class=\"panel-grid-cell\"><div id=\"panel-2901-3-2-0\" class=\"so-panel widget widget_text panel-first-child panel-last-child\" data-index=\"8\"><h3 class=\"widget-title\">WHAT MAKES MATHS300 DIFFERENT?</h3><div class=\"textwidget\"><ul><li>Lessons appeal to students of all learning styles and at all levels.</li><li>Lessons encourage questioning, reasoning, justifying and communication skills.</li><li>Shares your wisdom and experience with others to improve mathematics education.</li><li>One-third of lessons are supported by software specially written to extend learning opportunities.</li></ul><p>Many teachers have reported significant changes in their classrooms: maths becoming more exciting, students more engaged, and broad participation.</p><p>Browse the extensive <a href=\"/lessons/listall\" target=\"_self\">lesson library</a>.</p></div></div></div></div>",
        ]);

    }
}