<?php

namespace App\Console\Commands;

use App\Jobs\CheckInvoiceStatus;
use Illuminate\Console\Command;

class CheckInvoiceStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check invoice status ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        CheckInvoiceStatus::dispatch();
        $this->info("Check invoice status successfully.");
    }
}
