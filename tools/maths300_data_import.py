import pandas as pd
import configparser
import logging
import logging.config
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

# logging config
logging.config.fileConfig(fname='logging.ini', disable_existing_loggers=False)
logger = logging.getLogger('sampleLogger')

# app config
config = configparser.ConfigParser()
config.read('app.ini')

# db config
db_config = config['DB']
DB_HOST = db_config['DB_HOST']
DB_PORT = db_config['DB_PORT']
DB_DATABASE = db_config['DB_DATABASE']
DB_USERNAME = db_config['DB_USERNAME']
DB_PASSWORD = db_config['DB_PASSWORD']

def read_data():
    data = pd.read_excel(r'./maths300_data_0812.xlsx')
    df = pd.DataFrame(data, columns=['Institution', 'Address', 'Country', 'State', 'Suburb', 'Postcode', 'AdministratorName', 'Phone', 'TeacherFirstname', 'TeacherLastname', 'Teacher_email', 'Invoicing_email', 'Focus', 'Sector', 'NoTeachers', 'LicenseNew', 'License', 'Renewal_date', 'Joining_date', 'Password'])
    return df

def get_db_connection():
    conn = mysql.connector.connect(host=DB_HOST,
                             database=DB_DATABASE,
                             user=DB_USERNAME,
                             password=DB_PASSWORD)
    logger.info("db connection opened")
    return conn


def close_db_connection(conn):
    # closing database connection.
    if(conn.is_connected()):
        conn.close()
        logger.info("db connection closed")

def delete_data(conn):
    cursor = conn.cursor()
    sql = """DELETE FROM `history_data`"""
    cursor.execute(sql)
    conn.commit()
    cursor.close()
    logger.info("delete data from db")


def insert_data(conn, data):
    cursor = conn.cursor()
    sql = """INSERT INTO `history_data` (`institution`, `address`, `country`, `state`, `suburb`,  `postcode`, `administrator_name`, `phone`, `teacher_first_name`, `teacher_last_name`, `teacher_email`, `invoicing_email`, `focus`, `sector`, `no_teachers`, `license_new`, `license`, `renewal_date`, `joining_date`, `password`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
"""
    # strip newlines/extra spaces
    params = (data.Institution.strip().replace('\n',' '),
        data.Address.strip().replace('\n',' '),
        data.Country.strip().replace('\n',' '),
        data.State.strip().replace('\n',' '),
        data.Suburb.strip().replace('\n',' '),
        data.Postcode.strip().replace('\n',' '),
        data.AdministratorName.strip().replace('\n',' '),
        data.Phone.strip().replace('\n',' '),
        data.TeacherFirstname.strip().replace('\n',' '),
        data.TeacherLastname.strip().replace('\n',' '),
        data.Teacher_email.strip().replace('\n',' '),
        data.Invoicing_email.strip().replace('\n',' '),
        data.Focus.strip().replace('\n',' '),
        data.Sector.strip().replace('\n',' '),
        data.NoTeachers.strip().replace('\n',' '),
        data.LicenseNew.strip().replace('\n',' '),
        data.License.strip().replace('\n',' '),
        data.Renewal_date,
        data.Joining_date,
        data.Password.strip().replace('\n',' '))
    cursor.execute(sql, params)
    conn.commit()
    cursor.close()

if __name__== "__main__":
    df = read_data()
    df.fillna('', inplace=True)
    conn = get_db_connection()
    delete_data(conn)
    for row in df.itertuples():
        insert_data(conn, row)
    close_db_connection(conn)
