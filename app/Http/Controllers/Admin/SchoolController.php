<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\Invoice;
use App\Models\School;
use App\Models\Location;
use App\Models\SchoolType;
use App\Models\SchoolSector;
use App\Models\SchoolSizeStudent;
use App\Models\SchoolSizeTeacher;
use App\Models\SubscriptionPlan;
use App\Models\YearLevel;
use App\Models\User;
use App\Models\Tag;
use Carbon\Carbon;
use App\Http\Requests\SchoolRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Response;
use Validator;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr['locations'] = Country::data4Select();
        $arr['schoolTypes'] = data4Select(SchoolType::class);
        $arr['schoolSectors'] = data4Select(SchoolSector::class);

        $query = School::query();
        

        if ($request->has('q_schoolId') && $request->q_schoolId != null) {
            $query->schoolId($request->q_schoolId);
        }

        if ($request->has('q_address_location') && $request->q_address_location != null) {
            $query->addressLocation($request->q_address_location);
        }

        if ($request->has('q_schoolType') && $request->q_schoolType != null) {
            $query->schoolType($request->q_schoolType);
        }

        if ($request->has('q_schoolSector') && $request->q_schoolSector != null) {
            $query->schoolSector($request->q_schoolSector);
        }

        if ($request->has('q_subscription_expiry_date_range') && $request->q_subscription_expiry_date_range != null) {
            $date_range = explode(' - ', $request->q_subscription_expiry_date_range);
            $from = Carbon::createFromFormat('d/m/Y', $date_range[0])->format('Y-m-d');
            $to = Carbon::createFromFormat('d/m/Y', $date_range[1])->format('Y-m-d');
            Log::info("q_subscription_expiry_date_range: " . $request->q_subscription_expiry_date_range . " from: " . $from . " to: " . $to);
            if ($from && $to) {
                $query->expiryDate($from, $to);
            }
        }

        if ($request->has('q_invoice') && $request->q_invoice != null) {
            $query->invoice($request->q_invoice);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("school search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        if($request->has('sortBy')) {
            $order = $request->has('order') && $request->order=='desc' ? 'desc' : 'asc';
            if($request->sortBy=='join_date') { 
                $query->orderBy('join_date', $order); 
            }
            if($request->sortBy=='expiry_date') {
                $query->orderBy('subscription_expiration_date', $order);
            }
            if($request->sortBy=='status') {
                $query->orderBy('status', $order);
            }
            if($request->sortBy=='payment') {
                // $query->orderBy('payment', $order);
            }
            if($request->sortBy=='name') {

                $query->orderBy('name', $order);
            }
            if($request->sortBy=='location') {
                $query->orderBy('address_location_id', $order);

            }
        }
        
        // not archived
        $query->statusNot(3);
        $paginator = $query->paginate(config('app.page_number'));

        if ($request->has('q_address_location')) {
            $paginator->appends(array('q_address_location' => $request->q_address_location));
        }  
        if ($request->has('q_schoolType')) {
            $paginator->appends(array('q_schoolType' => $request->q_schoolType));
        }  
        if ($request->has('q_schoolSector')) {
            $paginator->appends(array('q_schoolSector' => $request->q_schoolSector));
        }  
        if ($request->has('q_subscription_expiry_date_range')) {
            $paginator->appends(array('q_subscription_expiry_date_range' => $request->q_subscription_expiry_date_range));
        }  
        if ($request->has('q_invoice')) {
            $paginator->appends(array('q_invoice' => $request->q_invoice));
        }  
        if ($request->has('q')) {
            $paginator->appends(array('q' => $request->q));
        }  
         
        $arr['schools'] = $paginator;
        $arr['sortby'] = $request->sortBy;
        $arr['order'] = $request->order;
        $arr['q_address_location'] = $request->q_address_location;
        $arr['q_schoolType'] = $request->q_schoolType;
        $arr['q_schoolSector'] = $request->q_schoolSector;
        $arr['q_subscription_expiry_date_range'] = $request->q_subscription_expiry_date_range;
        $arr['q_invoice'] = $request->q_invoice;
        $arr['q'] = $request->q;
        
        // allow using Input::old() function in view to access the data.
        $request->flash();
        Log::info("school: " . json_encode($arr));

        return view('admin.schools.index')->with($arr);
    }

    /**
     * Display a listing of the archived schools.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showArchived(Request $request)
    {
        $arr['locations'] = Country::data4Select();
        $arr['schoolTypes'] = data4Select(SchoolType::class);
        $arr['schoolSectors'] = data4Select(SchoolSector::class);

        $query = School::query();

        if ($request->has('q_address_location') && $request->q_address_location != null) {
            $query->addressLocation($request->q_address_location);
        }

        if ($request->has('q_schoolType') && $request->q_schoolType != null) {
            $query->schoolType($request->q_schoolType);
        }

        if ($request->has('q_schoolSector') && $request->q_schoolSector != null) {
            $query->schoolSector($request->q_schoolSector);
        }

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        // only show status: archived
        $query->status(3);
        $arr['schools'] = $query->get();

        return view('admin.schools.showArchived')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['locations'] = Country::data4Select();
        $arr['schoolTypes'] = data4Select(SchoolType::class);
        $arr['schoolSectors'] = data4Select(SchoolSector::class);
        $arr['schoolSizeStudents'] = data4Select(SchoolSizeStudent::class);
        $arr['schoolSizeTeachers'] = data4Select(SchoolSizeTeacher::class);
        $arr['subscriptionPlans'] = SubscriptionPlan::data4Select();
        $arr['year_levels'] = data4Select(YearLevel::class);
        $arr['tags'] = Tag::all('id', 'name');
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://countriesnow.space/api/v0.1/countries/iso');
        $countries = json_decode($response->getBody(), true);
        $arr['countries'] =$countries['data'];
        return view('admin.schools.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\SchoolRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(SchoolRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        if (User::isUserEmailExists($validated['finance_officer_email'])) {
            // allow using Input::old() function in view to access the data.
            $request->flash();
            return back()->withErrors('Input finance officer email exists in the system');
        }

        if (User::isUserEmailExists($validated['coordinator_email'])) {
            // allow using Input::old() function in view to access the data.
            $request->flash();
            return back()->withErrors('Input coordinator email exists in the system');
        }
        
        $country_find = Country::where('name','LIKE',$validated['country'])->first();
        $countryId = '';
        $locationId = '';
        if(empty($country_find)){
            $country = new Country();
            $country->name = $validated['country'];
            $country->save();
            $countryId = $country->id;
        }else{
            $countryId = $country_find->id;
        }
        $location_find = Location::where('name','LIKE',$validated['location'])->where('country_id',$countryId)->first();
        if(empty($location_find)){
            $location = new Location();
            $location->name = $validated['location'];
            $location->country_id = $countryId;
            $location->save();
            $locationId = $location->id;
        }else{
            $locationId = $location_find->id;
        }
        
        $is_single_account = $validated['schoolSizeTeacher'] == 1 ? 1 : 0;

        $school = new School();
        $school->name = $validated['name'];
        $school->address_region = $validated['address_region'];
        $school->address_suburb = $validated['address_suburb'];
        $school->address_postcode = $validated['address_postcode'];
        $school->number_of_campuses = $validated['number_of_campuses'];
        $school->notes = $validated['notes'];
        $school->finance_invoice_email = $validated['finance_invoice_email'];
        $school->billing_address_region = $validated['billing_address_region'];
        $school->billing_address_suburb = $validated['billing_address_suburb'];
        $school->billing_address_postcode = $validated['billing_address_postcode'];
        $school->address_location_id = $locationId;
        if(isset($validated['lesson_pack'])){
            if(in_array('all',$validated['lesson_pack']) && count($validated['lesson_pack']) > 1){
                $request->flash();
                return back()->withErrors('No lesson packs can be added with "Default - All Lessons" pack');
            }else{
                $school->tags = $validated['lesson_pack'][0]=='all' ? '' : implode(",", (array)$validated['lesson_pack']);
            }
        }
        // default 0:inactive
        $school->status = 0;
        $school->join_date = today();
        $school->is_single_account = $is_single_account;

        DB::transaction(function () use ($school, $validated) {
            $school->save();

            // save relationships
            $schoolType = SchoolType::find($validated['schoolType']);
            $school->schoolType()->associate($schoolType)->save();

            $schoolSector = SchoolSector::find($validated['schoolSector']);
            $school->schoolSector()->associate($schoolSector)->save();

            // $address_location = Location::find($validated['address_location']);
            // $school->addressLocation()->associate($address_location)->save();

            $schoolSizeStudent = SchoolSizeStudent::find($validated['schoolSizeStudent']);
            $school->schoolSizeStudent()->associate($schoolSizeStudent)->save();

            $schoolSizeTeacher = SchoolSizeTeacher::find($validated['schoolSizeTeacher']);
            $school->schoolSizeTeacher()->associate($schoolSizeTeacher)->save();
            
            $select2_billing_address_country_find = Country::where('name','LIKE',$validated['billing_address_country'])->first();
            $select2_billing_address_countryId = '';
            $select2_billing_address_locationId = '';
            if(empty($select2_billing_address_country_find)){
                $select2_billing_address_country = new Country();
                $select2_billing_address_country->name = $validated['billing_address_country'];
                $select2_billing_address_country->save();
                $select2_billing_address_countryId = $select2_billing_address_country->id;
            }else{
                $select2_billing_address_countryId = $select2_billing_address_country_find->id;
            }
            $select2_billing_address_location_find = Location::where('name','LIKE',$validated['billing_address_location'])->where('country_id',$select2_billing_address_countryId)->first();
            if(empty($select2_billing_address_location_find)){
                $select2_billing_address_location = new Location();
                $select2_billing_address_location->name = $validated['billing_address_location'];
                $select2_billing_address_location->country_id = $select2_billing_address_countryId;
                $select2_billing_address_location->save();
                $select2_billing_address_locationId = $select2_billing_address_location->id;
            }else{
                $select2_billing_address_locationId = $select2_billing_address_location_find->id;
            }

            $billing_address_location = Location::find($select2_billing_address_locationId);
            $school->billingAddressLocation()->associate($billing_address_location)->save();

            $subscriptionPlan = SubscriptionPlan::find($validated['subscriptionPlan']);
            $school->subscriptionPlan()->associate($subscriptionPlan)->save();

            $data_finance_officer = array();
            $data_finance_officer["name"] = $validated['finance_officer_name'];
            $data_finance_officer["email"] = $validated['finance_officer_email'];
            $data_finance_officer["phone"] = $validated['finance_officer_phone'];

            if (!empty($validated['finance_officer_position'])) {
                $data_finance_officer["position"] = $validated['finance_officer_position'];
            }

            $data_coordinator = array();
            $data_coordinator["name"] = $validated['coordinator_name'];
            $data_coordinator["email"] = $validated['coordinator_email'];
            $data_coordinator["phone"] = $validated['coordinator_phone'];

            if (!empty($validated['coordinator_position'])) {
                $data_coordinator["position"] = $validated['coordinator_position'];
            }

            if (!empty($validated['coordinator_year_levels'])) {
                $data_coordinator["year_levels"] = $validated['coordinator_year_levels'];
            }

            $new_financeOfficer_email = $validated['finance_officer_email'];
            $new_coordinator_email = $validated['coordinator_email'];

            Log::info("create new users......");
            // create finance_officer/coordinator for this school (with status inactive)
            $finance_officer = User::createWithRoles($data_finance_officer, $school, 'school-finance-officer');
            $school->financeOfficer()->associate($finance_officer)->save();

            if ($new_financeOfficer_email == $new_coordinator_email) {
                $coordinator = $finance_officer;
                $coordinator->position = !empty($validated['coordinator_position']) ? $validated['coordinator_position'] : "";
                if (!empty($validated['coordinator_year_levels'])) {
                    $coordinator->yearLevels()->sync($validated['coordinator_year_levels']);
                }
                $coordinator->assignRole('school-finance-officer', 'school-admin');
            } else {
                $coordinator = User::createWithRoles($data_coordinator, $school, 'school-admin');
            }
            $school->coordinator()->associate($coordinator)->save();

            Log::info("create school id: " . $school->id . ", name: " . $school->name);
        });

        $request->session()->flash('alert-success', 'School added successfully.');

        return redirect()->route('admin.schools.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\School $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        $arr['school'] = $school;
        $arr['tags'] = Tag::all('id', 'name');
        Log::info("show school id: " . $school->id . ", name: ".$school->name);

        return view('admin.schools.show')->with($arr);
    }

    /**
     * Display the invoice of this school
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function viewInvoice(Request $request)
    {
        $id = $request->schoolID;
        $arr['q_to'] = $id;

        return redirect()->route('admin.invoices.index', $arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\School $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        $arr['locations'] = Country::data4Select();
        $arr['schoolTypes'] = data4Select(SchoolType::class);
        $arr['schoolSectors'] = data4Select(SchoolSector::class);
        $arr['schoolSizeStudents'] = data4Select(SchoolSizeStudent::class);
        $arr['schoolSizeTeachers'] = data4Select(SchoolSizeTeacher::class);
        $arr['subscriptionPlans'] = SubscriptionPlan::data4Select();
        $arr['year_levels'] = data4Select(YearLevel::class);
        $arr['school'] = $school;
        $arr['tags'] = Tag::all('id', 'name');
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://countriesnow.space/api/v0.1/countries/iso');
        $countries = json_decode($response->getBody(), true);
        $arr['countries'] =$countries['data'];
        $country_find = '';
        $location_find = Location::where('id',$school->addressLocation->id)->first();
        if(!empty($location_find)){
            $country_find = Country::where('id',$location_find->country_id)->first();
        }
        $billing_country_find = '';
        $billing_location_find = Location::where('id',$school->billingAddressLocation->id)->first();
        if(!empty($billing_location_find)){
            $billing_country_find = Country::where('id',$billing_location_find->country_id)->first();
        }
        $arr['country'] = $country_find->name;
        $arr['location'] = $location_find->name;
        $arr['billing_country'] = $billing_country_find->name;
        $arr['billing_location'] = $billing_location_find->name;
        
        return view('admin.schools.edit')->with($arr);
    }

    /**
     * update user info on school update
     * @param School $school
     * @param $new_email
     * @param $current_user
     * @param $role
     * @param $request_data
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    private function updateUserInfo(School $school, $new_email, $current_user, $role, $request_data)
    {
        Log::info("updateUserInfo school: " . $school->name . ", new email: " . $new_email);
        $user = null;
        // check if this email exists in the users table
        if ($school->isEmailExists($new_email)) {
            // add [finance officer] role into this user
            Log::info("new " . $role . " email address exists in the given school [" . $school->name . "] teachers list");
            // add [finance officer] role into this user
            $user = User::getUserByEmail($new_email);
            // update user info
            $user = User::updateWithoutEmailWithRoles($user, $request_data, $school, 'school-teacher', $role);
        } else {
            Log::info("new " . $role . " email address does not exist in the given school [" . $school->name . "] teachers list");
            if (User::isUserEmailExists($new_email)) {
                Log::info("new " . $role . " exists in other schools " . $new_email);
                throw new \Exception("Input " . $role . " email has already been taken");
            } else {
                Log::info("create a new " . $role . " " . $new_email);
                $user = User::createWithRoles($request_data, $school, $role);
            }
        }

        if ($user) {
            if ($role == "school-finance-officer") {
                // bind users
                $school->financeOfficer()->associate($user)->save();
                if ($current_user && $current_user->id != $user->id) {
                    Log::warning("current finance-officer [email: " . $current_user->email . "] remove role [school-finance-officer], assign role [school-teacher]");
                    // change the role of the previous coordinator
                    $current_user->removeRole('school-finance-officer');
                    $current_user->assignRole('school-teacher');
                }
            } else if ($role == "school-admin") {
                // bind users
                $school->coordinator()->associate($user)->save();
                if ($current_user && $current_user->id != $user->id) {
                    Log::warning("current coordinator [email: " . $current_user->email . "] remove role [school-admin], assign role [school-teacher]");
                    // change the role of the previous coordinator
                    $current_user->removeRole('school-admin');
                    $current_user->assignRole('school-teacher');
                }
            }
        } else {
            throw new \Exception("Unexpected Exception, query user is null, input role: " . $role . ", school: " . $school->name . ", email: " . $new_email);
        }
    }

    /**
     * @param School $school
     * @param $new_email
     * @param $current_user
     * @param $role
     * @param $request_data
     * @throws \Exception
     */
    private function updateUsersWithSameEmail(School $school, $new_email, $current_user, $role, $request_data)
    {
        Log::info("updateUsersWithSameEmail school: " . $school->name . ", new email: " . $new_email);
        $user = null;
        // check if this email exists in the users table
        if ($school->isEmailExists($new_email)) {
            Log::info("new " . $role . " email address exists in the given school [" . $school->name . "] teachers list");
            // add [finance officer] role into this user
            $user = User::getUserByEmail($new_email);
            // update user info
            $user = User::updateWithoutEmailWithRoles($user, $request_data, $school, 'school-teacher', 'school-finance-officer', 'school-admin');
        } else {
            Log::info("new " . $role . " email address does not exist in the given school [" . $school->name . "] teachers list");
            if (User::isUserEmailExists($new_email)) {
                Log::info("new " . $role . " email exists in other schools " . $new_email);
                throw new \Exception("Input " . $role . " email has already been taken");
            } else {
                Log::info("create a new " . $role . " " . $new_email);
                $user = User::createWithRoles($request_data, $school, 'school-finance-officer', 'school-admin');
            }
        }

        if ($user) {
            // bind users
            $school->financeOfficer()->associate($user)->save();
            $school->coordinator()->associate($user)->save();
            if ($role == "school-finance-officer") {
                if ($current_user && $current_user->id != $user->id) {
                    Log::warning("current finance-officer [email: " . $current_user->email . "] remove role [school-finance-officer], assign role [school-teacher]");
                    $current_user->removeRole('school-finance-officer');
                    $current_user->assignRole('school-teacher');
                }
            } else if ($role == "school-admin") {
                if ($current_user && $current_user->id != $user->id) {
                    Log::warning("current coordinator [email: " . $current_user->email . "] remove role [school-admin], assign role [school-teacher]");
                    // change the role of the previous coordinator
                    $current_user->removeRole('school-admin');
                    $current_user->assignRole('school-teacher');
                }
            }
        } else {
            throw new \Exception("Unexpected Exception, query user is null, input role: " . $role . ", school: " . $school->name . ", email: " . $new_email);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\SchoolRequest $request
     * @param \App\Models\School $school
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolRequest $request, School $school)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $country_find = Country::where('name','LIKE',$validated['country'])->first();
        $countryId = '';
        $locationId = '';
        if(empty($country_find)){
            $country = new Country();
            $country->name = $validated['country'];
            $country->save();
            $countryId = $country->id;
        }else{
            $countryId = $country_find->id;
        }
        $location_find = Location::where('name','LIKE',$validated['location'])->where('country_id',$countryId)->first();
        if(empty($location_find)){
            $location = new Location();
            $location->name = $validated['location'];
            $location->country_id = $countryId;
            $location->save();
            $locationId = $location->id;
        }else{
            $locationId = $location_find->id;
        }
        $is_single_account = $validated['schoolSizeTeacher'] == 1 ? 1 : 0;

        Log::info("request finance_officer_email: " . $validated['finance_officer_email'] . ", coordinator_email: " . $validated['coordinator_email']);

        $school->name = $validated['name'];
        $school->address_region = $validated['address_region'];
        $school->address_suburb = $validated['address_suburb'];
        $school->address_postcode = $validated['address_postcode'];
        $school->number_of_campuses = $validated['number_of_campuses'];
        $school->notes = $validated['notes'];
        $school->finance_invoice_email = $validated['finance_invoice_email'];
        $school->billing_address_region = $validated['billing_address_region'];
        $school->billing_address_suburb = $validated['billing_address_suburb'];
        $school->billing_address_postcode = $validated['billing_address_postcode'];
        $school->address_location_id = $locationId;
        if(isset($validated['lesson_pack'])){
            if(in_array('all',$validated['lesson_pack']) && count($validated['lesson_pack']) > 1){
                $request->flash();
                return back()->withErrors('No lesson packs can be added with "Default - All Lessons" pack');
            }else{
                $school->tags = $validated['lesson_pack'][0]=='all' ? '' : implode(",", (array)$validated['lesson_pack']);
            }
        }
        $school->is_single_account = $is_single_account;
        $school->purchase_order_no = $validated['purchase_order_no'];

        try {
            DB::transaction(function () use ($school, $validated) {
                $school->save();

                // save relationships
                $schoolType = SchoolType::find($validated['schoolType']);
                $school->schoolType()->associate($schoolType)->save();


                $schoolSector = SchoolSector::find($validated['schoolSector']);
                $school->schoolSector()->associate($schoolSector)->save();

                // $address_location = Location::find($validated['address_location']);
                // $school->addressLocation()->associate($address_location)->save();

                $schoolSizeStudent = SchoolSizeStudent::find($validated['schoolSizeStudent']);
                $school->schoolSizeStudent()->associate($schoolSizeStudent)->save();

                $schoolSizeTeacher = SchoolSizeTeacher::find($validated['schoolSizeTeacher']);
                $school->schoolSizeTeacher()->associate($schoolSizeTeacher)->save();
                
                $select2_billing_address_country_find = Country::where('name','LIKE',$validated['billing_address_country'])->first();
                $select2_billing_address_countryId = '';
                $select2_billing_address_locationId = '';
                if(empty($select2_billing_address_country_find)){
                    $select2_billing_address_country = new Country();
                    $select2_billing_address_country->name = $validated['billing_address_country'];
                    $select2_billing_address_country->save();
                    $select2_billing_address_countryId = $select2_billing_address_country->id;
                }else{
                    $select2_billing_address_countryId = $select2_billing_address_country_find->id;
                }
                
                $select2_billing_address_location_find = Location::where('name','LIKE',$validated['billing_address_location'])->where('country_id',$select2_billing_address_countryId)->first();
                if(empty($select2_billing_address_location_find)){
                    $select2_billing_address_location = new Location();
                    $select2_billing_address_location->name = $validated['billing_address_location'];
                    $select2_billing_address_location->country_id = $select2_billing_address_countryId;
                    $select2_billing_address_location->save();
                    $select2_billing_address_locationId = $select2_billing_address_location->id;
                }else{
                    $select2_billing_address_locationId = $select2_billing_address_location_find->id;
                }
                
                $billing_address_location = Location::find($select2_billing_address_locationId);
                $school->billingAddressLocation()->associate($billing_address_location)->save();

                $subscriptionPlan = SubscriptionPlan::find($validated['subscriptionPlan']);
                $school->subscriptionPlan()->associate($subscriptionPlan)->save();

                $current_financeOfficer = $school->financeOfficer;
                $current_coordinator = $school->coordinator;

                $data_finance_officer = array();
                $data_finance_officer["name"] = $validated['finance_officer_name'];
                $data_finance_officer["email"] = $validated['finance_officer_email'];
                $data_finance_officer["phone"] = $validated['finance_officer_phone'];

                if (!empty($validated['finance_officer_position'])) {
                    $data_finance_officer["position"] = $validated['finance_officer_position'];
                }

                $data_coordinator = array();
                $data_coordinator["name"] = $validated['coordinator_name'];
                $data_coordinator["email"] = $validated['coordinator_email'];
                $data_coordinator["phone"] = $validated['coordinator_phone'];

                if (!empty($validated['coordinator_position'])) {
                    $data_coordinator["position"] = $validated['coordinator_position'];
                }

                if (!empty($validated['coordinator_year_levels'])) {
                    $data_coordinator["year_levels"] = $validated['coordinator_year_levels'];
                }

                $new_financeOfficer_email = $validated['finance_officer_email'];
                $new_coordinator_email = $validated['coordinator_email'];

                // As of 26/09/2020, 10 schools got null finance_officer/coordinator

                // new requirements: 25/09/2020
                // 1. The Finance and School coordinator emails can be the same or different regardless of the school’s subscription
                //     (currently, school that has one user cannot have different emails and schools that have more than 1 user must have different emails).
                // 2. Allow an existing user to be reassigned as a coordinator or finance officer
                //     (currently, if a teacher is a user, the email cannot be used for coordinator or finance officer.
                //     We’ve to delete the teacher before we can add her/him as a coordinator or finance officer).

                // 4 different cases
                // case 1 : $current_financeOfficer null $current_coordinator null
                // this follows the create new school financeOfficer/coordinator logic
                if (empty($current_financeOfficer) && empty($current_coordinator)) {
                    Log::info("current financeOfficer and coordinator is null");
                    if (User::isUserEmailExists($new_financeOfficer_email)) {
                        Log::info("finance_officer_email exists " . $new_financeOfficer_email);
                        throw new \Exception('Input finance officer email exists in the system');
                    }
                    if (User::isUserEmailExists($new_coordinator_email)) {
                        Log::info("coordinator_email exists " . $new_coordinator_email);
                        throw new \Exception('Input coordinator email exists in the system');
                    }

                    Log::info("create new users......");
                    // create finance_officer/coordinator for this school (with status inactive)
                    $finance_officer = User::createWithRoles($data_finance_officer, $school, 'school-finance-officer');
                    $school->financeOfficer()->associate($finance_officer)->save();

                    if ($new_financeOfficer_email == $new_coordinator_email) {
                        $coordinator = $finance_officer;
                        $coordinator->position = !empty($validated['coordinator_position']) ? $validated['coordinator_position'] : "";
                        if (!empty($validated['coordinator_year_levels'])) {
                            $coordinator->yearLevels()->sync($validated['coordinator_year_levels']);
                        }
                        $coordinator->assignRole('school-finance-officer', 'school-admin');
                    } else {
                        $coordinator = User::createWithRoles($data_coordinator, $school, 'school-admin');
                    }
                    $school->coordinator()->associate($coordinator)->save();
                } else if (empty($current_financeOfficer) && !empty($current_coordinator)) { // case 2 : $current_financeOfficer null $current_coordinator not null
                    Log::info("current financeOfficer is null, coordinator is not null");
                    // create a new financeOfficer
                    if ($new_financeOfficer_email == $new_coordinator_email) {
                        // add a financeOfficer with the same email address with coordinator
                        Log::info("add a finance officer with the same email address with coordinator");
                        if ($new_coordinator_email == $current_coordinator->email) {
                            Log::info("new coordinator got email address unchanged");
                            $coordinator = User::updateWithoutEmailWithRoles($current_coordinator, $data_coordinator, $school, null, 'school-finance-officer', 'school-admin');
                            // bind users
                            $school->financeOfficer()->associate($coordinator)->save();
                            $school->coordinator()->associate($coordinator)->save();
                        } else {
                            Log::info("new coordinator got email address updated");
                            $this->updateUsersWithSameEmail($school, $new_coordinator_email, $current_coordinator, 'school-admin', $data_coordinator);
                        }
                    } else {
                        Log::info("add finance officer and coordinator with different email addresses");
                        // finance officer
                        $this->updateUserInfo($school, $new_financeOfficer_email, $current_financeOfficer, "school-finance-officer", $data_finance_officer);
                        // coordinator
                        $this->updateUserInfo($school, $new_coordinator_email, $current_coordinator, "school-admin", $data_coordinator);
                    }
                } else if (!empty($current_financeOfficer) && empty($current_coordinator)) { // case 3 : $current_financeOfficer not null $current_coordinator is null
                    Log::info("current financeOfficer is not null, coordinator is null");
                    // create a new financeOfficer
                    if ($new_financeOfficer_email == $new_coordinator_email) {
                        // add a financeOfficer with the same email address with coordinator
                        Log::info("add a finance officer with the same email address with coordinator");
                        if ($new_financeOfficer_email == $current_financeOfficer->email) {
                            Log::info("new financeOfficer got email address unchanged");
                            $finance_officer = User::updateWithoutEmailWithRoles($current_financeOfficer, $data_finance_officer, $school, null, 'school-finance-officer', 'school-admin');
                            // bind users
                            $school->financeOfficer()->associate($finance_officer)->save();
                            $school->coordinator()->associate($finance_officer)->save();
                        } else {
                            Log::info("new finance officer got email address updated");
                            $this->updateUsersWithSameEmail($school, $new_financeOfficer_email, $current_financeOfficer, 'school-finance-officer', $data_finance_officer);
                        }
                    } else {
                        Log::info("add finance officer and coordinator with different email addresses");
                        // finance officer
                        $this->updateUserInfo($school, $new_financeOfficer_email, $current_financeOfficer, "school-finance-officer", $data_finance_officer);
                        // coordinator
                        $this->updateUserInfo($school, $new_coordinator_email, $current_coordinator, "school-admin", $data_coordinator);
                    }
                } else if (!empty($current_financeOfficer) && !empty($current_coordinator)) { // case 4 : $current_financeOfficer not null $current_coordinator is not null
                    Log::info("current financeOfficer is not null, coordinator is not null");
                    // create a new financeOfficer
                    if ($new_financeOfficer_email == $new_coordinator_email) {
                        // add a financeOfficer with the same email address with coordinator
                        Log::info("add a finance officer with the same email address with coordinator");
                        if ($new_financeOfficer_email == $current_financeOfficer->email) {
                            if ($new_coordinator_email == $current_coordinator->email) {
                                Log::info("new financeOfficer got email address unchanged, new coordinator got email address unchanged");
                                $coordinator = User::updateWithoutEmailWithRoles($current_coordinator, $data_coordinator, $school, null, 'school-finance-officer', 'school-admin');
                                // bind users
                                $school->financeOfficer()->associate($coordinator)->save();
                                $school->coordinator()->associate($coordinator)->save();
                            } else {
                                Log::info("new financeOfficer got email address unchanged, new coordinator got email address updated");
                                $this->updateUsersWithSameEmail($school, $new_coordinator_email, $current_coordinator, 'school-admin', $data_coordinator);
                            }
                        } else {
                            if ($new_coordinator_email == $current_coordinator->email) {
                                Log::info("new financeOfficer got email address updated, new coordinator got email address unchanged");
                                $this->updateUsersWithSameEmail($school, $new_financeOfficer_email, $current_financeOfficer, 'school-finance-officer', $data_finance_officer);
                            } else {
                                Log::info("new financeOfficer got email address updated, new coordinator got email address updated");
                                $this->updateUsersWithSameEmail($school, $new_financeOfficer_email, $current_financeOfficer, 'school-finance-officer', $data_finance_officer);
                                $this->updateUsersWithSameEmail($school, $new_coordinator_email, $current_coordinator, 'school-admin', $data_coordinator);
                            }
                        }
                    } else {
                        Log::info("add finance officer and coordinator with different email addresses");
                        // finance officer
                        $this->updateUserInfo($school, $new_financeOfficer_email, $current_financeOfficer, "school-finance-officer", $data_finance_officer);
                        // coordinator
                        $this->updateUserInfo($school, $new_coordinator_email, $current_coordinator, "school-admin", $data_coordinator);
                    }
                }
                Log::info("update school id: " . $school->id . ", name: " . $school->name);
            });
            $request->session()->flash('alert-success', 'School updated successfully.');
            return redirect()->route('admin.schools.index');
        } catch (\Exception $e) {
            Log::error("Update school caught an exception: " . $e->getMessage());
            // allow using Input::old() function in view to access the data.
            $request->flash();
            return back()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = School::findOrFail($id);
        if (Invoice::query()->to($school->id)->count()) {
            return response()->json(['error' => 'Delete school is not allowed as pending invoices exist'], 400);
        }
        // delete operations in one transaction
        DB::transaction(function () use ($school) {
            Log::info("dissociate financeOfficer " . $school->financeOfficer->id);
            $school->financeOfficer()->dissociate()->save();

            Log::info("dissociate coordinator " . $school->coordinator->id);
            $school->coordinator()->dissociate()->save();

            // delete all teachers need to be confirmed.
            foreach ($school->users as $user) {
                Log::info("delete user " . $user->id);
                $user->delete();
            }
            $school->users()->delete();
            $school->delete();
            Log::info("delete school " . $school->id);
        });

        return response()->json($school);
    }

    /**
     * Change status to active.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        $id = $request->id_activate;

        $school = School::findOrFail($id);
        Log::info("activate school " . $id);
        $school->activate();

        $request->session()->flash('alert-success', 'School activated successfully.');

        return redirect()->route('admin.schools.index');
    }

    /**
     * Change status to suspended.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function suspend(Request $request)
    {
        $id = $request->id_suspend;

        $school = School::findOrFail($id);
        Log::info("suspend school " . $id);
        $school->suspend();

        $request->session()->flash('alert-success', 'School suspended successfully.');

        return redirect()->route('admin.schools.index');
    }

    /**
     * Sending mails to teachers.
     *
     */
    public function sendMailToTeachers(Request $schoolId)
    {
        echo "School Controller = ".$schoolId;exit;
        $id = $schoolId;
        $school = School::findOrFail($id);
        Log::info("suspend school " . $id);
        $school->sendMailToTeachers();
    }

    /**
     * Change status to archived.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request)
    {
        $id = $request->id_archive;

        $school = School::findOrFail($id);
        Log::info("archive school " . $id);
        $school->archive();

        $request->session()->flash('alert-success', 'School archived successfully.');

        return redirect()->route('admin.schools.index');
    }

    /**
     * Change status to inactived.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function reinstate(Request $request)
    {
        $id = $request->id_reinstate;

        $school = School::findOrFail($id);
        Log::info("reinstate school " . $id);
        $school->reinstate();

        $request->session()->flash('alert-success', 'School reinstated successfully.');

        return redirect()->route('admin.schools.index');
    }

    /**
     * setExpiryDate.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function setExpiryDate(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'id_school' => 'required|numeric|exists:schools,id',
            'expiry_date_school' => 'required',
        ])->validate();

        $id = $validated['id_school'];
        $expiry_date_request = $validated['expiry_date_school'];

        $expiry_date = Carbon::createFromFormat('d/m/Y', $expiry_date_request)->format('Y-m-d');

        $school = School::findOrFail($id);

        if (!empty($expiry_date)) {
            $school->setExpiryDate($expiry_date);
        }
        $request->session()->flash('alert-success', 'School set subscription expiry date successfully.');

        return redirect()->route('admin.schools.index');
    }
}
