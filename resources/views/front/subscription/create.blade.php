@section('css')
    <style>
        .content-inner {
            padding-bottom: 50px;
        }

        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        table thead tr {
            background-color: #78BE20;
            color: #333;
        }

        .btn-subscription:hover {
            background-color: #78BE20;
            border-color: #78BE20;
        }

    </style>
@stop

@extends('layouts.page')

@section('top_heading')
    Subscribe
@stop

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/subscribe"><span itemprop="name">Subscribe</span></a>
    </li>
@stop

@section('content')
    <article id="post-87" class="post-87 page type-page status-publish hentry pmpro-has-access">
        <div class="entry-content">
            <div class="box-body">
                @if(count($errors))
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <br/>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- form start -->
                <form id="schoolForm" role="form" method="post" action="{{ route('front.subscription.store') }}">
                    @csrf
                    <div class="box-body">

                        <h4>School Information</h4>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Name of the School</label>
                            <input type="text" class="form-control" id="name" name="name"
                                   value="{{ old('name') }}">
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('address_region') ? 'has-error' : '' }}">
                            <label for="address_region">Address</label>
                            <input type="text" class="form-control" id="address_region" name="address_region"
                                   value="{{ old('address_region') }}">
                            <span class="text-danger">{{ $errors->first('address_region') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('address_suburb') ? 'has-error' : '' }}">
                            <label for="address_region">Suburb/City</label>
                            <input type="text" class="form-control" id="address_suburb" name="address_suburb"
                                   value="{{ old('address_suburb') }}">
                            <span class="text-danger">{{ $errors->first('address_suburb') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                        <label for="country">Country</label>
                        <select data-placeholder="Select Country" class="form-control select2_country"
                                style="width: 100%;" id="country" name="country">
                            <option></option>
                            @foreach($countries as $k=>$val)
                            <option value="{{$val['name']}}">{{$val['name']}}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('country') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
                        <label for="location">Location</label>
                        <select data-placeholder="Select Location" class="form-control select2_location"
                                style="width: 100%;" id="location" name="location" disabled="disabled">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('location') }}</span>
                    </div>

                        <!--<div id="address_country_input"-->
                        <!--     class="form-group {{ $errors->has('address_country') ? 'has-error' : '' }}">-->
                        <!--    <label for="address_country">Country</label>-->
                        <!--    <input type="text" class="form-control" id="address_country" name="address_country"-->
                        <!--           value="{{ old('address_country') }}" placeholder="Please input your country">-->
                        <!--    <span class="text-danger"-->
                        <!--          id="address_country_input_error">{{ $errors->first('address_country') }}</span>-->
                        <!--</div>-->

                        <!--<div id="address_state_input"-->
                        <!--     class="form-group {{ $errors->has('address_state') ? 'has-error' : '' }}">-->
                        <!--    <label for="address_state">State/Region/Province</label>-->
                        <!--    <input type="text" class="form-control" id="address_state" name="address_state"-->
                        <!--           value="{{ old('address_state') }}" placeholder="Please input your state/region/province">-->
                        <!--    <span class="text-danger"-->
                        <!--          id="address_state_input_error">{{ $errors->first('address_state') }}</span>-->
                        <!--</div>-->

                        <div class="form-group {{ $errors->has('address_postcode') ? 'has-error' : '' }}">
                            <label for="address_postcode">PostCode</label>
                            <input type="text" class="form-control" id="address_postcode" name="address_postcode"
                                   value="{{ old('address_postcode') }}">
                            <span class="text-danger">{{ $errors->first('address_postcode') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('schoolType') ? 'has-error' : '' }}">
                            <label for="schoolType">School Type</label>
                            <select data-placeholder="Select a school type" class="form-control select2_schoolType"
                                    style="width: 100%;" id="schoolType" name="schoolType">
                                <option></option>
                            </select>
                            <span class="text-danger">{{ $errors->first('schoolType') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('schoolSector') ? 'has-error' : '' }}">
                            <label for="schoolSector">School Sector</label>
                            <select data-placeholder="Select a school sector" class="form-control select2_schoolSector"
                                    style="width: 100%;" id="schoolSector" name="schoolSector">
                                <option></option>
                            </select>
                            <span class="text-danger">{{ $errors->first('schoolSector') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('schoolSizeStudent') ? 'has-error' : '' }}">
                            <label for="schoolSizeStudent">School Size</label>
                            <select data-placeholder="Select a category" class="form-control select2_schoolSizeStudent"
                                    style="width: 100%;" id="schoolSizeStudent" name="schoolSizeStudent">
                                <option></option>
                            </select>
                            <span class="text-danger">{{ $errors->first('schoolSizeStudent') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('schoolSizeTeacher') ? 'has-error' : '' }}">
                            <label for="schoolSizeTeacher">Number of teachers</label>
                            <select data-placeholder="Select a category" class="form-control select2_schoolSizeTeacher"
                                    style="width: 100%;" id="schoolSizeTeacher" name="schoolSizeTeacher">
                                <option></option>
                            </select>
                            <span class="text-danger">{{ $errors->first('schoolSizeTeacher') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('number_of_campuses') ? 'has-error' : '' }}">
                            <label for="number_of_campuses">Number of campuses</label>
                            <input type="text" class="form-control" id="number_of_campuses" name="number_of_campuses"
                                   value="{{ old('number_of_campuses') }}">
                            <span class="text-danger">{{ $errors->first('number_of_campuses') }}</span>
                        </div>
                        <br/>

                        <p>
                            Subscription prices are based on the number of teachers at the campus. All prices are in AUD and include 10% GST. First time or lapsed subscribers will be charged a joining fee in addition to the annual fee.
                        </p>
                        <div class="form-group {{ $errors->has('subscriptionPlan') ? 'has-error' : '' }}">
                            <label for="subscriptionPlan">Subscription Plan</label>
                            <select data-placeholder="Select a subscription plan"
                                    class="form-control select2_subscriptionPlan"
                                    style="width: 100%;" id="subscriptionPlan" name="subscriptionPlan">
                                <option></option>
                            </select>
                            <span class="text-danger">{{ $errors->first('subscriptionPlan') }}</span>
                            <span id="individual_license_applied_note"
                                  class="text-info">Note: Individual license applied</span>
                        </div>

                        <h4>Finance Information</h4>
                        <div class="form-group {{ $errors->has('finance_officer_name') ? 'has-error' : '' }}">
                            <label for="finance_officer_name">Finance Officer Name</label>
                            <input type="text" class="form-control" id="finance_officer_name"
                                   name="finance_officer_name"
                                   value="{{ old('finance_officer_name') }}">
                            <span class="text-danger">{{ $errors->first('finance_officer_name') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('finance_officer_email') ? 'has-error' : '' }}">
                            <label for="finance_officer_email">Finance Officer Email</label>
                            <input type="text" class="form-control" id="finance_officer_email"
                                   name="finance_officer_email"
                                   value="{{ old('finance_officer_email') }}">
                            <span class="text-danger">{{ $errors->first('finance_officer_email') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('finance_officer_phone') ? 'has-error' : '' }}">
                            <label for="finance_officer_phone">Finance Officer Phone</label>
                            <input type="text" class="form-control" id="finance_officer_phone"
                                   name="finance_officer_phone"
                                   value="{{ old('finance_officer_phone') }}">
                            <span class="text-danger">{{ $errors->first('finance_officer_phone') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('finance_officer_position') ? 'has-error' : '' }}">
                            <label for="finance_officer_position">Finance Officer Position</label>
                            <input type="text" class="form-control" id="finance_officer_position"
                                   name="finance_officer_position"
                                   value="{{ old('finance_officer_position') }}">
                            <span class="text-danger">{{ $errors->first('finance_officer_position') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('finance_invoice_email') ? 'has-error' : '' }}">
                            <label for="finance_invoice_email">Invoice Email</label>
                            <br>
                            <input type="checkbox" id="tick_same_email">
                            Same email address as finance officer email
                            <input type="text" class="form-control" id="finance_invoice_email"
                                   name="finance_invoice_email"
                                   value="{{ old('finance_invoice_email') }}">

                            <span class="text-danger">{{ $errors->first('finance_invoice_email') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('billing_address_region') ? 'has-error' : '' }}">
                            <label for="billing_address_region">Billing Address</label>
                            <br>
                            <input type="checkbox" id="tick_same_address">
                            Same address as school
                            <input type="text" class="form-control" id="billing_address_region"
                                   name="billing_address_region"
                                   value="{{ old('billing_address_region') }}">
                            <span class="text-danger">{{ $errors->first('billing_address_region') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('billing_address_suburb') ? 'has-error' : '' }}">
                            <label for="billing_address_region">Suburb/City</label>
                            <input type="text" class="form-control" id="billing_address_suburb"
                                   name="billing_address_suburb"
                                   value="{{ old('billing_address_suburb') }}">
                            <span class="text-danger">{{ $errors->first('billing_address_suburb') }}</span>
                        </div>
                        
                        <!--New location-->
                        <div class="form-group {{ $errors->has('billing_address_country') ? 'has-error' : '' }}">
                        <label for="billing_address_country">Country</label>
                        <select data-placeholder="Select Country" class="form-control select2_billing_address_country"
                                style="width: 100%;" id="billing_address_country" name="billing_address_country">
                            <option></option>
                            @foreach($countries as $k=>$val)
                            <option value="{{$val['name']}}">{{$val['name']}}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('billing_address_country') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('billing_address_location') ? 'has-error' : '' }}">
                        <label for="billing_address_location">Location</label>
                        <select data-placeholder="Select Location" class="form-control select2_billing_address_location"
                                style="width: 100%;" id="billing_address_location" name="billing_address_location" disabled="disabled">
                            <option></option>
                        </select>
                        <span class="text-danger">{{ $errors->first('billing_address_location') }}</span>
                    </div>
                    
                    
                        <!--<div class="form-group {{ $errors->has('billing_address_location') ? 'has-error' : '' }}">-->
                        <!--    <label for="billing_address_location">Location</label>-->
                        <!--    <select data-placeholder="Select location"-->
                        <!--            class="form-control select2_billing_address_location"-->
                        <!--            style="width: 100%;" id="billing_address_location" name="billing_address_location">-->
                        <!--        <option></option>-->
                        <!--    </select>-->
                        <!--    <span class="text-danger">{{ $errors->first('billing_address_location') }}</span>-->
                        <!--</div>-->

                        <!--<div id="billing_address_country_input"-->
                        <!--     class="form-group {{ $errors->has('billing_address_country') ? 'has-error' : '' }}">-->
                        <!--    <label for="billing_address_country">Country</label>-->
                        <!--    <input type="text" class="form-control" id="billing_address_country"-->
                        <!--           name="billing_address_country"-->
                        <!--           value="{{ old('billing_address_country') }}"-->
                        <!--           placeholder="Please input your country">-->
                        <!--    <span class="text-danger"-->
                        <!--          id="billing_address_country_input_error">{{ $errors->first('billing_address_country') }}</span>-->
                        <!--</div>-->

                        <!--<div id="billing_address_state_input"-->
                        <!--     class="form-group {{ $errors->has('billing_address_state') ? 'has-error' : '' }}">-->
                        <!--    <label for="billing_address_state">State/Region/Province</label>-->
                        <!--    <input type="text" class="form-control" id="billing_address_state"-->
                        <!--           name="billing_address_state"-->
                        <!--           value="{{ old('billing_address_state') }}" placeholder="Please input your state/region/province">-->
                        <!--    <span class="text-danger"-->
                        <!--          id="billing_address_state_input_error">{{ $errors->first('billing_address_state') }}</span>-->
                        <!--</div>-->


                        <div class="form-group {{ $errors->has('billing_address_postcode') ? 'has-error' : '' }}">
                            <label for="billing_address_postcode">PostCode</label>
                            <input type="text" class="form-control" id="billing_address_postcode"
                                   name="billing_address_postcode"
                                   value="{{ old('billing_address_postcode') }}">
                            <span class="text-danger">{{ $errors->first('billing_address_postcode') }}</span>
                        </div>


                        <div class="form-group {{ $errors->has('purchase_order_no') ? 'has-error' : '' }}">
                            <label for="purchase_order_no">Purchase Order Number</label>

                            <input type="text" class="form-control" id="purchase_order_no" name="purchase_order_no"
                                   value="{{ old('purchase_order_no') }}">
                            <span class="text-danger">{{ $errors->first('purchase_order_no') }}</span>
                        </div>

                        <h4>Coordinator Information</h4>
                        <div class="form-group {{ $errors->has('coordinator_name') ? 'has-error' : '' }}">
                            <label for="coordinator_name">Coordinator Name</label>
                            <input type="text" class="form-control" id="coordinator_name" name="coordinator_name"
                                   value="{{ old('coordinator_name') }}">
                            <span class="text-danger">{{ $errors->first('coordinator_name') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('coordinator_email') ? 'has-error' : '' }}">
                            <label for="coordinator_email">Coordinator Email</label>
                            <input type="text" class="form-control" id="coordinator_email" name="coordinator_email"
                                   value="{{ old('coordinator_email') }}">
                            <span class="text-danger">{{ $errors->first('coordinator_email') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('coordinator_phone') ? 'has-error' : '' }}">
                            <label for="coordinator_phone">Coordinator Phone</label>
                            <input type="text" class="form-control" id="coordinator_phone" name="coordinator_phone"
                                   value="{{ old('coordinator_phone') }}">
                            <span class="text-danger">{{ $errors->first('coordinator_phone') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('coordinator_position') ? 'has-error' : '' }}">
                            <label for="coordinator_position">Coordinator Position</label>
                            <input type="text" class="form-control" id="coordinator_position"
                                   name="coordinator_position"
                                   value="{{ old('coordinator_position') }}">
                            <span class="text-danger">{{ $errors->first('coordinator_position') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('coordinator_year_levels') ? 'has-error' : '' }}">
                            <label for="coordinator_year_levels">Coordinator Year Levels</label>
                            <select class="form-control select2_coordinator_year_levels" style="width: 100%;"
                                    id="coordinator_year_levels"
                                    name="coordinator_year_levels[]"
                                    multiple="multiple"
                                    data-placeholder="Select year levels">
                                    
                            </select>
                            <span class="text-danger">{{ $errors->first('coordinator_year_levels') }}</span>
                        </div>
                        
                        <div class="form-group {{ $errors->has('lesson_pack') ? 'has-error' : '' }}" style="display:none">
                        <label for="lesson_pack">Lesson Pack</label>
                        <select class="form-control select2-lesson_pack" style="width: 100%;"
                                id="lesson_pack"
                                name="lesson_pack[]"
                                multiple="multiple"
                                data-placeholder="Select lesson packs">
                            <option value="all" selected>Default - All Lessons</option>
                           
                        </select>
                        <span class="text-danger">{{ $errors->first('lesson_pack') }}</span>
                    </div>



                        <br/>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary ladda-button btn-subscription"
                                data-style="expand-right"><span
                                    class="ladda-label">Submit</span></button>
                    </div>
                </form>

            </div>
        </div>
    </article>
@stop


@section('js')
    <!-- Select2 -->
    <script src="{{ asset ('vendor/adminlte/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- icheck checkboxes -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            jQuery(".select2_country").change(function(e){
               var val = jQuery(this).val();
               jQuery(".select2_location").empty();
               jQuery.ajax({
                url:"https://countriesnow.space/api/v0.1/countries/states",
                type:"POST",
                data: {
                  country:val,
                 },
                success:function(response)
                {
                    var html = "<option></option>";
                    
                    if(response.error == false){
                        var data = response.data.states;
                        jQuery.each(data, function( index, value ) {
                            html += "<option value=\""+value['name']+"\">"+value['name']+"</option>";
                        });
                    }
                    jQuery(".select2_location").append(html);
                    jQuery(".select2_location").removeAttr('disabled');
                    let select2_location =  jQuery('.select2_location').select2({
                        allowClear: true,
                    });
                    @if(old('location'))
                    select2_location.val('{!! old('location') !!}').trigger('change');
                    @endif
                }
              });
            })
            
            let select2_country =  jQuery('.select2_country').select2({
                allowClear: true,
            });
            @if(old('country'))
            select2_country.val('{!! old('country') !!}').trigger('change');
            @endif
            
            jQuery(".select2_billing_address_country").change(function(e){
               var val = jQuery(this).val();
               jQuery(".select2_billing_address_location").empty();
               jQuery.ajax({
                url:"https://countriesnow.space/api/v0.1/countries/states",
                type:"POST",
                data: {
                  country:val,
                 },
                success:function(response)
                {
                    var html = "<option></option>";
                    
                    if(response.error == false){
                        var data = response.data.states;
                        jQuery.each(data, function( index, value ) {
                            html += "<option value=\""+value['name']+"\">"+value['name']+"</option>";
                        });
                    }
                    jQuery(".select2_billing_address_location").append(html);
                    jQuery(".select2_billing_address_location").removeAttr('disabled');
                    let select2_billing_address_location =  jQuery('.select2_billing_address_location').select2({
                        allowClear: true,
                    });
                    @if(old('billing_address_location'))
                    select2_billing_address_location.val('{!! old('billing_address_location') !!}').trigger('change');
                    @endif
                    if(jQuery("#tick_same_address").is(':checked')){
                        select2_billing_address_location.val(jQuery("#location").val()).trigger('change');
                    }
                }
              });
            })
            
            let select2_billing_address_country =  jQuery('.select2_billing_address_country').select2({
                allowClear: true,
            });
            @if(old('billing_address_country'))
            select2_billing_address_country.val('{!! old('billing_address_country') !!}').trigger('change');
            @endif
            
            // Initialize Select2 elements
            let select2_address_location = jQuery('.select2_address_location').select2({
                allowClear: true,
                data: {!! json_encode($locations)!!}
            });

            @if(old('schoolSizeTeacher') == 1)
            jQuery("#individual_license_applied_note").show();
            @else
            jQuery("#individual_license_applied_note").hide();
            @endif

            @if(old('address_location'))
            select2_address_location.val({!! old('address_location') !!}).trigger('change');
            console.log(jQuery('#address_location :selected').text());
            if (jQuery('#address_location :selected').text() === "NotFound") {
                jQuery('#address_country_input').show();
                jQuery('#address_state_input').show();
            } else {
                jQuery('#address_country_input').hide();
                jQuery('#address_state_input').hide();
            }
            @endif

            select2_address_location.on('select2:select', function (e) {
                let data = e.params.data;
                if (data.text === "NotFound") {
                    jQuery('#address_country_input').show();
                    jQuery('#address_state_input').show();
                } else {
                    jQuery('#address_country_input').hide();
                    jQuery('#address_state_input').hide();
                }
            });

            let select2_billing_address_location = jQuery('.select2_billing_address_location').select2({
                allowClear: true,
                data: {!! json_encode($locations)!!}
            });

            @if(old('billing_address_location'))
            select2_billing_address_location.val('{!! old('billing_address_location') !!}').trigger('change');

            if (jQuery('#billing_address_location :selected').text() === "NotFound") {
                jQuery('#billing_address_country_input').show();
                jQuery('#billing_address_country').val(jQuery("#address_country").val());
                jQuery('#billing_address_state_input').show();
                jQuery('#billing_address_state').val(jQuery("#address_state").val());
            } else {
                jQuery('#billing_address_country_input').hide();
                jQuery('#billing_address_state_input').hide();
            }

            @endif

            select2_billing_address_location.on('select2:select', function (e) {
                let data = e.params.data;
                if (data.text === "NotFound") {
                    jQuery('#billing_address_country_input').show();
                    jQuery('#billing_address_state_input').show();
                } else {
                    jQuery('#billing_address_country_input').hide();
                    jQuery('#billing_address_state_input').hide();
                }
            });

            let select2_schoolType = jQuery('.select2_schoolType').select2({
                allowClear: true,
                data: {!! json_encode($schoolTypes)!!}
            });

            @if(old('schoolType'))
            select2_schoolType.val({!! old('schoolType') !!}).trigger('change');
                    @endif

            let select2_schoolSector = jQuery('.select2_schoolSector').select2({
                    allowClear: true,
                    data: {!! json_encode($schoolSectors)!!}
                });

            @if(old('schoolSector'))
            select2_schoolSector.val({!! old('schoolSector') !!}).trigger('change');
                    @endif

            let select2_schoolSizeStudent = jQuery('.select2_schoolSizeStudent').select2({
                    allowClear: true,
                    data: {!! json_encode($schoolSizeStudents)!!}
                });

            @if(old('schoolSizeStudent'))
            select2_schoolSizeStudent.val({!! old('schoolSizeStudent') !!}).trigger('change');
                    @endif

            let select2_schoolSizeTeacher = jQuery('.select2_schoolSizeTeacher').select2({
                    allowClear: true,
                    data: {!! json_encode($schoolSizeTeachers)!!}
                });

            @if(old('schoolSizeTeacher'))
            select2_schoolSizeTeacher.val({!! old('schoolSizeTeacher') !!}).trigger('change');
                    @endif

            let select2_subscriptionPlan = jQuery('.select2_subscriptionPlan').select2({
                    allowClear: true,
                    data: {!! json_encode($subscriptionPlans)!!}
                });

            @if(old('subscriptionPlan'))
            select2_subscriptionPlan.val({!! old('subscriptionPlan') !!}).trigger('change');
                    @endif

            let select2_coordinator_year_levels = jQuery('.select2_coordinator_year_levels').select2({
                    allowClear: true,
                    data: {!! json_encode($year_levels)!!}
                });

            @if(old('coordinator_year_levels'))
            select2_coordinator_year_levels.val({!! json_encode( collect(old('coordinator_year_levels')) ) !!}).trigger('change');
            @endif
            
            let select2_lesson_pack = jQuery('.select2-lesson_pack').select2({
                    allowClear: true,
                });

            @if(old('lesson_pack'))
            select2_lesson_pack.val({!! json_encode( collect(old('lesson_pack')) ) !!}).trigger('change');
            @endif
            select2_schoolSizeTeacher.on('select2:select', function (evt) {
                // console.log(evt);
                let school_size_teacher_id = evt.params.data.id;
                // is_single_account
                if (school_size_teacher_id == 1) {
                    jQuery("#individual_license_applied_note").show();
                } else {
                    jQuery("#individual_license_applied_note").hide();
                }

                // ajax request data
                jQuery.ajax({
                    method: "GET",
                    url: location.origin + "/subscribe/select/" + school_size_teacher_id,
                })
                    .done(function (data) {
                        select2_subscriptionPlan.empty();
                        if (data.length > 0) {
                            let subscription_plan_id = data[0].id;
                            // re-popup select2 array
                            jQuery('.select2_subscriptionPlan').select2({
                                allowClear: true,
                                data: data
                            });
                            select2_subscriptionPlan.val(subscription_plan_id).trigger("change");
                        }
                    });
            });

            jQuery("#schoolForm").submit(function () {
                let validated = true;
                // validate data
                if (jQuery('#address_location :selected').text() === "NotFound") {
                    console.log(jQuery('#address_country').val());
                    if (jQuery('#address_country').val() === "") {
                        jQuery('#address_country_input').addClass('has-error')
                        jQuery("#address_country_input_error").text("The country/region field is required.").show();
                        validated = false;
                    } else {
                        jQuery('#address_country_input').removeClass('has-error')
                        jQuery("#address_country_input_error").hide();
                    }
                    if (jQuery('#address_state').val() === "") {
                        jQuery('#address_state_input').addClass('has-error')
                        jQuery("#address_state_input_error").text("The states field is required.").show();
                        validated = false;
                    }else {
                        jQuery('#address_state_input').removeClass('has-error')
                        jQuery("#address_state_input_error").hide();
                    }
                }

                return validated;
            });

            jQuery("#tick_same_address").click(function () {
                jQuery('#billing_address_region').val(jQuery("#address_region").val());
                jQuery('#billing_address_suburb').val(jQuery("#address_suburb").val());
                select2_billing_address_country.val(jQuery("#country").val()).trigger('change');
                if (jQuery('#billing_address_location :selected').text() === "NotFound") {
                    jQuery('#billing_address_country_input').show();
                    jQuery('#billing_address_country').val(jQuery("#address_country").val());
                    jQuery('#billing_address_state_input').show();
                    jQuery('#billing_address_state').val(jQuery("#address_state").val());
                } else {
                    jQuery('#billing_address_country_input').hide();
                    jQuery('#billing_address_state_input').hide();
                }

                jQuery('#billing_address_postcode').val(jQuery("#address_postcode").val());
            });

            jQuery("#tick_same_email").click(function () {
                jQuery('#finance_invoice_email').val(jQuery("#finance_officer_email").val());
            });

            if (jQuery('#address_location :selected').text() === "NotFound") {
                jQuery('#address_country_input').show();
                jQuery('#address_state_input').show();
            } else {
                jQuery('#address_country_input').hide();
                jQuery('#address_state_input').hide();
            }


            if (jQuery('#billing_address_location :selected').text() === "NotFound") {
                jQuery('#billing_address_country_input').show();
                jQuery('#billing_address_state_input').show();
            } else {
                jQuery('#billing_address_country_input').hide();
                jQuery('#billing_address_state_input').hide();
            }
        });
    </script>
@endsection
