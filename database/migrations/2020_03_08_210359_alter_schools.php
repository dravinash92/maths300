<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->integer('is_imported')->after('subscription_plan_id')->default(0); // 0: FALSE 1: TRUE
            $table->integer('is_single_account')->after('subscription_plan_id')->default(0); // 0: FALSE 1: TRUE
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn('is_imported');
            $table->dropColumn('is_single_account');
        });
    }
}
