
@extends('layouts.email')

@section('content')
    <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
        Dear {{ $user->name }},
        <br><br>
        We are writing to let you know that your Maths300 account has now been disabled due to the expiry of your subscription.
        <br><br>
        Please contact the Maths300 team at AAMT to resubscribe. 

        <br><br>
    </div>
@stop
