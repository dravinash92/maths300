@extends('layouts.fronthome')

@section('sub_breadcrumbs')
<a itemprop="item" href="{{ route('front.users.invoices') }}" title="Invoices"><span itemprop="name">Invoices</span></a>
@endsection

@section('page-content')

<div id="invoices-content" class="myaccount-right">
    <table id="tab_invoices" class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Invoice ID</th>
            <th>Issue Date</th>
            <th>Due Date</th>
            <th>Total</th>
            <th>Status</th>
        </tr>
        @csrf
        </thead>
        <tbody>
        @if(count($invoices))
        @foreach($invoices as $l)
            <tr class="item{{ $l->id }}">
                <td><a href='{{ route('front.users.showInvoice',$l->id) }}'>{{ $l->invoice_id }}</a></td>
                <td>{{ $l->issueDateFormat() }}</td>
                <td>{{ $l->dueDateFormat() }}</td>
                <td>${{ $l->amount_due }} AUD</td>
                <td>{{ $l->getStatus() }}</td>
            </tr>
        @endforeach
        @else
            <tr class="no-item">
                <td colspan="5">No data available.</td>
            </tr>
        @endif
        </tbody>
    </table>

    <div class="bbp-pagination">
        <div class="bbp-pagination-links">
            {{ $invoices->links() }}
        </div>
    </div>

</div>
@stop

@push('js')

@endpush