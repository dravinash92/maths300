@extends('layouts.pageframe')

@section('site-content')
    <div class="container site-content sidebar-right">
        <div class="row">
            <main id="main" class="site-main col-sm-9 alignleft">
                <div id="thim-course-archive" class="blog-content">
                    @yield('content')
                </div>

                <div class="pagination loop-pagination">
                    @yield('pagination')
                </div>
            </main>

            <div id="sidebar" class="widget-area col-sm-3 sticky-sidebar" role="complementary">
                @yield('sidebar')
            </div><!-- #secondary -->
        </div>
    </div>
@stop

