<?php

namespace App\Http\Controllers\Admin;

use App\Models\YearLevel;
use Illuminate\Http\Request;
use App\Http\Requests\YearLevelRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class YearLevelController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = YearLevel::query();
        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['yearlevels'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.yearlevels.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.yearlevels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\YearLevelRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(YearLevelRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $yearlevel = new YearLevel();
        $yearlevel->name = $validated['name'];

        $yearlevel->save();

        $request->session()->flash('alert-success', 'Year level added successfully.');

        return redirect()->route('admin.yearlevels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\YearLevel $yearlevel
     * @return \Illuminate\Http\Response
     */
    public function show(YearLevel $yearlevel)
    {
        $arr['yearlevel'] = $yearlevel;

        return view('admin.yearlevels.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\YearLevel $yearLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(YearLevel $yearlevel)
    {
        $arr['yearlevel'] = $yearlevel;
        return view('admin.yearlevels.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\YearLevelRequest $request
     * @param  \App\Models\YearLevel $yearlevel
     * @return \Illuminate\Http\Response
     */
    public function update(YearLevelRequest $request, YearLevel $yearlevel)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $yearlevel->name = $validated['name'];

        $yearlevel->save();

        $request->session()->flash('alert-success', 'Year level updated successfully.');

        return redirect()->route('admin.yearlevels.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $yearlevel = YearLevel::findOrFail($id);
        Log::info("delete yearlevel " . $id);
        
        $yearlevel->delete();

        return response()->json($yearlevel);
    }
}
