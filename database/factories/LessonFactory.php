<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Lesson::class, function (Faker $faker) {
    return [
        'title' => $faker->lexify('Lesson ???'),
        'summary' => $faker->text(300),
        'lesson_notes' => $faker->text(300),
        'content_strands' => $faker->randomElement($array = array ('number','algebra','measurement','geometry','statistics','probability'), $count = 3),
        'year_level' => $faker->randomElement($array = array ('1','2','3','4','5','6','7','8','9','10','11','12'), $count = 4),
        'number_of_views' => $faker->numberBetween($min = 100, $max = 1999),
    ];
});