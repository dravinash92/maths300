<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForumRequest;
use App\Models\Forum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Forum::query();

        if ($request->has('q') && $request->q != null) {
            // record user input keywords for further analysis in the future
            Log::info("search_keyword: " . $request->q);
            $query->keyWords($request->q);
        }

        $arr['forums'] = $query->get();

        // allow using Input::old() function in view to access the data.
        $request->flash();

        return view('admin.forums.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.forums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ForumRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ForumRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $forum = new Forum();
        $forum->title = $validated['title'];
        if(!empty($validated['description'])) {
            $forum->description = $validated['description'];
        }
        $forum->sticky = $validated['sticky'];
        $forum->visibility = $validated['visibility'];
        $forum->save();

        Log::info("create forum " . $forum->id);

        $request->session()->flash('alert-success', 'Forum added successfully.');

        return redirect()->route('admin.forums.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function show(Forum $forum)
    {
        $arr['forum'] = $forum;

        return view('admin.forums.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function edit(Forum $forum)
    {
        $arr['forum'] = $forum;

        return view('admin.forums.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ForumRequest  $request
     * @param  \App\Models\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function update(ForumRequest $request, Forum $forum)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        $forum->title = $validated['title'];
        if(!empty($validated['description'])) {
            $forum->description = $validated['description'];
        }
        $forum->sticky = $validated['sticky'];
        $forum->visibility = $validated['visibility'];
        $forum->save();

        Log::info("update forum " . $forum->id);

        $request->session()->flash('alert-success', 'Forum updated successfully.');

        return redirect()->route('admin.forums.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forum = Forum::findOrFail($id);
        Log::info("delete forum " . $id);

        /**
         * Note: all topics/replies related will be deleted cascade
         */
        $forum->delete();

        return response()->json($forum);
    }

    /**
     * Change status to opened.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function open(Request $request)
    {
        $id = $request->id_open;

        $forum = Forum::findOrFail($id);
        Log::info("open forum " . $id);

        $forum->status = 1;
        $forum->save();

        $request->session()->flash('alert-success', 'Forum opened successfully.');

        return redirect()->route('admin.forums.index');
    }

    /**
     * Change status to closed.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function close(Request $request)
    {
        $id = $request->id_close;

        $forum = Forum::findOrFail($id);
        Log::info("close forum " . $id);

        $forum->status = 0;
        $forum->save();

        $request->session()->flash('alert-success', 'Forum closed successfully.');

        return redirect()->route('admin.forums.index');
    }
}
